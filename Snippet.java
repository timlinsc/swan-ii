import java.util.ArrayList;

class Snippet {
	private ArrayList<String> port_names;
	private ArrayList<String[]> connections;

	public Snippet(ArrayList<String> port_names, ArrayList<String[]> connections) {
		this.port_names = port_names;
		this.connections = connections; 
	}

	public Snippet(int size) {
		port_names = new ArrayList<String>(size);
		connections = new ArrayList<String[]>(size);
	}

	public ArrayList<String> ports()
	{	return port_names;	}

	public ArrayList<String[]> connections()
	{	return connections;	}

	public void addPort(String p) {
		port_names.add(p);
	}

	public void addConnections(String[] c) {
		connections.add(c);
	}

	// All connections to old and change to newConn
	public void rewire(int old, int newConn) {
		String old_s = String.valueOf(old);
		String new_s = String.valueOf(newConn);
		for (String[] conns : connections)
			for(int i=0; i<conns.length; i++) {
				if(conns[i].contains(old_s)) 
					conns[i] = conns[i].replaceFirst("(?<=^|\\s)"+old_s+"(?=\\s)", new_s);
			}
	}
}