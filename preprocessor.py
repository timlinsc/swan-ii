# preprocessor.py
# This is a front-end Swan tool that preprocesses a SystemVerilog file for these keywords: SECURE, INTERNAL, PERMUTE, SNIPPET, CANARY
#   SECURE   -  marks a signal (logic, reg, wire, etc.). These signals are added to the dc_shell script to
# 				ensure that they are preserved to the netlist. They are also written to a log and picked up by Lamppost.
#				Lamppost will ensure that all gates touching these wires will have larger equivalence sets.
#   INTERNAL -  marks an input to be assigned a value directly by the Swan fabric. When the fabric is generated, these inputs are
#				connected directly to memory cells rather than to any external part of the design. This allows us to synthesize
#				multiple functionally-equivalent compute paths, different interpretations of a given state variable, and potentially
#				encrypt (or just "encrypt") signals in the design. 
#   PERMUTE  -  marks a parameter as a value that should be swept over all possible values given its bit width. The resulting set of
#				permutations can then be merged into a large, highly-flexible black box of reconfigurable logic and built into the Swan
#   SNIPPET  -  flags an instantiation of a module which contains `permute`'d parameters. These modules will be marked set_dont_touch in
#				synthesis, and information about them will be preserved through Lamppost.
#   CANARY   -  marks an input in a way similar to INTERNAL. These inputs will be driven by the canary logic of the Swan fabric. 
#				A canary gate is added alongside every gate that has been marked SECURE. For the purposes of the HDL, these inputs 
#				are external sources whose value should never change, but that cannot be determined at compile time. These are left 
#				exposed to the programmer so that they can program the desired failure condition and action. 
#
# USAGE: python preprocessor.py <FILEIN> <LIBRARY> [MODULE1, MODULE2, ...]
# 
# In the interests of my sanity, I assume that there is only one occurance of secure/internal per line.
# i.e. the following DOES NOT OCCUR: "secure wire foo; secure wire bar;\n"
# However, "wire foo; secure wire bar; wire baz;" should work.
#
# For internal, I don't support the following:
# internal input foo,\n bar;
# 
# TODO: 
#    1) Add support for multidimensional arrays 



import sys
import os
import re
import itertools
from subprocess import call
from enum import Enum

SECURE = re.compile(r"secure\s+(input\s+|output\s+)?(logic|wire|reg|tri|wor|wand)")
SECURE_TO_EOL = re.compile(r"secure\s+(input\s+|output\s+)?(logic|wire|reg|tri|wor|wand).*?;")
SECURE_KEYWORD = re.compile(r"(?<!\w)secure\s+")

INTERNAL = re.compile(r"internal\s+input")
INTERNAL_TO_EOL = re.compile(r"(internal\s+input\s+)(logic|reg|tri|wor|wand)?(\s*\[.+:.+\]\s+)?(.*);?")
INTERNAL_KEYWORD = re.compile(r"(?<!\w)internal\s+")

CANARY = re.compile(r"canary\s+input")
CANARY_TO_EOL = re.compile(r"(canary\s+input\s+)(logic|reg|tri|wor|wand)?(\s*\[.+:.+\]\s+)?(.*);?")
CANARY_KEYWORD = re.compile(r"(?<!\w)canary\s+")

PERM = re.compile(r"permute\s+parameter")
PERM_KEYWORD = re.compile(r"(?<!\w)permute\s+")
PERM_TO_EOL = re.compile(r"(permute\s+parameter\s+)(\w+)(\s*=\s*)(\d+)'(b|h|d)\d+\s*;")

SNIPPET = re.compile(r"^\s*snippet\s")
SNIPPET_W_MOD = re.compile(r"(^\s*snippet\s+)(\w+)")

VALID_NAME = re.compile(r"[^;, \t]+")

### Parse arguments
if len(sys.argv) < 2:
	print ("Error, invalid args")
	print ("USAGE: python preprocessor.py <FILEIN> [LIB_NAME] [MODULE1, MODULE2, ...]")
	print ("Library names: arm, ng15, 470 (Use arm. The others are legacy)")
	sys.exit(0)
else:
	filein = sys.argv[1]
	lib = sys.argv[2] if len(sys.argv)>2 else "470"
	modules = sys.argv[3:] if len(sys.argv)>3 else []

split_file_name = []
if '/' in filein:
	split_file_name = filein.rsplit('/', 1)
	os.system("cd "+split_file_name[0])

#control signals
class State(Enum):
	DEFAULT = 1
	SECURE = 2
	INTERNAL = 3
	CANARY = 4
	PERM = 5
	SNIPPET = 6

status = State.DEFAULT
reading_sigs = False
prev_line = ''

#results
clean_file = ''
secured_sigs = []
internal_sigs = []
canary_sigs = []
permute_params = []
permutations = []
snippets = []


with open(filein, 'r') as fin:
	print("Starting: "+filein)
	lines = fin.readlines()
	for l in lines:
		if reading_sigs:
			if not l.strip().endswith(';'):
				prev_line += l[:-1] #remove \n
				clean_file += l
				continue
			else:
				reading_sigs = False
				clean_file += l
		elif re.search(SECURE, l) is not None: # Found 'secure' keyword
			if not l.strip().endswith(';'):
				reading_sigs = True
				prev_line = l[:-1] #remove \n
			status = State.SECURE
			clean_file += re.sub(SECURE_KEYWORD, "", l)

		elif re.search(INTERNAL, l) is not None: # Found 'internal' keyword
			# No support for multi-line declarations because these lines are not always terminated with a ;
			status = State.INTERNAL
			clean_file += re.sub(INTERNAL_KEYWORD, "", l)
		elif re.search(CANARY, l) is not None: # Found 'canary' keyword
			# No support for multi-line declarations because these lines are not always terminated with a ;
			status = State.CANARY
			clean_file += re.sub(CANARY_KEYWORD, "", l)

		elif re.search(PERM, l) is not None: # Found 'permute' keyword
			status = State.PERM
			clean_file += re.sub(PERM_KEYWORD, "", l)

		elif re.match(SNIPPET, l) is not None: # Found 'snippet' keyword
			if not l.strip().endswith(';'):
				reading_sigs = True
				prev_line = l[:-1] #remove \n
			status = State.SNIPPET
			clean_file += re.sub(SNIPPET, "", l)

		else:
			clean_file += l


		if status == State.SECURE and not reading_sigs:
			l = re.search(SECURE_TO_EOL, prev_line+l).group(0)
			toks = l.split()
			if toks[2].startswith('['):
				# vector definition
				starting = 3
			else: #standard definition.
				starting = 2

			for i in range(starting, len(toks)):
				match = re.match(VALID_NAME, toks[i])
				if match is not None:
					secured_sigs.append(match.group(0))
			status = State.DEFAULT
		elif status == State.INTERNAL:
			l = re.search(INTERNAL_TO_EOL, l).group(4)
			toks = l.split()
			for i in range(len(toks)):
				match = re.match(VALID_NAME, toks[i])
				if match is not None:
					internal_sigs.append(match.group(0))
			status = State.DEFAULT
		elif status == State.CANARY:
			l = re.search(CANARY_TO_EOL, l).group(4)
			toks = l.split()
			for i in range(len(toks)):
				match = re.match(VALID_NAME, toks[i])
				if match is not None:
					canary_sigs.append(match.group(0))
			status = State.DEFAULT
		elif status == State.PERM:
			match = re.search(PERM_TO_EOL, l)
			bitwidth = int(match.group(4))
			param_name = match.group(2)
			permute_params.append(param_name)
			permutations.append(range(2**bitwidth))

			status = State.DEFAULT
		elif status == State.SNIPPET and not reading_sigs:
			match = re.match(SNIPPET_W_MOD, prev_line+l)
			snippets.append(match.group(2))
			status = State.DEFAULT


if secured_sigs:
	with open('pp_secured.txt', 'w') as secure_out:
		for sig in secured_sigs:
			secure_out.write(sig+'\n')

if internal_sigs:
	with open('pp_internals.txt', 'w') as internals_out:
		for sig in internal_sigs:
			internals_out.write(sig+'\n')

if canary_sigs:
	with open('pp_canaries.txt', 'w') as canary_out:
		for sig in canary_sigs:
			canary_out.write(sig+'\n')

if snippets:
	with open('pp_snippets.txt', 'w') as snippets_out:
		for sig in snippets:
			snippets_out.write(sig+'\n')

if split_file_name:
	with open(split_file_name[0]+'/clean_'+split_file_name[1], 'w') as verilog_out:
		verilog_out.write(clean_file)
else:
	with open('clean_'+filein, 'w') as verilog_out:
		verilog_out.write(clean_file)






# If we parsed the whole file, we still want to generate a synthesis script
# if not modules:
# 	modules.append("top") #Maybe do some checking to try to guess top module, maybe. 

# first = True

# for m in modules:
# 	for param_set in itertools.product(*permutations):
# 		dc_shell_script = """# ================================ SETUP =================================

# set src [list """+(split_file_name[0]+'/' if False else '')+"""clean_"""+(split_file_name[1] if split_file_name else filein)+"""]
# set top_module """+m+("""

# set target_library /afs/eecs.umich.edu/kits/ARM/TSMC_65lp/sc_rvt/arm/tsmc/cln65lp/sc9_base_rvt/r0p0/db/sc9_cln65lp_base_rvt_ss_typical_max_1p08v_125c.db
# set link_library /afs/eecs.umich.edu/kits/ARM/TSMC_65lp/sc_rvt/arm/tsmc/cln65lp/sc9_base_rvt/r0p0/db/sc9_cln65lp_base_rvt_ss_typical_max_1p08v_125c.db
# """
# if lib == 'arm' else """

# # set target_library ./lib/NanGate_15nm/NanGate_15nm_OCL.db
# # set link_library ./lib/NanGate_15nm/NanGate_15nm_OCL.db

# # set_dont_use { NanGate_15nm_OCL/FA_X1 NanGate_15nm_OCL/HA_X1 }
# # set_dont_use { NanGate_15nm_OCL/AOI21_X1 NanGate_15nm_OCL/AOI21_X2 NanGate_15nm_OCL/AOI22_X1 NanGate_15nm_OCL/AOI22_X2 NanGate_15nm_OCL/OAI21_X1 NanGate_15nm_OCL/OAI21_X2 NanGate_15nm_OCL/OAI22_X1 NanGate_15nm_OCL/OAI22_X2  }
# # set_dont_use { NanGate_15nm_OCL/AND2_X2 NanGate_15nm_OCL/AND3_X1 NanGate_15nm_OCL/AND3_X2 NanGate_15nm_OCL/AND4_X1 NanGate_15nm_OCL/AND4_X2 NanGate_15nm_OCL/INV_X2 NanGate_15nm_OCL/INV_X4 NanGate_15nm_OCL/INV_X8 NanGate_15nm_OCL/INV_X12 NanGate_15nm_OCL/INV_X16 NanGate_15nm_OCL/NAND2_X2 NanGate_15nm_OCL/NAND3_X1 NanGate_15nm_OCL/NAND3_X2 NanGate_15nm_OCL/NAND4_X1 NanGate_15nm_OCL/NAND4_X2 NanGate_15nm_OCL/NOR2_X2 NanGate_15nm_OCL/NOR3_X1 NanGate_15nm_OCL/NOR3_X2 NanGate_15nm_OCL/NOR4_X1 NanGate_15nm_OCL/NOR4_X2 NanGate_15nm_OCL/OR2_X2 NanGate_15nm_OCL/OR3_X1 NanGate_15nm_OCL/OR3_X2 NanGate_15nm_OCL/OR4_X1 NanGate_15nm_OCL/OR4_X2 }
# # set_dont_use { NanGate_15nm_OCL/AND2_X1 NanGate_15nm_OCL/OR2_X1 NanGate_15nm_OCL/MUX2_X1 NanGate_15nm_OCL/XOR2_X1 NanGate_15nm_OCL/XNOR2_X1 }

# """
# if lib == 'ng15' else """

# set target_library ./lib/synopsys/lec25dscc25_TT.db
# set link_library ./lib/synopsys/lec25dscc25_TT.db

# set_dont_use {lec25dscc25_TT/*s2 lec25dscc25_TT/*s3 lec25dscc25_TT/*s4 lec25dscc25_TT/*s5 lec25dscc25_TT/*s6 lec25dscc25_TT/*s7 lec25dscc25_TT/*s8 lec25dscc25_TT/*s10 }
# set_dont_use {lec25dscc25_TT/*3s* lec25dscc25_TT/*4s* lec25dscc25_TT/*5s* lec25dscc25_TT/*6s* lec25dscc25_TT/*7s* lec25dscc25_TT/*8s*}
# set_dont_use {lec25dscc25_TT/*41s* lec25dscc25_TT/*31s* lec25dscc25_TT/*21s*}
# # set_dont_use {lec25dscc25_TT/xn* lec25dscc25_TT/a* lec25dscc25_TT/o* lec25dscc25_TT/h* lec25dscc25_TT/f* lec25dscc25_TT/ib* lec25dscc25_TT/nb*}
# set_dont_use {lec25dscc25_TT/ao* lec25dscc25_TT/oa* lec25dscc25_TT/h* lec25dscc25_TT/f* lec25dscc25_TT/ib* lec25dscc25_TT/nb*}
# set_dont_use {lec25dscc25_TT/dffscs* lec25dscc25_TT/dffles* lec25dscc25_TT/dffss* lec25dscc25_TT/dffa* lec25dscc25_TT/dff*2}""")+"""

# # Suppress some warnings that I expect
# suppress_message [list LINT-28 LINT-29 LINT-31]

# # use port names in final netlist
# define_name_rules name_rules -equal_ports_nets -max_length 999
# set default_name_rules name_rules

# set_flatten true -effort medium -minimize multiple_output

# define_design_lib WORK -path "../work"

# # set enable_keep_signal true
# set enable_keep_signal_dt_net true
# set hdlin_keep_signal_name user_driving
# set_app_var hdlin_enable_upf_compatible_naming true

# analyze -format sverilog -lib WORK $src
# elaborate $top_module -lib WORK"""+(" -parameters " if param_set else "")+",".join([str(x) for x in param_set])+"""
# current_design $top_module

# """ +("set_dont_touch [get_nets {"+" ".join(secured_sigs)+"}] true" if secured_sigs else "") + """
# """+("set_dont_touch {"+"} {".join(snippets)+"}" if snippets else "")+"""

# link
# uniquify

# # =============================== CLOCKING ===============================

# create_clock -period 10.0 clock

# set real_inputs [remove_from_collection [remove_from_collection [all_inputs] [list clock """ +(" ".join(internal_sigs) if internal_sigs else "") + """] ] e]

# set_input_delay -clock clock -max 0 $real_inputs
# set_output_delay -clock clock -max 0 [all_outputs]

# set_max_delay 10.0 [all_outputs]

# # =============================== REPORTS ================================

# ungroup -flatten all

# # changed down from map_effort high
# check_design
# # compile -area_effort high -map_effort high -power_effort high
# compile_ultra -no_boundary_optimization


# report_area > area.rpt
# report_timing > timing.rpt
# report_power > power.rpt
# write -format verilog -output """+(split_file_name[0]+'/' if False else '')+"nl_" + m + "_".join([str(x) for x in param_set])+".sv"+"""

# exit"""
# 		dc_script_name = (split_file_name[0]+'/' if split_file_name else '')+m+"_".join([str(x) for x in param_set])+".tcl"
# 		with open(dc_script_name, 'w') as dc_script_out:
# 			dc_script_out.write(dc_shell_script)
		# if lib != 'arm':
		# 	print("Calling "+dc_script_name)
		# 	os.system("dc_shell < "+dc_script_name) 
		# else:
		# 	full_file_name = (split_file_name[0]+'/' if split_file_name else '')+"clean_"+(split_file_name[1] if split_file_name else filein)
		# 	os.system("scp "+dc_script_name+" joker:~/Swan/synthesis/"+m+"_".join([str(x) for x in param_set])+".dc")
		# 	if first:
		# 		first = False
		# 		os.system("scp "+full_file_name+" joker:~/Swan/synthesis/clean_"+(split_file_name[1] if split_file_name else filein))
	# os.system("rm *.log")
	# os.system("rm *.svf")
