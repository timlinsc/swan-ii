import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Util {
	public static final long seed_numbers = 4815162342L;

	private static Random rnd = new Random(seed_numbers);

	// Returns the number of bits required to express x
	// returns ceil(log2(x))
	public static int ceil_log2(int x) {
		int res=1;
		Boolean roundUp = false;
		while(x>>1 > 0) {
			res++;
			if((x & 0x1) == 1) {
				roundUp = true;
			}
			x >>= 1;
		} 
		return roundUp ? res : res-1;
	}

	// Returns a list of ints 1..max in shuffled order. 
	public static ArrayList<Integer> getShuffledRangeList(int max) {
		ArrayList<Integer> l = new ArrayList<Integer>(max);
		for(int i=0; i<max; i++) {
			l.add(i);
		}
		Collections.shuffle(l, rnd);
		return l;
	}
}