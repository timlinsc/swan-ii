class portAggregator {
	String base;
	int high, low;
	Boolean isMultibit;

	public portAggregator(String wire){
		int lastBrace = wire.lastIndexOf("[");
		if(lastBrace > -1) {
			isMultibit = true;
			base = wire.substring(0,lastBrace);
			String indexStr = wire.substring(lastBrace+1, wire.lastIndexOf("]"));
			high = Integer.parseInt(indexStr);
			low = high;
		}
		else {
			isMultibit = false;
			base = wire;
			high = -1;
			low = -1;
		}
	}

	public void addWire(String wire) {
		int lastBrace = wire.lastIndexOf("[");
		if(lastBrace > -1) {
			String indexStr = wire.substring(lastBrace+1, wire.lastIndexOf("]"));
			int index = Integer.parseInt(indexStr);
			if(index > high)
				high = index;
			else if(index < low)
				low = index;
		}
	}

	public String toString() {
		if(isMultibit)
			return "["+high+":"+low+"] "+base;
		else
			return base;
	}
}