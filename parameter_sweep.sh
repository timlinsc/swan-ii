#!/bin/bash

FILEIN="netlist"
THRESH=2000

# Abort on error
set -e

while test $# -gt 0; do
        case "$1" in
                -t)
                        shift
                        if test $# -gt 0; then
                                export THRESH=$1
                        else
                                echo "no threshold specified"
                                exit 1
                        fi
                        shift
                        ;;
                -f)
                        shift
                        if test $# -gt 0; then
                                export FILEIN=$1
                        else
                                echo "no input files specified"
                                exit 1
                        fi
                        shift
                        ;;
        esac
done

# Rename the output files for convenience
MODULE=$FILEIN
MODULE=${MODULE##*/}
MODULE=${MODULE##*nl_}
MODULE=${MODULE%.*} 

echo "Assuming module name: "$MODULE

python3.6 preprocessor.py $FILEIN ng15 $MODULE 
# Runs with default settings 3/2/2
./createFabric.sh -t $THRESH -f "nl_"$MODULE.sv -ea -k
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_3.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_3.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_3.sv

java -ea Swan "nl_"$MODULE.sv  2 3 4
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_4.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_4.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_4.sv

java -ea Swan "nl_"$MODULE.sv  3 4 6
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_6.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_6.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_6.sv

java -ea Swan "nl_"$MODULE.sv  4 6 8
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_8.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_8.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_8.sv

java -ea Swan "nl_"$MODULE.sv 6 10 12
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_12.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_12.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_12.sv

java -ea Swan "nl_"$MODULE.sv 8 14 16
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_16.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_16.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_16.sv

java -ea Swan "nl_"$MODULE.sv 14 24 28
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_28.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_28.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_28.sv

java -ea Swan "nl_"$MODULE.sv 24 40 48
sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
mv -v components/swan_full_custom.sv components_mux/$MODULE/swan_$MODULE"_48.sv"
mv -v components/swan_config.sv components_mux/$MODULE/swan_$MODULE"_config_48.sv"
mv -v components/frequentSubgraphs.sv components_mux/$MODULE/frequentSubgraphs_48.sv



# mv ./components/swan_full_custom.sv './components/swan_'$MODULE'.sv'
# sed -i -e 's/swan/'$MODULE'/g' ./components/swan_config.sv
# mv ./components/swan_config.sv './components/'$MODULE'_config.sv'
