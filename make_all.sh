#!/bin/bash

set -e

javac Swan.java VerilogGenerator.java CanaryManager.java setFinder.java
./parameter_sweep.sh -t 30 -f DecodeUnit.sv
./parameter_sweep.sh -t 40 -f Frontend.sv
./parameter_sweep.sh -t 120 -f designs/CSRFile.sv
./parameter_sweep.sh -t 100 -f TLB.sv
./parameter_sweep.sh -t 100 -f PTW.sv

# ./ratio_sweep.sh -t 100 -f TLB.sv

# ./timing_gen.sh -t 30 -f DecodeUnit.sv
# ./timing_gen.sh -t 40 -f Frontend.sv
# ./timing_gen.sh -t 120 -f designs/CSRFile.sv
# ./timing_gen.sh -t 100 -f TLB.sv
# ./timing_gen.sh -t 100 -f PTW.sv

cp -r ~/Documents/swan-ii/components/ ~/Documents/penguin/synth/