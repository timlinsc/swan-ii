import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.TreeSet;

public class Gate implements Comparable<Gate> {
	public static final String PLACEHOLDER = "canary#";
	public static final int UNSET = -1;
	private static final int SET = 1;

	private static final int IN_START = 2;
	private static final int OUT_START = 5;

	private int label;			// This is label in Netlist
	private int index; 			// position in the Netlist
	private Boolean valid;
	
	private ArrayList<Gate> outputs; 		//connected gates
	private ArrayList<Integer> out_ports; 	// labels of wires 
	private ArrayList<Gate> inputs;
	private ArrayList<Integer> in_ports;

	private Boolean secured;	// should this Gate be placed in an exceptionally large equivalence set? For inputs, this is an internal signal defined as a memory block
	private Snippet port_description; // if this is a snippet, fully describe its ports. 
	
	private int layer;
	private int mapping;		// Index of the gate in the equivalence set that we map this logical gate to
	private Gate logical_i_implement, physical_implementing_me; // New mapping scheme
	private String netlist_line; // Verilog describing this gate and its xbars

	private ArrayList<Gate> equiv_gates; 	// what gates can this gate be reconfigured to act as?
	private Boolean canary_added;			// has a canary been added to the equivalence set on account of this gate? (True if not secured)


	public Gate(int label, int index, Boolean secured) {
		this.label = label;
		this.index = index;
		this.secured = secured;
		canary_added = !secured; // only set if secured is true.
		valid = true;
		layer = UNSET;
		mapping = UNSET;
		inputs = new ArrayList<Gate>(IN_START);
		outputs = new ArrayList<Gate>(OUT_START);
		out_ports = new ArrayList<Integer>(OUT_START);
		in_ports = new ArrayList<Integer>(IN_START);

		port_description = null;

		//DEBUG output
		/*if(index == 2990) {
			StackTraceElement[] st = Thread.currentThread().getStackTrace();
			for(StackTraceElement ste : st)
				System.out.println(ste);
		}*/
	}

	// Used to build a CGLB. Replaces gates in toReplace with a monolithic block
	public Gate(ArrayList<Gate> toReplace, subgraphPattern pattern, int label, int index, Boolean secured) {
		this.label = label;
		this.index = index;
		this.secured = secured;
		canary_added = !secured; // only set if secured is true.
		valid = true;
		layer = UNSET;
		mapping = UNSET;
		inputs = new ArrayList<Gate>(IN_START*toReplace.size());
		outputs = new ArrayList<Gate>(OUT_START*toReplace.size());
		out_ports = new ArrayList<Integer>(OUT_START*toReplace.size());
		in_ports = new ArrayList<Integer>(IN_START*toReplace.size());

		port_description = null;
		ArrayList<Edge> patternEdges = new ArrayList<Edge>(pattern.edges);

		int gateCnt = 1;
		int ins_from_here = 0;
		for (Gate g: toReplace) {
			for(int i=0; i<g.inputs.size(); i++) {
				if(! toReplace.contains(g.inputs.get(i))) {
					inputs.add(g.inputs.get(i));
					int newEdgeLabel = g.in_ports.get(i)+100*gateCnt;
					in_ports.add(newEdgeLabel);

					// Update the corresponding input to point to the CGLB.
					int idx = g.inputs.get(i).outputs.indexOf(g);
					assert(idx!=-1) : "An input gate does not reference this in it outputs! I am "+index+" "+label+". I was currently replacing "+g.index+" "+g.label+" valid? "+g.valid+". The input was "+g.inputs.get(i).index+" "+g.inputs.get(i).label+" valid? "+g.inputs.get(i).valid;
					g.inputs.get(i).outputs.set(idx, this);
					g.inputs.get(i).out_ports.set(idx, newEdgeLabel);
					if(g.inputs.get(i).isSnippet()) {
						g.inputs.get(i).Snippet().rewire(g.Index(), index);
					}
				}
				//input comes from within CGLB
				else if (!checkForEdgeAndRemove(patternEdges, toReplace.indexOf(g.inputs.get(i)), toReplace.indexOf(g), g.in_ports.get(i))) { 
					int otherGateIdx = toReplace.indexOf(g.inputs.get(i));
					int newEdgeLabel = g.in_ports.get(i) + 100*gateCnt + 1000*otherGateIdx;
					inputs.add(this);
					in_ports.add(newEdgeLabel);

					outputs.add(this);
					out_ports.add(newEdgeLabel);
				}
			}
			for(int i=0; i<g.outputs.size(); i++) {
				if(! toReplace.contains(g.outputs.get(i))) {
					outputs.add(g.outputs.get(i));
					int newEdgeLabel = g.out_ports.get(i)+1000*gateCnt;
					out_ports.add(newEdgeLabel);
					for(int indx=0; indx<g.outputs.get(i).inputs.size(); indx++) {
						if(g.outputs.get(i).inputs.get(indx) == g) {
							// assert(newEdgeLabel == g.outputs.get(i).in_ports.get(indx)+1000*gateCnt) : "Label mismatch: "+newEdgeLabel+" != "+(g.outputs.get(i).in_ports.get(indx)+1000*gateCnt);
							assert(g.outputs.get(i).in_ports.get(indx)<1000) : "Already touched this label";
							newEdgeLabel = g.outputs.get(i).in_ports.get(indx)+1000*gateCnt;
							g.outputs.get(i).in_ports.set(indx, newEdgeLabel);
							g.outputs.get(i).inputs.set(indx, this);
							if(g.outputs.get(i).isSnippet()) {
								g.outputs.get(i).Snippet().rewire(g.Index(), index);
							}
						}
					}
				}
			}
			gateCnt++;
			ins_from_here = 0;
		}
		
		// if(index == 13460 || index == 13464 || index == 6065) {
		// 	System.out.println(this);
		// }
	}

	// TODO: DEPRECATE??
	// Used to add a register in front of a given Gate
	// @param targetLab: output port of toRetime that we affix this register to 
	public Gate(Gate toRetime, int[] targetLab, int label, int index, Boolean secured) {
		this.label = label;
		this.index = index;
		this.secured = secured;
		canary_added = !secured; // only set if secured is true.
		valid = true;
		layer = 0;
		mapping = UNSET;
		inputs = new ArrayList<Gate>(1);
		outputs = new ArrayList<Gate>(toRetime.outputs.size());
		in_ports = new ArrayList<Integer>(1);
		out_ports = new ArrayList<Integer>(toRetime.outputs.size());

		// inputs to this DFF
		inputs.add(toRetime);
		in_ports.add(Label.fullFromSrc(targetLab));

		// outputs of this DFF
		ArrayList<Integer> toRemove = new ArrayList<Integer>(toRetime.outputs.size());
		for(int i=0; i<toRetime.outputs.size(); i++) {
			if(Label.srcMatches(targetLab, toRetime.out_ports.get(i))) {
				Gate target = toRetime.outputs.get(i);
				// update this gate
				outputs.add(target);
				out_ports.add(Label.stripSrc(toRetime.out_ports.get(i)));

				// Update the gate we output to
				for(int j=0; j<target.inputs.size(); j++) {
					if(target.inputs.get(j)==toRetime && Label.srcMatches(targetLab, target.in_ports.get(j))) {
						target.inputs.set(j, this);
						target.in_ports.set(j, Label.stripSrc(target.in_ports.get(j)));
					}
				}

				// Update the original gate
				toRetime.outputs.set(i, this);
				toRetime.out_ports.set(i, Label.stripSink(toRetime.out_ports.get(i)));
			}
		}
	}

	// If an edge between two gates exisits within the FSG pattern, then remove it from the list of edges in the pattern.
	// This allows us to distinguish between connections between the same gates which are and are not part of the FSG.
	private Boolean checkForEdgeAndRemove(ArrayList<Edge> edges, int from, int to, int lab) {
		Edge hit = null;
		for(Edge e : edges) {
			if(e.start==from && e.end==to && e.label==lab) {
				hit = e;
				break;
			}
		}
		if(hit != null) {
			edges.remove(hit);
			return true;
		}
		return false;
	}

	public void AddInput(Gate g, int label) {
		inputs.add(g);
		in_ports.add(new Integer(label));
	}

	public void AddOutput(Gate g, int label) {
		outputs.add(g);
		out_ports.add(new Integer(label));
	}

	public void setLabel(int l) {
		label = l;
	}

	public void setValid(Boolean v) {
		valid = v;
	}

	public ArrayList<Gate> Outputs() {
		return outputs;
	}

	public ArrayList<Integer> OutputLabels() {
		return out_ports;
	}

	public ArrayList<Gate> Inputs() {
		return inputs;
	}

	public ArrayList<Integer> InputLabels() {
		return in_ports;
	}

	public void setIndex(int ind) { index = ind; }

	public int Label() { return label;	}

	public int Index() {	return index;	}

	public Boolean Valid() { return valid; }

	// Prints the vertice index and type and its outputs. 	
	public String toString() {
		String ret = "v " + index + " " + label + "\n";
		for(int i=0; i<outputs.size(); i++) {
			ret += "e " + index + " " + outputs.get(i).Index() + " " + out_ports.get(i) + "\n";
		}	
		return ret;
	}

	// Print a vertex and all outputs going to any node in inGraph
	public String toString(ArrayList<Gate> inGraph) {
		String ret = "v " + index + " " + label + "\n";
		for(int i=0; i<outputs.size(); i++) {
			if(inGraph.contains(outputs.get(i)))
				ret += "e " + index + " " + outputs.get(i).Index() + " " + out_ports.get(i) + "\n";
		}	
		return ret;
	}

	public String VertexString() {
		return "v " + index + " " + label + "\n";
	}

	public String EdgesString() {
		String ret = "";
		for(int i=0; i<outputs.size(); i++) {
			ret += "e " + index + " " + outputs.get(i).Index() + " " + out_ports.get(i) + "\n";
		}	
		return ret;
	}

	public String EdgesString(ArrayList<Gate> inGraph) {
		String ret = "";
		for(int i=0; i<outputs.size(); i++) {
			if(inGraph.contains(outputs.get(i)))
				ret += "e " + index + " " + outputs.get(i).Index() + " " + out_ports.get(i) + "\n";
		}	
		return ret;
	}




	// LEVILIZER
	public int Layer()
	{	return layer; 	}

	public void setLayer(int l) {
		layer = l;
	}

	private Boolean InArray(Integer[] arr, int v) {
		if(arr[0] == null)
			return false;
		for(Integer i: arr)
			if(i==v)
				return true;
		return false;
	}

	public Boolean hasString()
	{	return netlist_line != null;	}

	public String verilogString()
	{	return netlist_line;	}

	public void setString(String s) {
		netlist_line = s;
	}



	// For all input labels, get a unique list of the sink subgate/symmetry possibilities
	// In effect, this retrieves a list of ports, simplified given symmetry
	public ArrayList<int[]> getUniqueifiedSinkLabels() {
		ArrayList<int[]> res = new ArrayList<int[]>(in_ports.size());
		for(Integer port : in_ports) {
			int[] sink_label = Label.sinkLabelfromFull(port);
			Boolean list_contains_label = false;
			for(int[] lab : res)
				if(Label.arrayCompare(lab, sink_label)) {
					list_contains_label = true;
					break;
				}
			if(!list_contains_label)
				res.add(sink_label);
		}
		return res;
	}

	// For all input labels, get a unique list of the sink subgate/symmetry possibilities
	// In effect, this retrieves a list of ports, simplified given symmetry
	public TreeSet<Integer> getSinkLabelsAsSortedSet() {
		TreeSet<Integer> res = new TreeSet<Integer>();
		for(Integer port : in_ports) {
			int[] sink_label = Label.sinkLabelfromFull(port);
			res.add(sink_label[Label.SUBGATE]*10+sink_label[Label.SYMM]);
		}
		return res;
	}

	// Look through my inputs to find how many times a give label appears.
	// Should find 2 for symmetric inputs and 1 for assymetric. 
	public int countSinkLabel(int sinkLabel) {
		int res = 0;
		for (Integer l : in_ports) {
			int[] tmp = Label.sinkLabelfromFull(l);
			int lab = tmp[Label.SUBGATE]*10 + tmp[Label.SYMM];
			if(lab == sinkLabel)
				res++;
		}
		return res;
	}

	// For all output labels, get a unique list of the src subgate/symmetry possibilities
	// In effect, this retrieves a list of ports, simplified given symmetry
	public ArrayList<int[]> getUniqueifiedSrcLabels() {
		ArrayList<int[]> res = new ArrayList<int[]>(out_ports.size());
		for(Integer port : out_ports) {
			int[] src_label = Label.srcLabelfromFull(port);
			Boolean list_contains_label = false;
			for(int[] lab : res)
				if(Label.arrayCompare(lab, src_label)) {
					list_contains_label = true;
					break;
				}
			if(!list_contains_label)
				res.add(src_label);
		}
		return res;
	}

	// The equivalent gate approach
	public ArrayList<Gate> Equivalents()
	{	return equiv_gates;	}

	public Boolean hasAllEquivalents(int max)
	{	return equiv_gates != null && equiv_gates.size()>=max; }

	public void setEquivalents(ArrayList<Gate> eqv) {
		equiv_gates = eqv;
	}

	public void setEquivalents(List<Gate> eqv) {
		equiv_gates = new ArrayList<Gate>(eqv);
	}

	// Deprecated
	public int Mapping()
	{	return mapping;		}

	public void Map(int map) {
		mapping = map;
	}

	/* Stores mapping for both gates represented in this object:
	 * me_to_logical - the index in the equivalence set of the logical that is mapped to this physical gate
	 * physical_to_me - the index in the equivalence set of the physical gate that this logical gate is mapped to
	 */
	public void Map(int me_to_logical, int physical_to_me) {
		mapping = SET;
		logical_i_implement = equiv_gates.get(me_to_logical);
		physical_implementing_me = equiv_gates.get(physical_to_me);
		assert(logical_i_implement != null && physical_implementing_me != null) : "Failed to perform mapping! "+index+", "+label;
	}

	public Gate MappingOfLogical()
	{	return physical_implementing_me; }

	public Gate MappingToPhysical()
	{	return logical_i_implement; 	}

	public Boolean hasCanary()
	{	return canary_added;	}

	public void bestowCanary() {
		canary_added = true;
	}


	public Boolean Secured()
	{	return secured;		}

	public void makeSecured() {
		secured = true;
	}

	// Is this gate or any other gate in its equivalence set marked as secure?
	public Boolean inSecureSet() {
		for(Gate g : equiv_gates)
			if(g.secured)
				return true;
		return false;
	}

	public Boolean isSnippet()
	{	return port_description != null;	}

	public Snippet Snippet()
	{	return port_description;	}

	public void setupSnippet(ArrayList<String> port_names, ArrayList<String[]> connections) {
		port_description = new Snippet(port_names, connections);
	}

	public void setupSnippet(Snippet snipp) {
		port_description = snipp;
	}

	// Gates are sorted by layer, then by index in the netlist.
	// Index is an arbitrary, but unique value for resolving conflicts. 
	public int compareTo(Gate o) {
		if(o.layer != this.layer)
			return this.layer - o.layer;
		return o.index - this.index;
	}

	// Removes the gate from the netlist by shorting its inputs to its outputs.
	// Should only be used on gates with one input port and one output port (e.g. buffers).
	// I only check the first condition in this method with an assertion.
	public void Short() {
		assert(inputs.size()==1) : "Too many inputs to short this gate!";
		Gate srcGate = inputs.get(0);
		int srcLabel = in_ports.get(0);
		for(int i=0; i<outputs.size(); i++) {
			Gate sinkGate = outputs.get(i);
			int sinkLabel = out_ports.get(i);
			int lab = Label.mergeIntoFull(srcLabel, sinkLabel);

			for(int j=0; j<srcGate.outputs.size(); j++) {
				if(srcGate.outputs.get(j)==this) {
					srcGate.outputs.remove(j);
					srcGate.out_ports.remove(j);
					break;
				}
			}
			for(int j=0; j<sinkGate.inputs.size(); j++) {
				if(sinkGate.inputs.get(j)==this) {
					sinkGate.inputs.remove(j);
					sinkGate.in_ports.remove(j);
					j--; // Hold position to ensure we don't skip elements.
				}
			}

			srcGate.AddOutput(sinkGate, lab);
			sinkGate.AddInput(srcGate, lab);
		}

		valid = false;
	}
} //end class