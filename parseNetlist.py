# pasreNetlist.py
# v4.0
# Parses a group of netlists into a text representation of a graph so that GraMi can parse it easily
# Also parses the snippets source code to identify port directions. 
# Outputs a file describing each gate encountered, its ports, their directions, and their symmetry number
# 
######### USAGE: python3.6 pasreNetlist.py <FILEIN> <FILEOUT>
#
# Assumes the following about port names:
#  - Outputs are named Y, Q, or QN
#  - All A\d, B\d, etc. belong in the same symmetric sets
#      - These gates will be listed one after the other
#  - A, B, C, etc. belong in a single symmetric set
#
# Known issues: 
# - will not support enums, typedef, or other user-defined types, but I haven't really tried. 
# - will not support constants (1'bX, etc.) in the netlist
# - will not support assignment of a signal to an input


import sys
import os
import re
from subprocess import call
import argparse
import natsort

#### COMMAND LINE PARSER
parser = argparse.ArgumentParser(description='Translate a netlist into a graph using preprocessor annotations.')
parser.add_argument('filein', help='input filename')
parser.add_argument('fileout', help='output filename')
args = parser.parse_args()

filein = args.filein
fileout = args.fileout

class Wire(object):
	def __init__(self):
		self.src = -1
		self.srcSymm = -1 # the symm number of the driving port
		self.sinks = [] # list of lists. All receivers receiving this sig on an input.
		self.invalid = False

	def addSrc(self, src = -1, symm = -1):
		assert self.src == -1, "Src was already "+str(self.src)
		self.src = src
		self.srcSymm = symm

	def addSink(self, sink = -1, symm = -1):
		self.sinks.append([sink, symm])

	def completeEdge(self):
		return self.src != -1 and (self.sinks)

##### CONSTANTS
# Indices into the two-element arrays used in the Wire Class
SINK = 0
SYMM = 1
# Indices into the two-element arrays used in the structures dict
DIR = 0
# SYMM = 1 # As above

OUTPUT_NODE = 9999
INPUT_NODE = 9998
CANARY_NET = 9997
VSS = 9996
VDD = 9995
HI = "1'b1"
LO = "1'b0"
reserved_words = ["wire", "reg", "logic", "bit", "tri", "trireg", "tri0", "tri1", "trior", "triand", "supply0", "supply1", "wor", "wand"]
fixed_wires = ["SYNOPSYS_UNCONNECTED__"]

###########################################
###### LIBRARY-SPECIFIC CONSTANTS!! #######
######       CHANGE AS NEEDED.      #######
###########################################
clock_names = ['CK', 'CLK', 'clock']

# Ports of cells that will always be connected to one value. 
# Examples may include clock ports and reset lines.
hardwired_ports = {'clock':'clock', 'CK':'CK', 'CLK':'CLK', 'RN':"1'b1"} # 'SN':"1'b1"

# Names of ports that are outputs. Note that we assume there are no conflicts where
# a port is an input in one cell and an output in another. This required us to restrict which gates we used
# in the NanGate 15nm PDK for example. 
output_port_names = ['Y', 'YC', 'YS', 'Q'] # for NanGate 45nm freePDK
# output_port_names = ['Y', 'Q', 'QN', 'SUM', 'CO', 'CON', 'S'] # for ARM 65lp library
# output_port_names = ['Z', 'ZN', 'Q', 'QD', 'CO', 'IQ', 'o'] # for NG15nm library. Add 'S' if using adders instead of muxes

##### HELPER FUNCITONS
def parseSnippets(snippets_files):
	reading_inputs = False
	reading_outputs = False
	snippets_structure = {}
	module_name = 'foo'

	for file in snippets_files:
		with open(file, 'r') as snipp_file:
			lines = snipp_file.readlines()
			for l in lines:
				l = l.strip()
				if l.strip().startswith(')'):
					continue
				if l.startswith('module'):
					toks = l.strip().split()
					module_name = toks[1].replace('(','')
					snippets_structure[module_name] = {}

				elif l.startswith('input') or reading_inputs and not l.strip().startswith('output'):
					if not l.endswith(';'): reading_inputs = True
					if reading_inputs and l.endswith(';'): reading_inputs = False
					vals = l.replace(',','').replace(';','').split()
				
					for val in [v for v in vals if v not in reserved_words and '[' not in v]:
						# ind = val.find('[') # for the purposes of direction, we ignore multibit information
						# if ind != -1:
						# 	val = val[:ind]
						if val != 'input': #and val != 'clock' and val not in clock_names: 
							# Snippets don't support internal inputs, so we don't need to bother checking. 
							snippets_structure[module_name][val] = 'i'

				elif l.startswith('output') or reading_outputs and not l.strip().startswith('input'):
					if not l.endswith(';'): reading_outputs = True
					if reading_outputs and l.endswith(';'): reading_outputs = False
					vals = l.replace(',','').replace(';','').split()
				
					for val in [v for v in vals if v not in reserved_words and v != 'output' and v not in clock_names and '[' not in v]:
						# ind = val.find('[') # for the purposes of direction, we ignore multibit information
						# if ind != -1:
						# 	val = val[:ind]
						if val not in clock_names: 
							# Snippets don't support internal inputs, so we don't need to bother checking. 
							snippets_structure[module_name][val] = 'o'
	return snippets_structure

def parseFile(filename):
	ret = []
	try:
		with open(filename, 'r') as list:
			lines = list.readlines()
			for l in lines:
				ret.append(re.compile("(^|\W)"+l[:-1]+"(\W|\n|$)")) # Remove the \n
	except IOError:
		print("Unable to read "+filename+". It may not have been generated by Arrow.")
		return []
	return ret

def wireSort(l):
	prefix = ''
	subsec_start = 0
	for i in range(len(l)):
		prefix_here = '' if '[' not in l[i] else l[i][:l[i].rfind('[')]
		if not prefix_here or prefix_here != prefix:
			if i > subsec_start+1:
				l = l[:subsec_start] + natsort.natsorted(l[subsec_start:i], reverse=True) + l[i:]
			prefix = prefix_here
			subsec_start = i

	if len(l) > subsec_start+1:
		l = l[:subsec_start] + natsort.natsorted(l[subsec_start:], reverse=True)
	return l

	
###################################################
################### Parse module ##################
###################################################
	
# Parse the module inputs, outputs, internal signals, and gates/FFs/assorted-logic-elements
gateTypeIDs = {} # Maps a type (NAND, DFF, etc.) to a numeric label
maxGateType = 0

graphVertices = ['# t 1\n', 'v 0 '+str(VSS)+'\n', 'v 1 '+str(VDD)+'\n']
graphEdges = []

currentGate = 2
ports = {}
structures = {} # dict of dict of list. Describes the port structure of a type of gate. 

# Parse the input file name and acquire the target files. 
last_slash = filein.rfind('/')
if last_slash != -1: # filein includes directories
	folder = filein[:last_slash]
	prefix = filein[last_slash+1:]
else:
	folder = '.'
	prefix = filein
file_list = [f for f in os.listdir(folder) if f.startswith(prefix) and f.endswith('.sv')]
if not file_list:
	print( 'No files found when looking for '+prefix)
	sys.exit(-1)
fNum = 1

# Parse the secured signal list from Arrow
secured = parseFile("./pp_secured.txt")
sec_inds = []

# Parse the internal inputs list from Arrow
internals = parseFile("./pp_internals.txt")
internal_inds = []

# Parse the snippets list from Arrow
# We need strings, not regex, so we don't call the helper function.
snippets = []
snipp_inds = []
try:
	with open("./snippets.txt", 'r') as snippets_list:
		lines = snippets_list.readlines()
		for l in lines:
			snippets.append(l[:-1]) # Remove the \n
except IOError:
		print("Unable to read snippets.txt. It may not have been generated by Arrow.")

#Parse the canary list from Arrow
canaries = parseFile("./pp_canaries.txt")
canary_inds = []

# Parse the snippets Verilog source files
print("Looking up snippets:")
print(["./snippets/"+x+".sv" for x in snippets])
snippets_structure = parseSnippets(["./snippets/"+x+".sv" for x in snippets if x])

########### Add fixed values to gates.txt
gateTypeIDs['output'] = OUTPUT_NODE
gateTypeIDs['input'] = INPUT_NODE
gateTypeIDs['net'] = CANARY_NET
gateTypeIDs['VSS'] = VSS
gateTypeIDs['VDD'] = VDD
# gateTypeIDs['DFFQ_X1M_A9TR'] = 0  # Unique to ARM library
# gateTypeIDs['XNOR2_X1M_A9TR'] = 1 # Unique to ARM library
# gateTypeIDs['AND4_X1M_A9TR'] = 2  # Unique to ARM library
gateTypeIDs['DFFPOSX1'] = 0  # NanGate cells
gateTypeIDs['XNOR2X1'] = 1 # NanGate cells
gateTypeIDs['AND2X1'] = 2  # NanGate cells
maxGateType = 3
# structures['DFFQ_X1M_A9TR'] = {'D':[False,0], 'CK':['CK',None], 'Q':[True,0]}  # Unique to ARM library
# structures['XNOR2_X1M_A9TR'] = {'A':[False,0],'B':[False,0],'Y':[True,0]}      # Unique to ARM library
# structures['AND4_X1M_A9TR'] = {'A':[False,0],'B':[False,0],'C':[False,0],'D':[False,0],'Y':[True,0]} # Unique to ARM library
structures['DFFPOSX1'] = {'D':[False,0], 'CLK':['CLK',None], 'Q':[True,0]}  # NanGate cells
structures['XNOR2X1'] = {'A':[False,0],'B':[False,0],'Y':[True,0]}      # NanGate cells
structures['AND2X1'] = {'A':[False,0],'B':[False,0],'Y':[True,0]} # NanGate cells

past_graph_files = []
for f in file_list:
	assigns = {} 	 				# Maps one wire name to another
	gateIDs = {'VSS':0, 'VDD':1}	# Maps a gate name (e.g. X1443) to a numberic label
	wires = {} 		 				# Maps a wire name (e.g. x133) to a Wire object
	snippets_db = {}

	with open(folder+'/'+f, 'r') as fin:

		lines = fin.readlines()
		
		module_started = 0
		netlist_started = 0
		reading_inputs = 0
		reading_outputs = 0
		reading_signals = 0
		reading_gate = 0
		prev_line = ''
		dummy_wire_idx = 0
		
		currentGraphFile = ['# t 1\n', 'v 0 '+str(VSS)+'\n', 'v 1 '+str(VDD)+'\n']

		for l in lines:
			l = l.strip()
			
			if l.startswith('//'):
				continue

			# Determine when actual module starts (i.e. skip unneeded header stuff)
			##### ASSUMPTION: File does not begin "module foo (input bar,\n"
			elif l.startswith('module'):
				module_started = 1
				continue
			
			# Quit if module end reached
			elif l.startswith('endmodule'):
				break
			
			# Determine when actual netlist starts (i.e. signal/module definitions done)
			elif module_started != 0 and l == '':
				print("netlist okay!")
				print( 'Parsing input module for gates...')
				netlist_started = 1
				continue
			
			# Parse inputs
			elif module_started != 0 and netlist_started == 0:		
				if l.startswith('input') or reading_inputs != 0:
					# g_inputs_declared.append(l)
					if not l.endswith(';'): reading_inputs = 1
					if reading_inputs != 0 and l.endswith(';'): reading_inputs = 0
					vals = l.replace(',','').replace(';','').split()
				
					next_is_multibit = False
					next_bit_min = 0
					next_bit_max = 0
					for val in [v for v in vals if v not in reserved_words]:
						if val.startswith('['):
							next_is_multibit = True
							next_bit0 = int(val.split(':')[0][1:])
							next_bit1 = int(val.split(':')[1][:len(val.split(':')[1])-1])
							next_bit_min = min(next_bit0, next_bit1)
							next_bit_max = max(next_bit0, next_bit1)
						else:
							if next_is_multibit:
								# next_is_multibit = False
								for i in range(next_bit_min, next_bit_max+1):
									#check if this isn't a real input, but just an internal one from the preprocessor
									vert = 'v ' + str(currentGate) + ' ' + str(INPUT_NODE) + '\n'
									for sig in internals:
										if re.search(sig, val):
											internal_inds.append(currentGate)
									for sig in canaries:
										if re.search(sig, val):
											canary_inds.append(currentGate)
											vert = 'v ' + str(currentGate) + ' ' + str(CANARY_NET) + '\n'


									name = val+'['+str(i)+']'
									wires[name] = Wire()
									wires[name].addSrc(currentGate, 0)
									ports[currentGate] = name
									
									#output graph vertices
									graphVertices.append(vert)
									currentGraphFile.append(vert)

									currentGate += 1
							elif val != 'input' and val != 'clock' and val != 'CLK': # add vertices for the inputs
								vert = 'v ' + str(currentGate) + ' ' + str(INPUT_NODE) + '\n'

								#check if this isn't a real input, but just an internal one from Arrow
								for sig in internals:
									if re.search(sig, val):
										internal_inds.append(currentGate)
								for sig in canaries:
									if re.search(sig, val):
										print("Found canary input: "+val)
										canary_inds.append(currentGate)
										vert = 'v ' + str(currentGate) + ' ' + str(CANARY_NET) + '\n'

								graphVertices.append(vert)
								currentGraphFile.append(vert)
								wires[val] = Wire()
								wires[val].addSrc(currentGate, 0)
								ports[currentGate] = val
								currentGate += 1
					continue


				# Parse outputs
				elif l.startswith('output') or reading_outputs != 0:
					if not l.endswith(';'): reading_outputs = 1
					if reading_outputs != 0 and l.endswith(';'): reading_outputs = 0
					vals = l.replace(',','').replace(';','').split()
				
					next_is_multibit = 0
					next_bit_min = 0
					next_bit_max = 0
					for val in vals:
						if val.startswith('['):
							next_is_multibit = 1
							next_bit0 = int(val.split(':')[0][1:])
							next_bit1 = int(val.split(':')[1][:len(val.split(':')[1])-1])
							next_bit_min = min(next_bit0, next_bit1)
							next_bit_max = max(next_bit0, next_bit1)
						else:
							if next_is_multibit != 0:
								next_is_multibit = 0
								for i in range(next_bit_min, next_bit_max+1):
									name = val+'['+str(i)+']'
									wires[name] = Wire()
									wires[name].addSink(currentGate, 0)
									ports[currentGate] = name
									
									#output graph vertices
									vert = 'v ' + str(currentGate) + ' ' + str(OUTPUT_NODE) + '\n'
									graphVertices.append(vert)
									currentGraphFile.append(vert)
									currentGate += 1
							elif val != 'output':
								vert = 'v ' + str(currentGate) + ' ' + str(OUTPUT_NODE) + '\n'
								graphVertices.append(vert)
								currentGraphFile.append(vert)
								wires[val] = Wire()
								wires[val].addSink(currentGate, 0)
								ports[currentGate] = val
								currentGate += 1
					continue

				# Parse assign statements
				if l.startswith('assign'):
					tokens = l.replace(';',' ;').replace('  ',' ').split()
					if(len(tokens) > 4 and tokens[4].startswith('[')):
						tokens[3] += tokens[4]
					assigns[tokens[1]] = tokens[3]
				
			# Parse actual netlist
			elif module_started != 0 and netlist_started != 0:
				
				# if re.match(r"wire\s+\[", l):
				# 	wire_lengths[]

				if not l.startswith('wire ') and not l.startswith('reg ') and not l.startswith('logic ') and not l.startswith('tri '): # parsing a gate or a snippet
					l = l.replace('  ',' ');
					if not l.endswith(';'):
						if reading_gate:
							if prev_line.endswith('(') and not prev_line[-2]==' ':	prev_line += l #port name needing wire name
							else: prev_line += ' '+l
						else:
							reading_gate = 1
							prev_line = l
						continue
					elif reading_gate != 0:
						reading_gate = 0
						if prev_line.endswith('(') and not prev_line[-2]==' ':
							l = prev_line+l
						else:
							l = prev_line+' '+l
						prev_line = ''
					# if "\\at/genblk1.at/genblk1.at/genblk1.al/ad[0].an/genblk2.ls_adder[1" in l:
					# 	print(l)

					ltok = l.replace(' [', '[').split()

					#check if this gate interacts with a secured signal
					for s in secured:
						if re.search(s, l):
							sec_inds.append(currentGate)

					#extract element type
					elem_type = ltok[0]
					# if '-' in elem_type: elem_type = elem_type.replace('-','_')
					elem_type = re.sub("INVX\d$", "INVX1", elem_type) # Unique to NanGate library
					# elem_type = re.sub("_X.*[MAB]_", "_X1M_", elem_type) # Unique to ARM library
					if elem_type not in gateTypeIDs:
						gateTypeIDs[elem_type] = maxGateType
						maxGateType += 1

					#extract element name
					elem_name = ltok[1]
					gateIDs[elem_name] = currentGate

					vert = 'v ' + str(currentGate) + ' ' + str(gateTypeIDs[elem_type]) + '\n'
					graphVertices.append(vert)
					currentGraphFile.append(vert)

					if elem_type in snippets:
						snippets_db[currentGate] = {}
						reading_multibit = False
						# build graph edges
						for i in range(3,len(ltok)-1):
							if not reading_multibit:
								try:
									port_name = ltok[i][1:ltok[i].index('(')]
								except:
									continue
							if '(' in ltok[i]:
								if ')' in ltok[i]:
									signal_name = ltok[i][ltok[i].index('(')+1:ltok[i].index(')')]
								else:
									signal_name = ltok[i][ltok[i].index('(')+1:]
									i += 1
							else: # the above does not hold when parsing multibit 
								signal_name = ltok[i]
								i += 1

							if '{' in signal_name:
								signal_name = signal_name[signal_name.index('{')+1:-1] # remove '{' and ','
								reading_multibit = True
							elif '}' in signal_name:
								signal_name = signal_name[:signal_name.index('}')] # removes '}),' as applicable
								reading_multibit = False
							elif reading_multibit and signal_name.endswith(','):
								signal_name = signal_name[:-1] # remove ','
							assert signal_name != '{'

							# don't process 
							if signal_name == '':
								continue
							# if signal_name in fixed_wires 					 or signal_name==LO or signal_name==HI:
							if True in [(w in signal_name) for w in fixed_wires] or signal_name==LO or signal_name==HI:
								if port_name not in snippets_db[currentGate]:
									snippets_db[currentGate][port_name] = []								
								snippets_db[currentGate][port_name].append(signal_name)
								# print("Fixed wire: "+port_name+" -> "+signal_name)
								continue

							# Handle implicit and explicit multibit expressions
							# Note that this will only handle wires we have seen before!!
							if signal_name+"[0]" in wires:
								signal_names = [signal_name] # In case not every x[i] is already in wires, we will process the rest at the end.
								for w in wires:
									if w.startswith(signal_name+'['):
										signal_names.append(w)
							elif ':' in signal_name: #This is a multibit expression, treat each wire separately
								signal_names = []
								prefix = signal_name[:signal_name.rfind('[')]
								first = int(signal_name[signal_name.rfind('[')+1:signal_name.rfind(':')])
								second = int(signal_name[signal_name.rfind(':')+1:signal_name.rfind(']')])
								if first > second:
									r = range(second, first+1)
								else:
									r = range(first, second+1)
								for i in r:
									signal_names.append(prefix+'['+str(i)+']')
							else:
								signal_names = [signal_name]

							if port_name not in snippets_db[currentGate]:
									snippets_db[currentGate][port_name] = []
							for sig in signal_names:
								# Honor the assign statements!
								if sig in assigns:
									sig = assigns[sig]

								snippets_db[currentGate][port_name].append(sig)								

								if sig not in wires:
									wires[sig] = Wire()

								if snippets_structure[elem_type][port_name] == 'i':
									wires[sig].addSink(currentGate, 0)
								else:
									wires[sig].addSrc(currentGate, 0)
							if signal_name+"[0]" in wires:
								wires[signal_name].invalid = True # this is a placeholder, so ignore it in the end.
					elif elem_type not in structures: # Gate structure is not known
						structures[elem_type] = {}
						in_symm = -1
						out_symm = 0
						myChar = ''
						sameGroup = re.compile("foobar") # needs to fail on the first iteration.
						lastWasInverted = False # Only useful in the ARM library
						last_port = ''

						# build graph edges
						for i in range(3,len(ltok)-1):
							try:
								port_name = ltok[i][1:ltok[i].index('(')]
							except:
								continue

							if port_name in hardwired_ports:
								structures[elem_type][port_name] = [hardwired_ports[port_name], '']
								continue

							try:
								signal_name = ltok[i][ltok[i].index('(')+1:ltok[i].index(')')]
							except:
								signal_name = ltok[i][ltok[i].index('(')+1:]
								i += 1

							# Honor the assign statements!
							if signal_name in assigns:
								signal_name = assigns[signal_name]

							if signal_name not in wires:
								wires[signal_name] = Wire()
								if signal_name == HI:
									print('HI detected')
									wires[HI].addSrc(gateIDs['VSS'], 0)
								elif signal_name == LO:
									print('LO detected')
									wires[LO].addSrc(gateIDs['VDD'], 0)


							if port_name in output_port_names:
								wires[signal_name].addSrc(currentGate, out_symm)
								structures[elem_type][port_name] = [True, out_symm]
								out_symm += 1
							else:
								if lastWasInverted or not re.fullmatch(sameGroup, port_name):
									in_symm += 1
								myChar = port_name[0]
								if elem_type.startswith('NAND') or elem_type.startswith('NOR'):
									nextChar = chr(ord(myChar) + 1) # In NAND3 and NOR3, A+B+C is one group. For other gates, this is not the case.
								else:	
									nextChar = chr(ord(myChar) + 1) if (myChar=='A' or myChar=='C') else "" # In NanGate 45nm, A+B is separate from C+D
								# B will not be symmetric with A1
								sameGroup = re.compile(myChar+r"\d+") if port_name[-1] in '1234567890D' else re.compile(myChar+r"\d+|"+nextChar)
								wires[signal_name].addSink(currentGate, in_symm)
								structures[elem_type][port_name] = [False, in_symm]

							lastWasInverted = port_name.endswith("N") # Only useful in the ARM library
							last_port = port_name

						hasOut = False
						for port in structures[elem_type]:
							if port in output_port_names:
								hasOut = True
								break
						if not hasOut:
							del structures[elem_type]

					else: # Gate structure is known
						# build graph edges
						for i in range(3,len(ltok)-1):
							try:
								port_name = ltok[i][1:ltok[i].index('(')]
							except:
								continue

							if port_name=='' or port_name in hardwired_ports:
								continue

							try:
								signal_name = ltok[i][ltok[i].index('(')+1:ltok[i].index(')')]
							except:
								signal_name = ltok[i][ltok[i].index('(')+1:]
								i += 1
							# Honor the assign statements!
							if signal_name in assigns:
								signal_name = assigns[signal_name]

							if signal_name not in wires:
								wires[signal_name] = Wire()
								if signal_name == HI:
									print('HI detected')
									wires[HI].addSrc(gateIDs['VSS'], 0)
								elif signal_name == LO:
									wires[LO].addSrc(gateIDs['VDD'], 0)

							if structures[elem_type][port_name][DIR]: # This is an output
								wires[signal_name].addSrc(currentGate, structures[elem_type][port_name][SYMM])
							else:
								wires[signal_name].addSink(currentGate, structures[elem_type][port_name][SYMM])
								

					currentGate += 1

	print( 'Using assigns to try to complete graph...')
	for assignee in assigns:
		assigner = assigns[assignee]
		pDest = -1
		pSrc = -1
		for p in ports:
			if ports[p] == assigner:
				pSrc = p
			elif ports[p] == assignee:
				pDest = p
		if pDest != -1:
			if assigner == HI or assigner == LO:
				if assigner not in wires:
					wires[assigner] = Wire()
					wires[assigner].addSrc(0 if assigner==HI else 1, 0)
				wires[assigner].addSink(pDest, 0)
			elif pSrc == -1 and assigner in wires:
				wires[assigner].addSink(pDest, 0)
				wires[ports[pDest]].invalid = True # Now that another port is driving the output, don't worry about the port as a wire.
				assert len(wires[ports[pDest]].sinks) == 1
			elif pSrc != -1:
				if assigner not in wires:
					wires[assigner] = Wire()
				wires[assigner].addSink(pDest, 0) # This should be an output port, so symmetry is unimportant.
				if wires[assigner].src == -1:
					wires[assigner].addSrc(pSrc, 0)
				wires[ports[pDest]].invalid = True # Now that another port is driving the output, don't worry about the port as a wire.
				assert len(wires[ports[pDest]].sinks) == 1
				# print('Found connection for '+assigner+': '+assignee)
	print( 'Parsing input module for wires...')
	snipp_conn_annotations = {}
	for w in wires:
		if wires[w].invalid:
			continue
		# Skip wires going to ports
		if not wires[w].completeEdge():
			if '[' in w and w[:w.rfind('[')] in wires:
				if not wires[w].sinks:
					for snk in wires[w[:w.rfind('[')]].sinks: # should only happen for snippets and ports
						wires[w].addSink(snk[0], snk[1])
				else:
					wires[w].addSrc(wires[w[:w.rfind('[')]].src, wires[w[:w.rfind('[')]].srcSymm) 
			elif not wires[w].sinks: # has src needs dest
				dests = []
				for k in assigns:
					if assigns[k]==w:
						dests.append(k)
				for k in ports:
					if ports[k] in dests:
						wires[w].addSink(k, 1) # This should be an output port, so symmetry is unimportant.
				if not wires[w].completeEdge() and w+"[0]" not in wires:
					print( w + ' is unmatched, without a sink. May be an unused port or a clock.')
					continue
			elif w+"[0]" not in wires:
				print( w + ' is unmatched, without a source. May be an unused port or a clock.')
				continue

		for dest in wires[w].sinks:
			if wires[w].src in snippets_db and not dest[SINK] in snippets_db:
				if (str(wires[w].src) + ' ' + str(dest[SINK])) not in snipp_conn_annotations:
					snipp_conn_annotations[str(wires[w].src) + ' ' + str(dest[SINK])] = 0
				else:
					snipp_conn_annotations[str(wires[w].src) + ' ' + str(dest[SINK])] += 1
				ss = snipp_conn_annotations[str(wires[w].src) + ' ' + str(dest[SINK])]
				edgeLabel = (str(ss) if ss > 0 else '') + str(dest[SYMM])
			else:
				edgeLabel = (str(wires[w].srcSymm) if wires[w].srcSymm > 0 else '') + str(dest[SYMM])

			if wires[w].src == -1:
				print("Bad wires: "+w)
				sys.exit(-1)

			edge = 'e ' + str(wires[w].src) + ' ' + str(dest[SINK]) + ' ' + edgeLabel + '\n'
			graphEdges.append(edge)
			currentGraphFile.append(edge)

	f_parts = f.split('.')
	f_out_name = f_parts[0]+'.lg'
	with open(f_out_name, 'w') as log:
		print( 'Writing output to '+f_out_name+'...')
		for l in currentGraphFile:
			log.write(l)
	past_graph_files.append(currentGraphFile)

with open(fileout, 'w') as log:
	print( 'Writing output to '+fileout+'...')
	for v in graphVertices:
		# Since we don't want to mine subgraphs on IO ports, this keeps GraMi from trying. 
		if re.match(r"v \d+ 999\d", v):
			log.write(re.sub(r"999\d$", str(maxGateType), v))
			maxGateType += 1
		else:
			log.write(v)
			
	for e in graphEdges:
		log.write(e)


print("Writing supplimental mapping files")

with open('gates.txt', 'w') as gates:
	# print( 'Definitions of labels:')
	for k in gateTypeIDs:
		# print( k + ' : ' + str(gateTypeIDs[k]))
		gates.write(k + ' : ' + str(gateTypeIDs[k]) + '\n')
		if k in structures:
			for port in structures[k]:
				if port in hardwired_ports:
					gates.write('\t'+port+' '+hardwired_ports[port]+'\n')
				else:
					gates.write('\t'+port+' '+('1' if structures[k][port][DIR] else '0')+' '+str(structures[k][port][SYMM])+'\n')


with open('secure.txt', 'w') as sec_out:
	for s in sec_inds:
		sec_out.write(str(s)+'\n')

with open('internals.txt', 'w') as internals_out:
	for sig in internal_inds:
		internals_out.write(str(sig)+'\n')

with open('canaries.txt', 'w') as canary_out:
	for sig in canary_inds:
		canary_out.write(str(sig)+'\n')		

with open('snippets.txt', 'w') as snippets_out:
	for snip in snippets_db: #foreach snippet
		# Since "<SRC> <SINK>" is insufficient as a key, we want to use the snippet's "symmetry"
		#  to distinguish ports that connect to the same external gate
		# This dict maps "<SRC> <SINK>" to the "symmetry" present
		# WARNING: Two snippets with many connections could cause overflow here in bad ways, but I'm not sure
		snipp_conn_annotations = {}

		snippets_out.write(str(snip)+'\n')
		for port in snippets_db[snip]: #foreach port
			snippets_out.write(' '+port+'\n')
				
			if port in hardwired_ports:
				snippets_db[snip][port] = [hardwired_ports[port]]
				snippets_out.write('  '+snippets_db[snip][port][0] + '\n') # get rid of extra \n??
				continue

			# Check if we need to split implicit multibit expressions up
			need_to_update = False
			new_list = []
			for w in snippets_db[snip][port]: #foreach bit in that port (multibit)
				# if w in wires and not wires[w].completeEdge() and w+"[0]" in wires:
				if w in wires and w+"[0]" in wires:
					need_to_update = True
					for wire in [wy for wy in wires if wy.startswith(w+'[')]:
						if wire not in new_list:
							new_list.append(wire)
				elif w not in new_list:
					new_list.append(w)
			if need_to_update:
				snippets_db[snip][port] = wireSort(new_list)
			else:
				snippets_db[snip][port] = wireSort(snippets_db[snip][port])

			for i in range(len(snippets_db[snip][port])): #foreach bit in that port (multibit)
				if snippets_db[snip][port][i] in wires and not True in [(w in snippets_db[snip][port][i]) for w in fixed_wires]: # check both for sanity's sake
					if wires[snippets_db[snip][port][i]].src == snip: # The snippet is the src
						old_wire_name = snippets_db[snip][port][i]
						snippets_db[snip][port][i] = []
						for dest in wires[old_wire_name].sinks:
							if dest[SINK] in snippets_db: # Connection to another snippet: preserve wire name
								snippets_db[snip][port][i].append(old_wire_name)
								continue

							base_key = str(snip) + ' ' + str(dest[SINK])

							num_conns_to_target = ""
							if not base_key in snipp_conn_annotations:
								snipp_conn_annotations[base_key] = 1
							else:
								num_conns_to_target = str(snipp_conn_annotations[base_key])
								print("Appending "+num_conns_to_target+" to key")
								snipp_conn_annotations[base_key] += 1
							snippets_db[snip][port][i].append(base_key + ' ' + num_conns_to_target + str(dest[SYMM]))

					else: #it is the sink
						old_wire_name = snippets_db[snip][port][i]
						if not wires[snippets_db[snip][port][i]].src in snippets_db: # Incoming wire from another snippet -> PRESERVE OLD NAME
							base_key = str(wires[snippets_db[snip][port][i]].src) + ' ' + str(snip)
							num_conns_to_target = "0"
							if not base_key in snipp_conn_annotations:
								snipp_conn_annotations[base_key] = 1
							else:
								num_conns_to_target = str(snipp_conn_annotations[base_key])
								snipp_conn_annotations[base_key] += 1
							snippets_db[snip][port][i] = base_key + ' ' + (str(wires[snippets_db[snip][port][i]].srcSymm) if wires[snippets_db[snip][port][i]].srcSymm > 0 else '') + num_conns_to_target

				# output the connections themselves
				if isinstance(snippets_db[snip][port][i], list):
					print(old_wire_name+'  '+' : '.join(snippets_db[snip][port][i]))
					snippets_out.write('  '+' : '.join(snippets_db[snip][port][i]) + '\n') # get rid of extra \n??
				else:
					print(old_wire_name+'  '+snippets_db[snip][port][i])
					snippets_out.write('  '+snippets_db[snip][port][i] + '\n') # get rid of extra \n??

with open('ports.txt', 'w') as ports_out:
	for p in ports:
		ports_out.write(str(p)+" "+ports[p]+"\n")

print("parseNetlist.py complete!\n")
