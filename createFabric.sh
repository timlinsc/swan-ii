#!/bin/bash

FILEIN="netlist"
THRESH=15
EA_FLAG=""
DO_TIMING=""
RM_FILES=true

# Abort on error
set -e

while test $# -gt 0; do
        case "$1" in
                -h|--help)
                        echo "createFabric.sh - runs full SWAN toolchain."
                        echo "IMPORTANT: Swan uses a number of constants that must be specified for the user's target library."
                        echo "Search the codebase for 'LIBRARY-SPECIFIC CONSTANTS' to find constants to specify."
                        echo " "
                        echo "options:"
                        echo "-h, --help        show brief help"
                        echo "-t                specify starting point for sweep of FSG threshold for GraMi"
                        echo "-f                specify input file or common prefix to all input files. i.e. \"netlist\" -> \"netlist*\""
                        echo "-ea               enable Java assertions"
                        echo "-k                keep intermediate files"
                        exit 0
                        ;;
                -t)
                        shift
                        if test $# -gt 0; then
                                export THRESH=$1
                        else
                                echo "no threshold specified"
                                exit 1
                        fi
                        shift
                        ;;
                -f)
                        shift
                        if test $# -gt 0; then
                                export FILEIN=$1
                        else
                                echo "no input files specified"
                                exit 1
                        fi
                        shift
                        ;;
                -ea)
                        shift
                        export EA_FLAG="-ea"
                        ;;
                -k)
                        shift
                        export RM_FILES=false
                        ;;
                -T)
                        shift
                        export DO_TIMING="1 1 1 -t"
                        ;;
        esac
done

python3.6 parseNetlist.py $FILEIN nl_graph.lg &> /dev/null
mv nl_graph.lg ./GraMi/Datasets/nl_graph.lg

STEP=5

# Smallest inter-CGLB wire count achieved
# Start with largest signed 32-bit int
LAST_WIRES=4294967295
WIRES=4294967295
# Threshold value corresponding to best result
BEST_THRESH=$THRESH
FIRST_ITER=1
while : ; do
        if [[ $THRESH -le 50 ]]; then
                STEP=10
        elif [[ $THRESH -le 100 ]]; then
                STEP=20
        elif [[ $THRESH -le 200 ]]; then
                STEP=25
        else
                STEP=50
        fi

        if [[ $FIRST_ITER -ne 1 ]]; then
                THRESH=$(($THRESH+$STEP))
        else
                FIRST_ITER=0
        fi

        LAST_WIRES=$WIRES

        cd ./GraMi
        echo "Calling GraMi with threshold: "$THRESH
        # We should not fail on a timeout
        set +e
        timeout 60s ./grami -f nl_graph.lg -s $THRESH -t 1 -p 0 > output.log
        # Continue if GraMi timed out, otherwise proceed
        if [[ $? != 124 ]]; then
                cp output.log ../output.log
                cp Output.txt ../Output.txt
                cd ../

                # Swan returns a value to the shell. It must not be taken as an error
                java $EA_FLAG Swan $FILEIN $DO_TIMING
                WIRES=$(cat cost.bin)
                echo "Swan returned "$WIRES" wires connected to crossbars."
        else
                echo "GraMi timed out."
                cd ../
        fi
        set -e

        # Evaluate the condition on the do-while loop
        [[ $WIRES -lt $LAST_WIRES ]] || break
done

THRESH=$(($THRESH-$STEP))
cd ./GraMi
echo "Calling GraMi with threshold: "$THRESH
./grami -f nl_graph.lg -s $THRESH -t 1 -p 0 > output.log
cp output.log ../output.log
cp Output.txt ../Output.txt
cd ../
set +e
java $EA_FLAG Swan $FILEIN $DO_TIMING
set -e

if [ "$RM_FILES" = true ] ; then
        rm *.lg
        rm output.log
        rm canaries.txt gates.txt Output.txt secure.txt fsg_templates.txt internals.txt ports.txt snippets.txt
        rm *.snp
fi

# Use this code for single-use runs of this script.
# parameter_sweep.sh will do this for us, though.
# Rename the output files for convenience
# MODULE=$FILEIN
# MODULE=${MODULE##*/}
# MODULE=${MODULE##*nl_}
# MODULE=${MODULE%.*} 

# echo "Assuming module name: "$MODULE
# sed -i -e 's/module swan/module '$MODULE'/g' ./components/swan_full_custom.sv
# sed -i -e 's/module: swan/module: '$MODULE'/g' ./components/swan_full_custom.sv
# mv ./components/swan_full_custom.sv './components/swan_'$MODULE'.sv'
# sed -i -e 's/swan/'$MODULE'/g' ./components/swan_config.sv
# mv ./components/swan_config.sv './components/'$MODULE'_config.sv'
