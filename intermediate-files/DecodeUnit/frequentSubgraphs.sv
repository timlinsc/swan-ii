module m3 (
	input IN10, IN20, 
	output OUT30, OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X2 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
	INVX1 U3 ( .A(OUT20), .Y(OUT30) );
endmodule
module m1 (
	input IN10, IN20, 
	output OUT30, OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
	INVX1 U3 ( .A(OUT20), .Y(OUT30) );
endmodule
module m10 (
	input IN10_1, IN10, 
	output OUT10, OUT20
);
	AND2X2 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m11 (
	input IN10_1, IN10, 
	output OUT10, OUT20
);
	OR2X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m8 (
	input IN10, IN21, IN20, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN21) );
endmodule
module m7 (
	input IN10, IN20_1, IN20, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	NOR3X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN20_1) );
endmodule
module m6 (
	input IN10, IN20_1, IN20, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	NAND3X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN20_1) );
endmodule
module m4 (
	input IN10, IN20, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X2 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m5 (
	input IN10, IN20, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OR2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m0 (
	input IN10, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m2 (
	input IN10, IN20, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m9 (
	input IN10_1, IN10, 
	output OUT10, OUT20
);
	AND2X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
