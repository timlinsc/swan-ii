#What are all these files? 
This folder contains all the intermediate files created by the SWAN toolchain to pass information between its various components. In this example, I ran the toolchain on the RISC-V PTW in its 3/2/2 parameterization using multiplexors. What follows is a description of all the files found under the PTW directory. The only difference between this and the other directories is the naming: replace "PTW" with the corresponding module name. I generated these results with the following -t values passed to createFabric.sh: DecodeUnit-15, CSRFile-95, Frontend-25, PTW-65, TLB-85.

##clean_PTW.sv
A version of designs/PTW.sv which has been stripped of the 'secure' and 'canary' flags.

##pp_canaries.txt 
The names of the signals flagged with 'canary,' created by the preprocessor.

##pp_secured.txt 
The names of the signals flagged with 'secure,' created by the preprocessor.

##nl_PTW.sv
The synthesized netlist of the PTW with the canary and secure signals preserved by Design Compiler.

##nl_PTW.lg 
The vertex-edge representation of the PTW netlist created by parseNetlist.py for consumption by GraMi.

##gates.txt
A list of the types of gates used in the design and their ports. The format is as follows:
GATE_NAME: <GATE_ID>
	<PORT_NAME> <DIR> <LABEL>
This information is inferred by parseNetlist.py.

##canaries.txt
A list of which vertex ID number in nl_PTW.lg has been marked with the 'canary' flag. Created py parseNetlist.py.

##secure.txt
A list of which vertex ID numbers in nl_PTW.lg have been marked with the 'secure' flag. Created py parseNetlist.py.

##ports.txt
A map of the names of ports on the module to the vertices they correspond to in the netlist graph so that we can restore the port names in the final fabric.

##output.log
The full output of all print statements from GraMi. Processed by Swan.java in order to discover the list of locations where the frequent subgraphs can be found. 

##Output.txt
The list of frequent subgraphs found by GraMi, sorted by size and listed in vertex-edge format. Consumed by Swan.java and generate_fsg_verilog.py. 

##fsg_templates.txt
The FSG from GraMi that will be used in the final fabric. The toolchain discards FSGs that contain ports and FSGs that do not show up a sufficient number of times after we have already chosen larger FSGs out of the netlist graph.

##frequentSubgraphs.sv
The SystemVerilog representation of the frequent subgraphs used in the final fabric generated by generate_fsg_verilog.py. The modules follow a standardized format and are named "m<ID>" where <ID> is the ID of FSG assigned by GraMi.

##swan_full_custom.sv
The SystemVerilog of the final SWAN fabric generated by Swan.java. In this example, we use muxes for the crossbars. 

##swan_config.sv
The configuration file used to correctly program the SWAN fabric. This is instantiated in the SWAN .sv and used to get more accurate power and delay estimates. We only generate this file when generating a fabric with muxes since the crossbar modules do not synthesize with configuration bits driving anything. 

