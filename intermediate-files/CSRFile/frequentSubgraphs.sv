module m16 (
	input IN10, IN40, CLK, IN21, 
	output OUT30, OUT10, OUT40, OUT20, OUT60, OUT50
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(OUT60), .C(IN21) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	AND2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
	INVX1 U5 ( .A(OUT40), .Y(OUT50) );
	INVX1 U6 ( .A(OUT30), .Y(OUT60) );
endmodule
module m13 (
	input IN20, IN40, CLK, IN21, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN21) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	AND2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
	INVX1 U5 ( .A(OUT40), .Y(OUT50) );
endmodule
module m58 (
	input IN50, IN11, IN10, CLK, IN30, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	logic CLK;
	AOI22X1 U1 ( .Y(OUT10), .A(OUT30), .C(OUT50), .B(IN10), .D(IN11) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	INVX1 U4 ( .A(OUT30), .Y(OUT40) );
	DFFPOSX1 U5 ( .Q(OUT50), .D(IN50), .CLK(CLK) );
endmodule
module m60 (
	input IN11, IN10, IN40, CLK, IN30, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	logic CLK;
	AOI22X1 U1 ( .Y(OUT10), .A(OUT30), .C(OUT40), .B(IN10), .D(IN11) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	DFFPOSX1 U4 ( .Q(OUT40), .D(IN40), .CLK(CLK) );
	INVX1 U5 ( .A(OUT40), .Y(OUT50) );
endmodule
module m26 (
	input IN20, IN40, CLK, IN30, IN21, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN21) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	AND2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
	INVX1 U5 ( .A(OUT40), .Y(OUT50) );
endmodule
module m18 (
	input IN50, IN10, IN40, CLK, IN21, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(OUT50), .C(IN21) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	AND2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
	INVX1 U5 ( .Y(OUT50), .A(IN50) );
endmodule
module m17 (
	input IN10, IN40, CLK, IN21, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(OUT30), .C(IN21) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	AND2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
	INVX1 U5 ( .A(OUT40), .Y(OUT50) );
endmodule
module m49 (
	input IN10, CLK, IN50, IN50_1, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	AND2X1 U1 ( .Y(OUT10), .A(OUT30), .B(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(OUT50), .CLK(CLK) );
	INVX1 U4 ( .A(OUT30), .Y(OUT40) );
	OAI21X1 U5 ( .C(OUT20), .Y(OUT50), .A(IN50), .B(IN50_1) );
endmodule
module m68 (
	input IN10, IN40, CLK, IN31, IN30, 
	output OUT30, OUT10, OUT40, OUT20, OUT50
);
	logic CLK;
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	AOI22X1 U3 ( .A(OUT10), .C(OUT40), .Y(OUT30), .B(IN30), .D(IN31) );
	DFFPOSX1 U4 ( .Q(OUT40), .D(IN40), .CLK(CLK) );
	INVX1 U5 ( .A(OUT40), .Y(OUT50) );
endmodule
module m14 (
	input IN20, IN40, CLK, IN21, 
	output OUT30, OUT40, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN21) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	AND2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
endmodule
module m63 (
	input IN11, IN10, CLK, IN30, IN10_1, 
	output OUT30, OUT40, OUT20, OUT10
);
	AOI22X1 U1 ( .Y(OUT10), .C(OUT30), .A(IN10), .B(IN10_1), .D(IN11) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	INVX1 U4 ( .A(OUT30), .Y(OUT40) );
endmodule
module m59 (
	input IN11, IN10, CLK, IN11_1, IN30, 
	output OUT30, OUT40, OUT20, OUT10
);
	AOI22X1 U1 ( .Y(OUT10), .A(OUT30), .B(IN10), .C(IN11), .D(IN11_1) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	INVX1 U4 ( .A(OUT30), .Y(OUT40) );
endmodule
module m61 (
	input IN11, IN10, IN40, CLK, IN30, 
	output OUT30, OUT40, OUT20, OUT10
);
	logic CLK;
	AOI22X1 U1 ( .Y(OUT10), .A(OUT30), .C(OUT40), .B(IN10), .D(IN11) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	DFFPOSX1 U4 ( .Q(OUT40), .D(IN40), .CLK(CLK) );
endmodule
module m7 (
	input IN20, IN10, IN40, IN41, IN30, IN21, IN41_1, 
	output OUT30, OUT40, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AOI22X1 U2 ( .A(OUT10), .C(OUT30), .Y(OUT20), .B(IN20), .D(IN21) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
	AOI22X1 U4 ( .A(OUT10), .Y(OUT40), .B(IN40), .C(IN41), .D(IN41_1) );
endmodule
module m22 (
	input IN10, IN30, IN40, 
	output OUT30, OUT40, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .B(OUT30), .C(OUT40), .Y(OUT20) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
	INVX1 U4 ( .Y(OUT40), .A(IN40) );
endmodule
module m1 (
	input IN30_1, IN31, IN30, IN40, 
	output OUT30, OUT40, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	AOI22X1 U3 ( .Y(OUT30), .C(OUT40), .A(IN30), .B(IN30_1), .D(IN31) );
	INVX1 U4 ( .Y(OUT40), .A(IN40) );
endmodule
module m52 (
	input IN10, IN40, CLK, IN41, IN40_1, 
	output OUT30, OUT40, OUT20, OUT10
);
	AND2X1 U1 ( .Y(OUT10), .A(OUT30), .B(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(OUT40), .CLK(CLK) );
	OAI21X1 U4 ( .Y(OUT40), .A(IN40), .B(IN40_1), .C(IN41) );
endmodule
module m50 (
	input IN10, IN30, CLK, 
	output OUT30, OUT40, OUT20, OUT10
);
	AND2X1 U1 ( .Y(OUT10), .A(OUT30), .B(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	INVX1 U4 ( .A(OUT30), .Y(OUT40) );
endmodule
module m51 (
	input IN10, IN30, CLK, IN40, 
	output OUT30, OUT40, OUT20, OUT10
);
	AND2X1 U1 ( .Y(OUT10), .A(OUT30), .B(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	AND2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
endmodule
module m71 (
	input IN10, IN40, CLK, IN31, IN30, 
	output OUT30, OUT40, OUT20, OUT10
);
	logic CLK;
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	AOI22X1 U3 ( .C(OUT10), .A(OUT40), .Y(OUT30), .B(IN30), .D(IN31) );
	DFFPOSX1 U4 ( .Q(OUT40), .D(IN40), .CLK(CLK) );
endmodule
module m69 (
	input IN10, IN40, CLK, IN31, IN30, 
	output OUT30, OUT40, OUT20, OUT10
);
	logic CLK;
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	AOI22X1 U3 ( .A(OUT10), .C(OUT40), .Y(OUT30), .B(IN30), .D(IN31) );
	DFFPOSX1 U4 ( .Q(OUT40), .D(IN40), .CLK(CLK) );
endmodule
module m45 (
	input IN20, CLK, IN22, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	MUX2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m15 (
	input IN20, CLK, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN21) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m62 (
	input IN11, IN10, CLK, IN11_1, IN30, 
	output OUT30, OUT20, OUT10
);
	AOI22X1 U1 ( .Y(OUT10), .A(OUT30), .B(IN10), .C(IN11), .D(IN11_1) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
endmodule
module m56 (
	input IN11, IN10, CLK, IN30, IN10_1, 
	output OUT30, OUT20, OUT10
);
	AOI22X1 U1 ( .Y(OUT10), .C(OUT30), .A(IN10), .B(IN10_1), .D(IN11) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
endmodule
module m9 (
	input IN20, IN21_1, IN10, IN31_1, IN31, IN30, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AOI22X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN21), .D(IN21_1) );
	AOI22X1 U3 ( .A(OUT10), .Y(OUT30), .B(IN30), .C(IN31), .D(IN31_1) );
endmodule
module m40 (
	input IN20, IN10, IN30, IN20_1, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AOI22X1 U2 ( .C(OUT10), .D(OUT30), .Y(OUT20), .A(IN20), .B(IN20_1) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m39 (
	input IN20, IN10, IN20_1, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AOI22X1 U2 ( .C(OUT10), .Y(OUT20), .A(IN20), .B(IN20_1), .D(IN21) );
	INVX1 U3 ( .A(OUT20), .Y(OUT30) );
endmodule
module m8 (
	input IN20, IN10, IN30, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AOI22X1 U2 ( .A(OUT10), .C(OUT30), .Y(OUT20), .B(IN20), .D(IN21) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m41 (
	input IN20, IN20_1, CLK, IN30, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	AOI22X1 U2 ( .C(OUT10), .Y(OUT20), .A(IN20), .B(IN20_1), .D(IN21) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
endmodule
module m25 (
	input IN20, IN10, IN30, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .C(OUT30), .Y(OUT20), .B(IN20) );
	BUFX2 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m24 (
	input IN20, IN10, IN30, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .C(OUT30), .Y(OUT20), .B(IN20) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m23 (
	input IN10, IN30, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .B(OUT30), .Y(OUT20), .C(IN21) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m43 (
	input IN20, IN10, CLK, IN20_1, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .C(OUT10), .Y(OUT20), .A(IN20), .B(IN20_1) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m37 (
	input IN20, IN30, IN30_1, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	NAND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
	AND2X1 U3 ( .Y(OUT30), .A(IN30), .B(IN30_1) );
endmodule
module m31 (
	input IN20, IN10, IN30, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	NAND3X1 U2 ( .A(OUT10), .B(OUT30), .Y(OUT20), .C(IN20) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m2 (
	input IN30_1, IN31, IN30, IN31_1, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	AOI22X1 U3 ( .Y(OUT30), .A(IN30), .B(IN30_1), .C(IN31), .D(IN31_1) );
endmodule
module m0 (
	input IN10, CLK, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m34 (
	input IN10, IN30, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OR2X1 U2 ( .A(OUT10), .B(OUT30), .Y(OUT20) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m33 (
	input IN20, IN10, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OR2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
	INVX1 U3 ( .A(OUT20), .Y(OUT30) );
endmodule
module m5 (
	input IN10, IN30, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X1 U2 ( .A(OUT10), .B(OUT30), .Y(OUT20) );
	INVX1 U3 ( .Y(OUT30), .A(IN30) );
endmodule
module m4 (
	input IN20, IN10, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
	INVX1 U3 ( .A(OUT20), .Y(OUT30) );
endmodule
module m53 (
	input IN10, IN30, CLK, 
	output OUT30, OUT20, OUT10
);
	AND2X1 U1 ( .Y(OUT10), .A(OUT30), .B(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
endmodule
module m65 (
	input IN11, IN10, IN11_1, IN10_1, 
	output OUT20, OUT10
);
	AOI22X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1), .C(IN11), .D(IN11_1) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m57 (
	input IN11, IN10, IN11_1, IN10_1, 
	output OUT20, OUT10
);
	AOI22X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1), .C(IN11), .D(IN11_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m85 (
	input IN10, IN10_1, 
	output OUT20, OUT10
);
	AND2X2 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m84 (
	input IN11, IN10, CLK, IN10_1, 
	output OUT20, OUT10
);
	OAI21X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1), .C(IN11) );
	DFFPOSX1 U2 ( .D(OUT10), .Q(OUT20), .CLK(CLK) );
endmodule
module m83 (
	input IN20, IN10, IN20_1, 
	output OUT20, OUT10
);
	BUFX2 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .C(OUT10), .Y(OUT20), .A(IN20), .B(IN20_1) );
endmodule
module m82 (
	input IN20, IN10, IN20_1, 
	output OUT20, OUT10
);
	BUFX2 U1 ( .Y(OUT10), .A(IN10) );
	NAND3X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN20_1) );
endmodule
module m48 (
	input IN20, IN10, IN22, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	MUX2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .S(IN22) );
endmodule
module m30 (
	input IN20, IN10, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X2 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m44 (
	input IN20, IN10, IN20_1, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .C(OUT10), .Y(OUT20), .A(IN20), .B(IN20_1) );
endmodule
module m29 (
	input IN20, IN10, IN21, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OAI21X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN21) );
endmodule
module m38 (
	input IN20, IN10, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	NAND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m32 (
	input IN20, IN10, IN20_1, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	NAND3X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .C(IN20_1) );
endmodule
module m3 (
	input IN10, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m35 (
	input IN20, IN10, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	OR2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m6 (
	input IN20, IN10, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	AND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m12 (
	input IN10, CLK, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	DFFPOSX1 U2 ( .D(OUT10), .Q(OUT20), .CLK(CLK) );
endmodule
module m86 (
	input IN10, IN10_1, 
	output OUT20, OUT10
);
	OR2X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m54 (
	input IN10, IN10_1, 
	output OUT20, OUT10
);
	AND2X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m55 (
	input IN20, IN10, IN10_1, 
	output OUT20, OUT10
);
	AND2X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	AND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
module m81 (
	input IN20, IN20_1, IN10, CLK, IN21, 
	output OUT20, OUT10
);
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	AOI22X1 U2 ( .C(OUT10), .Y(OUT20), .A(IN20), .B(IN20_1), .D(IN21) );
endmodule
module m75 (
	input IN10, CLK, 
	output OUT20, OUT10
);
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m78 (
	input IN20, IN10, CLK, 
	output OUT20, OUT10
);
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	AND2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
