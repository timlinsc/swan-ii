module m0 (
	input IN40, IN20, IN22, CLK, 
	output OUT10, OUT30, OUT40, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	MUX2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	XNOR2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
endmodule
module m2 (
	input IN40, CLK, IN20, IN22, IN10, 
	output OUT10, OUT30, OUT40, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	MUX2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	XNOR2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
endmodule
module m1 (
	input IN20, IN22, CLK, 
	output OUT10, OUT30, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	MUX2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m3 (
	input CLK, IN20, IN22, IN10, 
	output OUT10, OUT30, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	MUX2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m9 (
	input IN32, IN31, CLK, 
	output OUT10, OUT30, OUT20
);
	DFFPOSX1 U1 ( .Q(OUT10), .D(OUT30), .CLK(CLK) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
	MUX2X1 U3 ( .B(OUT20), .Y(OUT30), .A(IN31), .S(IN32) );
endmodule
module m15 (
	input IN11_1, IN11, IN10_1, IN10, 
	output OUT10, OUT20
);
	AOI22X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1), .C(IN11), .D(IN11_1) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m6 (
	input IN20, IN22, IN10, 
	output OUT10, OUT20
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	MUX2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20), .S(IN22) );
endmodule
module m14 (
	input IN10_1, IN10, 
	output OUT10, OUT20
);
	AND2X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m10 (
	input CLK, IN10, 
	output OUT10, OUT20
);
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m12 (
	input CLK, IN20, IN10, 
	output OUT10, OUT20
);
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	XNOR2X1 U2 ( .A(OUT10), .Y(OUT20), .B(IN20) );
endmodule
