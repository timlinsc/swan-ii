/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Ultra(TM) in wire load mode
// Version   : L-2016.03-SP2
// Date      : Fri Nov 23 15:12:45 2018
/////////////////////////////////////////////////////////////


module Frontend_frontend ( clock, reset, io_mem_0_a_ready, io_mem_0_a_valid, 
        io_mem_0_a_bits_opcode, io_mem_0_a_bits_param, io_mem_0_a_bits_size, 
        io_mem_0_a_bits_source, io_mem_0_a_bits_address, io_mem_0_a_bits_mask, 
        io_mem_0_a_bits_data, io_mem_0_b_ready, io_mem_0_b_valid, 
        io_mem_0_b_bits_opcode, io_mem_0_b_bits_param, io_mem_0_b_bits_size, 
        io_mem_0_b_bits_source, io_mem_0_b_bits_address, io_mem_0_b_bits_mask, 
        io_mem_0_b_bits_data, io_mem_0_c_ready, io_mem_0_c_valid, 
        io_mem_0_c_bits_opcode, io_mem_0_c_bits_param, io_mem_0_c_bits_size, 
        io_mem_0_c_bits_source, io_mem_0_c_bits_address, io_mem_0_c_bits_data, 
        io_mem_0_c_bits_error, io_mem_0_d_ready, io_mem_0_d_valid, 
        io_mem_0_d_bits_opcode, io_mem_0_d_bits_param, io_mem_0_d_bits_size, 
        io_mem_0_d_bits_source, io_mem_0_d_bits_sink, io_mem_0_d_bits_addr_lo, 
        io_mem_0_d_bits_data, io_mem_0_d_bits_error, io_mem_0_e_ready, 
        io_mem_0_e_valid, io_mem_0_e_bits_sink, io_cpu_req_valid, 
        io_cpu_req_bits_pc, io_cpu_req_bits_speculative, io_cpu_resp_ready, 
        io_cpu_resp_valid, io_cpu_resp_bits_btb_valid, 
        io_cpu_resp_bits_btb_bits_taken, io_cpu_resp_bits_btb_bits_mask, 
        io_cpu_resp_bits_btb_bits_bridx, io_cpu_resp_bits_btb_bits_target, 
        io_cpu_resp_bits_btb_bits_entry, io_cpu_resp_bits_btb_bits_bht_history, 
        io_cpu_resp_bits_btb_bits_bht_value, io_cpu_resp_bits_pc, 
        io_cpu_resp_bits_data, io_cpu_resp_bits_mask, io_cpu_resp_bits_xcpt_if, 
        io_cpu_resp_bits_replay, io_cpu_btb_update_valid, 
        io_cpu_btb_update_bits_prediction_valid, 
        io_cpu_btb_update_bits_prediction_bits_taken, 
        io_cpu_btb_update_bits_prediction_bits_mask, 
        io_cpu_btb_update_bits_prediction_bits_bridx, 
        io_cpu_btb_update_bits_prediction_bits_target, 
        io_cpu_btb_update_bits_prediction_bits_entry, 
        io_cpu_btb_update_bits_prediction_bits_bht_history, 
        io_cpu_btb_update_bits_prediction_bits_bht_value, 
        io_cpu_btb_update_bits_pc, io_cpu_btb_update_bits_target, 
        io_cpu_btb_update_bits_taken, io_cpu_btb_update_bits_isValid, 
        io_cpu_btb_update_bits_isJump, io_cpu_btb_update_bits_isReturn, 
        io_cpu_btb_update_bits_br_pc, io_cpu_bht_update_valid, 
        io_cpu_bht_update_bits_prediction_valid, 
        io_cpu_bht_update_bits_prediction_bits_taken, 
        io_cpu_bht_update_bits_prediction_bits_mask, 
        io_cpu_bht_update_bits_prediction_bits_bridx, 
        io_cpu_bht_update_bits_prediction_bits_target, 
        io_cpu_bht_update_bits_prediction_bits_entry, 
        io_cpu_bht_update_bits_prediction_bits_bht_history, 
        io_cpu_bht_update_bits_prediction_bits_bht_value, 
        io_cpu_bht_update_bits_pc, io_cpu_bht_update_bits_taken, 
        io_cpu_bht_update_bits_mispredict, io_cpu_ras_update_valid, 
        io_cpu_ras_update_bits_isCall, io_cpu_ras_update_bits_isReturn, 
        io_cpu_ras_update_bits_returnAddr, 
        io_cpu_ras_update_bits_prediction_valid, 
        io_cpu_ras_update_bits_prediction_bits_taken, 
        io_cpu_ras_update_bits_prediction_bits_mask, 
        io_cpu_ras_update_bits_prediction_bits_bridx, 
        io_cpu_ras_update_bits_prediction_bits_target, 
        io_cpu_ras_update_bits_prediction_bits_entry, 
        io_cpu_ras_update_bits_prediction_bits_bht_history, 
        io_cpu_ras_update_bits_prediction_bits_bht_value, io_cpu_flush_icache, 
        io_cpu_flush_tlb, io_cpu_npc, io_ptw_req_ready, io_ptw_req_valid, 
        io_ptw_req_bits_prv, io_ptw_req_bits_pum, io_ptw_req_bits_mxr, 
        io_ptw_req_bits_addr, io_ptw_req_bits_store, io_ptw_req_bits_fetch, 
        io_ptw_resp_valid, io_ptw_resp_bits_pte_reserved_for_hardware, 
        io_ptw_resp_bits_pte_ppn, io_ptw_resp_bits_pte_reserved_for_software, 
        io_ptw_resp_bits_pte_d, io_ptw_resp_bits_pte_a, io_ptw_resp_bits_pte_g, 
        io_ptw_resp_bits_pte_u, io_ptw_resp_bits_pte_x, io_ptw_resp_bits_pte_w, 
        io_ptw_resp_bits_pte_r, io_ptw_resp_bits_pte_v, io_ptw_ptbr_asid, 
        io_ptw_ptbr_ppn, io_ptw_invalidate, io_ptw_status_debug, 
        io_ptw_status_isa, io_ptw_status_prv, io_ptw_status_sd, 
        io_ptw_status_zero3, io_ptw_status_sd_rv32, io_ptw_status_zero2, 
        io_ptw_status_vm, io_ptw_status_zero1, io_ptw_status_mxr, 
        io_ptw_status_pum, io_ptw_status_mprv, io_ptw_status_xs, 
        io_ptw_status_fs, io_ptw_status_mpp, io_ptw_status_hpp, 
        io_ptw_status_spp, io_ptw_status_mpie, io_ptw_status_hpie, 
        io_ptw_status_spie, io_ptw_status_upie, io_ptw_status_mie, 
        io_ptw_status_hie, io_ptw_status_sie, io_ptw_status_uie, 
        io_resetVector, icache_clock, icache_reset, icache_io_req_valid, 
        icache_io_req_bits_addr, icache_io_s1_ppn, icache_io_s1_kill, 
        icache_io_s2_kill, icache_io_resp_ready, icache_io_resp_valid, 
        icache_io_resp_bits_data, icache_io_resp_bits_datablock, 
        icache_io_invalidate, icache_io_mem_0_a_ready, icache_io_mem_0_a_valid, 
        icache_io_mem_0_a_bits_opcode, icache_io_mem_0_a_bits_param, 
        icache_io_mem_0_a_bits_size, icache_io_mem_0_a_bits_source, 
        icache_io_mem_0_a_bits_address, icache_io_mem_0_a_bits_mask, 
        icache_io_mem_0_a_bits_data, icache_io_mem_0_b_ready, 
        icache_io_mem_0_b_valid, icache_io_mem_0_b_bits_opcode, 
        icache_io_mem_0_b_bits_param, icache_io_mem_0_b_bits_size, 
        icache_io_mem_0_b_bits_source, icache_io_mem_0_b_bits_address, 
        icache_io_mem_0_b_bits_mask, icache_io_mem_0_b_bits_data, 
        icache_io_mem_0_c_ready, icache_io_mem_0_c_valid, 
        icache_io_mem_0_c_bits_opcode, icache_io_mem_0_c_bits_param, 
        icache_io_mem_0_c_bits_size, icache_io_mem_0_c_bits_source, 
        icache_io_mem_0_c_bits_address, icache_io_mem_0_c_bits_data, 
        icache_io_mem_0_c_bits_error, icache_io_mem_0_d_ready, 
        icache_io_mem_0_d_valid, icache_io_mem_0_d_bits_opcode, 
        icache_io_mem_0_d_bits_param, icache_io_mem_0_d_bits_size, 
        icache_io_mem_0_d_bits_source, icache_io_mem_0_d_bits_sink, 
        icache_io_mem_0_d_bits_addr_lo, icache_io_mem_0_d_bits_data, 
        icache_io_mem_0_d_bits_error, icache_io_mem_0_e_ready, 
        icache_io_mem_0_e_valid, icache_io_mem_0_e_bits_sink, tlb_clock, 
        tlb_reset, tlb_io_req_ready, tlb_io_req_valid, tlb_io_req_bits_vpn, 
        tlb_io_req_bits_passthrough, tlb_io_req_bits_instruction, 
        tlb_io_req_bits_store, tlb_io_resp_miss, tlb_io_resp_ppn, 
        tlb_io_resp_xcpt_ld, tlb_io_resp_xcpt_st, tlb_io_resp_xcpt_if, 
        tlb_io_resp_cacheable, tlb_io_ptw_req_ready, tlb_io_ptw_req_valid, 
        tlb_io_ptw_req_bits_prv, tlb_io_ptw_req_bits_pum, 
        tlb_io_ptw_req_bits_mxr, tlb_io_ptw_req_bits_addr, 
        tlb_io_ptw_req_bits_store, tlb_io_ptw_req_bits_fetch, 
        tlb_io_ptw_resp_valid, tlb_io_ptw_resp_bits_pte_reserved_for_hardware, 
        tlb_io_ptw_resp_bits_pte_ppn, 
        tlb_io_ptw_resp_bits_pte_reserved_for_software, 
        tlb_io_ptw_resp_bits_pte_d, tlb_io_ptw_resp_bits_pte_a, 
        tlb_io_ptw_resp_bits_pte_g, tlb_io_ptw_resp_bits_pte_u, 
        tlb_io_ptw_resp_bits_pte_x, tlb_io_ptw_resp_bits_pte_w, 
        tlb_io_ptw_resp_bits_pte_r, tlb_io_ptw_resp_bits_pte_v, 
        tlb_io_ptw_ptbr_asid, tlb_io_ptw_ptbr_ppn, tlb_io_ptw_invalidate, 
        tlb_io_ptw_status_debug, tlb_io_ptw_status_isa, tlb_io_ptw_status_prv, 
        tlb_io_ptw_status_sd, tlb_io_ptw_status_zero3, 
        tlb_io_ptw_status_sd_rv32, tlb_io_ptw_status_zero2, 
        tlb_io_ptw_status_vm, tlb_io_ptw_status_zero1, tlb_io_ptw_status_mxr, 
        tlb_io_ptw_status_pum, tlb_io_ptw_status_mprv, tlb_io_ptw_status_xs, 
        tlb_io_ptw_status_fs, tlb_io_ptw_status_mpp, tlb_io_ptw_status_hpp, 
        tlb_io_ptw_status_spp, tlb_io_ptw_status_mpie, tlb_io_ptw_status_hpie, 
        tlb_io_ptw_status_spie, tlb_io_ptw_status_upie, tlb_io_ptw_status_mie, 
        tlb_io_ptw_status_hie, tlb_io_ptw_status_sie, tlb_io_ptw_status_uie, 
        BTB_clock, BTB_reset, BTB_io_req_valid, BTB_io_req_bits_addr, 
        BTB_io_resp_valid, BTB_io_resp_bits_taken, BTB_io_resp_bits_mask, 
        BTB_io_resp_bits_bridx, BTB_io_resp_bits_target, 
        BTB_io_resp_bits_entry, BTB_io_resp_bits_bht_history, 
        BTB_io_resp_bits_bht_value, BTB_io_btb_update_valid, 
        BTB_io_btb_update_bits_prediction_valid, 
        BTB_io_btb_update_bits_prediction_bits_taken, 
        BTB_io_btb_update_bits_prediction_bits_mask, 
        BTB_io_btb_update_bits_prediction_bits_bridx, 
        BTB_io_btb_update_bits_prediction_bits_target, 
        BTB_io_btb_update_bits_prediction_bits_entry, 
        BTB_io_btb_update_bits_prediction_bits_bht_history, 
        BTB_io_btb_update_bits_prediction_bits_bht_value, 
        BTB_io_btb_update_bits_pc, BTB_io_btb_update_bits_target, 
        BTB_io_btb_update_bits_taken, BTB_io_btb_update_bits_isValid, 
        BTB_io_btb_update_bits_isJump, BTB_io_btb_update_bits_isReturn, 
        BTB_io_btb_update_bits_br_pc, BTB_io_bht_update_valid, 
        BTB_io_bht_update_bits_prediction_valid, 
        BTB_io_bht_update_bits_prediction_bits_taken, 
        BTB_io_bht_update_bits_prediction_bits_mask, 
        BTB_io_bht_update_bits_prediction_bits_bridx, 
        BTB_io_bht_update_bits_prediction_bits_target, 
        BTB_io_bht_update_bits_prediction_bits_entry, 
        BTB_io_bht_update_bits_prediction_bits_bht_history, 
        BTB_io_bht_update_bits_prediction_bits_bht_value, 
        BTB_io_bht_update_bits_pc, BTB_io_bht_update_bits_taken, 
        BTB_io_bht_update_bits_mispredict, BTB_io_ras_update_valid, 
        BTB_io_ras_update_bits_isCall, BTB_io_ras_update_bits_isReturn, 
        BTB_io_ras_update_bits_returnAddr, 
        BTB_io_ras_update_bits_prediction_valid, 
        BTB_io_ras_update_bits_prediction_bits_taken, 
        BTB_io_ras_update_bits_prediction_bits_mask, 
        BTB_io_ras_update_bits_prediction_bits_bridx, 
        BTB_io_ras_update_bits_prediction_bits_target, 
        BTB_io_ras_update_bits_prediction_bits_entry, 
        BTB_io_ras_update_bits_prediction_bits_bht_history, 
        BTB_io_ras_update_bits_prediction_bits_bht_value, canary_in, alarm );
  output [2:0] io_mem_0_a_bits_opcode;
  output [2:0] io_mem_0_a_bits_param;
  output [3:0] io_mem_0_a_bits_size;
  output [31:0] io_mem_0_a_bits_address;
  output [7:0] io_mem_0_a_bits_mask;
  output [63:0] io_mem_0_a_bits_data;
  input [2:0] io_mem_0_b_bits_opcode;
  input [1:0] io_mem_0_b_bits_param;
  input [3:0] io_mem_0_b_bits_size;
  input [31:0] io_mem_0_b_bits_address;
  input [7:0] io_mem_0_b_bits_mask;
  input [63:0] io_mem_0_b_bits_data;
  output [2:0] io_mem_0_c_bits_opcode;
  output [2:0] io_mem_0_c_bits_param;
  output [3:0] io_mem_0_c_bits_size;
  output [31:0] io_mem_0_c_bits_address;
  output [63:0] io_mem_0_c_bits_data;
  input [2:0] io_mem_0_d_bits_opcode;
  input [1:0] io_mem_0_d_bits_param;
  input [3:0] io_mem_0_d_bits_size;
  input [3:0] io_mem_0_d_bits_sink;
  input [2:0] io_mem_0_d_bits_addr_lo;
  input [63:0] io_mem_0_d_bits_data;
  output [3:0] io_mem_0_e_bits_sink;
  input [39:0] io_cpu_req_bits_pc;
  output [38:0] io_cpu_resp_bits_btb_bits_target;
  output [5:0] io_cpu_resp_bits_btb_bits_entry;
  output [6:0] io_cpu_resp_bits_btb_bits_bht_history;
  output [1:0] io_cpu_resp_bits_btb_bits_bht_value;
  output [39:0] io_cpu_resp_bits_pc;
  output [31:0] io_cpu_resp_bits_data;
  input [38:0] io_cpu_btb_update_bits_prediction_bits_target;
  input [5:0] io_cpu_btb_update_bits_prediction_bits_entry;
  input [6:0] io_cpu_btb_update_bits_prediction_bits_bht_history;
  input [1:0] io_cpu_btb_update_bits_prediction_bits_bht_value;
  input [38:0] io_cpu_btb_update_bits_pc;
  input [38:0] io_cpu_btb_update_bits_target;
  input [38:0] io_cpu_btb_update_bits_br_pc;
  input [38:0] io_cpu_bht_update_bits_prediction_bits_target;
  input [5:0] io_cpu_bht_update_bits_prediction_bits_entry;
  input [6:0] io_cpu_bht_update_bits_prediction_bits_bht_history;
  input [1:0] io_cpu_bht_update_bits_prediction_bits_bht_value;
  input [38:0] io_cpu_bht_update_bits_pc;
  input [38:0] io_cpu_ras_update_bits_returnAddr;
  input [38:0] io_cpu_ras_update_bits_prediction_bits_target;
  input [5:0] io_cpu_ras_update_bits_prediction_bits_entry;
  input [6:0] io_cpu_ras_update_bits_prediction_bits_bht_history;
  input [1:0] io_cpu_ras_update_bits_prediction_bits_bht_value;
  output [39:0] io_cpu_npc;
  output [1:0] io_ptw_req_bits_prv;
  output [26:0] io_ptw_req_bits_addr;
  input [15:0] io_ptw_resp_bits_pte_reserved_for_hardware;
  input [37:0] io_ptw_resp_bits_pte_ppn;
  input [1:0] io_ptw_resp_bits_pte_reserved_for_software;
  input [6:0] io_ptw_ptbr_asid;
  input [37:0] io_ptw_ptbr_ppn;
  input [31:0] io_ptw_status_isa;
  input [1:0] io_ptw_status_prv;
  input [30:0] io_ptw_status_zero3;
  input [1:0] io_ptw_status_zero2;
  input [4:0] io_ptw_status_vm;
  input [3:0] io_ptw_status_zero1;
  input [1:0] io_ptw_status_xs;
  input [1:0] io_ptw_status_fs;
  input [1:0] io_ptw_status_mpp;
  input [1:0] io_ptw_status_hpp;
  input [39:0] io_resetVector;
  output [38:0] icache_io_req_bits_addr;
  output [19:0] icache_io_s1_ppn;
  input [31:0] icache_io_resp_bits_data;
  input [63:0] icache_io_resp_bits_datablock;
  input [2:0] icache_io_mem_0_a_bits_opcode;
  input [2:0] icache_io_mem_0_a_bits_param;
  input [3:0] icache_io_mem_0_a_bits_size;
  input [31:0] icache_io_mem_0_a_bits_address;
  input [7:0] icache_io_mem_0_a_bits_mask;
  input [63:0] icache_io_mem_0_a_bits_data;
  output [2:0] icache_io_mem_0_b_bits_opcode;
  output [1:0] icache_io_mem_0_b_bits_param;
  output [3:0] icache_io_mem_0_b_bits_size;
  output [31:0] icache_io_mem_0_b_bits_address;
  output [7:0] icache_io_mem_0_b_bits_mask;
  output [63:0] icache_io_mem_0_b_bits_data;
  input [2:0] icache_io_mem_0_c_bits_opcode;
  input [2:0] icache_io_mem_0_c_bits_param;
  input [3:0] icache_io_mem_0_c_bits_size;
  input [31:0] icache_io_mem_0_c_bits_address;
  input [63:0] icache_io_mem_0_c_bits_data;
  output [2:0] icache_io_mem_0_d_bits_opcode;
  output [1:0] icache_io_mem_0_d_bits_param;
  output [3:0] icache_io_mem_0_d_bits_size;
  output [3:0] icache_io_mem_0_d_bits_sink;
  output [2:0] icache_io_mem_0_d_bits_addr_lo;
  output [63:0] icache_io_mem_0_d_bits_data;
  input [3:0] icache_io_mem_0_e_bits_sink;
  output [27:0] tlb_io_req_bits_vpn;
  input [19:0] tlb_io_resp_ppn;
  input [1:0] tlb_io_ptw_req_bits_prv;
  input [26:0] tlb_io_ptw_req_bits_addr;
  output [15:0] tlb_io_ptw_resp_bits_pte_reserved_for_hardware;
  output [37:0] tlb_io_ptw_resp_bits_pte_ppn;
  output [1:0] tlb_io_ptw_resp_bits_pte_reserved_for_software;
  output [6:0] tlb_io_ptw_ptbr_asid;
  output [37:0] tlb_io_ptw_ptbr_ppn;
  output [31:0] tlb_io_ptw_status_isa;
  output [1:0] tlb_io_ptw_status_prv;
  output [30:0] tlb_io_ptw_status_zero3;
  output [1:0] tlb_io_ptw_status_zero2;
  output [4:0] tlb_io_ptw_status_vm;
  output [3:0] tlb_io_ptw_status_zero1;
  output [1:0] tlb_io_ptw_status_xs;
  output [1:0] tlb_io_ptw_status_fs;
  output [1:0] tlb_io_ptw_status_mpp;
  output [1:0] tlb_io_ptw_status_hpp;
  output [38:0] BTB_io_req_bits_addr;
  input [38:0] BTB_io_resp_bits_target;
  input [5:0] BTB_io_resp_bits_entry;
  input [6:0] BTB_io_resp_bits_bht_history;
  input [1:0] BTB_io_resp_bits_bht_value;
  output [38:0] BTB_io_btb_update_bits_prediction_bits_target;
  output [5:0] BTB_io_btb_update_bits_prediction_bits_entry;
  output [6:0] BTB_io_btb_update_bits_prediction_bits_bht_history;
  output [1:0] BTB_io_btb_update_bits_prediction_bits_bht_value;
  output [38:0] BTB_io_btb_update_bits_pc;
  output [38:0] BTB_io_btb_update_bits_target;
  output [38:0] BTB_io_btb_update_bits_br_pc;
  output [38:0] BTB_io_bht_update_bits_prediction_bits_target;
  output [5:0] BTB_io_bht_update_bits_prediction_bits_entry;
  output [6:0] BTB_io_bht_update_bits_prediction_bits_bht_history;
  output [1:0] BTB_io_bht_update_bits_prediction_bits_bht_value;
  output [38:0] BTB_io_bht_update_bits_pc;
  output [38:0] BTB_io_ras_update_bits_returnAddr;
  output [38:0] BTB_io_ras_update_bits_prediction_bits_target;
  output [5:0] BTB_io_ras_update_bits_prediction_bits_entry;
  output [6:0] BTB_io_ras_update_bits_prediction_bits_bht_history;
  output [1:0] BTB_io_ras_update_bits_prediction_bits_bht_value;
  input clock, reset, io_mem_0_a_ready, io_mem_0_b_valid,
         io_mem_0_b_bits_source, io_mem_0_c_ready, io_mem_0_d_valid,
         io_mem_0_d_bits_source, io_mem_0_d_bits_error, io_mem_0_e_ready,
         io_cpu_req_valid, io_cpu_req_bits_speculative, io_cpu_resp_ready,
         io_cpu_btb_update_valid, io_cpu_btb_update_bits_prediction_valid,
         io_cpu_btb_update_bits_prediction_bits_taken,
         io_cpu_btb_update_bits_prediction_bits_mask,
         io_cpu_btb_update_bits_prediction_bits_bridx,
         io_cpu_btb_update_bits_taken, io_cpu_btb_update_bits_isValid,
         io_cpu_btb_update_bits_isJump, io_cpu_btb_update_bits_isReturn,
         io_cpu_bht_update_valid, io_cpu_bht_update_bits_prediction_valid,
         io_cpu_bht_update_bits_prediction_bits_taken,
         io_cpu_bht_update_bits_prediction_bits_mask,
         io_cpu_bht_update_bits_prediction_bits_bridx,
         io_cpu_bht_update_bits_taken, io_cpu_bht_update_bits_mispredict,
         io_cpu_ras_update_valid, io_cpu_ras_update_bits_isCall,
         io_cpu_ras_update_bits_isReturn,
         io_cpu_ras_update_bits_prediction_valid,
         io_cpu_ras_update_bits_prediction_bits_taken,
         io_cpu_ras_update_bits_prediction_bits_mask,
         io_cpu_ras_update_bits_prediction_bits_bridx, io_cpu_flush_icache,
         io_cpu_flush_tlb, io_ptw_req_ready, io_ptw_resp_valid,
         io_ptw_resp_bits_pte_d, io_ptw_resp_bits_pte_a,
         io_ptw_resp_bits_pte_g, io_ptw_resp_bits_pte_u,
         io_ptw_resp_bits_pte_x, io_ptw_resp_bits_pte_w,
         io_ptw_resp_bits_pte_r, io_ptw_resp_bits_pte_v, io_ptw_invalidate,
         io_ptw_status_debug, io_ptw_status_sd, io_ptw_status_sd_rv32,
         io_ptw_status_mxr, io_ptw_status_pum, io_ptw_status_mprv,
         io_ptw_status_spp, io_ptw_status_mpie, io_ptw_status_hpie,
         io_ptw_status_spie, io_ptw_status_upie, io_ptw_status_mie,
         io_ptw_status_hie, io_ptw_status_sie, io_ptw_status_uie,
         icache_io_resp_valid, icache_io_mem_0_a_valid,
         icache_io_mem_0_a_bits_source, icache_io_mem_0_b_ready,
         icache_io_mem_0_c_valid, icache_io_mem_0_c_bits_source,
         icache_io_mem_0_c_bits_error, icache_io_mem_0_d_ready,
         icache_io_mem_0_e_valid, tlb_io_req_ready, tlb_io_resp_miss,
         tlb_io_resp_xcpt_ld, tlb_io_resp_xcpt_st, tlb_io_resp_xcpt_if,
         tlb_io_resp_cacheable, tlb_io_ptw_req_valid, tlb_io_ptw_req_bits_pum,
         tlb_io_ptw_req_bits_mxr, tlb_io_ptw_req_bits_store,
         tlb_io_ptw_req_bits_fetch, BTB_io_resp_valid, BTB_io_resp_bits_taken,
         BTB_io_resp_bits_mask, BTB_io_resp_bits_bridx, canary_in;
  output io_mem_0_a_valid, io_mem_0_a_bits_source, io_mem_0_b_ready,
         io_mem_0_c_valid, io_mem_0_c_bits_source, io_mem_0_c_bits_error,
         io_mem_0_d_ready, io_mem_0_e_valid, io_cpu_resp_valid,
         io_cpu_resp_bits_btb_valid, io_cpu_resp_bits_btb_bits_taken,
         io_cpu_resp_bits_btb_bits_mask, io_cpu_resp_bits_btb_bits_bridx,
         io_cpu_resp_bits_mask, io_cpu_resp_bits_xcpt_if,
         io_cpu_resp_bits_replay, io_ptw_req_valid, io_ptw_req_bits_pum,
         io_ptw_req_bits_mxr, io_ptw_req_bits_store, io_ptw_req_bits_fetch,
         icache_clock, icache_reset, icache_io_req_valid, icache_io_s1_kill,
         icache_io_s2_kill, icache_io_resp_ready, icache_io_invalidate,
         icache_io_mem_0_a_ready, icache_io_mem_0_b_valid,
         icache_io_mem_0_b_bits_source, icache_io_mem_0_c_ready,
         icache_io_mem_0_d_valid, icache_io_mem_0_d_bits_source,
         icache_io_mem_0_d_bits_error, icache_io_mem_0_e_ready, tlb_clock,
         tlb_reset, tlb_io_req_valid, tlb_io_req_bits_passthrough,
         tlb_io_req_bits_instruction, tlb_io_req_bits_store,
         tlb_io_ptw_req_ready, tlb_io_ptw_resp_valid,
         tlb_io_ptw_resp_bits_pte_d, tlb_io_ptw_resp_bits_pte_a,
         tlb_io_ptw_resp_bits_pte_g, tlb_io_ptw_resp_bits_pte_u,
         tlb_io_ptw_resp_bits_pte_x, tlb_io_ptw_resp_bits_pte_w,
         tlb_io_ptw_resp_bits_pte_r, tlb_io_ptw_resp_bits_pte_v,
         tlb_io_ptw_invalidate, tlb_io_ptw_status_debug, tlb_io_ptw_status_sd,
         tlb_io_ptw_status_sd_rv32, tlb_io_ptw_status_mxr,
         tlb_io_ptw_status_pum, tlb_io_ptw_status_mprv, tlb_io_ptw_status_spp,
         tlb_io_ptw_status_mpie, tlb_io_ptw_status_hpie,
         tlb_io_ptw_status_spie, tlb_io_ptw_status_upie, tlb_io_ptw_status_mie,
         tlb_io_ptw_status_hie, tlb_io_ptw_status_sie, tlb_io_ptw_status_uie,
         BTB_clock, BTB_reset, BTB_io_req_valid, BTB_io_btb_update_valid,
         BTB_io_btb_update_bits_prediction_valid,
         BTB_io_btb_update_bits_prediction_bits_taken,
         BTB_io_btb_update_bits_prediction_bits_mask,
         BTB_io_btb_update_bits_prediction_bits_bridx,
         BTB_io_btb_update_bits_taken, BTB_io_btb_update_bits_isValid,
         BTB_io_btb_update_bits_isJump, BTB_io_btb_update_bits_isReturn,
         BTB_io_bht_update_valid, BTB_io_bht_update_bits_prediction_valid,
         BTB_io_bht_update_bits_prediction_bits_taken,
         BTB_io_bht_update_bits_prediction_bits_mask,
         BTB_io_bht_update_bits_prediction_bits_bridx,
         BTB_io_bht_update_bits_taken, BTB_io_bht_update_bits_mispredict,
         BTB_io_ras_update_valid, BTB_io_ras_update_bits_isCall,
         BTB_io_ras_update_bits_isReturn,
         BTB_io_ras_update_bits_prediction_valid,
         BTB_io_ras_update_bits_prediction_bits_taken,
         BTB_io_ras_update_bits_prediction_bits_mask,
         BTB_io_ras_update_bits_prediction_bits_bridx, alarm;
  wire   clock, reset, io_mem_0_a_ready, io_mem_0_b_valid,
         io_mem_0_b_bits_source, io_mem_0_c_ready, io_mem_0_d_valid,
         io_mem_0_d_bits_source, io_mem_0_d_bits_error, io_mem_0_e_ready,
         n2396, n2397, n2398, io_cpu_btb_update_valid,
         io_cpu_btb_update_bits_prediction_valid,
         io_cpu_btb_update_bits_prediction_bits_taken,
         io_cpu_btb_update_bits_prediction_bits_mask,
         io_cpu_btb_update_bits_prediction_bits_bridx,
         io_cpu_btb_update_bits_taken, io_cpu_btb_update_bits_isValid,
         io_cpu_btb_update_bits_isJump, io_cpu_btb_update_bits_isReturn,
         io_cpu_bht_update_valid, io_cpu_bht_update_bits_prediction_valid,
         io_cpu_bht_update_bits_prediction_bits_taken,
         io_cpu_bht_update_bits_prediction_bits_mask,
         io_cpu_bht_update_bits_prediction_bits_bridx,
         io_cpu_bht_update_bits_taken, io_cpu_bht_update_bits_mispredict,
         io_cpu_ras_update_valid, io_cpu_ras_update_bits_isCall,
         io_cpu_ras_update_bits_isReturn,
         io_cpu_ras_update_bits_prediction_valid,
         io_cpu_ras_update_bits_prediction_bits_taken,
         io_cpu_ras_update_bits_prediction_bits_mask,
         io_cpu_ras_update_bits_prediction_bits_bridx, io_cpu_flush_icache,
         io_cpu_npc_26, io_cpu_npc_24, io_cpu_npc_16, io_cpu_npc_1,
         io_cpu_npc_0, n2399, io_ptw_req_ready, io_ptw_resp_valid,
         io_ptw_resp_bits_pte_d, io_ptw_resp_bits_pte_a,
         io_ptw_resp_bits_pte_g, io_ptw_resp_bits_pte_u,
         io_ptw_resp_bits_pte_x, io_ptw_resp_bits_pte_w,
         io_ptw_resp_bits_pte_r, io_ptw_resp_bits_pte_v, io_ptw_invalidate,
         io_ptw_status_debug, io_ptw_status_sd, io_ptw_status_sd_rv32,
         io_ptw_status_mxr, io_ptw_status_pum, io_ptw_status_mprv,
         io_ptw_status_spp, io_ptw_status_mpie, io_ptw_status_hpie,
         io_ptw_status_spie, io_ptw_status_upie, io_ptw_status_mie,
         io_ptw_status_hie, io_ptw_status_sie, io_ptw_status_uie, n2402, n2403,
         icache_io_mem_0_a_valid, icache_io_mem_0_a_bits_source,
         icache_io_mem_0_b_ready, icache_io_mem_0_c_valid,
         icache_io_mem_0_c_bits_source, icache_io_mem_0_c_bits_error,
         icache_io_mem_0_d_ready, icache_io_mem_0_e_valid,
         tlb_io_ptw_req_valid, tlb_io_ptw_req_bits_pum,
         tlb_io_ptw_req_bits_mxr, tlb_io_ptw_req_bits_store,
         tlb_io_ptw_req_bits_fetch, BTB_io_req_bits_addr_32,
         BTB_io_req_bits_addr_31, BTB_io_req_bits_addr_30,
         BTB_io_req_bits_addr_26, BTB_io_req_bits_addr_25,
         BTB_io_req_bits_addr_24, BTB_io_req_bits_addr_23,
         BTB_io_req_bits_addr_22, BTB_io_req_bits_addr_21,
         BTB_io_req_bits_addr_20, BTB_io_req_bits_addr_19,
         BTB_io_req_bits_addr_18, BTB_io_req_bits_addr_17,
         BTB_io_req_bits_addr_15, BTB_io_req_bits_addr_12, n2404, n2405, n2406,
         n2407, n2408, n2409, n2410, n2411, n2412, n2413, n2414, n2415, n2416,
         n2417, n2418, n2419, canary_in, \s1_pc_[39] , s2_valid,
         s2_speculative, s1_speculative, s2_cacheable, s1_same_block, n331,
         n332, n333, n334, n335, n336, n337, n338, n339, n340, n341, n342,
         n343, n344, n345, n346, n347, n348, n349, n350, n351, n352, n353,
         n354, n355, n356, n357, n358, n359, n360, n361, n362, n363, n364,
         n365, n366, n367, n368, n369, n370, n371, n372, n373, n374, n375,
         n376, n377, n378, n379, n382, n383, n384, n386, n387, n388, n389,
         n390, n391, n396, n404, n405, n406, n408, n409, n410, n411, n412,
         n414, n416, n417, n420, n424, n426, n427, n431, n435, n436, n437,
         n439, n441, n442, n443, n449, n450, n451, n452, n453, n454, n455,
         n456, n457, n459, n464, n471, n475, n476, n477, n478, n479, n480,
         n481, n482, n483, n484, n485, n486, n487, n488, n489, n491, n492,
         n493, n494, n495, n496, n500, n501, n502, n506, n507, n509, n510,
         n511, n513, n514, n515, n516, n517, n518, n519, n520, n521, n522,
         \icache_io_req_bits_addr[5] , n524, n525, n526, n527, n528,
         \tlb_io_req_bits_vpn[14] , n530, n532, n533, n534, n535, n536, n537,
         n538, n539, n540, n541, n542, \tlb_io_req_bits_vpn[20] ,
         \tlb_io_req_bits_vpn[18] , n546, n547, n567, n572, n574, n576, n578,
         n579, n582, n583, n584, n586, n588, n589, n590, n593, n594, n597,
         n598, n600, n605, n606, n607, n608, n612, \io_cpu_npc[25] ,
         \icache_io_req_bits_addr[32] , \io_cpu_npc[29] , \io_cpu_npc[31] ,
         \io_cpu_npc[38] , n626, n632, n633, n634, n636, n637, n638, n639,
         n640, n644, n646, n647, n648, n649, n650, n651, n652, n653, n654,
         n655, n656, n657, n658, n659, n660, n661, n662, n663, n664, n665,
         n666, n667, n668, n669, n670, n671, n672, n673, n674, n675, n676,
         n677, n678, n679, n680, n681, n682, n683, n684, n685, n686, n687,
         n688, n690, n691, n692, n694, n695, n696, n697, n698, n701, n702,
         n703, n704, n706, n708, n709, n711, n713, n714, n716, n717, n718,
         n719, n720, n721, n722, n723, n724, n726, n727, n728, n733, n734,
         n740, n741, n742, n743, n744, n745, n747, n748, n755, n757, n758,
         n759, n762, n763, n764, n765, n770, n773, n774, n775, n778, n779,
         n781, n782, n783, n785, n786, n787, n789, n791, n793, n794, n795,
         n796, \tlb_io_req_bits_vpn[0] , n799, n800, n801,
         \tlb_io_req_bits_vpn[19] , n805, n806, n807, n808, n809, n810, n812,
         n813, n814, n815, n816, n817, n818, n819, n821, n822, n823, n824,
         n825, n827, n828, n829, n831, n832, n833, n835, n836, n838, n839,
         n840, n842, n843, n844, n845, n846, n848, n850, n853, n855, n856,
         n857, n858, n859, n860, n861, n862, n863, n864, n865, n866, n867,
         n868, n869, n870, n871, n872, n873, n874, n875, n876, n877, n878,
         n879, n880, n881, n882, n883, n884, n885, n886, n887, n888, n889,
         n890, n891, n892, n893, n894, n895, n896, n897, n898, n899, n900,
         n901, n902, n903, n904, n905, n906, n907, n908, n909, n911, n912,
         n914, n916, n918, n920, n922, n924, n926, n928, n930, n932, n934,
         n936, n938, n940, n944, n945, n946, n947, n948, n949, n950, n951,
         n953, n954, n955, n956, n957, n958, n959, n960, n961, n962, n963,
         n964, n965, n966, n967, n968, n969, n970, n972, n975, n976, n977,
         n978, n979, n984, n986, n987, n988, n989, n992, n994, n995, n996,
         n997, \io_cpu_npc[4] , \io_cpu_npc[23] ,
         \icache_io_req_bits_addr[28] , \icache_io_req_bits_addr[22] ,
         \icache_io_req_bits_addr[21] , \icache_io_req_bits_addr[19] ,
         \icache_io_req_bits_addr[30] , n1008, n1010, n1011, n1012, n1013,
         n1016, n1017, n1019, n1020, n1021, n1022, n1023, n1024, n1025, n1026,
         n1027, n1028, \icache_io_req_bits_addr[3] , n1034, n1036, n1037,
         n1038, n1039, n1040, n1041, n1042, n1043, n1045, n1046,
         \io_cpu_npc[11] , n1049, n1050, n1051, n1052, n1053, n1054, n1055,
         n1056, n1057, n1058, n1059, n1060, n1061, n1062, n1064, n1066, n1067,
         n1068, n1069, n1070, n1071, n1072, n1075, n1080, n1085, n1086, n1088,
         n1092, n1093, n1095, n1101, n1103, n1104, n1105, n1107, n1108, n1110,
         n1111, n1113, n1114, n1115, n1116, n1118, n1119, n1121, n1122, n1124,
         n1125, n1126, n1127, n1128, n1129, n1130, n1131, n1133, n1135, n1136,
         n1137, n1139, n1140, n1141, n1142, n1144, n1145, n1146, n1147, n1148,
         n1149, n1150, n1151, n1152, n1153, n1155, n1156, n1157, n1158, n1160,
         n1161, n1162, n1163, n1164, n1165, n1166, n1168, n1170, n1172, n1174,
         n1176, n1178, n1180, n1182, n1184, n1186, n1188, n1190, n1192, n1194,
         n1196, n1198, n1199, n1200, n1201, n1202, n1203, n1204, n1205, n1206,
         n1208, n1210, n1211, n1212, n1213, n1214, n1217, n1218, n1219, n1220,
         n1222, n1223, n1224, n1225, n1226, n1227, n1228, n1231, n1232, n1233,
         n1236, n1237, n1238, n1239, n1245, n1247, n1248, n1253, n1254, n1255,
         n1256, n1257, n1258, n1259, n1260, n1261, n1262, n1263, n1264, n1265,
         n1266, n1267, n1268, n1269, n1270, n1271, n1272, n1274, n1275, n1276,
         n1277, n1279, n1280, n1281, n1282, n1284, n1285, n1287, n1288, n1289,
         n1290, n1291, n1292, n1293, n1294, n1296, n1298, n1299, n1301, n1302,
         n1303, n1305, n1307, n1308, n1311, n1312, n1313, n1315, n1316, n1317,
         n1318, n1319, n1320, n1321, n1322, n1323, n1325, n1326, n1327, n1328,
         n1329, n1330, n1331, n1333, n1335, n1336, n1337, n1338, n1340, n1341,
         n1343, n1346, n1347, n1348, n1349, n1350, n1351,
         \tlb_io_req_bits_vpn[13] , n1354, n1356, n1357, n1358, n1359, n1360,
         n1361, n1362, n1363, \io_cpu_npc[9] , n1365, n1366, n1368, n1369,
         n1370, n1371, n1372, n1373, n1374, n1375, n1376, n1378, n1379, n1381,
         n1385, n1386, n1387, n1388, n1389, n1390, n1391, n1392, n1393, n1394,
         n1397, n1405, n1406, n1407, n1408, n1411, n1412, n1413, n1414, n1419,
         n1421, n1422, n1423, n1424, n1425, n1427, n1429, n1430, n1431,
         \io_cpu_npc[34] , n1436, n1437, n1440, n1441, n1442, n1445, n1446,
         n1447, n1448, n1449, n1452, \icache_io_req_bits_addr[2] , n1454,
         n1455, n1457, n1458, n1459, n1460, n1461, n1462, n1463, n1464, n1465,
         n1466, n1467, n1468, n1469, n1470, n1472, n1473, n1474, n1475, n1476,
         n1477, n1478, n1479, n1480, n1481, n1482, n1484, n1485, n1486, n1487,
         n1488, n1489, n1491, n1492, n1493, n1494, n1495, n1496, n1497, n1498,
         n1500, n1501, n1502, n1503, n1504, n1505, n1506, n1507, n1508,
         \tlb_io_req_bits_vpn[5] , n1510, n1511, n1512, n1513, n1514, n1515,
         n1516, n1517, n1518, n1519, n1520, n1521, n1522, n1523, n1524, n1525,
         n1526, n1527, n1528, n1529, n1530, n1531, n1533, n1534, n1535, n1536,
         n1537, n1538, n1539, \tlb_io_req_bits_vpn[11] , n1542, n1543, n1544,
         n1546, n1547, n1548, n1549, n1551, n1552, n1553, n1554, n1555, n1556,
         n1557, n1558, n1559, n1560, n1561, n1562, n1563, n1564, n1565, n1566,
         n1567, n1568, n1569, n1570, n1571, n1572, n1573, n1574, n1575, n1576,
         n1578, n1579, n1580, \icache_io_req_bits_addr[15] ,
         \icache_io_req_bits_addr[16] , \icache_io_req_bits_addr[24] ,
         \icache_io_req_bits_addr[27] , n1590, n1594, n1595, n1596, n1597,
         n1598, n1599, n1601, n1602, n1604, n1606, n1607, n1608, n1609, n1611,
         n1612, n1615, n1617, n1618, n1619, n1620, n1621, n1622, n1623, n1624,
         n1625, n1626, n1627, n1628, n1629, n1630, n1631, n1632, n1633, n1634,
         n1635, n1636, n1637, n1640, n1641, n1642, n1643, n1644, n1645, n1646,
         n1647, n1648, n1649, n1650, n1652, n1653, n1654, n1655, n1656, n1657,
         n1658, n1659, n1660, n1661, n1662, n1663, n1664, n1666, n1667, n1669,
         n1670, n1671, n1672, n1673, n1674, n1675, n1676, n1677, n1678, n1679,
         n1680, n1681, n1683, n1684, n1685, n1686, n1687, n1688, n1689, n1690,
         n1691, n1692, n1693, n1694, n1695, n1696, n1697, n1698, n1699, n1700,
         n1701, n1702, n1703, n1704, n1705, n1707, n1708, n1709, n1710, n1711,
         n1714, n1716, n1717, n1718, n1719, n1720, n1721, n1722, n1723, n1724,
         n1725, n1726, n1727, n1728, n1729, n1730, n1731, n1732, n1733, n1734,
         n1735, n1736, n1737, n1738, n1739, n1740, n1741, n1742, n1743, n1744,
         n1745, n1746, n1747, n1748, n1749, n1750, n1751, n1752, n1753, n1754,
         n1755, n1756, n1757, n1758, n1759, n1760, n1761, n1762, n1763, n1764,
         n1765, n1766, n1767, n1768, n1769, n1770, n1771, n1772, n1773, n1774,
         n1775, n1776, n1777, n1778, n1779, n1780, n1781, n1782, n1783, n1784,
         n1785, n1786, n1787, n1788, n1789, n1790, n1791, n1792, n1793, n1794,
         n1795, n1796, n1797, n1798, n1799, n1801, n1802, n1805,
         \icache_io_req_bits_addr[26] , n1808, n1809, n1810, n1811, n1812,
         n1813, n1814, n1815, n1817, \icache_io_req_bits_addr[1] ,
         \icache_io_req_bits_addr[0] , \io_cpu_npc[6] , n1822, n1825, n1826,
         n1827, n1828, n1829, n1830, n1831, n1833, n1835, n1836, n1837, n1838,
         n1839, n1840, n1842, n1843, n1844, n1845, n1846, n1847, n1849, n1850,
         n1851, n1852, n1853, n1854, n1855, n1856, n1857, n1858, n1859, n1860,
         n1861, n1862, n1863, n1864, n1865, n1867, n1868, n1869, n1870, n1871,
         n1872, n1873, n1874, n1875, n1876, n1877, n1879, n1880, n1883, n1885,
         n1886, n1887, n1888, n1889, n1890, n1891, n1892, n1893, n1894, n1895,
         n1896, n1897, n1898, n1899, n1900, n1901, n1902, n1903, n1904, n1905,
         n1906, n1907, n1908, n1909, n1911, n1912, n1913, n1914, n1915, n1916,
         n1917, n1918, n1919, n1920, n1921, n1922, n1923, n1924, n1925, n1926,
         n1927, n1928, n1929, n1931, n1932, n1933, n1934, n1935, n1936, n1937,
         n1938, n1939, n1940, n1941, n1942, n1943, n1944, n1945, n1946, n1947,
         n1949, n1951, n1952, n1953, n1954, n1955, n1956, n1957, n1958, n1959,
         n1960, n1961, n1962, n1963, n1964, \io_cpu_npc[14] , n1966, n1967,
         n1968, n1969, n1970, n1971, n1972, n1973, n1974, n1975, n1976, n1977,
         n1978, n1979, n1980, n1981, n1982, n1983, n1985, n1986, n1987, n1988,
         n1989, n1990, n1991, n1992, n1993, n1994, n1995, n1996, n1998, n1999,
         n2000, n2001, n2002, n2003, n2004, n2005, n2006, n2007, n2008, n2009,
         n2010, n2011, n2012, n2013, n2014, n2015, n2016, n2017, n2019, n2020,
         n2021, n2022, n2023, n2024, n2025, n2026, n2027, n2028, n2029, n2031,
         n2032, n2033, n2034, n2035, n2036, n2037, n2038, n2039, n2040, n2041,
         n2042, n2043, n2044, n2045, n2046, n2047, n2048, n2049, n2050, n2051,
         n2052, n2053, n2054, n2055, n2056, n2057, n2058, n2059, n2060, n2061,
         n2062, n2063, n2064, n2065, n2067, n2068, n2069, n2070, n2071, n2072,
         n2073, n2075, n2077, n2078, n2079, n2080, n2081, n2082, n2086, n2087,
         n2088, n2089, n2090, n2091, n2093, n2094, n2095, n2096, n2097, n2098,
         n2099, n2100, n2101, n2102, n2104, n2105, n2106, n2107, n2108, n2109,
         n2110, n2111, n2112, n2113, n2114, n2115, n2116, n2118, n2119, n2120,
         n2121, n2122, n2123, n2124, n2125, n2127, n2128, n2129, n2130, n2131,
         n2132, n2133, n2134, n2135, n2136, n2137, n2138, n2139, n2140, n2141,
         n2142, n2143, n2145, n2146, n2147, n2148, n2149, n2150, n2151, n2152,
         n2153, n2154, n2155, n2156, n2157, n2158, n2159, n2160, n2161, n2162,
         n2163, n2164, n2165, n2166, n2167, n2168, n2169, n2170, n2171, n2172,
         n2173, n2174, n2175, n2176, n2178, n2179, n2180, n2181, n2182, n2183,
         n2184, n2185, n2186, n2187, n2189, n2190, n2191, n2192, n2193, n2194,
         n2195, n2196, n2197, n2198, n2199, n2200, n2201, n2202, n2203, n2204,
         n2206, n2207, n2208, n2209, n2210, n2211, n2212, n2213, n2214, n2215,
         n2216, n2217, n2218, n2219, n2220, n2221, n2222, n2223, n2224, n2225,
         n2226, n2227, n2228, n2229, n2230, n2231, n2232, n2233, n2234, n2235,
         n2236, n2237, n2238, n2239, n2240, n2241, n2242, n2243, n2244, n2245,
         n2246, n2247, n2248, n2249, n2250, n2251, n2252, n2253, n2254, n2255,
         n2256, n2257, n2258, n2259, n2260, n2261, n2262, n2263, n2264, n2265,
         n2266, n2267, n2268, n2269, n2270, n2271, n2272, n2273, n2274, n2275,
         n2276, n2277, n2278, n2279, n2280, n2281, n2282, n2283, n2284, n2285,
         n2286, n2287, n2288, n2289, n2290, n2291, n2292, n2293, n2294, n2295,
         n2296, n2297, n2298, n2299, n2301, n2302, n2303, n2304, n2305, n2306,
         n2307, n2308, n2309, n2310, n2311, n2312, n2313, n2314, n2316, n2317,
         n2318, n2319, n2320, n2321, n2322, n2323, n2324, n2325, n2326, n2327,
         n2328, n2329, n2330, n2331, n2332, n2333, n2334, n2335, n2336, n2337,
         n2338, n2339, n2340, n2341, n2342, n2343, n2344, n2345, n2346, n2347,
         n2348, n2349, n2350, n2351, n2352, n2353, n2354, n2355, n2356, n2357,
         n2358, n2359, n2360, n2361, n2362, n2363, n2364, n2365, n2366, n2367,
         n2368, n2369, n2370, n2371, n2373, n2374, n2375, n2376, n2377, n2378,
         n2379, \io_cpu_npc[12] , \tlb_io_req_bits_vpn[12] ,
         \tlb_io_req_bits_vpn[10] , \tlb_io_req_bits_vpn[3] ,
         \tlb_io_req_bits_vpn[7] , \tlb_io_req_bits_vpn[9] ,
         \tlb_io_req_bits_vpn[8] , \tlb_io_req_bits_vpn[6] ;
  assign BTB_clock = clock;
  assign tlb_clock = clock;
  assign icache_clock = clock;
  assign BTB_reset = reset;
  assign tlb_reset = reset;
  assign icache_reset = reset;
  assign icache_io_mem_0_a_ready = io_mem_0_a_ready;
  assign icache_io_mem_0_b_valid = io_mem_0_b_valid;
  assign icache_io_mem_0_b_bits_opcode[2] = io_mem_0_b_bits_opcode[2];
  assign icache_io_mem_0_b_bits_opcode[1] = io_mem_0_b_bits_opcode[1];
  assign icache_io_mem_0_b_bits_opcode[0] = io_mem_0_b_bits_opcode[0];
  assign icache_io_mem_0_b_bits_param[1] = io_mem_0_b_bits_param[1];
  assign icache_io_mem_0_b_bits_param[0] = io_mem_0_b_bits_param[0];
  assign icache_io_mem_0_b_bits_size[3] = io_mem_0_b_bits_size[3];
  assign icache_io_mem_0_b_bits_size[2] = io_mem_0_b_bits_size[2];
  assign icache_io_mem_0_b_bits_size[1] = io_mem_0_b_bits_size[1];
  assign icache_io_mem_0_b_bits_size[0] = io_mem_0_b_bits_size[0];
  assign icache_io_mem_0_b_bits_source = io_mem_0_b_bits_source;
  assign icache_io_mem_0_b_bits_address[31] = io_mem_0_b_bits_address[31];
  assign icache_io_mem_0_b_bits_address[30] = io_mem_0_b_bits_address[30];
  assign icache_io_mem_0_b_bits_address[29] = io_mem_0_b_bits_address[29];
  assign icache_io_mem_0_b_bits_address[28] = io_mem_0_b_bits_address[28];
  assign icache_io_mem_0_b_bits_address[27] = io_mem_0_b_bits_address[27];
  assign icache_io_mem_0_b_bits_address[26] = io_mem_0_b_bits_address[26];
  assign icache_io_mem_0_b_bits_address[25] = io_mem_0_b_bits_address[25];
  assign icache_io_mem_0_b_bits_address[24] = io_mem_0_b_bits_address[24];
  assign icache_io_mem_0_b_bits_address[23] = io_mem_0_b_bits_address[23];
  assign icache_io_mem_0_b_bits_address[22] = io_mem_0_b_bits_address[22];
  assign icache_io_mem_0_b_bits_address[21] = io_mem_0_b_bits_address[21];
  assign icache_io_mem_0_b_bits_address[20] = io_mem_0_b_bits_address[20];
  assign icache_io_mem_0_b_bits_address[19] = io_mem_0_b_bits_address[19];
  assign icache_io_mem_0_b_bits_address[18] = io_mem_0_b_bits_address[18];
  assign icache_io_mem_0_b_bits_address[17] = io_mem_0_b_bits_address[17];
  assign icache_io_mem_0_b_bits_address[16] = io_mem_0_b_bits_address[16];
  assign icache_io_mem_0_b_bits_address[15] = io_mem_0_b_bits_address[15];
  assign icache_io_mem_0_b_bits_address[14] = io_mem_0_b_bits_address[14];
  assign icache_io_mem_0_b_bits_address[13] = io_mem_0_b_bits_address[13];
  assign icache_io_mem_0_b_bits_address[12] = io_mem_0_b_bits_address[12];
  assign icache_io_mem_0_b_bits_address[11] = io_mem_0_b_bits_address[11];
  assign icache_io_mem_0_b_bits_address[10] = io_mem_0_b_bits_address[10];
  assign icache_io_mem_0_b_bits_address[9] = io_mem_0_b_bits_address[9];
  assign icache_io_mem_0_b_bits_address[8] = io_mem_0_b_bits_address[8];
  assign icache_io_mem_0_b_bits_address[7] = io_mem_0_b_bits_address[7];
  assign icache_io_mem_0_b_bits_address[6] = io_mem_0_b_bits_address[6];
  assign icache_io_mem_0_b_bits_address[5] = io_mem_0_b_bits_address[5];
  assign icache_io_mem_0_b_bits_address[4] = io_mem_0_b_bits_address[4];
  assign icache_io_mem_0_b_bits_address[3] = io_mem_0_b_bits_address[3];
  assign icache_io_mem_0_b_bits_address[2] = io_mem_0_b_bits_address[2];
  assign icache_io_mem_0_b_bits_address[1] = io_mem_0_b_bits_address[1];
  assign icache_io_mem_0_b_bits_address[0] = io_mem_0_b_bits_address[0];
  assign icache_io_mem_0_b_bits_mask[7] = io_mem_0_b_bits_mask[7];
  assign icache_io_mem_0_b_bits_mask[6] = io_mem_0_b_bits_mask[6];
  assign icache_io_mem_0_b_bits_mask[5] = io_mem_0_b_bits_mask[5];
  assign icache_io_mem_0_b_bits_mask[4] = io_mem_0_b_bits_mask[4];
  assign icache_io_mem_0_b_bits_mask[3] = io_mem_0_b_bits_mask[3];
  assign icache_io_mem_0_b_bits_mask[2] = io_mem_0_b_bits_mask[2];
  assign icache_io_mem_0_b_bits_mask[1] = io_mem_0_b_bits_mask[1];
  assign icache_io_mem_0_b_bits_mask[0] = io_mem_0_b_bits_mask[0];
  assign icache_io_mem_0_b_bits_data[63] = io_mem_0_b_bits_data[63];
  assign icache_io_mem_0_b_bits_data[62] = io_mem_0_b_bits_data[62];
  assign icache_io_mem_0_b_bits_data[61] = io_mem_0_b_bits_data[61];
  assign icache_io_mem_0_b_bits_data[60] = io_mem_0_b_bits_data[60];
  assign icache_io_mem_0_b_bits_data[59] = io_mem_0_b_bits_data[59];
  assign icache_io_mem_0_b_bits_data[58] = io_mem_0_b_bits_data[58];
  assign icache_io_mem_0_b_bits_data[57] = io_mem_0_b_bits_data[57];
  assign icache_io_mem_0_b_bits_data[56] = io_mem_0_b_bits_data[56];
  assign icache_io_mem_0_b_bits_data[55] = io_mem_0_b_bits_data[55];
  assign icache_io_mem_0_b_bits_data[54] = io_mem_0_b_bits_data[54];
  assign icache_io_mem_0_b_bits_data[53] = io_mem_0_b_bits_data[53];
  assign icache_io_mem_0_b_bits_data[52] = io_mem_0_b_bits_data[52];
  assign icache_io_mem_0_b_bits_data[51] = io_mem_0_b_bits_data[51];
  assign icache_io_mem_0_b_bits_data[50] = io_mem_0_b_bits_data[50];
  assign icache_io_mem_0_b_bits_data[49] = io_mem_0_b_bits_data[49];
  assign icache_io_mem_0_b_bits_data[48] = io_mem_0_b_bits_data[48];
  assign icache_io_mem_0_b_bits_data[47] = io_mem_0_b_bits_data[47];
  assign icache_io_mem_0_b_bits_data[46] = io_mem_0_b_bits_data[46];
  assign icache_io_mem_0_b_bits_data[45] = io_mem_0_b_bits_data[45];
  assign icache_io_mem_0_b_bits_data[44] = io_mem_0_b_bits_data[44];
  assign icache_io_mem_0_b_bits_data[43] = io_mem_0_b_bits_data[43];
  assign icache_io_mem_0_b_bits_data[42] = io_mem_0_b_bits_data[42];
  assign icache_io_mem_0_b_bits_data[41] = io_mem_0_b_bits_data[41];
  assign icache_io_mem_0_b_bits_data[40] = io_mem_0_b_bits_data[40];
  assign icache_io_mem_0_b_bits_data[39] = io_mem_0_b_bits_data[39];
  assign icache_io_mem_0_b_bits_data[38] = io_mem_0_b_bits_data[38];
  assign icache_io_mem_0_b_bits_data[37] = io_mem_0_b_bits_data[37];
  assign icache_io_mem_0_b_bits_data[36] = io_mem_0_b_bits_data[36];
  assign icache_io_mem_0_b_bits_data[35] = io_mem_0_b_bits_data[35];
  assign icache_io_mem_0_b_bits_data[34] = io_mem_0_b_bits_data[34];
  assign icache_io_mem_0_b_bits_data[33] = io_mem_0_b_bits_data[33];
  assign icache_io_mem_0_b_bits_data[32] = io_mem_0_b_bits_data[32];
  assign icache_io_mem_0_b_bits_data[31] = io_mem_0_b_bits_data[31];
  assign icache_io_mem_0_b_bits_data[30] = io_mem_0_b_bits_data[30];
  assign icache_io_mem_0_b_bits_data[29] = io_mem_0_b_bits_data[29];
  assign icache_io_mem_0_b_bits_data[28] = io_mem_0_b_bits_data[28];
  assign icache_io_mem_0_b_bits_data[27] = io_mem_0_b_bits_data[27];
  assign icache_io_mem_0_b_bits_data[26] = io_mem_0_b_bits_data[26];
  assign icache_io_mem_0_b_bits_data[25] = io_mem_0_b_bits_data[25];
  assign icache_io_mem_0_b_bits_data[24] = io_mem_0_b_bits_data[24];
  assign icache_io_mem_0_b_bits_data[23] = io_mem_0_b_bits_data[23];
  assign icache_io_mem_0_b_bits_data[22] = io_mem_0_b_bits_data[22];
  assign icache_io_mem_0_b_bits_data[21] = io_mem_0_b_bits_data[21];
  assign icache_io_mem_0_b_bits_data[20] = io_mem_0_b_bits_data[20];
  assign icache_io_mem_0_b_bits_data[19] = io_mem_0_b_bits_data[19];
  assign icache_io_mem_0_b_bits_data[18] = io_mem_0_b_bits_data[18];
  assign icache_io_mem_0_b_bits_data[17] = io_mem_0_b_bits_data[17];
  assign icache_io_mem_0_b_bits_data[16] = io_mem_0_b_bits_data[16];
  assign icache_io_mem_0_b_bits_data[15] = io_mem_0_b_bits_data[15];
  assign icache_io_mem_0_b_bits_data[14] = io_mem_0_b_bits_data[14];
  assign icache_io_mem_0_b_bits_data[13] = io_mem_0_b_bits_data[13];
  assign icache_io_mem_0_b_bits_data[12] = io_mem_0_b_bits_data[12];
  assign icache_io_mem_0_b_bits_data[11] = io_mem_0_b_bits_data[11];
  assign icache_io_mem_0_b_bits_data[10] = io_mem_0_b_bits_data[10];
  assign icache_io_mem_0_b_bits_data[9] = io_mem_0_b_bits_data[9];
  assign icache_io_mem_0_b_bits_data[8] = io_mem_0_b_bits_data[8];
  assign icache_io_mem_0_b_bits_data[7] = io_mem_0_b_bits_data[7];
  assign icache_io_mem_0_b_bits_data[6] = io_mem_0_b_bits_data[6];
  assign icache_io_mem_0_b_bits_data[5] = io_mem_0_b_bits_data[5];
  assign icache_io_mem_0_b_bits_data[4] = io_mem_0_b_bits_data[4];
  assign icache_io_mem_0_b_bits_data[3] = io_mem_0_b_bits_data[3];
  assign icache_io_mem_0_b_bits_data[2] = io_mem_0_b_bits_data[2];
  assign icache_io_mem_0_b_bits_data[1] = io_mem_0_b_bits_data[1];
  assign icache_io_mem_0_b_bits_data[0] = io_mem_0_b_bits_data[0];
  assign icache_io_mem_0_c_ready = io_mem_0_c_ready;
  assign icache_io_mem_0_d_valid = io_mem_0_d_valid;
  assign icache_io_mem_0_d_bits_opcode[2] = io_mem_0_d_bits_opcode[2];
  assign icache_io_mem_0_d_bits_opcode[1] = io_mem_0_d_bits_opcode[1];
  assign icache_io_mem_0_d_bits_opcode[0] = io_mem_0_d_bits_opcode[0];
  assign icache_io_mem_0_d_bits_param[1] = io_mem_0_d_bits_param[1];
  assign icache_io_mem_0_d_bits_param[0] = io_mem_0_d_bits_param[0];
  assign icache_io_mem_0_d_bits_size[3] = io_mem_0_d_bits_size[3];
  assign icache_io_mem_0_d_bits_size[2] = io_mem_0_d_bits_size[2];
  assign icache_io_mem_0_d_bits_size[1] = io_mem_0_d_bits_size[1];
  assign icache_io_mem_0_d_bits_size[0] = io_mem_0_d_bits_size[0];
  assign icache_io_mem_0_d_bits_source = io_mem_0_d_bits_source;
  assign icache_io_mem_0_d_bits_sink[3] = io_mem_0_d_bits_sink[3];
  assign icache_io_mem_0_d_bits_sink[2] = io_mem_0_d_bits_sink[2];
  assign icache_io_mem_0_d_bits_sink[1] = io_mem_0_d_bits_sink[1];
  assign icache_io_mem_0_d_bits_sink[0] = io_mem_0_d_bits_sink[0];
  assign icache_io_mem_0_d_bits_addr_lo[2] = io_mem_0_d_bits_addr_lo[2];
  assign icache_io_mem_0_d_bits_addr_lo[1] = io_mem_0_d_bits_addr_lo[1];
  assign icache_io_mem_0_d_bits_addr_lo[0] = io_mem_0_d_bits_addr_lo[0];
  assign icache_io_mem_0_d_bits_data[63] = io_mem_0_d_bits_data[63];
  assign icache_io_mem_0_d_bits_data[62] = io_mem_0_d_bits_data[62];
  assign icache_io_mem_0_d_bits_data[61] = io_mem_0_d_bits_data[61];
  assign icache_io_mem_0_d_bits_data[60] = io_mem_0_d_bits_data[60];
  assign icache_io_mem_0_d_bits_data[59] = io_mem_0_d_bits_data[59];
  assign icache_io_mem_0_d_bits_data[58] = io_mem_0_d_bits_data[58];
  assign icache_io_mem_0_d_bits_data[57] = io_mem_0_d_bits_data[57];
  assign icache_io_mem_0_d_bits_data[56] = io_mem_0_d_bits_data[56];
  assign icache_io_mem_0_d_bits_data[55] = io_mem_0_d_bits_data[55];
  assign icache_io_mem_0_d_bits_data[54] = io_mem_0_d_bits_data[54];
  assign icache_io_mem_0_d_bits_data[53] = io_mem_0_d_bits_data[53];
  assign icache_io_mem_0_d_bits_data[52] = io_mem_0_d_bits_data[52];
  assign icache_io_mem_0_d_bits_data[51] = io_mem_0_d_bits_data[51];
  assign icache_io_mem_0_d_bits_data[50] = io_mem_0_d_bits_data[50];
  assign icache_io_mem_0_d_bits_data[49] = io_mem_0_d_bits_data[49];
  assign icache_io_mem_0_d_bits_data[48] = io_mem_0_d_bits_data[48];
  assign icache_io_mem_0_d_bits_data[47] = io_mem_0_d_bits_data[47];
  assign icache_io_mem_0_d_bits_data[46] = io_mem_0_d_bits_data[46];
  assign icache_io_mem_0_d_bits_data[45] = io_mem_0_d_bits_data[45];
  assign icache_io_mem_0_d_bits_data[44] = io_mem_0_d_bits_data[44];
  assign icache_io_mem_0_d_bits_data[43] = io_mem_0_d_bits_data[43];
  assign icache_io_mem_0_d_bits_data[42] = io_mem_0_d_bits_data[42];
  assign icache_io_mem_0_d_bits_data[41] = io_mem_0_d_bits_data[41];
  assign icache_io_mem_0_d_bits_data[40] = io_mem_0_d_bits_data[40];
  assign icache_io_mem_0_d_bits_data[39] = io_mem_0_d_bits_data[39];
  assign icache_io_mem_0_d_bits_data[38] = io_mem_0_d_bits_data[38];
  assign icache_io_mem_0_d_bits_data[37] = io_mem_0_d_bits_data[37];
  assign icache_io_mem_0_d_bits_data[36] = io_mem_0_d_bits_data[36];
  assign icache_io_mem_0_d_bits_data[35] = io_mem_0_d_bits_data[35];
  assign icache_io_mem_0_d_bits_data[34] = io_mem_0_d_bits_data[34];
  assign icache_io_mem_0_d_bits_data[33] = io_mem_0_d_bits_data[33];
  assign icache_io_mem_0_d_bits_data[32] = io_mem_0_d_bits_data[32];
  assign icache_io_mem_0_d_bits_data[31] = io_mem_0_d_bits_data[31];
  assign icache_io_mem_0_d_bits_data[30] = io_mem_0_d_bits_data[30];
  assign icache_io_mem_0_d_bits_data[29] = io_mem_0_d_bits_data[29];
  assign icache_io_mem_0_d_bits_data[28] = io_mem_0_d_bits_data[28];
  assign icache_io_mem_0_d_bits_data[27] = io_mem_0_d_bits_data[27];
  assign icache_io_mem_0_d_bits_data[26] = io_mem_0_d_bits_data[26];
  assign icache_io_mem_0_d_bits_data[25] = io_mem_0_d_bits_data[25];
  assign icache_io_mem_0_d_bits_data[24] = io_mem_0_d_bits_data[24];
  assign icache_io_mem_0_d_bits_data[23] = io_mem_0_d_bits_data[23];
  assign icache_io_mem_0_d_bits_data[22] = io_mem_0_d_bits_data[22];
  assign icache_io_mem_0_d_bits_data[21] = io_mem_0_d_bits_data[21];
  assign icache_io_mem_0_d_bits_data[20] = io_mem_0_d_bits_data[20];
  assign icache_io_mem_0_d_bits_data[19] = io_mem_0_d_bits_data[19];
  assign icache_io_mem_0_d_bits_data[18] = io_mem_0_d_bits_data[18];
  assign icache_io_mem_0_d_bits_data[17] = io_mem_0_d_bits_data[17];
  assign icache_io_mem_0_d_bits_data[16] = io_mem_0_d_bits_data[16];
  assign icache_io_mem_0_d_bits_data[15] = io_mem_0_d_bits_data[15];
  assign icache_io_mem_0_d_bits_data[14] = io_mem_0_d_bits_data[14];
  assign icache_io_mem_0_d_bits_data[13] = io_mem_0_d_bits_data[13];
  assign icache_io_mem_0_d_bits_data[12] = io_mem_0_d_bits_data[12];
  assign icache_io_mem_0_d_bits_data[11] = io_mem_0_d_bits_data[11];
  assign icache_io_mem_0_d_bits_data[10] = io_mem_0_d_bits_data[10];
  assign icache_io_mem_0_d_bits_data[9] = io_mem_0_d_bits_data[9];
  assign icache_io_mem_0_d_bits_data[8] = io_mem_0_d_bits_data[8];
  assign icache_io_mem_0_d_bits_data[7] = io_mem_0_d_bits_data[7];
  assign icache_io_mem_0_d_bits_data[6] = io_mem_0_d_bits_data[6];
  assign icache_io_mem_0_d_bits_data[5] = io_mem_0_d_bits_data[5];
  assign icache_io_mem_0_d_bits_data[4] = io_mem_0_d_bits_data[4];
  assign icache_io_mem_0_d_bits_data[3] = io_mem_0_d_bits_data[3];
  assign icache_io_mem_0_d_bits_data[2] = io_mem_0_d_bits_data[2];
  assign icache_io_mem_0_d_bits_data[1] = io_mem_0_d_bits_data[1];
  assign icache_io_mem_0_d_bits_data[0] = io_mem_0_d_bits_data[0];
  assign icache_io_mem_0_d_bits_error = io_mem_0_d_bits_error;
  assign icache_io_mem_0_e_ready = io_mem_0_e_ready;
  assign BTB_io_btb_update_valid = io_cpu_btb_update_valid;
  assign BTB_io_btb_update_bits_prediction_valid = io_cpu_btb_update_bits_prediction_valid;
  assign BTB_io_btb_update_bits_prediction_bits_taken = io_cpu_btb_update_bits_prediction_bits_taken;
  assign BTB_io_btb_update_bits_prediction_bits_mask = io_cpu_btb_update_bits_prediction_bits_mask;
  assign BTB_io_btb_update_bits_prediction_bits_bridx = io_cpu_btb_update_bits_prediction_bits_bridx;
  assign BTB_io_btb_update_bits_prediction_bits_target[38] = io_cpu_btb_update_bits_prediction_bits_target[38];
  assign BTB_io_btb_update_bits_prediction_bits_target[37] = io_cpu_btb_update_bits_prediction_bits_target[37];
  assign BTB_io_btb_update_bits_prediction_bits_target[36] = io_cpu_btb_update_bits_prediction_bits_target[36];
  assign BTB_io_btb_update_bits_prediction_bits_target[35] = io_cpu_btb_update_bits_prediction_bits_target[35];
  assign BTB_io_btb_update_bits_prediction_bits_target[34] = io_cpu_btb_update_bits_prediction_bits_target[34];
  assign BTB_io_btb_update_bits_prediction_bits_target[33] = io_cpu_btb_update_bits_prediction_bits_target[33];
  assign BTB_io_btb_update_bits_prediction_bits_target[32] = io_cpu_btb_update_bits_prediction_bits_target[32];
  assign BTB_io_btb_update_bits_prediction_bits_target[31] = io_cpu_btb_update_bits_prediction_bits_target[31];
  assign BTB_io_btb_update_bits_prediction_bits_target[30] = io_cpu_btb_update_bits_prediction_bits_target[30];
  assign BTB_io_btb_update_bits_prediction_bits_target[29] = io_cpu_btb_update_bits_prediction_bits_target[29];
  assign BTB_io_btb_update_bits_prediction_bits_target[28] = io_cpu_btb_update_bits_prediction_bits_target[28];
  assign BTB_io_btb_update_bits_prediction_bits_target[27] = io_cpu_btb_update_bits_prediction_bits_target[27];
  assign BTB_io_btb_update_bits_prediction_bits_target[26] = io_cpu_btb_update_bits_prediction_bits_target[26];
  assign BTB_io_btb_update_bits_prediction_bits_target[25] = io_cpu_btb_update_bits_prediction_bits_target[25];
  assign BTB_io_btb_update_bits_prediction_bits_target[24] = io_cpu_btb_update_bits_prediction_bits_target[24];
  assign BTB_io_btb_update_bits_prediction_bits_target[23] = io_cpu_btb_update_bits_prediction_bits_target[23];
  assign BTB_io_btb_update_bits_prediction_bits_target[22] = io_cpu_btb_update_bits_prediction_bits_target[22];
  assign BTB_io_btb_update_bits_prediction_bits_target[21] = io_cpu_btb_update_bits_prediction_bits_target[21];
  assign BTB_io_btb_update_bits_prediction_bits_target[20] = io_cpu_btb_update_bits_prediction_bits_target[20];
  assign BTB_io_btb_update_bits_prediction_bits_target[19] = io_cpu_btb_update_bits_prediction_bits_target[19];
  assign BTB_io_btb_update_bits_prediction_bits_target[18] = io_cpu_btb_update_bits_prediction_bits_target[18];
  assign BTB_io_btb_update_bits_prediction_bits_target[17] = io_cpu_btb_update_bits_prediction_bits_target[17];
  assign BTB_io_btb_update_bits_prediction_bits_target[16] = io_cpu_btb_update_bits_prediction_bits_target[16];
  assign BTB_io_btb_update_bits_prediction_bits_target[15] = io_cpu_btb_update_bits_prediction_bits_target[15];
  assign BTB_io_btb_update_bits_prediction_bits_target[14] = io_cpu_btb_update_bits_prediction_bits_target[14];
  assign BTB_io_btb_update_bits_prediction_bits_target[13] = io_cpu_btb_update_bits_prediction_bits_target[13];
  assign BTB_io_btb_update_bits_prediction_bits_target[12] = io_cpu_btb_update_bits_prediction_bits_target[12];
  assign BTB_io_btb_update_bits_prediction_bits_target[11] = io_cpu_btb_update_bits_prediction_bits_target[11];
  assign BTB_io_btb_update_bits_prediction_bits_target[10] = io_cpu_btb_update_bits_prediction_bits_target[10];
  assign BTB_io_btb_update_bits_prediction_bits_target[9] = io_cpu_btb_update_bits_prediction_bits_target[9];
  assign BTB_io_btb_update_bits_prediction_bits_target[8] = io_cpu_btb_update_bits_prediction_bits_target[8];
  assign BTB_io_btb_update_bits_prediction_bits_target[7] = io_cpu_btb_update_bits_prediction_bits_target[7];
  assign BTB_io_btb_update_bits_prediction_bits_target[6] = io_cpu_btb_update_bits_prediction_bits_target[6];
  assign BTB_io_btb_update_bits_prediction_bits_target[5] = io_cpu_btb_update_bits_prediction_bits_target[5];
  assign BTB_io_btb_update_bits_prediction_bits_target[4] = io_cpu_btb_update_bits_prediction_bits_target[4];
  assign BTB_io_btb_update_bits_prediction_bits_target[3] = io_cpu_btb_update_bits_prediction_bits_target[3];
  assign BTB_io_btb_update_bits_prediction_bits_target[2] = io_cpu_btb_update_bits_prediction_bits_target[2];
  assign BTB_io_btb_update_bits_prediction_bits_target[1] = io_cpu_btb_update_bits_prediction_bits_target[1];
  assign BTB_io_btb_update_bits_prediction_bits_target[0] = io_cpu_btb_update_bits_prediction_bits_target[0];
  assign BTB_io_btb_update_bits_prediction_bits_entry[5] = io_cpu_btb_update_bits_prediction_bits_entry[5];
  assign BTB_io_btb_update_bits_prediction_bits_entry[4] = io_cpu_btb_update_bits_prediction_bits_entry[4];
  assign BTB_io_btb_update_bits_prediction_bits_entry[3] = io_cpu_btb_update_bits_prediction_bits_entry[3];
  assign BTB_io_btb_update_bits_prediction_bits_entry[2] = io_cpu_btb_update_bits_prediction_bits_entry[2];
  assign BTB_io_btb_update_bits_prediction_bits_entry[1] = io_cpu_btb_update_bits_prediction_bits_entry[1];
  assign BTB_io_btb_update_bits_prediction_bits_entry[0] = io_cpu_btb_update_bits_prediction_bits_entry[0];
  assign BTB_io_btb_update_bits_prediction_bits_bht_history[6] = io_cpu_btb_update_bits_prediction_bits_bht_history[6];
  assign BTB_io_btb_update_bits_prediction_bits_bht_history[5] = io_cpu_btb_update_bits_prediction_bits_bht_history[5];
  assign BTB_io_btb_update_bits_prediction_bits_bht_history[4] = io_cpu_btb_update_bits_prediction_bits_bht_history[4];
  assign BTB_io_btb_update_bits_prediction_bits_bht_history[3] = io_cpu_btb_update_bits_prediction_bits_bht_history[3];
  assign BTB_io_btb_update_bits_prediction_bits_bht_history[2] = io_cpu_btb_update_bits_prediction_bits_bht_history[2];
  assign BTB_io_btb_update_bits_prediction_bits_bht_history[1] = io_cpu_btb_update_bits_prediction_bits_bht_history[1];
  assign BTB_io_btb_update_bits_prediction_bits_bht_history[0] = io_cpu_btb_update_bits_prediction_bits_bht_history[0];
  assign BTB_io_btb_update_bits_prediction_bits_bht_value[1] = io_cpu_btb_update_bits_prediction_bits_bht_value[1];
  assign BTB_io_btb_update_bits_prediction_bits_bht_value[0] = io_cpu_btb_update_bits_prediction_bits_bht_value[0];
  assign BTB_io_btb_update_bits_pc[38] = io_cpu_btb_update_bits_pc[38];
  assign BTB_io_btb_update_bits_pc[37] = io_cpu_btb_update_bits_pc[37];
  assign BTB_io_btb_update_bits_pc[36] = io_cpu_btb_update_bits_pc[36];
  assign BTB_io_btb_update_bits_pc[35] = io_cpu_btb_update_bits_pc[35];
  assign BTB_io_btb_update_bits_pc[34] = io_cpu_btb_update_bits_pc[34];
  assign BTB_io_btb_update_bits_pc[33] = io_cpu_btb_update_bits_pc[33];
  assign BTB_io_btb_update_bits_pc[32] = io_cpu_btb_update_bits_pc[32];
  assign BTB_io_btb_update_bits_pc[31] = io_cpu_btb_update_bits_pc[31];
  assign BTB_io_btb_update_bits_pc[30] = io_cpu_btb_update_bits_pc[30];
  assign BTB_io_btb_update_bits_pc[29] = io_cpu_btb_update_bits_pc[29];
  assign BTB_io_btb_update_bits_pc[28] = io_cpu_btb_update_bits_pc[28];
  assign BTB_io_btb_update_bits_pc[27] = io_cpu_btb_update_bits_pc[27];
  assign BTB_io_btb_update_bits_pc[26] = io_cpu_btb_update_bits_pc[26];
  assign BTB_io_btb_update_bits_pc[25] = io_cpu_btb_update_bits_pc[25];
  assign BTB_io_btb_update_bits_pc[24] = io_cpu_btb_update_bits_pc[24];
  assign BTB_io_btb_update_bits_pc[23] = io_cpu_btb_update_bits_pc[23];
  assign BTB_io_btb_update_bits_pc[22] = io_cpu_btb_update_bits_pc[22];
  assign BTB_io_btb_update_bits_pc[21] = io_cpu_btb_update_bits_pc[21];
  assign BTB_io_btb_update_bits_pc[20] = io_cpu_btb_update_bits_pc[20];
  assign BTB_io_btb_update_bits_pc[19] = io_cpu_btb_update_bits_pc[19];
  assign BTB_io_btb_update_bits_pc[18] = io_cpu_btb_update_bits_pc[18];
  assign BTB_io_btb_update_bits_pc[17] = io_cpu_btb_update_bits_pc[17];
  assign BTB_io_btb_update_bits_pc[16] = io_cpu_btb_update_bits_pc[16];
  assign BTB_io_btb_update_bits_pc[15] = io_cpu_btb_update_bits_pc[15];
  assign BTB_io_btb_update_bits_pc[14] = io_cpu_btb_update_bits_pc[14];
  assign BTB_io_btb_update_bits_pc[13] = io_cpu_btb_update_bits_pc[13];
  assign BTB_io_btb_update_bits_pc[12] = io_cpu_btb_update_bits_pc[12];
  assign BTB_io_btb_update_bits_pc[11] = io_cpu_btb_update_bits_pc[11];
  assign BTB_io_btb_update_bits_pc[10] = io_cpu_btb_update_bits_pc[10];
  assign BTB_io_btb_update_bits_pc[9] = io_cpu_btb_update_bits_pc[9];
  assign BTB_io_btb_update_bits_pc[8] = io_cpu_btb_update_bits_pc[8];
  assign BTB_io_btb_update_bits_pc[7] = io_cpu_btb_update_bits_pc[7];
  assign BTB_io_btb_update_bits_pc[6] = io_cpu_btb_update_bits_pc[6];
  assign BTB_io_btb_update_bits_pc[5] = io_cpu_btb_update_bits_pc[5];
  assign BTB_io_btb_update_bits_pc[4] = io_cpu_btb_update_bits_pc[4];
  assign BTB_io_btb_update_bits_pc[3] = io_cpu_btb_update_bits_pc[3];
  assign BTB_io_btb_update_bits_pc[2] = io_cpu_btb_update_bits_pc[2];
  assign BTB_io_btb_update_bits_pc[1] = io_cpu_btb_update_bits_pc[1];
  assign BTB_io_btb_update_bits_pc[0] = io_cpu_btb_update_bits_pc[0];
  assign BTB_io_btb_update_bits_target[38] = io_cpu_btb_update_bits_target[38];
  assign BTB_io_btb_update_bits_target[37] = io_cpu_btb_update_bits_target[37];
  assign BTB_io_btb_update_bits_target[36] = io_cpu_btb_update_bits_target[36];
  assign BTB_io_btb_update_bits_target[35] = io_cpu_btb_update_bits_target[35];
  assign BTB_io_btb_update_bits_target[34] = io_cpu_btb_update_bits_target[34];
  assign BTB_io_btb_update_bits_target[33] = io_cpu_btb_update_bits_target[33];
  assign BTB_io_btb_update_bits_target[32] = io_cpu_btb_update_bits_target[32];
  assign BTB_io_btb_update_bits_target[31] = io_cpu_btb_update_bits_target[31];
  assign BTB_io_btb_update_bits_target[30] = io_cpu_btb_update_bits_target[30];
  assign BTB_io_btb_update_bits_target[29] = io_cpu_btb_update_bits_target[29];
  assign BTB_io_btb_update_bits_target[28] = io_cpu_btb_update_bits_target[28];
  assign BTB_io_btb_update_bits_target[27] = io_cpu_btb_update_bits_target[27];
  assign BTB_io_btb_update_bits_target[26] = io_cpu_btb_update_bits_target[26];
  assign BTB_io_btb_update_bits_target[25] = io_cpu_btb_update_bits_target[25];
  assign BTB_io_btb_update_bits_target[24] = io_cpu_btb_update_bits_target[24];
  assign BTB_io_btb_update_bits_target[23] = io_cpu_btb_update_bits_target[23];
  assign BTB_io_btb_update_bits_target[22] = io_cpu_btb_update_bits_target[22];
  assign BTB_io_btb_update_bits_target[21] = io_cpu_btb_update_bits_target[21];
  assign BTB_io_btb_update_bits_target[20] = io_cpu_btb_update_bits_target[20];
  assign BTB_io_btb_update_bits_target[19] = io_cpu_btb_update_bits_target[19];
  assign BTB_io_btb_update_bits_target[18] = io_cpu_btb_update_bits_target[18];
  assign BTB_io_btb_update_bits_target[17] = io_cpu_btb_update_bits_target[17];
  assign BTB_io_btb_update_bits_target[16] = io_cpu_btb_update_bits_target[16];
  assign BTB_io_btb_update_bits_target[15] = io_cpu_btb_update_bits_target[15];
  assign BTB_io_btb_update_bits_target[14] = io_cpu_btb_update_bits_target[14];
  assign BTB_io_btb_update_bits_target[13] = io_cpu_btb_update_bits_target[13];
  assign BTB_io_btb_update_bits_target[12] = io_cpu_btb_update_bits_target[12];
  assign BTB_io_btb_update_bits_target[11] = io_cpu_btb_update_bits_target[11];
  assign BTB_io_btb_update_bits_target[10] = io_cpu_btb_update_bits_target[10];
  assign BTB_io_btb_update_bits_target[9] = io_cpu_btb_update_bits_target[9];
  assign BTB_io_btb_update_bits_target[8] = io_cpu_btb_update_bits_target[8];
  assign BTB_io_btb_update_bits_target[7] = io_cpu_btb_update_bits_target[7];
  assign BTB_io_btb_update_bits_target[6] = io_cpu_btb_update_bits_target[6];
  assign BTB_io_btb_update_bits_target[5] = io_cpu_btb_update_bits_target[5];
  assign BTB_io_btb_update_bits_target[4] = io_cpu_btb_update_bits_target[4];
  assign BTB_io_btb_update_bits_target[3] = io_cpu_btb_update_bits_target[3];
  assign BTB_io_btb_update_bits_target[2] = io_cpu_btb_update_bits_target[2];
  assign BTB_io_btb_update_bits_target[1] = io_cpu_btb_update_bits_target[1];
  assign BTB_io_btb_update_bits_target[0] = io_cpu_btb_update_bits_target[0];
  assign BTB_io_btb_update_bits_taken = io_cpu_btb_update_bits_taken;
  assign BTB_io_btb_update_bits_isValid = io_cpu_btb_update_bits_isValid;
  assign BTB_io_btb_update_bits_isJump = io_cpu_btb_update_bits_isJump;
  assign BTB_io_btb_update_bits_isReturn = io_cpu_btb_update_bits_isReturn;
  assign BTB_io_btb_update_bits_br_pc[38] = io_cpu_btb_update_bits_br_pc[38];
  assign BTB_io_btb_update_bits_br_pc[37] = io_cpu_btb_update_bits_br_pc[37];
  assign BTB_io_btb_update_bits_br_pc[36] = io_cpu_btb_update_bits_br_pc[36];
  assign BTB_io_btb_update_bits_br_pc[35] = io_cpu_btb_update_bits_br_pc[35];
  assign BTB_io_btb_update_bits_br_pc[34] = io_cpu_btb_update_bits_br_pc[34];
  assign BTB_io_btb_update_bits_br_pc[33] = io_cpu_btb_update_bits_br_pc[33];
  assign BTB_io_btb_update_bits_br_pc[32] = io_cpu_btb_update_bits_br_pc[32];
  assign BTB_io_btb_update_bits_br_pc[31] = io_cpu_btb_update_bits_br_pc[31];
  assign BTB_io_btb_update_bits_br_pc[30] = io_cpu_btb_update_bits_br_pc[30];
  assign BTB_io_btb_update_bits_br_pc[29] = io_cpu_btb_update_bits_br_pc[29];
  assign BTB_io_btb_update_bits_br_pc[28] = io_cpu_btb_update_bits_br_pc[28];
  assign BTB_io_btb_update_bits_br_pc[27] = io_cpu_btb_update_bits_br_pc[27];
  assign BTB_io_btb_update_bits_br_pc[26] = io_cpu_btb_update_bits_br_pc[26];
  assign BTB_io_btb_update_bits_br_pc[25] = io_cpu_btb_update_bits_br_pc[25];
  assign BTB_io_btb_update_bits_br_pc[24] = io_cpu_btb_update_bits_br_pc[24];
  assign BTB_io_btb_update_bits_br_pc[23] = io_cpu_btb_update_bits_br_pc[23];
  assign BTB_io_btb_update_bits_br_pc[22] = io_cpu_btb_update_bits_br_pc[22];
  assign BTB_io_btb_update_bits_br_pc[21] = io_cpu_btb_update_bits_br_pc[21];
  assign BTB_io_btb_update_bits_br_pc[20] = io_cpu_btb_update_bits_br_pc[20];
  assign BTB_io_btb_update_bits_br_pc[19] = io_cpu_btb_update_bits_br_pc[19];
  assign BTB_io_btb_update_bits_br_pc[18] = io_cpu_btb_update_bits_br_pc[18];
  assign BTB_io_btb_update_bits_br_pc[17] = io_cpu_btb_update_bits_br_pc[17];
  assign BTB_io_btb_update_bits_br_pc[16] = io_cpu_btb_update_bits_br_pc[16];
  assign BTB_io_btb_update_bits_br_pc[15] = io_cpu_btb_update_bits_br_pc[15];
  assign BTB_io_btb_update_bits_br_pc[14] = io_cpu_btb_update_bits_br_pc[14];
  assign BTB_io_btb_update_bits_br_pc[13] = io_cpu_btb_update_bits_br_pc[13];
  assign BTB_io_btb_update_bits_br_pc[12] = io_cpu_btb_update_bits_br_pc[12];
  assign BTB_io_btb_update_bits_br_pc[11] = io_cpu_btb_update_bits_br_pc[11];
  assign BTB_io_btb_update_bits_br_pc[10] = io_cpu_btb_update_bits_br_pc[10];
  assign BTB_io_btb_update_bits_br_pc[9] = io_cpu_btb_update_bits_br_pc[9];
  assign BTB_io_btb_update_bits_br_pc[8] = io_cpu_btb_update_bits_br_pc[8];
  assign BTB_io_btb_update_bits_br_pc[7] = io_cpu_btb_update_bits_br_pc[7];
  assign BTB_io_btb_update_bits_br_pc[6] = io_cpu_btb_update_bits_br_pc[6];
  assign BTB_io_btb_update_bits_br_pc[5] = io_cpu_btb_update_bits_br_pc[5];
  assign BTB_io_btb_update_bits_br_pc[4] = io_cpu_btb_update_bits_br_pc[4];
  assign BTB_io_btb_update_bits_br_pc[3] = io_cpu_btb_update_bits_br_pc[3];
  assign BTB_io_btb_update_bits_br_pc[2] = io_cpu_btb_update_bits_br_pc[2];
  assign BTB_io_btb_update_bits_br_pc[1] = io_cpu_btb_update_bits_br_pc[1];
  assign BTB_io_btb_update_bits_br_pc[0] = io_cpu_btb_update_bits_br_pc[0];
  assign BTB_io_bht_update_valid = io_cpu_bht_update_valid;
  assign BTB_io_bht_update_bits_prediction_valid = io_cpu_bht_update_bits_prediction_valid;
  assign BTB_io_bht_update_bits_prediction_bits_taken = io_cpu_bht_update_bits_prediction_bits_taken;
  assign BTB_io_bht_update_bits_prediction_bits_mask = io_cpu_bht_update_bits_prediction_bits_mask;
  assign BTB_io_bht_update_bits_prediction_bits_bridx = io_cpu_bht_update_bits_prediction_bits_bridx;
  assign BTB_io_bht_update_bits_prediction_bits_target[38] = io_cpu_bht_update_bits_prediction_bits_target[38];
  assign BTB_io_bht_update_bits_prediction_bits_target[37] = io_cpu_bht_update_bits_prediction_bits_target[37];
  assign BTB_io_bht_update_bits_prediction_bits_target[36] = io_cpu_bht_update_bits_prediction_bits_target[36];
  assign BTB_io_bht_update_bits_prediction_bits_target[35] = io_cpu_bht_update_bits_prediction_bits_target[35];
  assign BTB_io_bht_update_bits_prediction_bits_target[34] = io_cpu_bht_update_bits_prediction_bits_target[34];
  assign BTB_io_bht_update_bits_prediction_bits_target[33] = io_cpu_bht_update_bits_prediction_bits_target[33];
  assign BTB_io_bht_update_bits_prediction_bits_target[32] = io_cpu_bht_update_bits_prediction_bits_target[32];
  assign BTB_io_bht_update_bits_prediction_bits_target[31] = io_cpu_bht_update_bits_prediction_bits_target[31];
  assign BTB_io_bht_update_bits_prediction_bits_target[30] = io_cpu_bht_update_bits_prediction_bits_target[30];
  assign BTB_io_bht_update_bits_prediction_bits_target[29] = io_cpu_bht_update_bits_prediction_bits_target[29];
  assign BTB_io_bht_update_bits_prediction_bits_target[28] = io_cpu_bht_update_bits_prediction_bits_target[28];
  assign BTB_io_bht_update_bits_prediction_bits_target[27] = io_cpu_bht_update_bits_prediction_bits_target[27];
  assign BTB_io_bht_update_bits_prediction_bits_target[26] = io_cpu_bht_update_bits_prediction_bits_target[26];
  assign BTB_io_bht_update_bits_prediction_bits_target[25] = io_cpu_bht_update_bits_prediction_bits_target[25];
  assign BTB_io_bht_update_bits_prediction_bits_target[24] = io_cpu_bht_update_bits_prediction_bits_target[24];
  assign BTB_io_bht_update_bits_prediction_bits_target[23] = io_cpu_bht_update_bits_prediction_bits_target[23];
  assign BTB_io_bht_update_bits_prediction_bits_target[22] = io_cpu_bht_update_bits_prediction_bits_target[22];
  assign BTB_io_bht_update_bits_prediction_bits_target[21] = io_cpu_bht_update_bits_prediction_bits_target[21];
  assign BTB_io_bht_update_bits_prediction_bits_target[20] = io_cpu_bht_update_bits_prediction_bits_target[20];
  assign BTB_io_bht_update_bits_prediction_bits_target[19] = io_cpu_bht_update_bits_prediction_bits_target[19];
  assign BTB_io_bht_update_bits_prediction_bits_target[18] = io_cpu_bht_update_bits_prediction_bits_target[18];
  assign BTB_io_bht_update_bits_prediction_bits_target[17] = io_cpu_bht_update_bits_prediction_bits_target[17];
  assign BTB_io_bht_update_bits_prediction_bits_target[16] = io_cpu_bht_update_bits_prediction_bits_target[16];
  assign BTB_io_bht_update_bits_prediction_bits_target[15] = io_cpu_bht_update_bits_prediction_bits_target[15];
  assign BTB_io_bht_update_bits_prediction_bits_target[14] = io_cpu_bht_update_bits_prediction_bits_target[14];
  assign BTB_io_bht_update_bits_prediction_bits_target[13] = io_cpu_bht_update_bits_prediction_bits_target[13];
  assign BTB_io_bht_update_bits_prediction_bits_target[12] = io_cpu_bht_update_bits_prediction_bits_target[12];
  assign BTB_io_bht_update_bits_prediction_bits_target[11] = io_cpu_bht_update_bits_prediction_bits_target[11];
  assign BTB_io_bht_update_bits_prediction_bits_target[10] = io_cpu_bht_update_bits_prediction_bits_target[10];
  assign BTB_io_bht_update_bits_prediction_bits_target[9] = io_cpu_bht_update_bits_prediction_bits_target[9];
  assign BTB_io_bht_update_bits_prediction_bits_target[8] = io_cpu_bht_update_bits_prediction_bits_target[8];
  assign BTB_io_bht_update_bits_prediction_bits_target[7] = io_cpu_bht_update_bits_prediction_bits_target[7];
  assign BTB_io_bht_update_bits_prediction_bits_target[6] = io_cpu_bht_update_bits_prediction_bits_target[6];
  assign BTB_io_bht_update_bits_prediction_bits_target[5] = io_cpu_bht_update_bits_prediction_bits_target[5];
  assign BTB_io_bht_update_bits_prediction_bits_target[4] = io_cpu_bht_update_bits_prediction_bits_target[4];
  assign BTB_io_bht_update_bits_prediction_bits_target[3] = io_cpu_bht_update_bits_prediction_bits_target[3];
  assign BTB_io_bht_update_bits_prediction_bits_target[2] = io_cpu_bht_update_bits_prediction_bits_target[2];
  assign BTB_io_bht_update_bits_prediction_bits_target[1] = io_cpu_bht_update_bits_prediction_bits_target[1];
  assign BTB_io_bht_update_bits_prediction_bits_target[0] = io_cpu_bht_update_bits_prediction_bits_target[0];
  assign BTB_io_bht_update_bits_prediction_bits_entry[5] = io_cpu_bht_update_bits_prediction_bits_entry[5];
  assign BTB_io_bht_update_bits_prediction_bits_entry[4] = io_cpu_bht_update_bits_prediction_bits_entry[4];
  assign BTB_io_bht_update_bits_prediction_bits_entry[3] = io_cpu_bht_update_bits_prediction_bits_entry[3];
  assign BTB_io_bht_update_bits_prediction_bits_entry[2] = io_cpu_bht_update_bits_prediction_bits_entry[2];
  assign BTB_io_bht_update_bits_prediction_bits_entry[1] = io_cpu_bht_update_bits_prediction_bits_entry[1];
  assign BTB_io_bht_update_bits_prediction_bits_entry[0] = io_cpu_bht_update_bits_prediction_bits_entry[0];
  assign BTB_io_bht_update_bits_prediction_bits_bht_history[6] = io_cpu_bht_update_bits_prediction_bits_bht_history[6];
  assign BTB_io_bht_update_bits_prediction_bits_bht_history[5] = io_cpu_bht_update_bits_prediction_bits_bht_history[5];
  assign BTB_io_bht_update_bits_prediction_bits_bht_history[4] = io_cpu_bht_update_bits_prediction_bits_bht_history[4];
  assign BTB_io_bht_update_bits_prediction_bits_bht_history[3] = io_cpu_bht_update_bits_prediction_bits_bht_history[3];
  assign BTB_io_bht_update_bits_prediction_bits_bht_history[2] = io_cpu_bht_update_bits_prediction_bits_bht_history[2];
  assign BTB_io_bht_update_bits_prediction_bits_bht_history[1] = io_cpu_bht_update_bits_prediction_bits_bht_history[1];
  assign BTB_io_bht_update_bits_prediction_bits_bht_history[0] = io_cpu_bht_update_bits_prediction_bits_bht_history[0];
  assign BTB_io_bht_update_bits_prediction_bits_bht_value[1] = io_cpu_bht_update_bits_prediction_bits_bht_value[1];
  assign BTB_io_bht_update_bits_prediction_bits_bht_value[0] = io_cpu_bht_update_bits_prediction_bits_bht_value[0];
  assign BTB_io_bht_update_bits_pc[38] = io_cpu_bht_update_bits_pc[38];
  assign BTB_io_bht_update_bits_pc[37] = io_cpu_bht_update_bits_pc[37];
  assign BTB_io_bht_update_bits_pc[36] = io_cpu_bht_update_bits_pc[36];
  assign BTB_io_bht_update_bits_pc[35] = io_cpu_bht_update_bits_pc[35];
  assign BTB_io_bht_update_bits_pc[34] = io_cpu_bht_update_bits_pc[34];
  assign BTB_io_bht_update_bits_pc[33] = io_cpu_bht_update_bits_pc[33];
  assign BTB_io_bht_update_bits_pc[32] = io_cpu_bht_update_bits_pc[32];
  assign BTB_io_bht_update_bits_pc[31] = io_cpu_bht_update_bits_pc[31];
  assign BTB_io_bht_update_bits_pc[30] = io_cpu_bht_update_bits_pc[30];
  assign BTB_io_bht_update_bits_pc[29] = io_cpu_bht_update_bits_pc[29];
  assign BTB_io_bht_update_bits_pc[28] = io_cpu_bht_update_bits_pc[28];
  assign BTB_io_bht_update_bits_pc[27] = io_cpu_bht_update_bits_pc[27];
  assign BTB_io_bht_update_bits_pc[26] = io_cpu_bht_update_bits_pc[26];
  assign BTB_io_bht_update_bits_pc[25] = io_cpu_bht_update_bits_pc[25];
  assign BTB_io_bht_update_bits_pc[24] = io_cpu_bht_update_bits_pc[24];
  assign BTB_io_bht_update_bits_pc[23] = io_cpu_bht_update_bits_pc[23];
  assign BTB_io_bht_update_bits_pc[22] = io_cpu_bht_update_bits_pc[22];
  assign BTB_io_bht_update_bits_pc[21] = io_cpu_bht_update_bits_pc[21];
  assign BTB_io_bht_update_bits_pc[20] = io_cpu_bht_update_bits_pc[20];
  assign BTB_io_bht_update_bits_pc[19] = io_cpu_bht_update_bits_pc[19];
  assign BTB_io_bht_update_bits_pc[18] = io_cpu_bht_update_bits_pc[18];
  assign BTB_io_bht_update_bits_pc[17] = io_cpu_bht_update_bits_pc[17];
  assign BTB_io_bht_update_bits_pc[16] = io_cpu_bht_update_bits_pc[16];
  assign BTB_io_bht_update_bits_pc[15] = io_cpu_bht_update_bits_pc[15];
  assign BTB_io_bht_update_bits_pc[14] = io_cpu_bht_update_bits_pc[14];
  assign BTB_io_bht_update_bits_pc[13] = io_cpu_bht_update_bits_pc[13];
  assign BTB_io_bht_update_bits_pc[12] = io_cpu_bht_update_bits_pc[12];
  assign BTB_io_bht_update_bits_pc[11] = io_cpu_bht_update_bits_pc[11];
  assign BTB_io_bht_update_bits_pc[10] = io_cpu_bht_update_bits_pc[10];
  assign BTB_io_bht_update_bits_pc[9] = io_cpu_bht_update_bits_pc[9];
  assign BTB_io_bht_update_bits_pc[8] = io_cpu_bht_update_bits_pc[8];
  assign BTB_io_bht_update_bits_pc[7] = io_cpu_bht_update_bits_pc[7];
  assign BTB_io_bht_update_bits_pc[6] = io_cpu_bht_update_bits_pc[6];
  assign BTB_io_bht_update_bits_pc[5] = io_cpu_bht_update_bits_pc[5];
  assign BTB_io_bht_update_bits_pc[4] = io_cpu_bht_update_bits_pc[4];
  assign BTB_io_bht_update_bits_pc[3] = io_cpu_bht_update_bits_pc[3];
  assign BTB_io_bht_update_bits_pc[2] = io_cpu_bht_update_bits_pc[2];
  assign BTB_io_bht_update_bits_pc[1] = io_cpu_bht_update_bits_pc[1];
  assign BTB_io_bht_update_bits_pc[0] = io_cpu_bht_update_bits_pc[0];
  assign BTB_io_bht_update_bits_taken = io_cpu_bht_update_bits_taken;
  assign BTB_io_bht_update_bits_mispredict = io_cpu_bht_update_bits_mispredict;
  assign BTB_io_ras_update_valid = io_cpu_ras_update_valid;
  assign BTB_io_ras_update_bits_isCall = io_cpu_ras_update_bits_isCall;
  assign BTB_io_ras_update_bits_isReturn = io_cpu_ras_update_bits_isReturn;
  assign BTB_io_ras_update_bits_returnAddr[38] = io_cpu_ras_update_bits_returnAddr[38];
  assign BTB_io_ras_update_bits_returnAddr[37] = io_cpu_ras_update_bits_returnAddr[37];
  assign BTB_io_ras_update_bits_returnAddr[36] = io_cpu_ras_update_bits_returnAddr[36];
  assign BTB_io_ras_update_bits_returnAddr[35] = io_cpu_ras_update_bits_returnAddr[35];
  assign BTB_io_ras_update_bits_returnAddr[34] = io_cpu_ras_update_bits_returnAddr[34];
  assign BTB_io_ras_update_bits_returnAddr[33] = io_cpu_ras_update_bits_returnAddr[33];
  assign BTB_io_ras_update_bits_returnAddr[32] = io_cpu_ras_update_bits_returnAddr[32];
  assign BTB_io_ras_update_bits_returnAddr[31] = io_cpu_ras_update_bits_returnAddr[31];
  assign BTB_io_ras_update_bits_returnAddr[30] = io_cpu_ras_update_bits_returnAddr[30];
  assign BTB_io_ras_update_bits_returnAddr[29] = io_cpu_ras_update_bits_returnAddr[29];
  assign BTB_io_ras_update_bits_returnAddr[28] = io_cpu_ras_update_bits_returnAddr[28];
  assign BTB_io_ras_update_bits_returnAddr[27] = io_cpu_ras_update_bits_returnAddr[27];
  assign BTB_io_ras_update_bits_returnAddr[26] = io_cpu_ras_update_bits_returnAddr[26];
  assign BTB_io_ras_update_bits_returnAddr[25] = io_cpu_ras_update_bits_returnAddr[25];
  assign BTB_io_ras_update_bits_returnAddr[24] = io_cpu_ras_update_bits_returnAddr[24];
  assign BTB_io_ras_update_bits_returnAddr[23] = io_cpu_ras_update_bits_returnAddr[23];
  assign BTB_io_ras_update_bits_returnAddr[22] = io_cpu_ras_update_bits_returnAddr[22];
  assign BTB_io_ras_update_bits_returnAddr[21] = io_cpu_ras_update_bits_returnAddr[21];
  assign BTB_io_ras_update_bits_returnAddr[20] = io_cpu_ras_update_bits_returnAddr[20];
  assign BTB_io_ras_update_bits_returnAddr[19] = io_cpu_ras_update_bits_returnAddr[19];
  assign BTB_io_ras_update_bits_returnAddr[18] = io_cpu_ras_update_bits_returnAddr[18];
  assign BTB_io_ras_update_bits_returnAddr[17] = io_cpu_ras_update_bits_returnAddr[17];
  assign BTB_io_ras_update_bits_returnAddr[16] = io_cpu_ras_update_bits_returnAddr[16];
  assign BTB_io_ras_update_bits_returnAddr[15] = io_cpu_ras_update_bits_returnAddr[15];
  assign BTB_io_ras_update_bits_returnAddr[14] = io_cpu_ras_update_bits_returnAddr[14];
  assign BTB_io_ras_update_bits_returnAddr[13] = io_cpu_ras_update_bits_returnAddr[13];
  assign BTB_io_ras_update_bits_returnAddr[12] = io_cpu_ras_update_bits_returnAddr[12];
  assign BTB_io_ras_update_bits_returnAddr[11] = io_cpu_ras_update_bits_returnAddr[11];
  assign BTB_io_ras_update_bits_returnAddr[10] = io_cpu_ras_update_bits_returnAddr[10];
  assign BTB_io_ras_update_bits_returnAddr[9] = io_cpu_ras_update_bits_returnAddr[9];
  assign BTB_io_ras_update_bits_returnAddr[8] = io_cpu_ras_update_bits_returnAddr[8];
  assign BTB_io_ras_update_bits_returnAddr[7] = io_cpu_ras_update_bits_returnAddr[7];
  assign BTB_io_ras_update_bits_returnAddr[6] = io_cpu_ras_update_bits_returnAddr[6];
  assign BTB_io_ras_update_bits_returnAddr[5] = io_cpu_ras_update_bits_returnAddr[5];
  assign BTB_io_ras_update_bits_returnAddr[4] = io_cpu_ras_update_bits_returnAddr[4];
  assign BTB_io_ras_update_bits_returnAddr[3] = io_cpu_ras_update_bits_returnAddr[3];
  assign BTB_io_ras_update_bits_returnAddr[2] = io_cpu_ras_update_bits_returnAddr[2];
  assign BTB_io_ras_update_bits_returnAddr[1] = io_cpu_ras_update_bits_returnAddr[1];
  assign BTB_io_ras_update_bits_returnAddr[0] = io_cpu_ras_update_bits_returnAddr[0];
  assign BTB_io_ras_update_bits_prediction_valid = io_cpu_ras_update_bits_prediction_valid;
  assign BTB_io_ras_update_bits_prediction_bits_taken = io_cpu_ras_update_bits_prediction_bits_taken;
  assign BTB_io_ras_update_bits_prediction_bits_mask = io_cpu_ras_update_bits_prediction_bits_mask;
  assign BTB_io_ras_update_bits_prediction_bits_bridx = io_cpu_ras_update_bits_prediction_bits_bridx;
  assign BTB_io_ras_update_bits_prediction_bits_target[38] = io_cpu_ras_update_bits_prediction_bits_target[38];
  assign BTB_io_ras_update_bits_prediction_bits_target[37] = io_cpu_ras_update_bits_prediction_bits_target[37];
  assign BTB_io_ras_update_bits_prediction_bits_target[36] = io_cpu_ras_update_bits_prediction_bits_target[36];
  assign BTB_io_ras_update_bits_prediction_bits_target[35] = io_cpu_ras_update_bits_prediction_bits_target[35];
  assign BTB_io_ras_update_bits_prediction_bits_target[34] = io_cpu_ras_update_bits_prediction_bits_target[34];
  assign BTB_io_ras_update_bits_prediction_bits_target[33] = io_cpu_ras_update_bits_prediction_bits_target[33];
  assign BTB_io_ras_update_bits_prediction_bits_target[32] = io_cpu_ras_update_bits_prediction_bits_target[32];
  assign BTB_io_ras_update_bits_prediction_bits_target[31] = io_cpu_ras_update_bits_prediction_bits_target[31];
  assign BTB_io_ras_update_bits_prediction_bits_target[30] = io_cpu_ras_update_bits_prediction_bits_target[30];
  assign BTB_io_ras_update_bits_prediction_bits_target[29] = io_cpu_ras_update_bits_prediction_bits_target[29];
  assign BTB_io_ras_update_bits_prediction_bits_target[28] = io_cpu_ras_update_bits_prediction_bits_target[28];
  assign BTB_io_ras_update_bits_prediction_bits_target[27] = io_cpu_ras_update_bits_prediction_bits_target[27];
  assign BTB_io_ras_update_bits_prediction_bits_target[26] = io_cpu_ras_update_bits_prediction_bits_target[26];
  assign BTB_io_ras_update_bits_prediction_bits_target[25] = io_cpu_ras_update_bits_prediction_bits_target[25];
  assign BTB_io_ras_update_bits_prediction_bits_target[24] = io_cpu_ras_update_bits_prediction_bits_target[24];
  assign BTB_io_ras_update_bits_prediction_bits_target[23] = io_cpu_ras_update_bits_prediction_bits_target[23];
  assign BTB_io_ras_update_bits_prediction_bits_target[22] = io_cpu_ras_update_bits_prediction_bits_target[22];
  assign BTB_io_ras_update_bits_prediction_bits_target[21] = io_cpu_ras_update_bits_prediction_bits_target[21];
  assign BTB_io_ras_update_bits_prediction_bits_target[20] = io_cpu_ras_update_bits_prediction_bits_target[20];
  assign BTB_io_ras_update_bits_prediction_bits_target[19] = io_cpu_ras_update_bits_prediction_bits_target[19];
  assign BTB_io_ras_update_bits_prediction_bits_target[18] = io_cpu_ras_update_bits_prediction_bits_target[18];
  assign BTB_io_ras_update_bits_prediction_bits_target[17] = io_cpu_ras_update_bits_prediction_bits_target[17];
  assign BTB_io_ras_update_bits_prediction_bits_target[16] = io_cpu_ras_update_bits_prediction_bits_target[16];
  assign BTB_io_ras_update_bits_prediction_bits_target[15] = io_cpu_ras_update_bits_prediction_bits_target[15];
  assign BTB_io_ras_update_bits_prediction_bits_target[14] = io_cpu_ras_update_bits_prediction_bits_target[14];
  assign BTB_io_ras_update_bits_prediction_bits_target[13] = io_cpu_ras_update_bits_prediction_bits_target[13];
  assign BTB_io_ras_update_bits_prediction_bits_target[12] = io_cpu_ras_update_bits_prediction_bits_target[12];
  assign BTB_io_ras_update_bits_prediction_bits_target[11] = io_cpu_ras_update_bits_prediction_bits_target[11];
  assign BTB_io_ras_update_bits_prediction_bits_target[10] = io_cpu_ras_update_bits_prediction_bits_target[10];
  assign BTB_io_ras_update_bits_prediction_bits_target[9] = io_cpu_ras_update_bits_prediction_bits_target[9];
  assign BTB_io_ras_update_bits_prediction_bits_target[8] = io_cpu_ras_update_bits_prediction_bits_target[8];
  assign BTB_io_ras_update_bits_prediction_bits_target[7] = io_cpu_ras_update_bits_prediction_bits_target[7];
  assign BTB_io_ras_update_bits_prediction_bits_target[6] = io_cpu_ras_update_bits_prediction_bits_target[6];
  assign BTB_io_ras_update_bits_prediction_bits_target[5] = io_cpu_ras_update_bits_prediction_bits_target[5];
  assign BTB_io_ras_update_bits_prediction_bits_target[4] = io_cpu_ras_update_bits_prediction_bits_target[4];
  assign BTB_io_ras_update_bits_prediction_bits_target[3] = io_cpu_ras_update_bits_prediction_bits_target[3];
  assign BTB_io_ras_update_bits_prediction_bits_target[2] = io_cpu_ras_update_bits_prediction_bits_target[2];
  assign BTB_io_ras_update_bits_prediction_bits_target[1] = io_cpu_ras_update_bits_prediction_bits_target[1];
  assign BTB_io_ras_update_bits_prediction_bits_target[0] = io_cpu_ras_update_bits_prediction_bits_target[0];
  assign BTB_io_ras_update_bits_prediction_bits_entry[5] = io_cpu_ras_update_bits_prediction_bits_entry[5];
  assign BTB_io_ras_update_bits_prediction_bits_entry[4] = io_cpu_ras_update_bits_prediction_bits_entry[4];
  assign BTB_io_ras_update_bits_prediction_bits_entry[3] = io_cpu_ras_update_bits_prediction_bits_entry[3];
  assign BTB_io_ras_update_bits_prediction_bits_entry[2] = io_cpu_ras_update_bits_prediction_bits_entry[2];
  assign BTB_io_ras_update_bits_prediction_bits_entry[1] = io_cpu_ras_update_bits_prediction_bits_entry[1];
  assign BTB_io_ras_update_bits_prediction_bits_entry[0] = io_cpu_ras_update_bits_prediction_bits_entry[0];
  assign BTB_io_ras_update_bits_prediction_bits_bht_history[6] = io_cpu_ras_update_bits_prediction_bits_bht_history[6];
  assign BTB_io_ras_update_bits_prediction_bits_bht_history[5] = io_cpu_ras_update_bits_prediction_bits_bht_history[5];
  assign BTB_io_ras_update_bits_prediction_bits_bht_history[4] = io_cpu_ras_update_bits_prediction_bits_bht_history[4];
  assign BTB_io_ras_update_bits_prediction_bits_bht_history[3] = io_cpu_ras_update_bits_prediction_bits_bht_history[3];
  assign BTB_io_ras_update_bits_prediction_bits_bht_history[2] = io_cpu_ras_update_bits_prediction_bits_bht_history[2];
  assign BTB_io_ras_update_bits_prediction_bits_bht_history[1] = io_cpu_ras_update_bits_prediction_bits_bht_history[1];
  assign BTB_io_ras_update_bits_prediction_bits_bht_history[0] = io_cpu_ras_update_bits_prediction_bits_bht_history[0];
  assign BTB_io_ras_update_bits_prediction_bits_bht_value[1] = io_cpu_ras_update_bits_prediction_bits_bht_value[1];
  assign BTB_io_ras_update_bits_prediction_bits_bht_value[0] = io_cpu_ras_update_bits_prediction_bits_bht_value[0];
  assign icache_io_invalidate = io_cpu_flush_icache;
  assign icache_io_req_bits_addr[37] = io_cpu_npc[37];
  assign icache_io_req_bits_addr[36] = io_cpu_npc[36];
  assign icache_io_req_bits_addr[35] = io_cpu_npc[35];
  assign icache_io_req_bits_addr[33] = io_cpu_npc[33];
  assign icache_io_req_bits_addr[20] = io_cpu_npc[20];
  assign icache_io_req_bits_addr[18] = io_cpu_npc[18];
  assign icache_io_req_bits_addr[17] = io_cpu_npc[17];
  assign icache_io_req_bits_addr[13] = io_cpu_npc[13];
  assign icache_io_req_bits_addr[10] = io_cpu_npc[10];
  assign icache_io_req_bits_addr[8] = io_cpu_npc[8];
  assign icache_io_req_bits_addr[7] = io_cpu_npc[7];
  assign tlb_io_ptw_req_ready = io_ptw_req_ready;
  assign tlb_io_ptw_resp_valid = io_ptw_resp_valid;
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[15] = io_ptw_resp_bits_pte_reserved_for_hardware[15];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[14] = io_ptw_resp_bits_pte_reserved_for_hardware[14];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[13] = io_ptw_resp_bits_pte_reserved_for_hardware[13];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[12] = io_ptw_resp_bits_pte_reserved_for_hardware[12];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[11] = io_ptw_resp_bits_pte_reserved_for_hardware[11];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[10] = io_ptw_resp_bits_pte_reserved_for_hardware[10];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[9] = io_ptw_resp_bits_pte_reserved_for_hardware[9];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[8] = io_ptw_resp_bits_pte_reserved_for_hardware[8];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[7] = io_ptw_resp_bits_pte_reserved_for_hardware[7];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[6] = io_ptw_resp_bits_pte_reserved_for_hardware[6];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[5] = io_ptw_resp_bits_pte_reserved_for_hardware[5];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[4] = io_ptw_resp_bits_pte_reserved_for_hardware[4];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[3] = io_ptw_resp_bits_pte_reserved_for_hardware[3];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[2] = io_ptw_resp_bits_pte_reserved_for_hardware[2];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[1] = io_ptw_resp_bits_pte_reserved_for_hardware[1];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware[0] = io_ptw_resp_bits_pte_reserved_for_hardware[0];
  assign tlb_io_ptw_resp_bits_pte_ppn[37] = io_ptw_resp_bits_pte_ppn[37];
  assign tlb_io_ptw_resp_bits_pte_ppn[36] = io_ptw_resp_bits_pte_ppn[36];
  assign tlb_io_ptw_resp_bits_pte_ppn[35] = io_ptw_resp_bits_pte_ppn[35];
  assign tlb_io_ptw_resp_bits_pte_ppn[34] = io_ptw_resp_bits_pte_ppn[34];
  assign tlb_io_ptw_resp_bits_pte_ppn[33] = io_ptw_resp_bits_pte_ppn[33];
  assign tlb_io_ptw_resp_bits_pte_ppn[32] = io_ptw_resp_bits_pte_ppn[32];
  assign tlb_io_ptw_resp_bits_pte_ppn[31] = io_ptw_resp_bits_pte_ppn[31];
  assign tlb_io_ptw_resp_bits_pte_ppn[30] = io_ptw_resp_bits_pte_ppn[30];
  assign tlb_io_ptw_resp_bits_pte_ppn[29] = io_ptw_resp_bits_pte_ppn[29];
  assign tlb_io_ptw_resp_bits_pte_ppn[28] = io_ptw_resp_bits_pte_ppn[28];
  assign tlb_io_ptw_resp_bits_pte_ppn[27] = io_ptw_resp_bits_pte_ppn[27];
  assign tlb_io_ptw_resp_bits_pte_ppn[26] = io_ptw_resp_bits_pte_ppn[26];
  assign tlb_io_ptw_resp_bits_pte_ppn[25] = io_ptw_resp_bits_pte_ppn[25];
  assign tlb_io_ptw_resp_bits_pte_ppn[24] = io_ptw_resp_bits_pte_ppn[24];
  assign tlb_io_ptw_resp_bits_pte_ppn[23] = io_ptw_resp_bits_pte_ppn[23];
  assign tlb_io_ptw_resp_bits_pte_ppn[22] = io_ptw_resp_bits_pte_ppn[22];
  assign tlb_io_ptw_resp_bits_pte_ppn[21] = io_ptw_resp_bits_pte_ppn[21];
  assign tlb_io_ptw_resp_bits_pte_ppn[20] = io_ptw_resp_bits_pte_ppn[20];
  assign tlb_io_ptw_resp_bits_pte_ppn[19] = io_ptw_resp_bits_pte_ppn[19];
  assign tlb_io_ptw_resp_bits_pte_ppn[18] = io_ptw_resp_bits_pte_ppn[18];
  assign tlb_io_ptw_resp_bits_pte_ppn[17] = io_ptw_resp_bits_pte_ppn[17];
  assign tlb_io_ptw_resp_bits_pte_ppn[16] = io_ptw_resp_bits_pte_ppn[16];
  assign tlb_io_ptw_resp_bits_pte_ppn[15] = io_ptw_resp_bits_pte_ppn[15];
  assign tlb_io_ptw_resp_bits_pte_ppn[14] = io_ptw_resp_bits_pte_ppn[14];
  assign tlb_io_ptw_resp_bits_pte_ppn[13] = io_ptw_resp_bits_pte_ppn[13];
  assign tlb_io_ptw_resp_bits_pte_ppn[12] = io_ptw_resp_bits_pte_ppn[12];
  assign tlb_io_ptw_resp_bits_pte_ppn[11] = io_ptw_resp_bits_pte_ppn[11];
  assign tlb_io_ptw_resp_bits_pte_ppn[10] = io_ptw_resp_bits_pte_ppn[10];
  assign tlb_io_ptw_resp_bits_pte_ppn[9] = io_ptw_resp_bits_pte_ppn[9];
  assign tlb_io_ptw_resp_bits_pte_ppn[8] = io_ptw_resp_bits_pte_ppn[8];
  assign tlb_io_ptw_resp_bits_pte_ppn[7] = io_ptw_resp_bits_pte_ppn[7];
  assign tlb_io_ptw_resp_bits_pte_ppn[6] = io_ptw_resp_bits_pte_ppn[6];
  assign tlb_io_ptw_resp_bits_pte_ppn[5] = io_ptw_resp_bits_pte_ppn[5];
  assign tlb_io_ptw_resp_bits_pte_ppn[4] = io_ptw_resp_bits_pte_ppn[4];
  assign tlb_io_ptw_resp_bits_pte_ppn[3] = io_ptw_resp_bits_pte_ppn[3];
  assign tlb_io_ptw_resp_bits_pte_ppn[2] = io_ptw_resp_bits_pte_ppn[2];
  assign tlb_io_ptw_resp_bits_pte_ppn[1] = io_ptw_resp_bits_pte_ppn[1];
  assign tlb_io_ptw_resp_bits_pte_ppn[0] = io_ptw_resp_bits_pte_ppn[0];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_software[1] = io_ptw_resp_bits_pte_reserved_for_software[1];
  assign tlb_io_ptw_resp_bits_pte_reserved_for_software[0] = io_ptw_resp_bits_pte_reserved_for_software[0];
  assign tlb_io_ptw_resp_bits_pte_d = io_ptw_resp_bits_pte_d;
  assign tlb_io_ptw_resp_bits_pte_a = io_ptw_resp_bits_pte_a;
  assign tlb_io_ptw_resp_bits_pte_g = io_ptw_resp_bits_pte_g;
  assign tlb_io_ptw_resp_bits_pte_u = io_ptw_resp_bits_pte_u;
  assign tlb_io_ptw_resp_bits_pte_x = io_ptw_resp_bits_pte_x;
  assign tlb_io_ptw_resp_bits_pte_w = io_ptw_resp_bits_pte_w;
  assign tlb_io_ptw_resp_bits_pte_r = io_ptw_resp_bits_pte_r;
  assign tlb_io_ptw_resp_bits_pte_v = io_ptw_resp_bits_pte_v;
  assign tlb_io_ptw_ptbr_asid[6] = io_ptw_ptbr_asid[6];
  assign tlb_io_ptw_ptbr_asid[5] = io_ptw_ptbr_asid[5];
  assign tlb_io_ptw_ptbr_asid[4] = io_ptw_ptbr_asid[4];
  assign tlb_io_ptw_ptbr_asid[3] = io_ptw_ptbr_asid[3];
  assign tlb_io_ptw_ptbr_asid[2] = io_ptw_ptbr_asid[2];
  assign tlb_io_ptw_ptbr_asid[1] = io_ptw_ptbr_asid[1];
  assign tlb_io_ptw_ptbr_asid[0] = io_ptw_ptbr_asid[0];
  assign tlb_io_ptw_ptbr_ppn[37] = io_ptw_ptbr_ppn[37];
  assign tlb_io_ptw_ptbr_ppn[36] = io_ptw_ptbr_ppn[36];
  assign tlb_io_ptw_ptbr_ppn[35] = io_ptw_ptbr_ppn[35];
  assign tlb_io_ptw_ptbr_ppn[34] = io_ptw_ptbr_ppn[34];
  assign tlb_io_ptw_ptbr_ppn[33] = io_ptw_ptbr_ppn[33];
  assign tlb_io_ptw_ptbr_ppn[32] = io_ptw_ptbr_ppn[32];
  assign tlb_io_ptw_ptbr_ppn[31] = io_ptw_ptbr_ppn[31];
  assign tlb_io_ptw_ptbr_ppn[30] = io_ptw_ptbr_ppn[30];
  assign tlb_io_ptw_ptbr_ppn[29] = io_ptw_ptbr_ppn[29];
  assign tlb_io_ptw_ptbr_ppn[28] = io_ptw_ptbr_ppn[28];
  assign tlb_io_ptw_ptbr_ppn[27] = io_ptw_ptbr_ppn[27];
  assign tlb_io_ptw_ptbr_ppn[26] = io_ptw_ptbr_ppn[26];
  assign tlb_io_ptw_ptbr_ppn[25] = io_ptw_ptbr_ppn[25];
  assign tlb_io_ptw_ptbr_ppn[24] = io_ptw_ptbr_ppn[24];
  assign tlb_io_ptw_ptbr_ppn[23] = io_ptw_ptbr_ppn[23];
  assign tlb_io_ptw_ptbr_ppn[22] = io_ptw_ptbr_ppn[22];
  assign tlb_io_ptw_ptbr_ppn[21] = io_ptw_ptbr_ppn[21];
  assign tlb_io_ptw_ptbr_ppn[20] = io_ptw_ptbr_ppn[20];
  assign tlb_io_ptw_ptbr_ppn[19] = io_ptw_ptbr_ppn[19];
  assign tlb_io_ptw_ptbr_ppn[18] = io_ptw_ptbr_ppn[18];
  assign tlb_io_ptw_ptbr_ppn[17] = io_ptw_ptbr_ppn[17];
  assign tlb_io_ptw_ptbr_ppn[16] = io_ptw_ptbr_ppn[16];
  assign tlb_io_ptw_ptbr_ppn[15] = io_ptw_ptbr_ppn[15];
  assign tlb_io_ptw_ptbr_ppn[14] = io_ptw_ptbr_ppn[14];
  assign tlb_io_ptw_ptbr_ppn[13] = io_ptw_ptbr_ppn[13];
  assign tlb_io_ptw_ptbr_ppn[12] = io_ptw_ptbr_ppn[12];
  assign tlb_io_ptw_ptbr_ppn[11] = io_ptw_ptbr_ppn[11];
  assign tlb_io_ptw_ptbr_ppn[10] = io_ptw_ptbr_ppn[10];
  assign tlb_io_ptw_ptbr_ppn[9] = io_ptw_ptbr_ppn[9];
  assign tlb_io_ptw_ptbr_ppn[8] = io_ptw_ptbr_ppn[8];
  assign tlb_io_ptw_ptbr_ppn[7] = io_ptw_ptbr_ppn[7];
  assign tlb_io_ptw_ptbr_ppn[6] = io_ptw_ptbr_ppn[6];
  assign tlb_io_ptw_ptbr_ppn[5] = io_ptw_ptbr_ppn[5];
  assign tlb_io_ptw_ptbr_ppn[4] = io_ptw_ptbr_ppn[4];
  assign tlb_io_ptw_ptbr_ppn[3] = io_ptw_ptbr_ppn[3];
  assign tlb_io_ptw_ptbr_ppn[2] = io_ptw_ptbr_ppn[2];
  assign tlb_io_ptw_ptbr_ppn[1] = io_ptw_ptbr_ppn[1];
  assign tlb_io_ptw_ptbr_ppn[0] = io_ptw_ptbr_ppn[0];
  assign tlb_io_ptw_invalidate = io_ptw_invalidate;
  assign tlb_io_ptw_status_debug = io_ptw_status_debug;
  assign tlb_io_ptw_status_isa[31] = io_ptw_status_isa[31];
  assign tlb_io_ptw_status_isa[30] = io_ptw_status_isa[30];
  assign tlb_io_ptw_status_isa[29] = io_ptw_status_isa[29];
  assign tlb_io_ptw_status_isa[28] = io_ptw_status_isa[28];
  assign tlb_io_ptw_status_isa[27] = io_ptw_status_isa[27];
  assign tlb_io_ptw_status_isa[26] = io_ptw_status_isa[26];
  assign tlb_io_ptw_status_isa[25] = io_ptw_status_isa[25];
  assign tlb_io_ptw_status_isa[24] = io_ptw_status_isa[24];
  assign tlb_io_ptw_status_isa[23] = io_ptw_status_isa[23];
  assign tlb_io_ptw_status_isa[22] = io_ptw_status_isa[22];
  assign tlb_io_ptw_status_isa[21] = io_ptw_status_isa[21];
  assign tlb_io_ptw_status_isa[20] = io_ptw_status_isa[20];
  assign tlb_io_ptw_status_isa[19] = io_ptw_status_isa[19];
  assign tlb_io_ptw_status_isa[18] = io_ptw_status_isa[18];
  assign tlb_io_ptw_status_isa[17] = io_ptw_status_isa[17];
  assign tlb_io_ptw_status_isa[16] = io_ptw_status_isa[16];
  assign tlb_io_ptw_status_isa[15] = io_ptw_status_isa[15];
  assign tlb_io_ptw_status_isa[14] = io_ptw_status_isa[14];
  assign tlb_io_ptw_status_isa[13] = io_ptw_status_isa[13];
  assign tlb_io_ptw_status_isa[12] = io_ptw_status_isa[12];
  assign tlb_io_ptw_status_isa[11] = io_ptw_status_isa[11];
  assign tlb_io_ptw_status_isa[10] = io_ptw_status_isa[10];
  assign tlb_io_ptw_status_isa[9] = io_ptw_status_isa[9];
  assign tlb_io_ptw_status_isa[8] = io_ptw_status_isa[8];
  assign tlb_io_ptw_status_isa[7] = io_ptw_status_isa[7];
  assign tlb_io_ptw_status_isa[6] = io_ptw_status_isa[6];
  assign tlb_io_ptw_status_isa[5] = io_ptw_status_isa[5];
  assign tlb_io_ptw_status_isa[4] = io_ptw_status_isa[4];
  assign tlb_io_ptw_status_isa[3] = io_ptw_status_isa[3];
  assign tlb_io_ptw_status_isa[2] = io_ptw_status_isa[2];
  assign tlb_io_ptw_status_isa[1] = io_ptw_status_isa[1];
  assign tlb_io_ptw_status_isa[0] = io_ptw_status_isa[0];
  assign tlb_io_ptw_status_prv[1] = io_ptw_status_prv[1];
  assign tlb_io_ptw_status_prv[0] = io_ptw_status_prv[0];
  assign tlb_io_ptw_status_sd = io_ptw_status_sd;
  assign tlb_io_ptw_status_zero3[30] = io_ptw_status_zero3[30];
  assign tlb_io_ptw_status_zero3[29] = io_ptw_status_zero3[29];
  assign tlb_io_ptw_status_zero3[28] = io_ptw_status_zero3[28];
  assign tlb_io_ptw_status_zero3[27] = io_ptw_status_zero3[27];
  assign tlb_io_ptw_status_zero3[26] = io_ptw_status_zero3[26];
  assign tlb_io_ptw_status_zero3[25] = io_ptw_status_zero3[25];
  assign tlb_io_ptw_status_zero3[24] = io_ptw_status_zero3[24];
  assign tlb_io_ptw_status_zero3[23] = io_ptw_status_zero3[23];
  assign tlb_io_ptw_status_zero3[22] = io_ptw_status_zero3[22];
  assign tlb_io_ptw_status_zero3[21] = io_ptw_status_zero3[21];
  assign tlb_io_ptw_status_zero3[20] = io_ptw_status_zero3[20];
  assign tlb_io_ptw_status_zero3[19] = io_ptw_status_zero3[19];
  assign tlb_io_ptw_status_zero3[18] = io_ptw_status_zero3[18];
  assign tlb_io_ptw_status_zero3[17] = io_ptw_status_zero3[17];
  assign tlb_io_ptw_status_zero3[16] = io_ptw_status_zero3[16];
  assign tlb_io_ptw_status_zero3[15] = io_ptw_status_zero3[15];
  assign tlb_io_ptw_status_zero3[14] = io_ptw_status_zero3[14];
  assign tlb_io_ptw_status_zero3[13] = io_ptw_status_zero3[13];
  assign tlb_io_ptw_status_zero3[12] = io_ptw_status_zero3[12];
  assign tlb_io_ptw_status_zero3[11] = io_ptw_status_zero3[11];
  assign tlb_io_ptw_status_zero3[10] = io_ptw_status_zero3[10];
  assign tlb_io_ptw_status_zero3[9] = io_ptw_status_zero3[9];
  assign tlb_io_ptw_status_zero3[8] = io_ptw_status_zero3[8];
  assign tlb_io_ptw_status_zero3[7] = io_ptw_status_zero3[7];
  assign tlb_io_ptw_status_zero3[6] = io_ptw_status_zero3[6];
  assign tlb_io_ptw_status_zero3[5] = io_ptw_status_zero3[5];
  assign tlb_io_ptw_status_zero3[4] = io_ptw_status_zero3[4];
  assign tlb_io_ptw_status_zero3[3] = io_ptw_status_zero3[3];
  assign tlb_io_ptw_status_zero3[2] = io_ptw_status_zero3[2];
  assign tlb_io_ptw_status_zero3[1] = io_ptw_status_zero3[1];
  assign tlb_io_ptw_status_zero3[0] = io_ptw_status_zero3[0];
  assign tlb_io_ptw_status_sd_rv32 = io_ptw_status_sd_rv32;
  assign tlb_io_ptw_status_zero2[1] = io_ptw_status_zero2[1];
  assign tlb_io_ptw_status_zero2[0] = io_ptw_status_zero2[0];
  assign tlb_io_ptw_status_vm[4] = io_ptw_status_vm[4];
  assign tlb_io_ptw_status_vm[3] = io_ptw_status_vm[3];
  assign tlb_io_ptw_status_vm[2] = io_ptw_status_vm[2];
  assign tlb_io_ptw_status_vm[1] = io_ptw_status_vm[1];
  assign tlb_io_ptw_status_vm[0] = io_ptw_status_vm[0];
  assign tlb_io_ptw_status_zero1[3] = io_ptw_status_zero1[3];
  assign tlb_io_ptw_status_zero1[2] = io_ptw_status_zero1[2];
  assign tlb_io_ptw_status_zero1[1] = io_ptw_status_zero1[1];
  assign tlb_io_ptw_status_zero1[0] = io_ptw_status_zero1[0];
  assign tlb_io_ptw_status_mxr = io_ptw_status_mxr;
  assign tlb_io_ptw_status_pum = io_ptw_status_pum;
  assign tlb_io_ptw_status_mprv = io_ptw_status_mprv;
  assign tlb_io_ptw_status_xs[1] = io_ptw_status_xs[1];
  assign tlb_io_ptw_status_xs[0] = io_ptw_status_xs[0];
  assign tlb_io_ptw_status_fs[1] = io_ptw_status_fs[1];
  assign tlb_io_ptw_status_fs[0] = io_ptw_status_fs[0];
  assign tlb_io_ptw_status_mpp[1] = io_ptw_status_mpp[1];
  assign tlb_io_ptw_status_mpp[0] = io_ptw_status_mpp[0];
  assign tlb_io_ptw_status_hpp[1] = io_ptw_status_hpp[1];
  assign tlb_io_ptw_status_hpp[0] = io_ptw_status_hpp[0];
  assign tlb_io_ptw_status_spp = io_ptw_status_spp;
  assign tlb_io_ptw_status_mpie = io_ptw_status_mpie;
  assign tlb_io_ptw_status_hpie = io_ptw_status_hpie;
  assign tlb_io_ptw_status_spie = io_ptw_status_spie;
  assign tlb_io_ptw_status_upie = io_ptw_status_upie;
  assign tlb_io_ptw_status_mie = io_ptw_status_mie;
  assign tlb_io_ptw_status_hie = io_ptw_status_hie;
  assign tlb_io_ptw_status_sie = io_ptw_status_sie;
  assign tlb_io_ptw_status_uie = io_ptw_status_uie;
  assign io_mem_0_a_valid = icache_io_mem_0_a_valid;
  assign io_mem_0_a_bits_opcode[2] = icache_io_mem_0_a_bits_opcode[2];
  assign io_mem_0_a_bits_opcode[1] = icache_io_mem_0_a_bits_opcode[1];
  assign io_mem_0_a_bits_opcode[0] = icache_io_mem_0_a_bits_opcode[0];
  assign io_mem_0_a_bits_param[2] = icache_io_mem_0_a_bits_param[2];
  assign io_mem_0_a_bits_param[1] = icache_io_mem_0_a_bits_param[1];
  assign io_mem_0_a_bits_param[0] = icache_io_mem_0_a_bits_param[0];
  assign io_mem_0_a_bits_size[3] = icache_io_mem_0_a_bits_size[3];
  assign io_mem_0_a_bits_size[2] = icache_io_mem_0_a_bits_size[2];
  assign io_mem_0_a_bits_size[1] = icache_io_mem_0_a_bits_size[1];
  assign io_mem_0_a_bits_size[0] = icache_io_mem_0_a_bits_size[0];
  assign io_mem_0_a_bits_source = icache_io_mem_0_a_bits_source;
  assign io_mem_0_a_bits_address[31] = icache_io_mem_0_a_bits_address[31];
  assign io_mem_0_a_bits_address[30] = icache_io_mem_0_a_bits_address[30];
  assign io_mem_0_a_bits_address[29] = icache_io_mem_0_a_bits_address[29];
  assign io_mem_0_a_bits_address[28] = icache_io_mem_0_a_bits_address[28];
  assign io_mem_0_a_bits_address[27] = icache_io_mem_0_a_bits_address[27];
  assign io_mem_0_a_bits_address[26] = icache_io_mem_0_a_bits_address[26];
  assign io_mem_0_a_bits_address[25] = icache_io_mem_0_a_bits_address[25];
  assign io_mem_0_a_bits_address[24] = icache_io_mem_0_a_bits_address[24];
  assign io_mem_0_a_bits_address[23] = icache_io_mem_0_a_bits_address[23];
  assign io_mem_0_a_bits_address[22] = icache_io_mem_0_a_bits_address[22];
  assign io_mem_0_a_bits_address[21] = icache_io_mem_0_a_bits_address[21];
  assign io_mem_0_a_bits_address[20] = icache_io_mem_0_a_bits_address[20];
  assign io_mem_0_a_bits_address[19] = icache_io_mem_0_a_bits_address[19];
  assign io_mem_0_a_bits_address[18] = icache_io_mem_0_a_bits_address[18];
  assign io_mem_0_a_bits_address[17] = icache_io_mem_0_a_bits_address[17];
  assign io_mem_0_a_bits_address[16] = icache_io_mem_0_a_bits_address[16];
  assign io_mem_0_a_bits_address[15] = icache_io_mem_0_a_bits_address[15];
  assign io_mem_0_a_bits_address[14] = icache_io_mem_0_a_bits_address[14];
  assign io_mem_0_a_bits_address[13] = icache_io_mem_0_a_bits_address[13];
  assign io_mem_0_a_bits_address[12] = icache_io_mem_0_a_bits_address[12];
  assign io_mem_0_a_bits_address[11] = icache_io_mem_0_a_bits_address[11];
  assign io_mem_0_a_bits_address[10] = icache_io_mem_0_a_bits_address[10];
  assign io_mem_0_a_bits_address[9] = icache_io_mem_0_a_bits_address[9];
  assign io_mem_0_a_bits_address[8] = icache_io_mem_0_a_bits_address[8];
  assign io_mem_0_a_bits_address[7] = icache_io_mem_0_a_bits_address[7];
  assign io_mem_0_a_bits_address[6] = icache_io_mem_0_a_bits_address[6];
  assign io_mem_0_a_bits_address[5] = icache_io_mem_0_a_bits_address[5];
  assign io_mem_0_a_bits_address[4] = icache_io_mem_0_a_bits_address[4];
  assign io_mem_0_a_bits_address[3] = icache_io_mem_0_a_bits_address[3];
  assign io_mem_0_a_bits_address[2] = icache_io_mem_0_a_bits_address[2];
  assign io_mem_0_a_bits_address[1] = icache_io_mem_0_a_bits_address[1];
  assign io_mem_0_a_bits_address[0] = icache_io_mem_0_a_bits_address[0];
  assign io_mem_0_a_bits_mask[7] = icache_io_mem_0_a_bits_mask[7];
  assign io_mem_0_a_bits_mask[6] = icache_io_mem_0_a_bits_mask[6];
  assign io_mem_0_a_bits_mask[5] = icache_io_mem_0_a_bits_mask[5];
  assign io_mem_0_a_bits_mask[4] = icache_io_mem_0_a_bits_mask[4];
  assign io_mem_0_a_bits_mask[3] = icache_io_mem_0_a_bits_mask[3];
  assign io_mem_0_a_bits_mask[2] = icache_io_mem_0_a_bits_mask[2];
  assign io_mem_0_a_bits_mask[1] = icache_io_mem_0_a_bits_mask[1];
  assign io_mem_0_a_bits_mask[0] = icache_io_mem_0_a_bits_mask[0];
  assign io_mem_0_a_bits_data[63] = icache_io_mem_0_a_bits_data[63];
  assign io_mem_0_a_bits_data[62] = icache_io_mem_0_a_bits_data[62];
  assign io_mem_0_a_bits_data[61] = icache_io_mem_0_a_bits_data[61];
  assign io_mem_0_a_bits_data[60] = icache_io_mem_0_a_bits_data[60];
  assign io_mem_0_a_bits_data[59] = icache_io_mem_0_a_bits_data[59];
  assign io_mem_0_a_bits_data[58] = icache_io_mem_0_a_bits_data[58];
  assign io_mem_0_a_bits_data[57] = icache_io_mem_0_a_bits_data[57];
  assign io_mem_0_a_bits_data[56] = icache_io_mem_0_a_bits_data[56];
  assign io_mem_0_a_bits_data[55] = icache_io_mem_0_a_bits_data[55];
  assign io_mem_0_a_bits_data[54] = icache_io_mem_0_a_bits_data[54];
  assign io_mem_0_a_bits_data[53] = icache_io_mem_0_a_bits_data[53];
  assign io_mem_0_a_bits_data[52] = icache_io_mem_0_a_bits_data[52];
  assign io_mem_0_a_bits_data[51] = icache_io_mem_0_a_bits_data[51];
  assign io_mem_0_a_bits_data[50] = icache_io_mem_0_a_bits_data[50];
  assign io_mem_0_a_bits_data[49] = icache_io_mem_0_a_bits_data[49];
  assign io_mem_0_a_bits_data[48] = icache_io_mem_0_a_bits_data[48];
  assign io_mem_0_a_bits_data[47] = icache_io_mem_0_a_bits_data[47];
  assign io_mem_0_a_bits_data[46] = icache_io_mem_0_a_bits_data[46];
  assign io_mem_0_a_bits_data[45] = icache_io_mem_0_a_bits_data[45];
  assign io_mem_0_a_bits_data[44] = icache_io_mem_0_a_bits_data[44];
  assign io_mem_0_a_bits_data[43] = icache_io_mem_0_a_bits_data[43];
  assign io_mem_0_a_bits_data[42] = icache_io_mem_0_a_bits_data[42];
  assign io_mem_0_a_bits_data[41] = icache_io_mem_0_a_bits_data[41];
  assign io_mem_0_a_bits_data[40] = icache_io_mem_0_a_bits_data[40];
  assign io_mem_0_a_bits_data[39] = icache_io_mem_0_a_bits_data[39];
  assign io_mem_0_a_bits_data[38] = icache_io_mem_0_a_bits_data[38];
  assign io_mem_0_a_bits_data[37] = icache_io_mem_0_a_bits_data[37];
  assign io_mem_0_a_bits_data[36] = icache_io_mem_0_a_bits_data[36];
  assign io_mem_0_a_bits_data[35] = icache_io_mem_0_a_bits_data[35];
  assign io_mem_0_a_bits_data[34] = icache_io_mem_0_a_bits_data[34];
  assign io_mem_0_a_bits_data[33] = icache_io_mem_0_a_bits_data[33];
  assign io_mem_0_a_bits_data[32] = icache_io_mem_0_a_bits_data[32];
  assign io_mem_0_a_bits_data[31] = icache_io_mem_0_a_bits_data[31];
  assign io_mem_0_a_bits_data[30] = icache_io_mem_0_a_bits_data[30];
  assign io_mem_0_a_bits_data[29] = icache_io_mem_0_a_bits_data[29];
  assign io_mem_0_a_bits_data[28] = icache_io_mem_0_a_bits_data[28];
  assign io_mem_0_a_bits_data[27] = icache_io_mem_0_a_bits_data[27];
  assign io_mem_0_a_bits_data[26] = icache_io_mem_0_a_bits_data[26];
  assign io_mem_0_a_bits_data[25] = icache_io_mem_0_a_bits_data[25];
  assign io_mem_0_a_bits_data[24] = icache_io_mem_0_a_bits_data[24];
  assign io_mem_0_a_bits_data[23] = icache_io_mem_0_a_bits_data[23];
  assign io_mem_0_a_bits_data[22] = icache_io_mem_0_a_bits_data[22];
  assign io_mem_0_a_bits_data[21] = icache_io_mem_0_a_bits_data[21];
  assign io_mem_0_a_bits_data[20] = icache_io_mem_0_a_bits_data[20];
  assign io_mem_0_a_bits_data[19] = icache_io_mem_0_a_bits_data[19];
  assign io_mem_0_a_bits_data[18] = icache_io_mem_0_a_bits_data[18];
  assign io_mem_0_a_bits_data[17] = icache_io_mem_0_a_bits_data[17];
  assign io_mem_0_a_bits_data[16] = icache_io_mem_0_a_bits_data[16];
  assign io_mem_0_a_bits_data[15] = icache_io_mem_0_a_bits_data[15];
  assign io_mem_0_a_bits_data[14] = icache_io_mem_0_a_bits_data[14];
  assign io_mem_0_a_bits_data[13] = icache_io_mem_0_a_bits_data[13];
  assign io_mem_0_a_bits_data[12] = icache_io_mem_0_a_bits_data[12];
  assign io_mem_0_a_bits_data[11] = icache_io_mem_0_a_bits_data[11];
  assign io_mem_0_a_bits_data[10] = icache_io_mem_0_a_bits_data[10];
  assign io_mem_0_a_bits_data[9] = icache_io_mem_0_a_bits_data[9];
  assign io_mem_0_a_bits_data[8] = icache_io_mem_0_a_bits_data[8];
  assign io_mem_0_a_bits_data[7] = icache_io_mem_0_a_bits_data[7];
  assign io_mem_0_a_bits_data[6] = icache_io_mem_0_a_bits_data[6];
  assign io_mem_0_a_bits_data[5] = icache_io_mem_0_a_bits_data[5];
  assign io_mem_0_a_bits_data[4] = icache_io_mem_0_a_bits_data[4];
  assign io_mem_0_a_bits_data[3] = icache_io_mem_0_a_bits_data[3];
  assign io_mem_0_a_bits_data[2] = icache_io_mem_0_a_bits_data[2];
  assign io_mem_0_a_bits_data[1] = icache_io_mem_0_a_bits_data[1];
  assign io_mem_0_a_bits_data[0] = icache_io_mem_0_a_bits_data[0];
  assign io_mem_0_b_ready = icache_io_mem_0_b_ready;
  assign io_mem_0_c_valid = icache_io_mem_0_c_valid;
  assign io_mem_0_c_bits_opcode[2] = icache_io_mem_0_c_bits_opcode[2];
  assign io_mem_0_c_bits_opcode[1] = icache_io_mem_0_c_bits_opcode[1];
  assign io_mem_0_c_bits_opcode[0] = icache_io_mem_0_c_bits_opcode[0];
  assign io_mem_0_c_bits_param[2] = icache_io_mem_0_c_bits_param[2];
  assign io_mem_0_c_bits_param[1] = icache_io_mem_0_c_bits_param[1];
  assign io_mem_0_c_bits_param[0] = icache_io_mem_0_c_bits_param[0];
  assign io_mem_0_c_bits_size[3] = icache_io_mem_0_c_bits_size[3];
  assign io_mem_0_c_bits_size[2] = icache_io_mem_0_c_bits_size[2];
  assign io_mem_0_c_bits_size[1] = icache_io_mem_0_c_bits_size[1];
  assign io_mem_0_c_bits_size[0] = icache_io_mem_0_c_bits_size[0];
  assign io_mem_0_c_bits_source = icache_io_mem_0_c_bits_source;
  assign io_mem_0_c_bits_address[31] = icache_io_mem_0_c_bits_address[31];
  assign io_mem_0_c_bits_address[30] = icache_io_mem_0_c_bits_address[30];
  assign io_mem_0_c_bits_address[29] = icache_io_mem_0_c_bits_address[29];
  assign io_mem_0_c_bits_address[28] = icache_io_mem_0_c_bits_address[28];
  assign io_mem_0_c_bits_address[27] = icache_io_mem_0_c_bits_address[27];
  assign io_mem_0_c_bits_address[26] = icache_io_mem_0_c_bits_address[26];
  assign io_mem_0_c_bits_address[25] = icache_io_mem_0_c_bits_address[25];
  assign io_mem_0_c_bits_address[24] = icache_io_mem_0_c_bits_address[24];
  assign io_mem_0_c_bits_address[23] = icache_io_mem_0_c_bits_address[23];
  assign io_mem_0_c_bits_address[22] = icache_io_mem_0_c_bits_address[22];
  assign io_mem_0_c_bits_address[21] = icache_io_mem_0_c_bits_address[21];
  assign io_mem_0_c_bits_address[20] = icache_io_mem_0_c_bits_address[20];
  assign io_mem_0_c_bits_address[19] = icache_io_mem_0_c_bits_address[19];
  assign io_mem_0_c_bits_address[18] = icache_io_mem_0_c_bits_address[18];
  assign io_mem_0_c_bits_address[17] = icache_io_mem_0_c_bits_address[17];
  assign io_mem_0_c_bits_address[16] = icache_io_mem_0_c_bits_address[16];
  assign io_mem_0_c_bits_address[15] = icache_io_mem_0_c_bits_address[15];
  assign io_mem_0_c_bits_address[14] = icache_io_mem_0_c_bits_address[14];
  assign io_mem_0_c_bits_address[13] = icache_io_mem_0_c_bits_address[13];
  assign io_mem_0_c_bits_address[12] = icache_io_mem_0_c_bits_address[12];
  assign io_mem_0_c_bits_address[11] = icache_io_mem_0_c_bits_address[11];
  assign io_mem_0_c_bits_address[10] = icache_io_mem_0_c_bits_address[10];
  assign io_mem_0_c_bits_address[9] = icache_io_mem_0_c_bits_address[9];
  assign io_mem_0_c_bits_address[8] = icache_io_mem_0_c_bits_address[8];
  assign io_mem_0_c_bits_address[7] = icache_io_mem_0_c_bits_address[7];
  assign io_mem_0_c_bits_address[6] = icache_io_mem_0_c_bits_address[6];
  assign io_mem_0_c_bits_address[5] = icache_io_mem_0_c_bits_address[5];
  assign io_mem_0_c_bits_address[4] = icache_io_mem_0_c_bits_address[4];
  assign io_mem_0_c_bits_address[3] = icache_io_mem_0_c_bits_address[3];
  assign io_mem_0_c_bits_address[2] = icache_io_mem_0_c_bits_address[2];
  assign io_mem_0_c_bits_address[1] = icache_io_mem_0_c_bits_address[1];
  assign io_mem_0_c_bits_address[0] = icache_io_mem_0_c_bits_address[0];
  assign io_mem_0_c_bits_data[63] = icache_io_mem_0_c_bits_data[63];
  assign io_mem_0_c_bits_data[62] = icache_io_mem_0_c_bits_data[62];
  assign io_mem_0_c_bits_data[61] = icache_io_mem_0_c_bits_data[61];
  assign io_mem_0_c_bits_data[60] = icache_io_mem_0_c_bits_data[60];
  assign io_mem_0_c_bits_data[59] = icache_io_mem_0_c_bits_data[59];
  assign io_mem_0_c_bits_data[58] = icache_io_mem_0_c_bits_data[58];
  assign io_mem_0_c_bits_data[57] = icache_io_mem_0_c_bits_data[57];
  assign io_mem_0_c_bits_data[56] = icache_io_mem_0_c_bits_data[56];
  assign io_mem_0_c_bits_data[55] = icache_io_mem_0_c_bits_data[55];
  assign io_mem_0_c_bits_data[54] = icache_io_mem_0_c_bits_data[54];
  assign io_mem_0_c_bits_data[53] = icache_io_mem_0_c_bits_data[53];
  assign io_mem_0_c_bits_data[52] = icache_io_mem_0_c_bits_data[52];
  assign io_mem_0_c_bits_data[51] = icache_io_mem_0_c_bits_data[51];
  assign io_mem_0_c_bits_data[50] = icache_io_mem_0_c_bits_data[50];
  assign io_mem_0_c_bits_data[49] = icache_io_mem_0_c_bits_data[49];
  assign io_mem_0_c_bits_data[48] = icache_io_mem_0_c_bits_data[48];
  assign io_mem_0_c_bits_data[47] = icache_io_mem_0_c_bits_data[47];
  assign io_mem_0_c_bits_data[46] = icache_io_mem_0_c_bits_data[46];
  assign io_mem_0_c_bits_data[45] = icache_io_mem_0_c_bits_data[45];
  assign io_mem_0_c_bits_data[44] = icache_io_mem_0_c_bits_data[44];
  assign io_mem_0_c_bits_data[43] = icache_io_mem_0_c_bits_data[43];
  assign io_mem_0_c_bits_data[42] = icache_io_mem_0_c_bits_data[42];
  assign io_mem_0_c_bits_data[41] = icache_io_mem_0_c_bits_data[41];
  assign io_mem_0_c_bits_data[40] = icache_io_mem_0_c_bits_data[40];
  assign io_mem_0_c_bits_data[39] = icache_io_mem_0_c_bits_data[39];
  assign io_mem_0_c_bits_data[38] = icache_io_mem_0_c_bits_data[38];
  assign io_mem_0_c_bits_data[37] = icache_io_mem_0_c_bits_data[37];
  assign io_mem_0_c_bits_data[36] = icache_io_mem_0_c_bits_data[36];
  assign io_mem_0_c_bits_data[35] = icache_io_mem_0_c_bits_data[35];
  assign io_mem_0_c_bits_data[34] = icache_io_mem_0_c_bits_data[34];
  assign io_mem_0_c_bits_data[33] = icache_io_mem_0_c_bits_data[33];
  assign io_mem_0_c_bits_data[32] = icache_io_mem_0_c_bits_data[32];
  assign io_mem_0_c_bits_data[31] = icache_io_mem_0_c_bits_data[31];
  assign io_mem_0_c_bits_data[30] = icache_io_mem_0_c_bits_data[30];
  assign io_mem_0_c_bits_data[29] = icache_io_mem_0_c_bits_data[29];
  assign io_mem_0_c_bits_data[28] = icache_io_mem_0_c_bits_data[28];
  assign io_mem_0_c_bits_data[27] = icache_io_mem_0_c_bits_data[27];
  assign io_mem_0_c_bits_data[26] = icache_io_mem_0_c_bits_data[26];
  assign io_mem_0_c_bits_data[25] = icache_io_mem_0_c_bits_data[25];
  assign io_mem_0_c_bits_data[24] = icache_io_mem_0_c_bits_data[24];
  assign io_mem_0_c_bits_data[23] = icache_io_mem_0_c_bits_data[23];
  assign io_mem_0_c_bits_data[22] = icache_io_mem_0_c_bits_data[22];
  assign io_mem_0_c_bits_data[21] = icache_io_mem_0_c_bits_data[21];
  assign io_mem_0_c_bits_data[20] = icache_io_mem_0_c_bits_data[20];
  assign io_mem_0_c_bits_data[19] = icache_io_mem_0_c_bits_data[19];
  assign io_mem_0_c_bits_data[18] = icache_io_mem_0_c_bits_data[18];
  assign io_mem_0_c_bits_data[17] = icache_io_mem_0_c_bits_data[17];
  assign io_mem_0_c_bits_data[16] = icache_io_mem_0_c_bits_data[16];
  assign io_mem_0_c_bits_data[15] = icache_io_mem_0_c_bits_data[15];
  assign io_mem_0_c_bits_data[14] = icache_io_mem_0_c_bits_data[14];
  assign io_mem_0_c_bits_data[13] = icache_io_mem_0_c_bits_data[13];
  assign io_mem_0_c_bits_data[12] = icache_io_mem_0_c_bits_data[12];
  assign io_mem_0_c_bits_data[11] = icache_io_mem_0_c_bits_data[11];
  assign io_mem_0_c_bits_data[10] = icache_io_mem_0_c_bits_data[10];
  assign io_mem_0_c_bits_data[9] = icache_io_mem_0_c_bits_data[9];
  assign io_mem_0_c_bits_data[8] = icache_io_mem_0_c_bits_data[8];
  assign io_mem_0_c_bits_data[7] = icache_io_mem_0_c_bits_data[7];
  assign io_mem_0_c_bits_data[6] = icache_io_mem_0_c_bits_data[6];
  assign io_mem_0_c_bits_data[5] = icache_io_mem_0_c_bits_data[5];
  assign io_mem_0_c_bits_data[4] = icache_io_mem_0_c_bits_data[4];
  assign io_mem_0_c_bits_data[3] = icache_io_mem_0_c_bits_data[3];
  assign io_mem_0_c_bits_data[2] = icache_io_mem_0_c_bits_data[2];
  assign io_mem_0_c_bits_data[1] = icache_io_mem_0_c_bits_data[1];
  assign io_mem_0_c_bits_data[0] = icache_io_mem_0_c_bits_data[0];
  assign io_mem_0_c_bits_error = icache_io_mem_0_c_bits_error;
  assign io_mem_0_d_ready = icache_io_mem_0_d_ready;
  assign io_mem_0_e_valid = icache_io_mem_0_e_valid;
  assign io_mem_0_e_bits_sink[3] = icache_io_mem_0_e_bits_sink[3];
  assign io_mem_0_e_bits_sink[2] = icache_io_mem_0_e_bits_sink[2];
  assign io_mem_0_e_bits_sink[1] = icache_io_mem_0_e_bits_sink[1];
  assign io_mem_0_e_bits_sink[0] = icache_io_mem_0_e_bits_sink[0];
  assign icache_io_s1_ppn[19] = tlb_io_resp_ppn[19];
  assign icache_io_s1_ppn[18] = tlb_io_resp_ppn[18];
  assign icache_io_s1_ppn[17] = tlb_io_resp_ppn[17];
  assign icache_io_s1_ppn[16] = tlb_io_resp_ppn[16];
  assign icache_io_s1_ppn[15] = tlb_io_resp_ppn[15];
  assign icache_io_s1_ppn[14] = tlb_io_resp_ppn[14];
  assign icache_io_s1_ppn[13] = tlb_io_resp_ppn[13];
  assign icache_io_s1_ppn[12] = tlb_io_resp_ppn[12];
  assign icache_io_s1_ppn[11] = tlb_io_resp_ppn[11];
  assign icache_io_s1_ppn[10] = tlb_io_resp_ppn[10];
  assign icache_io_s1_ppn[9] = tlb_io_resp_ppn[9];
  assign icache_io_s1_ppn[8] = tlb_io_resp_ppn[8];
  assign icache_io_s1_ppn[7] = tlb_io_resp_ppn[7];
  assign icache_io_s1_ppn[6] = tlb_io_resp_ppn[6];
  assign icache_io_s1_ppn[5] = tlb_io_resp_ppn[5];
  assign icache_io_s1_ppn[4] = tlb_io_resp_ppn[4];
  assign icache_io_s1_ppn[3] = tlb_io_resp_ppn[3];
  assign icache_io_s1_ppn[2] = tlb_io_resp_ppn[2];
  assign icache_io_s1_ppn[1] = tlb_io_resp_ppn[1];
  assign icache_io_s1_ppn[0] = tlb_io_resp_ppn[0];
  assign io_ptw_req_valid = tlb_io_ptw_req_valid;
  assign io_ptw_req_bits_prv[1] = tlb_io_ptw_req_bits_prv[1];
  assign io_ptw_req_bits_prv[0] = tlb_io_ptw_req_bits_prv[0];
  assign io_ptw_req_bits_pum = tlb_io_ptw_req_bits_pum;
  assign io_ptw_req_bits_mxr = tlb_io_ptw_req_bits_mxr;
  assign io_ptw_req_bits_addr[26] = tlb_io_ptw_req_bits_addr[26];
  assign io_ptw_req_bits_addr[25] = tlb_io_ptw_req_bits_addr[25];
  assign io_ptw_req_bits_addr[24] = tlb_io_ptw_req_bits_addr[24];
  assign io_ptw_req_bits_addr[23] = tlb_io_ptw_req_bits_addr[23];
  assign io_ptw_req_bits_addr[22] = tlb_io_ptw_req_bits_addr[22];
  assign io_ptw_req_bits_addr[21] = tlb_io_ptw_req_bits_addr[21];
  assign io_ptw_req_bits_addr[20] = tlb_io_ptw_req_bits_addr[20];
  assign io_ptw_req_bits_addr[19] = tlb_io_ptw_req_bits_addr[19];
  assign io_ptw_req_bits_addr[18] = tlb_io_ptw_req_bits_addr[18];
  assign io_ptw_req_bits_addr[17] = tlb_io_ptw_req_bits_addr[17];
  assign io_ptw_req_bits_addr[16] = tlb_io_ptw_req_bits_addr[16];
  assign io_ptw_req_bits_addr[15] = tlb_io_ptw_req_bits_addr[15];
  assign io_ptw_req_bits_addr[14] = tlb_io_ptw_req_bits_addr[14];
  assign io_ptw_req_bits_addr[13] = tlb_io_ptw_req_bits_addr[13];
  assign io_ptw_req_bits_addr[12] = tlb_io_ptw_req_bits_addr[12];
  assign io_ptw_req_bits_addr[11] = tlb_io_ptw_req_bits_addr[11];
  assign io_ptw_req_bits_addr[10] = tlb_io_ptw_req_bits_addr[10];
  assign io_ptw_req_bits_addr[9] = tlb_io_ptw_req_bits_addr[9];
  assign io_ptw_req_bits_addr[8] = tlb_io_ptw_req_bits_addr[8];
  assign io_ptw_req_bits_addr[7] = tlb_io_ptw_req_bits_addr[7];
  assign io_ptw_req_bits_addr[6] = tlb_io_ptw_req_bits_addr[6];
  assign io_ptw_req_bits_addr[5] = tlb_io_ptw_req_bits_addr[5];
  assign io_ptw_req_bits_addr[4] = tlb_io_ptw_req_bits_addr[4];
  assign io_ptw_req_bits_addr[3] = tlb_io_ptw_req_bits_addr[3];
  assign io_ptw_req_bits_addr[2] = tlb_io_ptw_req_bits_addr[2];
  assign io_ptw_req_bits_addr[1] = tlb_io_ptw_req_bits_addr[1];
  assign io_ptw_req_bits_addr[0] = tlb_io_ptw_req_bits_addr[0];
  assign io_ptw_req_bits_store = tlb_io_ptw_req_bits_store;
  assign io_ptw_req_bits_fetch = tlb_io_ptw_req_bits_fetch;
  assign tlb_io_req_bits_vpn[26] = BTB_io_req_bits_addr[38];
  assign tlb_io_req_bits_vpn[25] = BTB_io_req_bits_addr[37];
  assign tlb_io_req_bits_vpn[24] = BTB_io_req_bits_addr[36];
  assign tlb_io_req_bits_vpn[23] = BTB_io_req_bits_addr[35];
  assign tlb_io_req_bits_vpn[22] = BTB_io_req_bits_addr[34];
  assign tlb_io_req_bits_vpn[21] = BTB_io_req_bits_addr[33];
  assign tlb_io_req_bits_vpn[17] = BTB_io_req_bits_addr[29];
  assign tlb_io_req_bits_vpn[16] = BTB_io_req_bits_addr[28];
  assign tlb_io_req_bits_vpn[15] = BTB_io_req_bits_addr[27];
  assign tlb_io_req_bits_vpn[4] = BTB_io_req_bits_addr[16];
  assign tlb_io_req_bits_vpn[2] = BTB_io_req_bits_addr[14];
  assign tlb_io_req_bits_vpn[1] = BTB_io_req_bits_addr[13];
  assign alarm = canary_in;
  assign tlb_io_req_bits_vpn[27] = \s1_pc_[39] ;
  assign tlb_io_req_bits_instruction = 1'b1;
  assign io_cpu_resp_bits_mask = 1'b1;
  assign tlb_io_req_bits_store = 1'b0;
  assign tlb_io_req_bits_passthrough = 1'b0;
  assign io_cpu_npc[5] = \icache_io_req_bits_addr[5] ;
  assign icache_io_req_bits_addr[5] = \icache_io_req_bits_addr[5] ;
  assign BTB_io_req_bits_addr[26] = \tlb_io_req_bits_vpn[14] ;
  assign tlb_io_req_bits_vpn[14] = \tlb_io_req_bits_vpn[14] ;
  assign BTB_io_req_bits_addr[32] = \tlb_io_req_bits_vpn[20] ;
  assign tlb_io_req_bits_vpn[20] = \tlb_io_req_bits_vpn[20] ;
  assign BTB_io_req_bits_addr[30] = \tlb_io_req_bits_vpn[18] ;
  assign tlb_io_req_bits_vpn[18] = \tlb_io_req_bits_vpn[18] ;
  assign icache_io_req_bits_addr[25] = \io_cpu_npc[25] ;
  assign io_cpu_npc[25] = \io_cpu_npc[25] ;
  assign io_cpu_npc[32] = \icache_io_req_bits_addr[32] ;
  assign icache_io_req_bits_addr[32] = \icache_io_req_bits_addr[32] ;
  assign icache_io_req_bits_addr[29] = \io_cpu_npc[29] ;
  assign io_cpu_npc[29] = \io_cpu_npc[29] ;
  assign icache_io_req_bits_addr[31] = \io_cpu_npc[31] ;
  assign io_cpu_npc[31] = \io_cpu_npc[31] ;
  assign icache_io_req_bits_addr[38] = \io_cpu_npc[38] ;
  assign io_cpu_npc[38] = \io_cpu_npc[38] ;
  assign BTB_io_req_bits_addr[12] = \tlb_io_req_bits_vpn[0] ;
  assign tlb_io_req_bits_vpn[0] = \tlb_io_req_bits_vpn[0] ;
  assign BTB_io_req_bits_addr[31] = \tlb_io_req_bits_vpn[19] ;
  assign tlb_io_req_bits_vpn[19] = \tlb_io_req_bits_vpn[19] ;
  assign icache_io_req_bits_addr[4] = \io_cpu_npc[4] ;
  assign io_cpu_npc[4] = \io_cpu_npc[4] ;
  assign icache_io_req_bits_addr[23] = \io_cpu_npc[23] ;
  assign io_cpu_npc[23] = \io_cpu_npc[23] ;
  assign io_cpu_npc[28] = \icache_io_req_bits_addr[28] ;
  assign icache_io_req_bits_addr[28] = \icache_io_req_bits_addr[28] ;
  assign io_cpu_npc[22] = \icache_io_req_bits_addr[22] ;
  assign icache_io_req_bits_addr[22] = \icache_io_req_bits_addr[22] ;
  assign io_cpu_npc[21] = \icache_io_req_bits_addr[21] ;
  assign icache_io_req_bits_addr[21] = \icache_io_req_bits_addr[21] ;
  assign io_cpu_npc[19] = \icache_io_req_bits_addr[19] ;
  assign icache_io_req_bits_addr[19] = \icache_io_req_bits_addr[19] ;
  assign io_cpu_npc[30] = \icache_io_req_bits_addr[30] ;
  assign icache_io_req_bits_addr[30] = \icache_io_req_bits_addr[30] ;
  assign io_cpu_npc[3] = \icache_io_req_bits_addr[3] ;
  assign icache_io_req_bits_addr[3] = \icache_io_req_bits_addr[3] ;
  assign icache_io_req_bits_addr[11] = \io_cpu_npc[11] ;
  assign io_cpu_npc[11] = \io_cpu_npc[11] ;
  assign BTB_io_req_bits_addr[25] = \tlb_io_req_bits_vpn[13] ;
  assign tlb_io_req_bits_vpn[13] = \tlb_io_req_bits_vpn[13] ;
  assign icache_io_req_bits_addr[9] = \io_cpu_npc[9] ;
  assign io_cpu_npc[9] = \io_cpu_npc[9] ;
  assign icache_io_req_bits_addr[34] = \io_cpu_npc[34] ;
  assign io_cpu_npc[34] = \io_cpu_npc[34] ;
  assign io_cpu_npc[2] = \icache_io_req_bits_addr[2] ;
  assign icache_io_req_bits_addr[2] = \icache_io_req_bits_addr[2] ;
  assign BTB_io_req_bits_addr[17] = \tlb_io_req_bits_vpn[5] ;
  assign tlb_io_req_bits_vpn[5] = \tlb_io_req_bits_vpn[5] ;
  assign BTB_io_req_bits_addr[23] = \tlb_io_req_bits_vpn[11] ;
  assign tlb_io_req_bits_vpn[11] = \tlb_io_req_bits_vpn[11] ;
  assign io_cpu_npc[15] = \icache_io_req_bits_addr[15] ;
  assign icache_io_req_bits_addr[15] = \icache_io_req_bits_addr[15] ;
  assign io_cpu_npc[16] = \icache_io_req_bits_addr[16] ;
  assign icache_io_req_bits_addr[16] = \icache_io_req_bits_addr[16] ;
  assign io_cpu_npc[24] = \icache_io_req_bits_addr[24] ;
  assign icache_io_req_bits_addr[24] = \icache_io_req_bits_addr[24] ;
  assign io_cpu_npc[27] = \icache_io_req_bits_addr[27] ;
  assign icache_io_req_bits_addr[27] = \icache_io_req_bits_addr[27] ;
  assign io_cpu_npc[26] = \icache_io_req_bits_addr[26] ;
  assign icache_io_req_bits_addr[26] = \icache_io_req_bits_addr[26] ;
  assign io_cpu_npc[1] = \icache_io_req_bits_addr[1] ;
  assign icache_io_req_bits_addr[1] = \icache_io_req_bits_addr[1] ;
  assign io_cpu_npc[0] = \icache_io_req_bits_addr[0] ;
  assign icache_io_req_bits_addr[0] = \icache_io_req_bits_addr[0] ;
  assign icache_io_req_bits_addr[6] = \io_cpu_npc[6] ;
  assign io_cpu_npc[6] = \io_cpu_npc[6] ;
  assign icache_io_req_bits_addr[14] = \io_cpu_npc[14] ;
  assign io_cpu_npc[14] = \io_cpu_npc[14] ;
  assign icache_io_req_bits_addr[12] = \io_cpu_npc[12] ;
  assign io_cpu_npc[12] = \io_cpu_npc[12] ;
  assign BTB_io_req_bits_addr[24] = \tlb_io_req_bits_vpn[12] ;
  assign tlb_io_req_bits_vpn[12] = \tlb_io_req_bits_vpn[12] ;
  assign BTB_io_req_bits_addr[22] = \tlb_io_req_bits_vpn[10] ;
  assign tlb_io_req_bits_vpn[10] = \tlb_io_req_bits_vpn[10] ;
  assign BTB_io_req_bits_addr[15] = \tlb_io_req_bits_vpn[3] ;
  assign tlb_io_req_bits_vpn[3] = \tlb_io_req_bits_vpn[3] ;
  assign BTB_io_req_bits_addr[19] = \tlb_io_req_bits_vpn[7] ;
  assign tlb_io_req_bits_vpn[7] = \tlb_io_req_bits_vpn[7] ;
  assign BTB_io_req_bits_addr[21] = \tlb_io_req_bits_vpn[9] ;
  assign tlb_io_req_bits_vpn[9] = \tlb_io_req_bits_vpn[9] ;
  assign BTB_io_req_bits_addr[20] = \tlb_io_req_bits_vpn[8] ;
  assign tlb_io_req_bits_vpn[8] = \tlb_io_req_bits_vpn[8] ;
  assign BTB_io_req_bits_addr[18] = \tlb_io_req_bits_vpn[6] ;
  assign tlb_io_req_bits_vpn[6] = \tlb_io_req_bits_vpn[6] ;

  DFFPOSX1 s2_cacheable_reg ( .D(n659), .CLK(clock), .Q(s2_cacheable) );
  DFFPOSX1 s2_btb_resp_bits_taken_reg ( .D(n387), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_taken) );
  DFFPOSX1 s2_btb_resp_bits_mask_reg ( .D(n386), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_mask) );
  DFFPOSX1 s2_btb_resp_bits_bridx_reg ( .D(n684), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bridx) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[0]  ( .D(n384), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[0]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[38]  ( .D(n383), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[38]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[37]  ( .D(n655), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[37]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[36]  ( .D(n654), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[36]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[35]  ( .D(n653), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[35]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[34]  ( .D(n652), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[34]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[33]  ( .D(n378), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[33]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[32]  ( .D(n377), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[32]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[31]  ( .D(n376), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[31]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[30]  ( .D(n375), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[30]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[29]  ( .D(n374), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[29]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[28]  ( .D(n373), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[28]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[27]  ( .D(n372), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[27]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[26]  ( .D(n371), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[26]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[25]  ( .D(n370), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[25]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[24]  ( .D(n369), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[24]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[23]  ( .D(n368), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[23]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[22]  ( .D(n367), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[22]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[21]  ( .D(n366), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[21]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[20]  ( .D(n365), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[20]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[19]  ( .D(n364), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[19]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[18]  ( .D(n363), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[18]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[17]  ( .D(n362), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[17]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[16]  ( .D(n361), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[16]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[15]  ( .D(n360), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[15]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[14]  ( .D(n359), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[14]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[13]  ( .D(n358), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[13]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[12]  ( .D(n357), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[12]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[11]  ( .D(n356), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[11]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[10]  ( .D(n355), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[10]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[9]  ( .D(n354), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[9]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[8]  ( .D(n353), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[8]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[7]  ( .D(n352), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[7]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[6]  ( .D(n351), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[6]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[5]  ( .D(n350), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[5]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[4]  ( .D(n349), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[4]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[3]  ( .D(n348), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[3]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[2]  ( .D(n347), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[2]) );
  DFFPOSX1 \s2_btb_resp_bits_target_reg[1]  ( .D(n346), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_target[1]) );
  DFFPOSX1 \s2_btb_resp_bits_entry_reg[0]  ( .D(n345), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_entry[0]) );
  DFFPOSX1 \s2_btb_resp_bits_entry_reg[5]  ( .D(n344), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_entry[5]) );
  DFFPOSX1 \s2_btb_resp_bits_entry_reg[4]  ( .D(n343), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_entry[4]) );
  DFFPOSX1 \s2_btb_resp_bits_entry_reg[3]  ( .D(n342), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_entry[3]) );
  DFFPOSX1 \s2_btb_resp_bits_entry_reg[2]  ( .D(n341), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_entry[2]) );
  DFFPOSX1 \s2_btb_resp_bits_entry_reg[1]  ( .D(n340), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_entry[1]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_history_reg[0]  ( .D(n339), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_history[0]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_history_reg[6]  ( .D(n338), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_history[6]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_history_reg[5]  ( .D(n337), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_history[5]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_history_reg[4]  ( .D(n336), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_history[4]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_history_reg[3]  ( .D(n335), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_history[3]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_history_reg[2]  ( .D(n334), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_history[2]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_history_reg[1]  ( .D(n333), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_history[1]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_value_reg[0]  ( .D(n332), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_value[0]) );
  DFFPOSX1 \s2_btb_resp_bits_bht_value_reg[1]  ( .D(n331), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_bits_bht_value[1]) );
  DFFPOSX1 s2_speculative_reg ( .D(n663), .CLK(clock), .Q(s2_speculative) );
  DFFPOSX1 s1_speculative_reg ( .D(n827), .CLK(clock), .Q(s1_speculative) );
  DFFPOSX1 s2_xcpt_if_reg ( .D(n680), .CLK(clock), .Q(n2397) );
  DFFPOSX1 \s2_pc_reg[0]  ( .D(n657), .CLK(clock), .Q(io_cpu_resp_bits_pc[0])
         );
  DFFPOSX1 \s2_pc_reg[1]  ( .D(n660), .CLK(clock), .Q(io_cpu_resp_bits_pc[1])
         );
  DFFPOSX1 s2_valid_reg ( .D(n679), .CLK(clock), .Q(s2_valid) );
  DFFPOSX1 \s1_pc__reg[0]  ( .D(n427), .CLK(clock), .Q(BTB_io_req_bits_addr[0]) );
  DFFPOSX1 \s1_pc__reg[1]  ( .D(n426), .CLK(clock), .Q(BTB_io_req_bits_addr[1]) );
  DFFPOSX1 \s1_pc__reg[2]  ( .D(n650), .CLK(clock), .Q(n2419) );
  DFFPOSX1 \s2_pc_reg[2]  ( .D(n685), .CLK(clock), .Q(n2396) );
  DFFPOSX1 \s1_pc__reg[3]  ( .D(n424), .CLK(clock), .Q(n2418) );
  DFFPOSX1 s1_same_block_reg ( .D(n471), .CLK(clock), .Q(s1_same_block) );
  DFFPOSX1 \s2_pc_reg[3]  ( .D(n464), .CLK(clock), .Q(io_cpu_resp_bits_pc[3])
         );
  DFFPOSX1 \s1_pc__reg[4]  ( .D(n1397), .CLK(clock), .Q(n2417) );
  DFFPOSX1 \s2_pc_reg[4]  ( .D(n681), .CLK(clock), .Q(io_cpu_resp_bits_pc[4])
         );
  DFFPOSX1 \s1_pc__reg[5]  ( .D(n662), .CLK(clock), .Q(n2416) );
  DFFPOSX1 \s2_pc_reg[5]  ( .D(n666), .CLK(clock), .Q(io_cpu_resp_bits_pc[5])
         );
  DFFPOSX1 \s1_pc__reg[6]  ( .D(n687), .CLK(clock), .Q(n2415) );
  DFFPOSX1 \s2_pc_reg[6]  ( .D(n667), .CLK(clock), .Q(io_cpu_resp_bits_pc[6])
         );
  DFFPOSX1 \s1_pc__reg[7]  ( .D(n420), .CLK(clock), .Q(n2414) );
  DFFPOSX1 \s2_pc_reg[7]  ( .D(n682), .CLK(clock), .Q(io_cpu_resp_bits_pc[7])
         );
  DFFPOSX1 \s1_pc__reg[8]  ( .D(n656), .CLK(clock), .Q(n2413) );
  DFFPOSX1 \s2_pc_reg[8]  ( .D(n459), .CLK(clock), .Q(io_cpu_resp_bits_pc[8])
         );
  DFFPOSX1 \s1_pc__reg[9]  ( .D(n649), .CLK(clock), .Q(n2412) );
  DFFPOSX1 \s2_pc_reg[9]  ( .D(n829), .CLK(clock), .Q(io_cpu_resp_bits_pc[9])
         );
  DFFPOSX1 \s1_pc__reg[10]  ( .D(n417), .CLK(clock), .Q(n2411) );
  DFFPOSX1 \s2_pc_reg[10]  ( .D(n457), .CLK(clock), .Q(io_cpu_resp_bits_pc[10]) );
  DFFPOSX1 \s1_pc__reg[11]  ( .D(n416), .CLK(clock), .Q(n2410) );
  DFFPOSX1 \s2_pc_reg[11]  ( .D(n456), .CLK(clock), .Q(io_cpu_resp_bits_pc[11]) );
  DFFPOSX1 \s1_pc__reg[12]  ( .D(n661), .CLK(clock), .Q(
        BTB_io_req_bits_addr_12) );
  DFFPOSX1 \s2_pc_reg[12]  ( .D(n455), .CLK(clock), .Q(io_cpu_resp_bits_pc[12]) );
  DFFPOSX1 \s1_pc__reg[13]  ( .D(n414), .CLK(clock), .Q(n2409) );
  DFFPOSX1 \s2_pc_reg[13]  ( .D(n454), .CLK(clock), .Q(io_cpu_resp_bits_pc[13]) );
  DFFPOSX1 \s1_pc__reg[14]  ( .D(n686), .CLK(clock), .Q(n2408) );
  DFFPOSX1 \s2_pc_reg[14]  ( .D(n453), .CLK(clock), .Q(io_cpu_resp_bits_pc[14]) );
  DFFPOSX1 \s1_pc__reg[15]  ( .D(n412), .CLK(clock), .Q(
        BTB_io_req_bits_addr_15) );
  DFFPOSX1 \s2_pc_reg[15]  ( .D(n452), .CLK(clock), .Q(io_cpu_resp_bits_pc[15]) );
  DFFPOSX1 \s1_pc__reg[16]  ( .D(n411), .CLK(clock), .Q(n2407) );
  DFFPOSX1 \s2_pc_reg[16]  ( .D(n451), .CLK(clock), .Q(io_cpu_resp_bits_pc[16]) );
  DFFPOSX1 \s1_pc__reg[17]  ( .D(n410), .CLK(clock), .Q(
        BTB_io_req_bits_addr_17) );
  DFFPOSX1 \s2_pc_reg[17]  ( .D(n450), .CLK(clock), .Q(io_cpu_resp_bits_pc[17]) );
  DFFPOSX1 \s1_pc__reg[18]  ( .D(n409), .CLK(clock), .Q(
        BTB_io_req_bits_addr_18) );
  DFFPOSX1 \s2_pc_reg[18]  ( .D(n449), .CLK(clock), .Q(io_cpu_resp_bits_pc[18]) );
  DFFPOSX1 \s1_pc__reg[19]  ( .D(n408), .CLK(clock), .Q(
        BTB_io_req_bits_addr_19) );
  DFFPOSX1 \s2_pc_reg[19]  ( .D(n665), .CLK(clock), .Q(io_cpu_resp_bits_pc[19]) );
  DFFPOSX1 \s1_pc__reg[20]  ( .D(n688), .CLK(clock), .Q(
        BTB_io_req_bits_addr_20) );
  DFFPOSX1 \s2_pc_reg[20]  ( .D(n668), .CLK(clock), .Q(io_cpu_resp_bits_pc[20]) );
  DFFPOSX1 \s1_pc__reg[21]  ( .D(n406), .CLK(clock), .Q(
        BTB_io_req_bits_addr_21) );
  DFFPOSX1 \s2_pc_reg[21]  ( .D(n669), .CLK(clock), .Q(io_cpu_resp_bits_pc[21]) );
  DFFPOSX1 \s1_pc__reg[22]  ( .D(n405), .CLK(clock), .Q(
        BTB_io_req_bits_addr_22) );
  DFFPOSX1 \s2_pc_reg[22]  ( .D(n670), .CLK(clock), .Q(io_cpu_resp_bits_pc[22]) );
  DFFPOSX1 \s1_pc__reg[23]  ( .D(n404), .CLK(clock), .Q(
        BTB_io_req_bits_addr_23) );
  DFFPOSX1 \s2_pc_reg[23]  ( .D(n671), .CLK(clock), .Q(io_cpu_resp_bits_pc[23]) );
  DFFPOSX1 \s1_pc__reg[24]  ( .D(n1604), .CLK(clock), .Q(
        BTB_io_req_bits_addr_24) );
  DFFPOSX1 \s2_pc_reg[24]  ( .D(n443), .CLK(clock), .Q(io_cpu_resp_bits_pc[24]) );
  DFFPOSX1 \s1_pc__reg[25]  ( .D(n644), .CLK(clock), .Q(
        BTB_io_req_bits_addr_25) );
  DFFPOSX1 \s2_pc_reg[25]  ( .D(n442), .CLK(clock), .Q(io_cpu_resp_bits_pc[25]) );
  DFFPOSX1 \s1_pc__reg[26]  ( .D(n646), .CLK(clock), .Q(
        BTB_io_req_bits_addr_26) );
  DFFPOSX1 \s2_pc_reg[26]  ( .D(n441), .CLK(clock), .Q(io_cpu_resp_bits_pc[26]) );
  DFFPOSX1 \s1_pc__reg[27]  ( .D(n1602), .CLK(clock), .Q(
        BTB_io_req_bits_addr[27]) );
  DFFPOSX1 \s2_pc_reg[27]  ( .D(n672), .CLK(clock), .Q(io_cpu_resp_bits_pc[27]) );
  DFFPOSX1 \s1_pc__reg[28]  ( .D(n1601), .CLK(clock), .Q(n2406) );
  DFFPOSX1 \s2_pc_reg[28]  ( .D(n439), .CLK(clock), .Q(io_cpu_resp_bits_pc[28]) );
  DFFPOSX1 \s1_pc__reg[29]  ( .D(n683), .CLK(clock), .Q(n2405) );
  DFFPOSX1 \s2_pc_reg[29]  ( .D(n673), .CLK(clock), .Q(io_cpu_resp_bits_pc[29]) );
  DFFPOSX1 \s1_pc__reg[30]  ( .D(n664), .CLK(clock), .Q(
        BTB_io_req_bits_addr_30) );
  DFFPOSX1 \s2_pc_reg[30]  ( .D(n437), .CLK(clock), .Q(io_cpu_resp_bits_pc[30]) );
  DFFPOSX1 \s1_pc__reg[31]  ( .D(n396), .CLK(clock), .Q(
        BTB_io_req_bits_addr_31) );
  DFFPOSX1 \s2_pc_reg[31]  ( .D(n436), .CLK(clock), .Q(io_cpu_resp_bits_pc[31]) );
  DFFPOSX1 \s1_pc__reg[32]  ( .D(n648), .CLK(clock), .Q(
        BTB_io_req_bits_addr_32) );
  DFFPOSX1 \s2_pc_reg[32]  ( .D(n435), .CLK(clock), .Q(io_cpu_resp_bits_pc[32]) );
  DFFPOSX1 \s1_pc__reg[33]  ( .D(n647), .CLK(clock), .Q(n2404) );
  DFFPOSX1 \s2_pc_reg[33]  ( .D(n1236), .CLK(clock), .Q(
        io_cpu_resp_bits_pc[33]) );
  DFFPOSX1 \s1_pc__reg[34]  ( .D(n1379), .CLK(clock), .Q(
        BTB_io_req_bits_addr[34]) );
  DFFPOSX1 \s2_pc_reg[34]  ( .D(n674), .CLK(clock), .Q(io_cpu_resp_bits_pc[34]) );
  DFFPOSX1 \s1_pc__reg[35]  ( .D(n651), .CLK(clock), .Q(
        BTB_io_req_bits_addr[35]) );
  DFFPOSX1 \s2_pc_reg[35]  ( .D(n675), .CLK(clock), .Q(io_cpu_resp_bits_pc[35]) );
  DFFPOSX1 \s1_pc__reg[36]  ( .D(n391), .CLK(clock), .Q(
        BTB_io_req_bits_addr[36]) );
  DFFPOSX1 \s2_pc_reg[36]  ( .D(n431), .CLK(clock), .Q(io_cpu_resp_bits_pc[36]) );
  DFFPOSX1 \s1_pc__reg[37]  ( .D(n390), .CLK(clock), .Q(
        BTB_io_req_bits_addr[37]) );
  DFFPOSX1 \s2_pc_reg[37]  ( .D(n676), .CLK(clock), .Q(io_cpu_resp_bits_pc[37]) );
  DFFPOSX1 \s1_pc__reg[38]  ( .D(n389), .CLK(clock), .Q(
        BTB_io_req_bits_addr[38]) );
  DFFPOSX1 \s2_pc_reg[38]  ( .D(n677), .CLK(clock), .Q(io_cpu_resp_bits_pc[38]) );
  DFFPOSX1 \s1_pc__reg[39]  ( .D(n388), .CLK(clock), .Q(\s1_pc_[39] ) );
  DFFPOSX1 \s2_pc_reg[39]  ( .D(n678), .CLK(clock), .Q(io_cpu_resp_bits_pc[39]) );
  DFFPOSX1 s2_btb_resp_valid_reg ( .D(n658), .CLK(clock), .Q(
        io_cpu_resp_bits_btb_valid) );
  OR2X1 U556 ( .A(n2275), .B(n1291), .Y(n333) );
  OR2X1 U557 ( .A(n2274), .B(n1422), .Y(n339) );
  OR2X1 U558 ( .A(n744), .B(n1424), .Y(n347) );
  INVX1 U559 ( .A(n382), .Y(n655) );
  INVX1 U560 ( .A(n1608), .Y(n658) );
  INVX1 U561 ( .A(n379), .Y(n652) );
  INVX1 U562 ( .A(n1406), .Y(n659) );
  INVX1 U563 ( .A(n1606), .Y(n680) );
  INVX1 U564 ( .A(n1607), .Y(n657) );
  INVX1 U565 ( .A(n1407), .Y(n660) );
  INVX1 U566 ( .A(n1649), .Y(n665) );
  INVX1 U567 ( .A(n1650), .Y(n668) );
  INVX1 U568 ( .A(n1385), .Y(n669) );
  INVX1 U569 ( .A(n1387), .Y(n671) );
  INVX1 U570 ( .A(n1388), .Y(n672) );
  INVX1 U571 ( .A(n1389), .Y(n673) );
  INVX1 U572 ( .A(n1392), .Y(n676) );
  INVX1 U573 ( .A(n1393), .Y(n677) );
  INVX1 U574 ( .A(n1394), .Y(n678) );
  INVX1 U575 ( .A(n1390), .Y(n674) );
  INVX1 U576 ( .A(n1386), .Y(n670) );
  INVX1 U577 ( .A(n1645), .Y(n666) );
  INVX1 U578 ( .A(n1646), .Y(n667) );
  INVX1 U579 ( .A(n1391), .Y(n675) );
  INVX1 U580 ( .A(n1644), .Y(n681) );
  INVX1 U581 ( .A(n1647), .Y(n682) );
  INVX1 U582 ( .A(n1381), .Y(n685) );
  AND2X1 U583 ( .A(n1543), .B(n1350), .Y(io_cpu_resp_valid) );
  OR2X1 U584 ( .A(n1378), .B(n1012), .Y(io_cpu_npc[18]) );
  OR2X1 U585 ( .A(n1927), .B(n1458), .Y(n1080) );
  OR2X1 U586 ( .A(n1282), .B(n1868), .Y(n1871) );
  OR2X1 U587 ( .A(n2115), .B(n1463), .Y(n2116) );
  AND2X1 U588 ( .A(n1298), .B(io_cpu_resp_bits_btb_bits_bht_history[1]), .Y(
        n1291) );
  AND2X1 U589 ( .A(n1708), .B(n1362), .Y(n1495) );
  OR2X1 U590 ( .A(n1125), .B(n1122), .Y(n2359) );
  OR2X1 U591 ( .A(n1126), .B(n1122), .Y(n2353) );
  AND2X1 U592 ( .A(n1037), .B(io_cpu_resp_bits_btb_bits_target[2]), .Y(n1424)
         );
  OR2X1 U593 ( .A(n1050), .B(\tlb_io_req_bits_vpn[12] ), .Y(n1508) );
  OR2X1 U594 ( .A(n1472), .B(n1037), .Y(n1671) );
  BUFX2 U595 ( .A(n2028), .Y(n701) );
  BUFX2 U596 ( .A(n1341), .Y(n762) );
  INVX1 U597 ( .A(n2302), .Y(n744) );
  INVX1 U598 ( .A(n2368), .Y(n743) );
  AND2X1 U599 ( .A(n2202), .B(n2180), .Y(n1461) );
  OR2X1 U600 ( .A(s1_same_block), .B(n1042), .Y(n2403) );
  INVX1 U601 ( .A(n1292), .Y(n722) );
  INVX1 U602 ( .A(n1164), .Y(n716) );
  INVX1 U603 ( .A(n1272), .Y(n721) );
  AND2X1 U604 ( .A(BTB_io_req_bits_addr_30), .B(n2202), .Y(n1854) );
  AND2X1 U605 ( .A(n2071), .B(\tlb_io_req_bits_vpn[18] ), .Y(n1526) );
  AND2X1 U606 ( .A(n2071), .B(\tlb_io_req_bits_vpn[0] ), .Y(n1208) );
  AND2X1 U607 ( .A(n1463), .B(\icache_io_req_bits_addr[2] ), .Y(n1055) );
  AND2X1 U608 ( .A(n1037), .B(io_cpu_resp_bits_btb_bits_target[9]), .Y(n1484)
         );
  BUFX2 U609 ( .A(n1463), .Y(n1139) );
  INVX1 U610 ( .A(n1165), .Y(n757) );
  INVX1 U611 ( .A(n1017), .Y(n765) );
  INVX1 U612 ( .A(n1648), .Y(n590) );
  INVX1 U613 ( .A(n2281), .Y(n605) );
  INVX1 U614 ( .A(n2357), .Y(n606) );
  INVX1 U615 ( .A(n2361), .Y(n607) );
  INVX1 U616 ( .A(n2371), .Y(n608) );
  INVX1 U617 ( .A(n2311), .Y(n594) );
  INVX1 U618 ( .A(n2303), .Y(n597) );
  INVX1 U619 ( .A(n2297), .Y(n598) );
  AND2X1 U620 ( .A(n2202), .B(n2139), .Y(n1272) );
  AND2X1 U621 ( .A(n2202), .B(n2128), .Y(n2129) );
  AND2X1 U622 ( .A(n2057), .B(s2_speculative), .Y(n1150) );
  OR2X1 U623 ( .A(n2170), .B(n2071), .Y(n1513) );
  OR2X1 U624 ( .A(n2071), .B(n2173), .Y(n1160) );
  AND2X1 U625 ( .A(BTB_io_req_bits_addr[28]), .B(n1053), .Y(n1858) );
  OR2X1 U626 ( .A(n1253), .B(n1298), .Y(n2281) );
  OR2X1 U627 ( .A(n1254), .B(n1298), .Y(n2357) );
  OR2X1 U628 ( .A(n1255), .B(n1298), .Y(n2361) );
  OR2X1 U629 ( .A(n1256), .B(n1298), .Y(n2371) );
  AND2X1 U630 ( .A(BTB_io_req_bits_addr[2]), .B(n2374), .Y(n1980) );
  OR2X1 U631 ( .A(BTB_io_req_bits_addr[28]), .B(n1918), .Y(n1860) );
  OR2X1 U632 ( .A(n1124), .B(n1122), .Y(n2311) );
  OR2X1 U633 ( .A(n1127), .B(n1122), .Y(n2303) );
  OR2X1 U634 ( .A(n1128), .B(n1122), .Y(n2297) );
  AND2X1 U635 ( .A(n1798), .B(n1552), .Y(n1849) );
  INVX1 U636 ( .A(BTB_io_req_bits_addr[35]), .Y(n494) );
  INVX1 U637 ( .A(n1054), .Y(BTB_io_req_bits_addr[33]) );
  INVX1 U638 ( .A(n1157), .Y(n537) );
  AND2X1 U639 ( .A(n2202), .B(n2121), .Y(n2122) );
  AND2X1 U640 ( .A(n1281), .B(n2202), .Y(n1280) );
  INVX1 U641 ( .A(BTB_io_req_bits_addr[13]), .Y(n2124) );
  AND2X1 U642 ( .A(n1305), .B(BTB_io_req_bits_addr[34]), .Y(n1142) );
  AND2X1 U643 ( .A(n2202), .B(n2113), .Y(n1204) );
  AND2X1 U644 ( .A(n1961), .B(BTB_io_resp_bits_target[22]), .Y(n2150) );
  AND2X1 U645 ( .A(n2114), .B(n2113), .Y(n1632) );
  AND2X1 U646 ( .A(n2194), .B(n2099), .Y(n2101) );
  OR2X1 U647 ( .A(n2137), .B(n1872), .Y(n1873) );
  OR2X1 U648 ( .A(n1709), .B(\tlb_io_req_bits_vpn[5] ), .Y(n1596) );
  AND2X1 U649 ( .A(BTB_io_resp_bits_target[3]), .B(n1961), .Y(n1689) );
  BUFX2 U650 ( .A(n1298), .Y(n1122) );
  INVX1 U651 ( .A(n1053), .Y(n1918) );
  BUFX2 U652 ( .A(n1872), .Y(n1042) );
  INVX1 U653 ( .A(n2280), .Y(n546) );
  AND2X1 U654 ( .A(n640), .B(n1943), .Y(n2125) );
  OR2X1 U655 ( .A(n1850), .B(n1266), .Y(n1027) );
  OR2X1 U656 ( .A(n1270), .B(n1850), .Y(n1954) );
  OR2X1 U657 ( .A(n1271), .B(n1850), .Y(n1956) );
  AND2X1 U658 ( .A(BTB_io_resp_bits_target[4]), .B(n1351), .Y(n1693) );
  AND2X1 U659 ( .A(n959), .B(n868), .Y(n1174) );
  INVX1 U660 ( .A(n1359), .Y(n506) );
  AND2X1 U661 ( .A(BTB_io_req_bits_addr[34]), .B(n2374), .Y(n1188) );
  AND2X1 U662 ( .A(BTB_io_req_bits_addr[37]), .B(n2374), .Y(n1192) );
  AND2X1 U663 ( .A(n1351), .B(BTB_io_resp_bits_target[19]), .Y(n2137) );
  OR2X1 U664 ( .A(n1521), .B(n1225), .Y(n1579) );
  AND2X1 U665 ( .A(BTB_io_req_bits_addr[5]), .B(n2374), .Y(n1168) );
  OR2X1 U666 ( .A(n1702), .B(n1052), .Y(n1527) );
  OR2X1 U667 ( .A(n2141), .B(n1872), .Y(n2046) );
  AND2X1 U668 ( .A(n2112), .B(io_cpu_resp_bits_pc[22]), .Y(n2149) );
  OR2X1 U669 ( .A(n1850), .B(n1269), .Y(n491) );
  OR2X1 U670 ( .A(n1481), .B(n1850), .Y(n1851) );
  OR2X1 U671 ( .A(n1482), .B(n1850), .Y(n1922) );
  AND2X1 U672 ( .A(\tlb_io_req_bits_vpn[11] ), .B(n2374), .Y(n1182) );
  AND2X1 U673 ( .A(\tlb_io_req_bits_vpn[7] ), .B(n2374), .Y(n1166) );
  AND2X1 U674 ( .A(\tlb_io_req_bits_vpn[8] ), .B(n2374), .Y(n1172) );
  OR2X1 U675 ( .A(n1477), .B(n1231), .Y(n1919) );
  OR2X1 U676 ( .A(n1476), .B(n1231), .Y(n1856) );
  AND2X1 U677 ( .A(BTB_io_resp_bits_target[34]), .B(n2350), .Y(n1038) );
  AND2X1 U678 ( .A(\tlb_io_req_bits_vpn[9] ), .B(n2374), .Y(n1178) );
  AND2X1 U679 ( .A(n1281), .B(n2374), .Y(n1184) );
  AND2X1 U680 ( .A(BTB_io_resp_bits_target[8]), .B(n1351), .Y(n1998) );
  AND2X1 U681 ( .A(BTB_io_resp_bits_target[6]), .B(n1961), .Y(n1988) );
  BUFX4 U682 ( .A(n1036), .Y(n1298) );
  OR2X1 U683 ( .A(n786), .B(io_cpu_req_valid), .Y(n2047) );
  INVX1 U684 ( .A(n1831), .Y(\tlb_io_req_bits_vpn[19] ) );
  INVX1 U685 ( .A(n2034), .Y(n819) );
  INVX1 U686 ( .A(n1547), .Y(n534) );
  INVX1 U687 ( .A(n1095), .Y(n1211) );
  AND2X1 U688 ( .A(n1911), .B(BTB_io_req_bits_addr_24), .Y(n1912) );
  AND2X1 U689 ( .A(n1374), .B(n2049), .Y(n1825) );
  AND2X1 U690 ( .A(n1942), .B(n1375), .Y(n1943) );
  OR2X1 U691 ( .A(n477), .B(n1850), .Y(n868) );
  BUFX2 U692 ( .A(n1441), .Y(n1458) );
  AND2X1 U693 ( .A(n1351), .B(BTB_io_resp_bits_target[20]), .Y(n2141) );
  AND2X1 U694 ( .A(n1052), .B(n1892), .Y(n1876) );
  AND2X1 U695 ( .A(BTB_io_resp_bits_target[37]), .B(n1351), .Y(n2190) );
  OR2X1 U696 ( .A(n1262), .B(n1850), .Y(n1944) );
  OR2X1 U697 ( .A(n1263), .B(n1850), .Y(n1936) );
  OR2X1 U698 ( .A(n1264), .B(n1850), .Y(n1844) );
  OR2X1 U699 ( .A(n1265), .B(n1850), .Y(n1932) );
  AND2X1 U700 ( .A(BTB_io_resp_bits_target[2]), .B(n1351), .Y(n1691) );
  AND2X1 U701 ( .A(BTB_io_resp_bits_target[7]), .B(n1351), .Y(n1487) );
  AND2X1 U702 ( .A(BTB_io_resp_bits_target[9]), .B(n1351), .Y(n1842) );
  AND2X1 U703 ( .A(BTB_io_resp_bits_target[34]), .B(n1470), .Y(n1329) );
  INVX2 U704 ( .A(n1303), .Y(n1454) );
  BUFX2 U705 ( .A(n2405), .Y(BTB_io_req_bits_addr[29]) );
  BUFX2 U706 ( .A(n1375), .Y(\tlb_io_req_bits_vpn[0] ) );
  INVX1 U707 ( .A(n1520), .Y(n786) );
  AND2X1 U708 ( .A(n1372), .B(n796), .Y(n1373) );
  AND2X1 U709 ( .A(n1200), .B(n1900), .Y(n1901) );
  BUFX2 U710 ( .A(n1522), .Y(n1225) );
  AND2X1 U711 ( .A(BTB_io_req_bits_addr[11]), .B(BTB_io_req_bits_addr[10]), 
        .Y(n1942) );
  OR2X1 U712 ( .A(icache_io_s2_kill), .B(n1064), .Y(n1543) );
  OR2X1 U713 ( .A(n1268), .B(n1850), .Y(n1520) );
  AND2X1 U714 ( .A(n1200), .B(\tlb_io_req_bits_vpn[18] ), .Y(n1308) );
  AND2X1 U715 ( .A(n530), .B(\tlb_io_req_bits_vpn[14] ), .Y(n1346) );
  OR2X1 U716 ( .A(n2127), .B(n1501), .Y(n1500) );
  INVX1 U717 ( .A(n2039), .Y(\tlb_io_req_bits_vpn[6] ) );
  AND2X1 U718 ( .A(BTB_io_req_bits_addr[5]), .B(n1357), .Y(n1996) );
  AND2X1 U719 ( .A(n1900), .B(n1924), .Y(n1886) );
  OR2X1 U720 ( .A(n1261), .B(n1850), .Y(n2136) );
  INVX1 U721 ( .A(n2059), .Y(\tlb_io_req_bits_vpn[18] ) );
  INVX1 U722 ( .A(BTB_io_req_bits_addr_18), .Y(n2039) );
  INVX1 U723 ( .A(n528), .Y(\tlb_io_req_bits_vpn[14] ) );
  INVX1 U724 ( .A(n1115), .Y(n593) );
  AND2X1 U725 ( .A(BTB_io_req_bits_addr_30), .B(BTB_io_req_bits_addr_31), .Y(
        n1900) );
  INVX2 U726 ( .A(n540), .Y(n1199) );
  INVX1 U727 ( .A(n2419), .Y(n1959) );
  INVX1 U728 ( .A(n2014), .Y(n1375) );
  INVX1 U729 ( .A(BTB_io_req_bits_addr_26), .Y(n528) );
  AND2X1 U730 ( .A(n2417), .B(n2413), .Y(n1103) );
  OR2X1 U731 ( .A(n1952), .B(n1951), .Y(n1538) );
  INVX1 U732 ( .A(n1227), .Y(n547) );
  AND2X1 U733 ( .A(n535), .B(n2099), .Y(n1528) );
  INVX1 U734 ( .A(n1853), .Y(n542) );
  BUFX2 U735 ( .A(n2406), .Y(BTB_io_req_bits_addr[28]) );
  INVX1 U736 ( .A(icache_io_resp_valid), .Y(n1952) );
  INVX1 U737 ( .A(io_cpu_resp_ready), .Y(n1951) );
  INVX2 U738 ( .A(n475), .Y(n1086) );
  NOR2X1 U739 ( .A(n1199), .B(n1533), .Y(n475) );
  INVX1 U740 ( .A(n476), .Y(n1578) );
  NOR2X1 U741 ( .A(n1034), .B(n1052), .Y(n476) );
  AND2X2 U742 ( .A(n2418), .B(n2419), .Y(n500) );
  OR2X2 U743 ( .A(n1231), .B(n1259), .Y(n2194) );
  NAND2X1 U744 ( .A(n515), .B(n957), .Y(n686) );
  INVX1 U745 ( .A(n1442), .Y(n1987) );
  AND2X2 U746 ( .A(n1442), .B(n1347), .Y(n640) );
  AND2X2 U747 ( .A(n2091), .B(BTB_io_req_bits_addr[36]), .Y(n2073) );
  AND2X2 U748 ( .A(n519), .B(n1333), .Y(n2171) );
  AND2X2 U749 ( .A(n1072), .B(n1136), .Y(n1333) );
  BUFX4 U750 ( .A(n2409), .Y(BTB_io_req_bits_addr[13]) );
  AOI21X1 U751 ( .A(n1854), .B(n538), .C(n1526), .Y(n1158) );
  INVX2 U752 ( .A(n1333), .Y(n1075) );
  INVX1 U753 ( .A(n1219), .Y(n719) );
  OAI21X1 U754 ( .A(n1517), .B(n1051), .C(n2131), .Y(n1516) );
  INVX1 U755 ( .A(io_cpu_resp_bits_pc[39]), .Y(n477) );
  OAI21X1 U756 ( .A(n1515), .B(n2071), .C(n701), .Y(n411) );
  NAND2X1 U757 ( .A(n2202), .B(n2174), .Y(n724) );
  NAND2X1 U758 ( .A(n1139), .B(n711), .Y(n679) );
  NAND2X1 U759 ( .A(n2130), .B(n1622), .Y(\icache_io_req_bits_addr[15] ) );
  NAND2X1 U760 ( .A(n1174), .B(n1714), .Y(n1711) );
  AOI22X1 U761 ( .A(io_cpu_req_bits_pc[15]), .B(io_cpu_req_valid), .C(
        BTB_io_resp_bits_target[15]), .D(n1351), .Y(n842) );
  AND2X2 U762 ( .A(n1101), .B(n478), .Y(n633) );
  AND2X2 U763 ( .A(n2417), .B(n2412), .Y(n478) );
  AOI21X1 U764 ( .A(n511), .B(n1095), .C(n510), .Y(n509) );
  OR2X2 U765 ( .A(n740), .B(n479), .Y(n1116) );
  NAND2X1 U766 ( .A(BTB_io_req_bits_addr_32), .B(BTB_io_req_bits_addr_31), .Y(
        n479) );
  INVX8 U767 ( .A(n1223), .Y(n1533) );
  AND2X2 U768 ( .A(n1371), .B(n1368), .Y(n1374) );
  AND2X2 U769 ( .A(n1442), .B(n1347), .Y(n1371) );
  NOR2X1 U770 ( .A(n481), .B(n480), .Y(n695) );
  NAND2X1 U771 ( .A(BTB_io_req_bits_addr_25), .B(BTB_io_req_bits_addr_24), .Y(
        n480) );
  NAND2X1 U772 ( .A(BTB_io_req_bits_addr_23), .B(BTB_io_req_bits_addr_22), .Y(
        n481) );
  NAND2X1 U773 ( .A(n2170), .B(n723), .Y(\io_cpu_npc[29] ) );
  NAND2X1 U774 ( .A(n1903), .B(n1492), .Y(n1338) );
  INVX1 U775 ( .A(n482), .Y(n521) );
  INVX1 U776 ( .A(n1488), .Y(n483) );
  INVX1 U777 ( .A(n1140), .Y(n484) );
  NAND3X1 U778 ( .A(n484), .B(n483), .C(n1141), .Y(n482) );
  INVX1 U779 ( .A(n1034), .Y(n507) );
  NAND3X1 U780 ( .A(n1296), .B(n1307), .C(n1308), .Y(n1034) );
  NOR3X1 U781 ( .A(n1052), .B(BTB_io_req_bits_addr[34]), .C(n1458), .Y(n770)
         );
  AND2X2 U782 ( .A(n1010), .B(n809), .Y(n1072) );
  NOR2X1 U783 ( .A(n742), .B(n747), .Y(n809) );
  BUFX4 U784 ( .A(n1231), .Y(n485) );
  INVX2 U785 ( .A(n2171), .Y(n538) );
  INVX1 U786 ( .A(n1835), .Y(n486) );
  NOR2X1 U787 ( .A(n1549), .B(n486), .Y(n1321) );
  NAND2X1 U788 ( .A(n2202), .B(n1983), .Y(n1627) );
  NAND2X1 U789 ( .A(n967), .B(n487), .Y(n662) );
  NAND2X1 U790 ( .A(n1463), .B(\icache_io_req_bits_addr[5] ), .Y(n487) );
  NOR2X1 U791 ( .A(n1151), .B(n1860), .Y(n696) );
  NAND2X1 U792 ( .A(n1865), .B(n1864), .Y(n789) );
  OR2X2 U793 ( .A(n1225), .B(n1129), .Y(n1714) );
  AOI21X1 U794 ( .A(n488), .B(n1155), .C(n977), .Y(n578) );
  NOR2X1 U795 ( .A(n1042), .B(n2150), .Y(n488) );
  NAND2X1 U796 ( .A(n1890), .B(n1448), .Y(n986) );
  NAND2X1 U797 ( .A(n1139), .B(io_cpu_npc[8]), .Y(n734) );
  NAND3X1 U798 ( .A(n1326), .B(n773), .C(n489), .Y(n646) );
  NAND2X1 U799 ( .A(n2161), .B(n1497), .Y(n489) );
  INVX2 U800 ( .A(n1527), .Y(n1157) );
  INVX1 U801 ( .A(n536), .Y(n1156) );
  NAND2X1 U802 ( .A(n526), .B(n1463), .Y(n1343) );
  NAND2X1 U803 ( .A(n796), .B(n530), .Y(n533) );
  INVX8 U804 ( .A(n783), .Y(n805) );
  INVX1 U805 ( .A(s2_valid), .Y(n1349) );
  NAND2X1 U806 ( .A(s2_valid), .B(n1538), .Y(n1833) );
  AND2X2 U807 ( .A(n1302), .B(n1853), .Y(n1200) );
  NAND2X1 U808 ( .A(n2202), .B(n1837), .Y(n720) );
  NAND2X1 U809 ( .A(io_cpu_resp_bits_pc[32]), .B(n2112), .Y(n1898) );
  NOR3X1 U810 ( .A(n1050), .B(n1075), .C(n1338), .Y(n1337) );
  AOI21X1 U811 ( .A(n1912), .B(n1506), .C(n1494), .Y(n755) );
  NAND2X1 U812 ( .A(n1049), .B(n716), .Y(\io_cpu_npc[25] ) );
  AND2X2 U813 ( .A(n1885), .B(n491), .Y(n2178) );
  NAND2X1 U814 ( .A(n2187), .B(n783), .Y(n812) );
  NAND2X1 U815 ( .A(n1893), .B(n807), .Y(n996) );
  AOI21X1 U816 ( .A(n1322), .B(n1894), .C(n1107), .Y(n703) );
  NAND2X1 U817 ( .A(n2202), .B(n2147), .Y(n639) );
  OAI21X1 U818 ( .A(n946), .B(n945), .C(n492), .Y(n651) );
  AOI21X1 U819 ( .A(n1323), .B(n1815), .C(n493), .Y(n492) );
  NOR2X1 U820 ( .A(n494), .B(n1323), .Y(n493) );
  AND2X2 U821 ( .A(n513), .B(n1514), .Y(n1822) );
  AND2X2 U822 ( .A(n779), .B(n1463), .Y(n1518) );
  NAND3X1 U823 ( .A(n1625), .B(n1694), .C(n1701), .Y(n779) );
  AND2X2 U824 ( .A(n539), .B(n695), .Y(n1223) );
  AND2X2 U825 ( .A(n1238), .B(n1239), .Y(n539) );
  AND2X2 U826 ( .A(n1092), .B(n807), .Y(n795) );
  AND2X2 U827 ( .A(n1296), .B(n1302), .Y(n1092) );
  INVX1 U828 ( .A(n495), .Y(n1088) );
  INVX1 U829 ( .A(n1356), .Y(n496) );
  OAI21X1 U830 ( .A(n1086), .B(n496), .C(n2064), .Y(n495) );
  NAND2X1 U831 ( .A(n2202), .B(n2166), .Y(n637) );
  NAND2X1 U832 ( .A(n2197), .B(n1838), .Y(n727) );
  AND2X2 U833 ( .A(n1373), .B(n2107), .Y(n2193) );
  NOR2X1 U834 ( .A(n1260), .B(n485), .Y(n814) );
  OAI21X1 U835 ( .A(n984), .B(n2134), .C(n2036), .Y(n410) );
  OAI21X1 U836 ( .A(n1686), .B(n1629), .C(n708), .Y(n2134) );
  NAND2X1 U837 ( .A(n1372), .B(\tlb_io_req_bits_vpn[5] ), .Y(n972) );
  OR2X2 U838 ( .A(n951), .B(n853), .Y(n663) );
  NAND2X1 U839 ( .A(n1789), .B(n1967), .Y(n827) );
  AND2X2 U840 ( .A(n1510), .B(n500), .Y(n1442) );
  INVX2 U841 ( .A(n1632), .Y(n782) );
  INVX1 U842 ( .A(n501), .Y(n522) );
  NOR2X1 U843 ( .A(n1067), .B(n1066), .Y(n501) );
  OAI21X1 U844 ( .A(n502), .B(n745), .C(n2116), .Y(n471) );
  OR2X2 U845 ( .A(n2199), .B(n1042), .Y(n502) );
  OAI21X1 U846 ( .A(n1064), .B(icache_io_s2_kill), .C(n787), .Y(n1316) );
  NOR2X1 U847 ( .A(n1318), .B(n1349), .Y(n787) );
  AOI22X1 U848 ( .A(BTB_io_req_bits_addr[13]), .B(n1052), .C(n1808), .D(n1947), 
        .Y(n838) );
  AND2X1 U849 ( .A(n1052), .B(\tlb_io_req_bits_vpn[12] ), .Y(n1162) );
  NAND2X1 U850 ( .A(BTB_io_req_bits_addr_30), .B(n2404), .Y(n740) );
  AND2X2 U851 ( .A(n1103), .B(n1104), .Y(n634) );
  NOR2X1 U852 ( .A(n1703), .B(n1448), .Y(n526) );
  INVX2 U853 ( .A(n1889), .Y(n1448) );
  NOR3X1 U854 ( .A(n532), .B(n534), .C(n533), .Y(n1889) );
  NAND2X1 U855 ( .A(n2033), .B(n1674), .Y(n708) );
  NOR3X1 U856 ( .A(n1533), .B(n1199), .C(n506), .Y(n2179) );
  NAND2X1 U857 ( .A(n1512), .B(n1348), .Y(n683) );
  AND2X2 U858 ( .A(n965), .B(n1611), .Y(n978) );
  NAND2X1 U859 ( .A(n1052), .B(n1877), .Y(n1637) );
  AOI21X1 U860 ( .A(n1276), .B(n1275), .C(n979), .Y(n582) );
  NOR2X1 U861 ( .A(n507), .B(n1830), .Y(n1488) );
  BUFX4 U862 ( .A(n1052), .Y(n1151) );
  NAND2X1 U863 ( .A(n2165), .B(n696), .Y(n763) );
  INVX8 U864 ( .A(n1533), .Y(n1296) );
  AND2X2 U865 ( .A(BTB_io_req_bits_addr[27]), .B(BTB_io_req_bits_addr_26), .Y(
        n1924) );
  INVX1 U866 ( .A(n509), .Y(n1059) );
  INVX1 U867 ( .A(n1210), .Y(n510) );
  INVX1 U868 ( .A(n1574), .Y(n511) );
  AND2X2 U869 ( .A(n1942), .B(n1347), .Y(n694) );
  AND2X2 U870 ( .A(n1375), .B(BTB_io_req_bits_addr[11]), .Y(n513) );
  INVX2 U871 ( .A(n514), .Y(n2019) );
  NAND3X1 U872 ( .A(BTB_io_req_bits_addr[14]), .B(n1371), .C(n1822), .Y(n514)
         );
  NAND2X1 U873 ( .A(n1463), .B(\io_cpu_npc[14] ), .Y(n515) );
  AND2X2 U874 ( .A(n1822), .B(n640), .Y(n1431) );
  AND2X2 U875 ( .A(n897), .B(n1233), .Y(n1049) );
  NAND2X1 U876 ( .A(n950), .B(n709), .Y(n1019) );
  AND2X2 U877 ( .A(n1912), .B(n1333), .Y(n1546) );
  NAND2X1 U878 ( .A(\tlb_io_req_bits_vpn[6] ), .B(n807), .Y(n785) );
  NAND2X1 U879 ( .A(n2140), .B(n721), .Y(\icache_io_req_bits_addr[19] ) );
  NOR2X1 U880 ( .A(n1914), .B(n1061), .Y(n588) );
  NAND2X1 U881 ( .A(n1619), .B(n726), .Y(n687) );
  AOI22X1 U882 ( .A(reset), .B(io_resetVector[6]), .C(io_cpu_resp_bits_pc[6]), 
        .D(n2057), .Y(n918) );
  AOI22X1 U883 ( .A(reset), .B(io_resetVector[22]), .C(io_cpu_resp_bits_pc[22]), .D(n2057), .Y(n924) );
  AOI22X1 U884 ( .A(reset), .B(io_resetVector[29]), .C(io_cpu_resp_bits_pc[29]), .D(n2057), .Y(n930) );
  AOI22X1 U885 ( .A(reset), .B(io_resetVector[38]), .C(io_cpu_resp_bits_pc[38]), .D(n2057), .Y(n938) );
  AOI22X1 U886 ( .A(reset), .B(io_resetVector[39]), .C(io_cpu_resp_bits_pc[39]), .D(n2057), .Y(n940) );
  AOI22X1 U887 ( .A(reset), .B(io_resetVector[4]), .C(io_cpu_resp_bits_pc[4]), 
        .D(n2057), .Y(n848) );
  AOI22X1 U888 ( .A(io_resetVector[7]), .B(reset), .C(io_cpu_resp_bits_pc[7]), 
        .D(n2057), .Y(n850) );
  AOI22X1 U889 ( .A(reset), .B(io_resetVector[19]), .C(io_cpu_resp_bits_pc[19]), .D(n2057), .Y(n914) );
  AOI22X1 U890 ( .A(reset), .B(io_resetVector[5]), .C(io_cpu_resp_bits_pc[5]), 
        .D(n2057), .Y(n916) );
  AOI22X1 U891 ( .A(reset), .B(io_resetVector[20]), .C(io_cpu_resp_bits_pc[20]), .D(n2057), .Y(n920) );
  AOI22X1 U892 ( .A(reset), .B(io_resetVector[21]), .C(io_cpu_resp_bits_pc[21]), .D(n2057), .Y(n922) );
  AOI22X1 U893 ( .A(reset), .B(io_resetVector[23]), .C(io_cpu_resp_bits_pc[23]), .D(n2057), .Y(n926) );
  AOI22X1 U894 ( .A(reset), .B(io_resetVector[27]), .C(io_cpu_resp_bits_pc[27]), .D(n2057), .Y(n928) );
  AOI22X1 U895 ( .A(reset), .B(io_resetVector[34]), .C(io_cpu_resp_bits_pc[34]), .D(n2057), .Y(n932) );
  AOI22X1 U896 ( .A(reset), .B(io_resetVector[35]), .C(io_cpu_resp_bits_pc[35]), .D(n2057), .Y(n934) );
  AOI22X1 U897 ( .A(reset), .B(io_resetVector[37]), .C(io_cpu_resp_bits_pc[37]), .D(n2057), .Y(n936) );
  NAND2X1 U898 ( .A(n1454), .B(n516), .Y(n1575) );
  AND2X2 U899 ( .A(n1296), .B(BTB_io_req_bits_addr[37]), .Y(n516) );
  NAND2X1 U900 ( .A(n1454), .B(n517), .Y(n1576) );
  AND2X2 U901 ( .A(n1296), .B(BTB_io_req_bits_addr[38]), .Y(n517) );
  AOI21X1 U902 ( .A(n2075), .B(n2187), .C(n1549), .Y(n579) );
  AOI21X1 U903 ( .A(n2093), .B(n2193), .C(n1549), .Y(n584) );
  NAND3X1 U904 ( .A(n1534), .B(n1871), .C(n518), .Y(n1602) );
  NAND2X1 U905 ( .A(n1870), .B(n636), .Y(n518) );
  NOR2X1 U906 ( .A(n1533), .B(n1449), .Y(n519) );
  NAND2X1 U907 ( .A(n2173), .B(n722), .Y(\icache_io_req_bits_addr[30] ) );
  INVX1 U908 ( .A(n1405), .Y(n649) );
  NAND2X1 U909 ( .A(n1463), .B(\io_cpu_npc[9] ), .Y(n911) );
  OAI21X1 U910 ( .A(n1203), .B(n520), .C(n1531), .Y(n388) );
  NAND2X1 U911 ( .A(n1301), .B(n2111), .Y(n520) );
  INVX1 U912 ( .A(n1888), .Y(n532) );
  INVX4 U913 ( .A(n1086), .Y(n783) );
  NAND2X1 U914 ( .A(BTB_io_req_bits_addr_17), .B(n2408), .Y(n741) );
  NAND2X1 U915 ( .A(n1158), .B(n1156), .Y(n664) );
  NAND2X1 U916 ( .A(n1069), .B(n902), .Y(n653) );
  NAND2X1 U917 ( .A(n1070), .B(n903), .Y(n654) );
  NAND2X1 U918 ( .A(n1671), .B(n907), .Y(n684) );
  NAND2X1 U919 ( .A(n734), .B(n1795), .Y(n656) );
  AOI22X1 U920 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[8]), .C(
        io_cpu_resp_bits_pc[8]), .D(n2112), .Y(n831) );
  AND2X2 U921 ( .A(n1524), .B(n1277), .Y(n692) );
  AOI21X1 U922 ( .A(n2185), .B(n1022), .C(n1019), .Y(n702) );
  AND2X2 U923 ( .A(n1335), .B(n1506), .Y(n1021) );
  NAND2X1 U924 ( .A(n1056), .B(n713), .Y(n650) );
  NAND2X1 U925 ( .A(n2202), .B(n1847), .Y(n690) );
  NAND2X1 U926 ( .A(n1463), .B(n781), .Y(n733) );
  AOI21X1 U927 ( .A(n2100), .B(n1060), .C(n1121), .Y(n574) );
  NAND2X1 U928 ( .A(n1220), .B(n719), .Y(\io_cpu_npc[23] ) );
  NAND2X1 U929 ( .A(n2176), .B(n720), .Y(\icache_io_req_bits_addr[32] ) );
  NAND2X1 U930 ( .A(n2202), .B(n2152), .Y(n638) );
  NAND2X1 U931 ( .A(n1198), .B(n1333), .Y(n1135) );
  INVX4 U932 ( .A(n1924), .Y(n541) );
  NAND2X1 U933 ( .A(s2_valid), .B(n1952), .Y(n778) );
  OAI21X1 U934 ( .A(n1688), .B(n1578), .C(n521), .Y(n396) );
  NAND2X1 U935 ( .A(n2175), .B(n724), .Y(\io_cpu_npc[31] ) );
  NOR2X1 U936 ( .A(n1533), .B(n833), .Y(n1114) );
  NAND2X1 U937 ( .A(n2202), .B(n2169), .Y(n723) );
  OAI21X1 U938 ( .A(n1908), .B(n1504), .C(n522), .Y(n404) );
  AOI21X1 U939 ( .A(n1829), .B(n1075), .C(n2159), .Y(n583) );
  NAND2X1 U940 ( .A(n2164), .B(n1045), .Y(\icache_io_req_bits_addr[27] ) );
  NAND2X1 U941 ( .A(n1280), .B(n909), .Y(n697) );
  NAND2X1 U942 ( .A(n1681), .B(n1787), .Y(n698) );
  NAND2X1 U943 ( .A(n912), .B(n714), .Y(n688) );
  NAND2X1 U944 ( .A(BTB_io_req_bits_addr_15), .B(n2407), .Y(n748) );
  NAND3X1 U945 ( .A(n1557), .B(n1695), .C(n1627), .Y(
        \icache_io_req_bits_addr[5] ) );
  NAND2X1 U946 ( .A(BTB_io_req_bits_addr_12), .B(n2411), .Y(n742) );
  NAND3X1 U947 ( .A(BTB_io_req_bits_addr[2]), .B(n1354), .C(n1357), .Y(n994)
         );
  INVX1 U948 ( .A(BTB_io_req_bits_addr[10]), .Y(n524) );
  INVX1 U949 ( .A(BTB_io_req_bits_addr[13]), .Y(n525) );
  NOR2X1 U950 ( .A(n525), .B(n524), .Y(n1514) );
  NOR2X1 U951 ( .A(n2071), .B(n2167), .Y(n758) );
  AND2X2 U952 ( .A(n1072), .B(n1548), .Y(n1307) );
  AND2X2 U953 ( .A(n633), .B(n692), .Y(n1548) );
  NOR2X1 U954 ( .A(n1151), .B(n1457), .Y(n636) );
  NAND3X1 U955 ( .A(n762), .B(n774), .C(n775), .Y(n648) );
  NOR2X1 U956 ( .A(n1052), .B(n2176), .Y(n759) );
  AND2X2 U957 ( .A(n1101), .B(n1058), .Y(n691) );
  NAND3X1 U958 ( .A(n1085), .B(n1228), .C(n1493), .Y(n644) );
  NAND3X1 U959 ( .A(n1369), .B(n986), .C(n1343), .Y(n647) );
  AND2X2 U960 ( .A(n809), .B(n1010), .Y(n796) );
  NOR2X1 U961 ( .A(n748), .B(n741), .Y(n1010) );
  NAND2X1 U962 ( .A(n2410), .B(n2409), .Y(n747) );
  NAND3X1 U963 ( .A(n1506), .B(n1454), .C(n1142), .Y(n576) );
  NAND2X1 U964 ( .A(n690), .B(n1849), .Y(n781) );
  NAND2X1 U965 ( .A(n733), .B(n718), .Y(n661) );
  BUFX4 U966 ( .A(n1333), .Y(n1506) );
  BUFX2 U967 ( .A(BTB_io_req_bits_addr_23), .Y(n527) );
  AND2X2 U968 ( .A(n539), .B(n695), .Y(n530) );
  INVX1 U969 ( .A(n1542), .Y(n535) );
  OAI21X1 U970 ( .A(n537), .B(n538), .C(n1160), .Y(n536) );
  NOR3X1 U971 ( .A(n541), .B(n542), .C(n1116), .Y(n540) );
  AND2X2 U972 ( .A(n1296), .B(BTB_io_req_bits_addr[35]), .Y(n1305) );
  BUFX2 U973 ( .A(BTB_io_req_bits_addr_32), .Y(\tlb_io_req_bits_vpn[20] ) );
  AND2X2 U974 ( .A(\tlb_io_req_bits_vpn[20] ), .B(n1053), .Y(n1902) );
  OR2X2 U975 ( .A(n1131), .B(n1298), .Y(n2280) );
  OR2X2 U976 ( .A(io_cpu_req_valid), .B(n1799), .Y(n1227) );
  BUFX2 U977 ( .A(n1237), .Y(n567) );
  BUFX2 U978 ( .A(n2017), .Y(n572) );
  OR2X2 U979 ( .A(n944), .B(n1088), .Y(n946) );
  INVX1 U980 ( .A(n1038), .Y(n586) );
  AND2X2 U981 ( .A(BTB_io_resp_bits_target[36]), .B(n1961), .Y(n1598) );
  INVX1 U982 ( .A(n1598), .Y(n589) );
  AND2X2 U983 ( .A(n1796), .B(n1617), .Y(n1648) );
  OR2X2 U984 ( .A(n1360), .B(n541), .Y(n1115) );
  AND2X2 U985 ( .A(n955), .B(n1637), .Y(n1152) );
  INVX1 U986 ( .A(n1152), .Y(n600) );
  AND2X2 U987 ( .A(n1201), .B(n1346), .Y(n1279) );
  INVX1 U988 ( .A(n1279), .Y(n612) );
  AND2X2 U989 ( .A(n1330), .B(n728), .Y(n1539) );
  AND2X2 U990 ( .A(n1315), .B(n727), .Y(n1440) );
  INVX1 U991 ( .A(n1440), .Y(\io_cpu_npc[38] ) );
  BUFX2 U992 ( .A(n1119), .Y(n626) );
  BUFX2 U993 ( .A(n1284), .Y(n632) );
  AND2X2 U994 ( .A(n2202), .B(n1133), .Y(n1378) );
  AND2X2 U995 ( .A(n1620), .B(n911), .Y(n1405) );
  AND2X2 U996 ( .A(n948), .B(n900), .Y(n379) );
  AND2X2 U997 ( .A(n1071), .B(n905), .Y(n382) );
  AND2X2 U998 ( .A(n1791), .B(n1676), .Y(n1607) );
  AND2X2 U999 ( .A(n1797), .B(n1678), .Y(n1608) );
  AND2X2 U1000 ( .A(n1788), .B(n1675), .Y(n1406) );
  AND2X2 U1001 ( .A(n1792), .B(n1677), .Y(n1407) );
  AND2X2 U1002 ( .A(n914), .B(n857), .Y(n1649) );
  AND2X2 U1003 ( .A(n916), .B(n860), .Y(n1645) );
  AND2X2 U1004 ( .A(n918), .B(n863), .Y(n1646) );
  AND2X2 U1005 ( .A(n920), .B(n866), .Y(n1650) );
  AND2X2 U1006 ( .A(n922), .B(n870), .Y(n1385) );
  AND2X2 U1007 ( .A(n924), .B(n873), .Y(n1386) );
  AND2X2 U1008 ( .A(n926), .B(n876), .Y(n1387) );
  AND2X2 U1009 ( .A(n928), .B(n879), .Y(n1388) );
  AND2X2 U1010 ( .A(n930), .B(n882), .Y(n1389) );
  AND2X2 U1011 ( .A(n932), .B(n885), .Y(n1390) );
  AND2X2 U1012 ( .A(n934), .B(n717), .Y(n1391) );
  AND2X2 U1013 ( .A(n936), .B(n888), .Y(n1392) );
  AND2X2 U1014 ( .A(n938), .B(n891), .Y(n1393) );
  AND2X2 U1015 ( .A(n940), .B(n894), .Y(n1394) );
  AND2X2 U1016 ( .A(n962), .B(n1790), .Y(n1606) );
  AND2X2 U1017 ( .A(n1794), .B(n848), .Y(n1644) );
  AND2X2 U1018 ( .A(n1680), .B(n850), .Y(n1647) );
  AND2X2 U1019 ( .A(n1793), .B(n1289), .Y(n1381) );
  BUFX2 U1020 ( .A(n1412), .Y(n704) );
  BUFX2 U1021 ( .A(n1217), .Y(n706) );
  BUFX2 U1022 ( .A(n1163), .Y(n709) );
  BUFX2 U1023 ( .A(n1212), .Y(n711) );
  INVX1 U1024 ( .A(n1055), .Y(n713) );
  INVX1 U1025 ( .A(n1321), .Y(n714) );
  AND2X2 U1026 ( .A(n2160), .B(n2202), .Y(n1164) );
  AND2X1 U1027 ( .A(BTB_io_req_bits_addr[35]), .B(n2374), .Y(n1190) );
  INVX1 U1028 ( .A(n1190), .Y(n717) );
  INVX1 U1029 ( .A(n1208), .Y(n718) );
  AND2X2 U1030 ( .A(n1812), .B(n1679), .Y(n1219) );
  AND2X2 U1031 ( .A(n2202), .B(n2172), .Y(n1292) );
  AND2X2 U1032 ( .A(n1463), .B(n1008), .Y(n1313) );
  INVX1 U1033 ( .A(n1313), .Y(n726) );
  INVX1 U1034 ( .A(n1329), .Y(n728) );
  AND2X2 U1035 ( .A(n2418), .B(n2414), .Y(n1058) );
  AND2X2 U1036 ( .A(n2412), .B(n2415), .Y(n1104) );
  OR2X2 U1037 ( .A(n1129), .B(n1298), .Y(n2368) );
  OR2X2 U1038 ( .A(n1130), .B(n1298), .Y(n2302) );
  AND2X2 U1039 ( .A(n2378), .B(n782), .Y(n1624) );
  INVX1 U1040 ( .A(n1624), .Y(n745) );
  AND2X2 U1041 ( .A(BTB_io_req_bits_addr_20), .B(BTB_io_req_bits_addr_21), .Y(
        n1239) );
  AND2X2 U1042 ( .A(BTB_io_req_bits_addr_19), .B(BTB_io_req_bits_addr_18), .Y(
        n1238) );
  AND2X2 U1043 ( .A(n2419), .B(n2416), .Y(n1101) );
  AND2X2 U1044 ( .A(n2415), .B(n2418), .Y(n1277) );
  OR2X2 U1045 ( .A(n2071), .B(n2178), .Y(n1165) );
  INVX1 U1046 ( .A(n1204), .Y(n764) );
  AND2X2 U1047 ( .A(n1609), .B(n1928), .Y(n1017) );
  AND2X2 U1048 ( .A(n1463), .B(n1921), .Y(n1325) );
  INVX1 U1049 ( .A(n1325), .Y(n773) );
  AND2X2 U1050 ( .A(n1902), .B(n793), .Y(n1340) );
  INVX1 U1051 ( .A(n1340), .Y(n774) );
  INVX1 U1052 ( .A(n1337), .Y(n775) );
  OR2X2 U1053 ( .A(n1052), .B(n2175), .Y(n1141) );
  OR2X2 U1054 ( .A(n1052), .B(n1202), .Y(n1549) );
  INVX1 U1055 ( .A(n1549), .Y(n791) );
  AND2X2 U1056 ( .A(n1492), .B(n1333), .Y(n1491) );
  INVX1 U1057 ( .A(n1491), .Y(n793) );
  INVX1 U1058 ( .A(n1491), .Y(n794) );
  AND2X1 U1059 ( .A(BTB_io_req_bits_addr[11]), .B(n2202), .Y(n1939) );
  INVX1 U1060 ( .A(n810), .Y(n808) );
  AND2X2 U1061 ( .A(n1296), .B(\s1_pc_[39] ), .Y(n1068) );
  INVX1 U1062 ( .A(n1296), .Y(n799) );
  OR2X2 U1063 ( .A(n1461), .B(n1707), .Y(\io_cpu_npc[34] ) );
  BUFX2 U1064 ( .A(BTB_io_req_bits_addr_20), .Y(n800) );
  BUFX2 U1065 ( .A(n1303), .Y(n801) );
  BUFX2 U1066 ( .A(n2410), .Y(BTB_io_req_bits_addr[11]) );
  INVX4 U1067 ( .A(n1507), .Y(n2071) );
  BUFX2 U1068 ( .A(n1199), .Y(n1303) );
  OAI21X1 U1069 ( .A(n1064), .B(icache_io_s2_kill), .C(n787), .Y(n806) );
  AND2X2 U1070 ( .A(n1072), .B(n1136), .Y(n807) );
  AND2X2 U1071 ( .A(n2187), .B(n783), .Y(n810) );
  AND2X1 U1072 ( .A(io_cpu_resp_bits_btb_bits_target[38]), .B(n1298), .Y(n1469) );
  INVX1 U1073 ( .A(n1469), .Y(n813) );
  INVX1 U1074 ( .A(n1944), .Y(n815) );
  INVX1 U1075 ( .A(n1936), .Y(n816) );
  INVX1 U1076 ( .A(n1844), .Y(n817) );
  INVX1 U1077 ( .A(n1932), .Y(n818) );
  OR2X2 U1078 ( .A(n1267), .B(n1850), .Y(n2034) );
  INVX1 U1079 ( .A(n1954), .Y(n821) );
  INVX1 U1080 ( .A(n1956), .Y(n822) );
  AND2X1 U1081 ( .A(n1897), .B(io_cpu_req_valid), .Y(n1290) );
  INVX1 U1082 ( .A(n1290), .Y(n823) );
  AND2X1 U1083 ( .A(io_cpu_resp_bits_btb_bits_bht_history[4]), .B(n1298), .Y(
        n1464) );
  INVX1 U1084 ( .A(n1464), .Y(n824) );
  AND2X1 U1085 ( .A(n1463), .B(n1498), .Y(n1497) );
  AND2X2 U1086 ( .A(n2202), .B(n2162), .Y(n1599) );
  INVX1 U1087 ( .A(n1599), .Y(n825) );
  INVX1 U1088 ( .A(n590), .Y(n828) );
  INVX1 U1089 ( .A(n828), .Y(n829) );
  AND2X1 U1090 ( .A(n1710), .B(n2099), .Y(n2082) );
  AND2X2 U1091 ( .A(n1053), .B(n1835), .Y(n1618) );
  INVX1 U1092 ( .A(n1618), .Y(n832) );
  INVX1 U1093 ( .A(n593), .Y(n833) );
  INVX1 U1094 ( .A(n567), .Y(n835) );
  INVX1 U1095 ( .A(n835), .Y(n836) );
  INVX1 U1096 ( .A(n572), .Y(n839) );
  INVX1 U1097 ( .A(n839), .Y(n840) );
  INVX1 U1098 ( .A(n1162), .Y(n843) );
  INVX1 U1099 ( .A(n846), .Y(n844) );
  INVX1 U1100 ( .A(n844), .Y(n845) );
  AND2X1 U1101 ( .A(io_cpu_req_bits_pc[31]), .B(io_cpu_req_valid), .Y(n1287)
         );
  INVX1 U1102 ( .A(n1287), .Y(n846) );
  INVX1 U1103 ( .A(n855), .Y(n853) );
  AND2X1 U1104 ( .A(s1_speculative), .B(n2374), .Y(n1149) );
  INVX1 U1105 ( .A(n1149), .Y(n855) );
  INVX1 U1106 ( .A(n858), .Y(n856) );
  INVX1 U1107 ( .A(n856), .Y(n857) );
  INVX1 U1108 ( .A(n1166), .Y(n858) );
  INVX1 U1109 ( .A(n861), .Y(n859) );
  INVX1 U1110 ( .A(n859), .Y(n860) );
  INVX1 U1111 ( .A(n1168), .Y(n861) );
  INVX1 U1112 ( .A(n864), .Y(n862) );
  INVX1 U1113 ( .A(n862), .Y(n863) );
  AND2X1 U1114 ( .A(BTB_io_req_bits_addr[6]), .B(n2374), .Y(n1170) );
  INVX1 U1115 ( .A(n1170), .Y(n864) );
  INVX1 U1116 ( .A(n867), .Y(n865) );
  INVX1 U1117 ( .A(n865), .Y(n866) );
  INVX1 U1118 ( .A(n1172), .Y(n867) );
  INVX1 U1119 ( .A(n871), .Y(n869) );
  INVX1 U1120 ( .A(n869), .Y(n870) );
  INVX1 U1121 ( .A(n1178), .Y(n871) );
  INVX1 U1122 ( .A(n874), .Y(n872) );
  INVX1 U1123 ( .A(n872), .Y(n873) );
  AND2X1 U1124 ( .A(\tlb_io_req_bits_vpn[10] ), .B(n2374), .Y(n1180) );
  INVX1 U1125 ( .A(n1180), .Y(n874) );
  INVX1 U1126 ( .A(n877), .Y(n875) );
  INVX1 U1127 ( .A(n875), .Y(n876) );
  INVX1 U1128 ( .A(n1182), .Y(n877) );
  INVX1 U1129 ( .A(n880), .Y(n878) );
  INVX1 U1130 ( .A(n878), .Y(n879) );
  INVX1 U1131 ( .A(n1184), .Y(n880) );
  INVX1 U1132 ( .A(n883), .Y(n881) );
  INVX1 U1133 ( .A(n881), .Y(n882) );
  AND2X1 U1134 ( .A(BTB_io_req_bits_addr[29]), .B(n2374), .Y(n1186) );
  INVX1 U1135 ( .A(n1186), .Y(n883) );
  INVX1 U1136 ( .A(n886), .Y(n884) );
  INVX1 U1137 ( .A(n884), .Y(n885) );
  INVX1 U1138 ( .A(n1188), .Y(n886) );
  INVX1 U1139 ( .A(n889), .Y(n887) );
  INVX1 U1140 ( .A(n887), .Y(n888) );
  INVX1 U1141 ( .A(n1192), .Y(n889) );
  INVX1 U1142 ( .A(n892), .Y(n890) );
  INVX1 U1143 ( .A(n890), .Y(n891) );
  AND2X1 U1144 ( .A(BTB_io_req_bits_addr[38]), .B(n2374), .Y(n1194) );
  INVX1 U1145 ( .A(n1194), .Y(n892) );
  INVX1 U1146 ( .A(n895), .Y(n893) );
  INVX1 U1147 ( .A(n893), .Y(n894) );
  AND2X1 U1148 ( .A(\s1_pc_[39] ), .B(n2374), .Y(n1196) );
  INVX1 U1149 ( .A(n1196), .Y(n895) );
  INVX1 U1150 ( .A(n898), .Y(n896) );
  INVX1 U1151 ( .A(n896), .Y(n897) );
  AND2X1 U1152 ( .A(BTB_io_resp_bits_target[25]), .B(n1351), .Y(n1232) );
  INVX1 U1153 ( .A(n1232), .Y(n898) );
  INVX1 U1154 ( .A(n901), .Y(n899) );
  INVX1 U1155 ( .A(n899), .Y(n900) );
  AND2X1 U1156 ( .A(io_cpu_resp_bits_btb_bits_target[34]), .B(n1298), .Y(n1465) );
  INVX1 U1157 ( .A(n1465), .Y(n901) );
  AND2X1 U1158 ( .A(io_cpu_resp_bits_btb_bits_target[35]), .B(n1298), .Y(n1466) );
  INVX1 U1159 ( .A(n1466), .Y(n902) );
  AND2X1 U1160 ( .A(io_cpu_resp_bits_btb_bits_target[36]), .B(n1298), .Y(n1467) );
  INVX1 U1161 ( .A(n1467), .Y(n903) );
  INVX1 U1162 ( .A(n906), .Y(n904) );
  INVX1 U1163 ( .A(n904), .Y(n905) );
  AND2X1 U1164 ( .A(io_cpu_resp_bits_btb_bits_target[37]), .B(n1299), .Y(n1468) );
  INVX1 U1165 ( .A(n1468), .Y(n906) );
  AND2X1 U1166 ( .A(io_cpu_resp_bits_btb_bits_bridx), .B(n1298), .Y(n1258) );
  INVX1 U1167 ( .A(n1258), .Y(n907) );
  INVX1 U1168 ( .A(n612), .Y(n908) );
  INVX1 U1169 ( .A(n908), .Y(n909) );
  AND2X2 U1170 ( .A(n1320), .B(n698), .Y(n1319) );
  INVX1 U1171 ( .A(n1319), .Y(n912) );
  INVX1 U1172 ( .A(n576), .Y(n944) );
  INVX1 U1173 ( .A(n791), .Y(n945) );
  INVX1 U1174 ( .A(n586), .Y(n947) );
  INVX1 U1175 ( .A(n947), .Y(n948) );
  INVX1 U1176 ( .A(n589), .Y(n949) );
  INVX1 U1177 ( .A(n949), .Y(n950) );
  INVX1 U1178 ( .A(n953), .Y(n951) );
  INVX1 U1179 ( .A(n1150), .Y(n953) );
  INVX1 U1180 ( .A(n956), .Y(n954) );
  INVX1 U1181 ( .A(n954), .Y(n955) );
  AND2X1 U1182 ( .A(n1880), .B(io_cpu_req_valid), .Y(n1153) );
  INVX1 U1183 ( .A(n1153), .Y(n956) );
  AND2X1 U1184 ( .A(n2071), .B(BTB_io_req_bits_addr[14]), .Y(n1311) );
  INVX1 U1185 ( .A(n1311), .Y(n957) );
  INVX1 U1186 ( .A(n960), .Y(n958) );
  INVX1 U1187 ( .A(n958), .Y(n959) );
  AND2X1 U1188 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[39]), .Y(n1176)
         );
  INVX1 U1189 ( .A(n1176), .Y(n960) );
  INVX1 U1190 ( .A(n963), .Y(n961) );
  INVX1 U1191 ( .A(n961), .Y(n962) );
  AND2X1 U1192 ( .A(n1214), .B(n2374), .Y(n1213) );
  INVX1 U1193 ( .A(n1213), .Y(n963) );
  INVX1 U1194 ( .A(n966), .Y(n964) );
  INVX1 U1195 ( .A(n964), .Y(n965) );
  AND2X1 U1196 ( .A(n1875), .B(io_cpu_req_valid), .Y(n1274) );
  INVX1 U1197 ( .A(n1274), .Y(n966) );
  AND2X1 U1198 ( .A(n1151), .B(BTB_io_req_bits_addr[5]), .Y(n1288) );
  INVX1 U1199 ( .A(n1288), .Y(n967) );
  INVX1 U1200 ( .A(n970), .Y(n968) );
  INVX1 U1201 ( .A(n968), .Y(n969) );
  BUFX2 U1202 ( .A(n546), .Y(n970) );
  INVX1 U1203 ( .A(n1017), .Y(n975) );
  INVX1 U1204 ( .A(n600), .Y(n976) );
  INVX1 U1205 ( .A(n976), .Y(n977) );
  INVX1 U1206 ( .A(n978), .Y(n979) );
  AND2X2 U1207 ( .A(n2135), .B(n1463), .Y(n2037) );
  INVX1 U1208 ( .A(n2037), .Y(n984) );
  INVX1 U1209 ( .A(n989), .Y(n987) );
  INVX1 U1210 ( .A(n987), .Y(n988) );
  OR2X1 U1211 ( .A(BTB_io_req_bits_addr[34]), .B(n1463), .Y(n1328) );
  INVX1 U1212 ( .A(n1328), .Y(n989) );
  AND2X2 U1213 ( .A(n1350), .B(n1294), .Y(n1452) );
  INVX1 U1214 ( .A(n1452), .Y(n992) );
  INVX1 U1215 ( .A(n1431), .Y(n995) );
  INVX1 U1216 ( .A(n779), .Y(n997) );
  INVX1 U1217 ( .A(n997), .Y(\io_cpu_npc[4] ) );
  AND2X2 U1218 ( .A(n2167), .B(n637), .Y(n1805) );
  INVX1 U1219 ( .A(n1805), .Y(\icache_io_req_bits_addr[28] ) );
  AND2X2 U1220 ( .A(n638), .B(n2153), .Y(n1437) );
  INVX1 U1221 ( .A(n1437), .Y(\icache_io_req_bits_addr[22] ) );
  AND2X2 U1222 ( .A(n639), .B(n2148), .Y(n1436) );
  INVX1 U1223 ( .A(n1436), .Y(\icache_io_req_bits_addr[21] ) );
  NAND3X1 U1224 ( .A(n1580), .B(n1558), .C(n1430), .Y(n1008) );
  AND2X2 U1225 ( .A(n1595), .B(n2040), .Y(n1446) );
  INVX1 U1226 ( .A(n1446), .Y(n1011) );
  INVX1 U1227 ( .A(n1446), .Y(n1012) );
  OR2X1 U1228 ( .A(n1474), .B(n1231), .Y(n2040) );
  BUFX2 U1229 ( .A(n789), .Y(n1013) );
  OR2X1 U1230 ( .A(n1478), .B(n1231), .Y(n1864) );
  BUFX2 U1231 ( .A(n782), .Y(n1016) );
  OR2X1 U1232 ( .A(n1479), .B(n1231), .Y(n1928) );
  BUFX2 U1233 ( .A(n786), .Y(n1020) );
  INVX1 U1234 ( .A(n1021), .Y(n1022) );
  INVX1 U1235 ( .A(n1021), .Y(n1023) );
  INVX1 U1236 ( .A(n1026), .Y(n1024) );
  INVX1 U1237 ( .A(n1024), .Y(n1025) );
  INVX1 U1238 ( .A(n2136), .Y(n1026) );
  INVX1 U1239 ( .A(n1027), .Y(n1028) );
  BUFX2 U1240 ( .A(n1635), .Y(\icache_io_req_bits_addr[3] ) );
  INVX1 U1241 ( .A(n1833), .Y(n1036) );
  INVX1 U1242 ( .A(n1833), .Y(n1037) );
  AND2X1 U1243 ( .A(n2059), .B(n1053), .Y(n1855) );
  BUFX2 U1244 ( .A(n1442), .Y(n1039) );
  AND2X1 U1245 ( .A(n1826), .B(n1111), .Y(n2032) );
  AND2X1 U1246 ( .A(n1057), .B(n1053), .Y(n1960) );
  AND2X1 U1247 ( .A(n1312), .B(n2202), .Y(n1525) );
  AND2X1 U1248 ( .A(BTB_io_resp_bits_target[10]), .B(n1961), .Y(n2007) );
  OR2X1 U1249 ( .A(n1480), .B(n1850), .Y(n2155) );
  INVX1 U1250 ( .A(n1247), .Y(n2168) );
  OR2X1 U1251 ( .A(n2145), .B(n1872), .Y(n1895) );
  AND2X1 U1252 ( .A(BTB_io_resp_bits_target[5]), .B(n1351), .Y(n1985) );
  AND2X1 U1253 ( .A(n1594), .B(n2024), .Y(n2131) );
  AND2X1 U1254 ( .A(n1961), .B(BTB_io_resp_bits_target[21]), .Y(n2145) );
  INVX1 U1255 ( .A(\tlb_io_req_bits_vpn[0] ), .Y(n1358) );
  OR2X1 U1256 ( .A(n1463), .B(n1057), .Y(n1056) );
  AND2X1 U1257 ( .A(n1299), .B(io_cpu_resp_bits_btb_bits_bht_value[0]), .Y(
        n1421) );
  AND2X1 U1258 ( .A(n1298), .B(io_cpu_resp_bits_btb_bits_bht_history[0]), .Y(
        n1422) );
  INVX1 U1259 ( .A(n2396), .Y(n1953) );
  OR2X1 U1260 ( .A(n1518), .B(n1519), .Y(n1397) );
  OR2X1 U1261 ( .A(n2271), .B(n1421), .Y(n332) );
  INVX1 U1262 ( .A(n1299), .Y(tlb_io_req_valid) );
  OR2X1 U1263 ( .A(n1810), .B(n1621), .Y(io_cpu_npc[13]) );
  INVX2 U1264 ( .A(io_cpu_req_valid), .Y(n2099) );
  INVX1 U1265 ( .A(BTB_io_req_bits_addr_31), .Y(n1831) );
  NOR3X1 U1266 ( .A(n1148), .B(n1146), .C(n1147), .Y(n1356) );
  AND2X2 U1267 ( .A(n1923), .B(n1922), .Y(n2170) );
  AOI21X1 U1268 ( .A(n1151), .B(n2077), .C(n2081), .Y(n1041) );
  AND2X2 U1269 ( .A(n1041), .B(n1040), .Y(n2086) );
  NAND3X1 U1270 ( .A(n2100), .B(n1817), .C(n2082), .Y(n1040) );
  OR2X2 U1271 ( .A(n2397), .B(icache_io_resp_valid), .Y(n1064) );
  AOI22X1 U1272 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[9]), .C(
        io_cpu_resp_bits_pc[9]), .D(n2112), .Y(n1551) );
  OAI21X1 U1273 ( .A(n1257), .B(n1122), .C(n1043), .Y(n354) );
  INVX1 U1274 ( .A(n1484), .Y(n1043) );
  AND2X2 U1275 ( .A(n632), .B(n697), .Y(n1045) );
  OR2X2 U1276 ( .A(n795), .B(n1859), .Y(n1046) );
  AND2X1 U1277 ( .A(io_cpu_resp_bits_pc[0]), .B(n2057), .Y(n1974) );
  AND2X1 U1278 ( .A(io_cpu_resp_bits_btb_valid), .B(n2057), .Y(n2375) );
  AND2X1 U1279 ( .A(n2063), .B(n1053), .Y(n1903) );
  AND2X1 U1280 ( .A(n1054), .B(n1053), .Y(n1891) );
  AND2X1 U1281 ( .A(n1829), .B(n1053), .Y(n2158) );
  INVX1 U1282 ( .A(n1053), .Y(n1202) );
  AND2X1 U1283 ( .A(\tlb_io_req_bits_vpn[3] ), .B(BTB_io_req_bits_addr[14]), 
        .Y(n1826) );
  INVX1 U1284 ( .A(n2053), .Y(\tlb_io_req_bits_vpn[13] ) );
  AND2X1 U1285 ( .A(\tlb_io_req_bits_vpn[10] ), .B(n527), .Y(n1909) );
  INVX1 U1286 ( .A(n1705), .Y(\io_cpu_npc[11] ) );
  INVX1 U1287 ( .A(n2181), .Y(n1815) );
  INVX1 U1288 ( .A(n2133), .Y(n1811) );
  INVX1 U1289 ( .A(n2118), .Y(n1809) );
  INVX1 U1290 ( .A(n2399), .Y(io_cpu_npc[20]) );
  AND2X1 U1291 ( .A(n2101), .B(n1714), .Y(n1060) );
  AND2X1 U1292 ( .A(n1553), .B(n1685), .Y(n1934) );
  AND2X1 U1293 ( .A(n1322), .B(BTB_io_req_bits_addr[1]), .Y(n1979) );
  INVX1 U1294 ( .A(n2365), .Y(n1069) );
  INVX1 U1295 ( .A(n2366), .Y(n1070) );
  INVX1 U1296 ( .A(n2367), .Y(n1071) );
  OR2X1 U1297 ( .A(n1918), .B(n2053), .Y(n1494) );
  INVX1 U1298 ( .A(n2071), .Y(n1362) );
  AND2X1 U1299 ( .A(n2055), .B(n2202), .Y(n1498) );
  AND2X1 U1300 ( .A(BTB_io_req_bits_addr[4]), .B(n2374), .Y(n1982) );
  INVX1 U1301 ( .A(n1463), .Y(n1050) );
  OR2X1 U1302 ( .A(n1829), .B(n1458), .Y(n1828) );
  AND2X1 U1303 ( .A(n2070), .B(n2202), .Y(n2186) );
  AND2X1 U1304 ( .A(n2064), .B(n2202), .Y(n2183) );
  AND2X1 U1305 ( .A(n1962), .B(n2202), .Y(n1963) );
  AND2X1 U1306 ( .A(BTB_io_req_bits_addr[13]), .B(n2202), .Y(n1947) );
  AND2X1 U1307 ( .A(BTB_io_req_bits_addr[9]), .B(n2374), .Y(n2003) );
  AND2X1 U1308 ( .A(n2077), .B(n2202), .Y(n2192) );
  OR2X1 U1309 ( .A(n2154), .B(n527), .Y(n1683) );
  AND2X1 U1310 ( .A(n1346), .B(n1869), .Y(n1285) );
  INVX1 U1311 ( .A(n1111), .Y(n1147) );
  AND2X1 U1312 ( .A(io_cpu_resp_bits_pc[1]), .B(n2057), .Y(n1977) );
  AND2X1 U1313 ( .A(n1970), .B(n2057), .Y(n1971) );
  BUFX2 U1314 ( .A(n1455), .Y(n1051) );
  OR2X1 U1315 ( .A(n2051), .B(n1441), .Y(n1331) );
  AND2X1 U1316 ( .A(n2049), .B(\tlb_io_req_bits_vpn[8] ), .Y(n1893) );
  INVX8 U1317 ( .A(n1316), .Y(n1052) );
  INVX8 U1318 ( .A(n1441), .Y(n1053) );
  AND2X1 U1319 ( .A(n1062), .B(n2099), .Y(n2377) );
  AND2X1 U1320 ( .A(n1350), .B(n1951), .Y(n1530) );
  AND2X1 U1321 ( .A(n1294), .B(io_cpu_resp_bits_pc[9]), .Y(n1293) );
  INVX1 U1322 ( .A(n1529), .Y(n1318) );
  AND2X1 U1323 ( .A(n1538), .B(n1969), .Y(n1294) );
  INVX1 U1324 ( .A(n2404), .Y(n1054) );
  INVX1 U1325 ( .A(io_cpu_resp_bits_pc[0]), .Y(n1270) );
  INVX1 U1326 ( .A(io_cpu_resp_bits_pc[1]), .Y(n1271) );
  AND2X1 U1327 ( .A(io_cpu_resp_bits_pc[34]), .B(n2099), .Y(n1917) );
  AND2X1 U1328 ( .A(n2378), .B(tlb_io_resp_xcpt_if), .Y(n1214) );
  AND2X2 U1329 ( .A(n1307), .B(n1346), .Y(n1282) );
  INVX1 U1330 ( .A(BTB_io_req_bits_addr[2]), .Y(n1057) );
  OAI21X1 U1331 ( .A(n1059), .B(n1203), .C(n2072), .Y(n391) );
  INVX1 U1332 ( .A(n1546), .Y(n1061) );
  BUFX2 U1333 ( .A(n778), .Y(n1062) );
  INVX1 U1334 ( .A(n1013), .Y(n2164) );
  INVX2 U1335 ( .A(n1463), .Y(n1370) );
  AND2X2 U1336 ( .A(n1373), .B(n1296), .Y(n2161) );
  INVX1 U1337 ( .A(n1072), .Y(n1146) );
  INVX1 U1338 ( .A(n1503), .Y(n1066) );
  INVX1 U1339 ( .A(n583), .Y(n1067) );
  NAND3X1 U1340 ( .A(n1454), .B(n1068), .C(n2201), .Y(n1301) );
  BUFX4 U1341 ( .A(n1036), .Y(n1299) );
  INVX1 U1342 ( .A(n588), .Y(n1493) );
  AND2X2 U1343 ( .A(n1548), .B(n1072), .Y(n1359) );
  AOI21X1 U1344 ( .A(n2042), .B(n1506), .C(n1011), .Y(n2044) );
  BUFX2 U1345 ( .A(n2411), .Y(BTB_io_req_bits_addr[10]) );
  BUFX2 U1346 ( .A(n2397), .Y(io_cpu_resp_bits_xcpt_if) );
  OAI21X1 U1347 ( .A(n1508), .B(n1080), .C(n1411), .Y(n1604) );
  INVX8 U1348 ( .A(n1459), .Y(n1441) );
  INVX8 U1349 ( .A(n1850), .Y(n2112) );
  NAND3X1 U1350 ( .A(n763), .B(n1245), .C(n1046), .Y(n1601) );
  INVX1 U1351 ( .A(n1282), .Y(n1457) );
  AOI21X1 U1352 ( .A(BTB_io_req_bits_addr_25), .B(n2071), .C(n755), .Y(n1085)
         );
  OAI21X1 U1353 ( .A(n1108), .B(n1203), .C(n1105), .Y(n406) );
  OAI21X1 U1354 ( .A(n1093), .B(n1203), .C(n1657), .Y(n408) );
  AND2X2 U1355 ( .A(n807), .B(n1092), .Y(n2165) );
  INVX1 U1356 ( .A(n2139), .Y(n1093) );
  XOR2X1 U1357 ( .A(n785), .B(n1892), .Y(n2139) );
  OAI21X1 U1358 ( .A(n1949), .B(n1370), .C(n838), .Y(n414) );
  OAI21X1 U1359 ( .A(n1941), .B(n1322), .C(n1612), .Y(n416) );
  OAI21X1 U1360 ( .A(n805), .B(n1211), .C(n2070), .Y(n1210) );
  AND2X2 U1361 ( .A(n807), .B(n2091), .Y(n1095) );
  AND2X2 U1362 ( .A(n807), .B(n2108), .Y(n2201) );
  AND2X2 U1363 ( .A(n634), .B(n691), .Y(n1136) );
  OAI21X1 U1364 ( .A(n1895), .B(n1896), .C(n703), .Y(n1105) );
  INVX1 U1365 ( .A(n823), .Y(n1107) );
  INVX1 U1366 ( .A(n2147), .Y(n1108) );
  XOR2X1 U1367 ( .A(n996), .B(n1894), .Y(n2147) );
  INVX8 U1368 ( .A(n1110), .Y(n1850) );
  AND2X2 U1369 ( .A(s2_valid), .B(n1840), .Y(n1110) );
  BUFX2 U1370 ( .A(n1136), .Y(n1111) );
  AND2X2 U1371 ( .A(n2416), .B(n2417), .Y(n1510) );
  INVX4 U1372 ( .A(n1113), .Y(n1247) );
  AND2X2 U1373 ( .A(n1374), .B(n1114), .Y(n1113) );
  AND2X2 U1374 ( .A(n809), .B(n1010), .Y(n1368) );
  AND2X2 U1375 ( .A(n2406), .B(n2405), .Y(n1853) );
  OAI21X1 U1376 ( .A(n1458), .B(n1485), .C(n1118), .Y(io_cpu_npc[7]) );
  AND2X2 U1377 ( .A(n626), .B(n1419), .Y(n1118) );
  AOI22X1 U1378 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[7]), .C(
        io_cpu_resp_bits_pc[7]), .D(n2112), .Y(n1119) );
  OR2X1 U1379 ( .A(n2102), .B(n2098), .Y(n1121) );
  INVX8 U1380 ( .A(n1298), .Y(BTB_io_req_valid) );
  INVX1 U1381 ( .A(BTB_io_resp_bits_target[7]), .Y(n1124) );
  INVX1 U1382 ( .A(BTB_io_resp_bits_target[31]), .Y(n1125) );
  INVX1 U1383 ( .A(BTB_io_resp_bits_target[28]), .Y(n1126) );
  INVX1 U1384 ( .A(BTB_io_resp_bits_target[3]), .Y(n1127) );
  INVX1 U1385 ( .A(BTB_io_resp_bits_target[0]), .Y(n1128) );
  INVX1 U1386 ( .A(BTB_io_resp_bits_target[38]), .Y(n1129) );
  INVX1 U1387 ( .A(BTB_io_resp_bits_target[2]), .Y(n1130) );
  INVX1 U1388 ( .A(BTB_io_resp_bits_bht_history[4]), .Y(n1131) );
  OAI21X1 U1389 ( .A(n2273), .B(BTB_io_req_valid), .C(n1749), .Y(n331) );
  OAI21X1 U1390 ( .A(n2319), .B(BTB_io_req_valid), .C(n1732), .Y(n356) );
  OAI21X1 U1391 ( .A(n2323), .B(BTB_io_req_valid), .C(n1730), .Y(n358) );
  OAI21X1 U1392 ( .A(n2352), .B(BTB_io_req_valid), .C(n1717), .Y(n372) );
  OAI21X1 U1393 ( .A(n2325), .B(BTB_io_req_valid), .C(n1729), .Y(n359) );
  OAI21X1 U1394 ( .A(n2335), .B(BTB_io_req_valid), .C(n1724), .Y(n364) );
  OAI21X1 U1395 ( .A(n2345), .B(BTB_io_req_valid), .C(n1719), .Y(n369) );
  OAI21X1 U1396 ( .A(n2277), .B(BTB_io_req_valid), .C(n1748), .Y(n334) );
  OAI21X1 U1397 ( .A(n2279), .B(BTB_io_req_valid), .C(n1662), .Y(n335) );
  OAI21X1 U1398 ( .A(n2284), .B(BTB_io_req_valid), .C(n1747), .Y(n338) );
  OAI21X1 U1399 ( .A(n2286), .B(BTB_io_req_valid), .C(n1741), .Y(n345) );
  OAI21X1 U1400 ( .A(n2290), .B(BTB_io_req_valid), .C(n1745), .Y(n341) );
  OAI21X1 U1401 ( .A(n2292), .B(BTB_io_req_valid), .C(n1744), .Y(n342) );
  OAI21X1 U1402 ( .A(n2296), .B(BTB_io_req_valid), .C(n1742), .Y(n344) );
  OAI21X1 U1403 ( .A(n2301), .B(BTB_io_req_valid), .C(n1740), .Y(n346) );
  OAI21X1 U1404 ( .A(n2312), .B(BTB_io_req_valid), .C(n1735), .Y(n352) );
  OAI21X1 U1405 ( .A(n2304), .B(BTB_io_req_valid), .C(n1739), .Y(n348) );
  OAI21X1 U1406 ( .A(n2298), .B(BTB_io_req_valid), .C(n1716), .Y(n384) );
  OAI21X1 U1407 ( .A(n2362), .B(BTB_io_req_valid), .C(n1669), .Y(n377) );
  OAI21X1 U1408 ( .A(n2282), .B(BTB_io_req_valid), .C(n1663), .Y(n337) );
  AND2X2 U1409 ( .A(n2413), .B(n2414), .Y(n1524) );
  XOR2X1 U1410 ( .A(n1506), .B(\tlb_io_req_bits_vpn[6] ), .Y(n1133) );
  XNOR2X1 U1411 ( .A(n1135), .B(\tlb_io_req_bits_vpn[10] ), .Y(n2152) );
  XOR2X1 U1412 ( .A(n1455), .B(n2132), .Y(n1460) );
  NAND3X1 U1413 ( .A(n1822), .B(n1111), .C(n1137), .Y(n1455) );
  INVX1 U1414 ( .A(n1500), .Y(n1137) );
  AND2X2 U1415 ( .A(n1354), .B(n2419), .Y(n1995) );
  INVX8 U1416 ( .A(n1052), .Y(n1463) );
  AND2X2 U1417 ( .A(n1052), .B(\tlb_io_req_bits_vpn[19] ), .Y(n1140) );
  NOR3X1 U1418 ( .A(n1441), .B(\tlb_io_req_bits_vpn[13] ), .C(n1052), .Y(n1913) );
  AND2X2 U1419 ( .A(n1333), .B(BTB_io_req_bits_addr[34]), .Y(n1144) );
  AND2X2 U1420 ( .A(n783), .B(n1144), .Y(n2184) );
  AND2X2 U1421 ( .A(n1145), .B(n1597), .Y(n2181) );
  AOI22X1 U1422 ( .A(io_cpu_req_bits_pc[35]), .B(io_cpu_req_valid), .C(
        io_cpu_resp_bits_pc[35]), .D(n2112), .Y(n1145) );
  INVX1 U1423 ( .A(BTB_io_req_bits_addr[34]), .Y(n1148) );
  OR2X2 U1424 ( .A(n1037), .B(reset), .Y(n1523) );
  AOI21X1 U1425 ( .A(n2183), .B(n2184), .C(n1815), .Y(n1412) );
  AOI22X1 U1426 ( .A(io_cpu_req_valid), .B(n2048), .C(n2045), .D(n1151), .Y(
        n1320) );
  NOR3X1 U1427 ( .A(n1151), .B(n1458), .C(n1684), .Y(n1503) );
  AOI22X1 U1428 ( .A(n1052), .B(BTB_io_req_bits_addr[11]), .C(n1630), .D(n1939), .Y(n1940) );
  OAI21X1 U1429 ( .A(n1151), .B(io_cpu_npc_1), .C(n1660), .Y(n426) );
  OAI21X1 U1430 ( .A(n1429), .B(n1151), .C(n2022), .Y(n412) );
  INVX1 U1431 ( .A(n1879), .Y(n1155) );
  AOI21X1 U1432 ( .A(n1463), .B(n765), .C(n1161), .Y(n1411) );
  OAI21X1 U1433 ( .A(n1331), .B(n2159), .C(n843), .Y(n1161) );
  AOI22X1 U1434 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[36]), .C(
        io_cpu_resp_bits_pc[36]), .D(n2112), .Y(n1163) );
  AOI21X1 U1435 ( .A(n2071), .B(n2404), .C(n757), .Y(n1369) );
  AOI22X1 U1436 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[2]), .C(n2396), 
        .D(n2112), .Y(n1555) );
  BUFX2 U1437 ( .A(n539), .Y(n1198) );
  AND2X2 U1438 ( .A(n1909), .B(n1198), .Y(n1911) );
  AND2X2 U1439 ( .A(BTB_io_req_bits_addr_22), .B(n1198), .Y(n2154) );
  INVX1 U1440 ( .A(n1200), .Y(n1449) );
  BUFX2 U1441 ( .A(n1307), .Y(n1201) );
  BUFX4 U1442 ( .A(n1549), .Y(n1203) );
  AND2X2 U1443 ( .A(n1201), .B(n2073), .Y(n2187) );
  NAND3X1 U1444 ( .A(n764), .B(n1205), .C(n1690), .Y(n1635) );
  AOI22X1 U1445 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[3]), .C(
        io_cpu_resp_bits_pc[3]), .D(n2112), .Y(n1205) );
  AND2X2 U1446 ( .A(n1524), .B(n1206), .Y(n1347) );
  AND2X2 U1447 ( .A(n2412), .B(n2415), .Y(n1206) );
  AOI21X1 U1448 ( .A(n2099), .B(n2350), .C(reset), .Y(n1212) );
  AOI22X1 U1449 ( .A(io_cpu_req_bits_pc[5]), .B(io_cpu_req_valid), .C(
        io_cpu_resp_bits_pc[5]), .D(n2112), .Y(n1986) );
  INVX1 U1450 ( .A(n2019), .Y(n1447) );
  AOI21X1 U1451 ( .A(n2019), .B(n2020), .C(n1445), .Y(n1628) );
  OAI21X1 U1452 ( .A(n485), .B(n1473), .C(n842), .Y(n1445) );
  OAI21X1 U1453 ( .A(n1634), .B(n1218), .C(n706), .Y(io_cpu_npc[39]) );
  AOI21X1 U1454 ( .A(n1634), .B(n2204), .C(n1711), .Y(n1217) );
  INVX1 U1455 ( .A(n2203), .Y(n1218) );
  AOI21X1 U1456 ( .A(n2158), .B(n1827), .C(n2157), .Y(n1220) );
  AND2X2 U1457 ( .A(s2_speculative), .B(n1317), .Y(icache_io_s2_kill) );
  AOI21X1 U1458 ( .A(BTB_io_resp_bits_target[31]), .B(n1961), .C(n1222), .Y(
        n2175) );
  OAI21X1 U1459 ( .A(n1489), .B(n1850), .C(n845), .Y(n1222) );
  AOI21X1 U1460 ( .A(n2191), .B(n808), .C(n1224), .Y(n1413) );
  NAND3X1 U1461 ( .A(n1817), .B(n1697), .C(n1710), .Y(n1224) );
  INVX8 U1462 ( .A(n1226), .Y(n1522) );
  INVX1 U1463 ( .A(n1522), .Y(n1470) );
  AND2X2 U1464 ( .A(n778), .B(n547), .Y(n1226) );
  OR2X2 U1465 ( .A(n1049), .B(n1370), .Y(n1228) );
  BUFX4 U1466 ( .A(n1850), .Y(n1231) );
  AOI22X1 U1467 ( .A(io_cpu_req_bits_pc[25]), .B(io_cpu_req_valid), .C(n2112), 
        .D(io_cpu_resp_bits_pc[25]), .Y(n1233) );
  INVX8 U1468 ( .A(n992), .Y(n2057) );
  INVX8 U1469 ( .A(n1523), .Y(n2374) );
  OAI21X1 U1470 ( .A(n1054), .B(n2068), .C(n836), .Y(n1236) );
  AOI22X1 U1471 ( .A(reset), .B(io_resetVector[33]), .C(
        io_cpu_resp_bits_pc[33]), .D(n2057), .Y(n1237) );
  AOI21X1 U1472 ( .A(BTB_io_req_bits_addr[28]), .B(n2071), .C(n758), .Y(n1245)
         );
  OAI21X1 U1473 ( .A(n2044), .B(n1370), .C(n1248), .Y(n409) );
  AOI22X1 U1474 ( .A(\tlb_io_req_bits_vpn[6] ), .B(n2071), .C(n1075), .D(n2043), .Y(n1248) );
  MUX2X1 U1475 ( .B(n1915), .A(n770), .S(n2179), .Y(n1327) );
  INVX1 U1476 ( .A(BTB_io_resp_bits_bht_history[5]), .Y(n1253) );
  INVX1 U1477 ( .A(BTB_io_resp_bits_target[30]), .Y(n1254) );
  INVX1 U1478 ( .A(BTB_io_resp_bits_target[32]), .Y(n1255) );
  INVX1 U1479 ( .A(BTB_io_resp_bits_taken), .Y(n1256) );
  INVX1 U1480 ( .A(BTB_io_resp_bits_target[9]), .Y(n1257) );
  OAI21X1 U1481 ( .A(n2373), .B(tlb_io_req_valid), .C(n1673), .Y(n387) );
  OAI21X1 U1482 ( .A(n2306), .B(tlb_io_req_valid), .C(n1738), .Y(n349) );
  OAI21X1 U1483 ( .A(n2347), .B(tlb_io_req_valid), .C(n1718), .Y(n370) );
  OAI21X1 U1484 ( .A(n2370), .B(tlb_io_req_valid), .C(n1672), .Y(n386) );
  INVX1 U1485 ( .A(io_cpu_resp_bits_pc[38]), .Y(n1259) );
  INVX1 U1486 ( .A(io_cpu_resp_bits_pc[37]), .Y(n1260) );
  INVX1 U1487 ( .A(io_cpu_resp_bits_pc[19]), .Y(n1261) );
  INVX1 U1488 ( .A(io_cpu_resp_bits_pc[13]), .Y(n1262) );
  INVX1 U1489 ( .A(io_cpu_resp_bits_pc[11]), .Y(n1263) );
  INVX1 U1490 ( .A(io_cpu_resp_bits_pc[12]), .Y(n1264) );
  INVX1 U1491 ( .A(io_cpu_resp_bits_pc[14]), .Y(n1265) );
  INVX1 U1492 ( .A(io_cpu_resp_bits_pc[21]), .Y(n1266) );
  INVX1 U1493 ( .A(io_cpu_resp_bits_pc[17]), .Y(n1267) );
  INVX1 U1494 ( .A(io_cpu_resp_bits_pc[20]), .Y(n1268) );
  INVX1 U1495 ( .A(io_cpu_resp_bits_pc[33]), .Y(n1269) );
  AOI22X1 U1496 ( .A(io_cpu_req_bits_pc[4]), .B(io_cpu_req_valid), .C(
        io_cpu_resp_bits_pc[4]), .D(n2112), .Y(n1964) );
  INVX1 U1497 ( .A(n1873), .Y(n1275) );
  INVX1 U1498 ( .A(n1874), .Y(n1276) );
  INVX1 U1499 ( .A(n1869), .Y(n1281) );
  NAND3X1 U1500 ( .A(n1201), .B(n2202), .C(n1285), .Y(n1284) );
  XNOR2X1 U1501 ( .A(n994), .B(BTB_io_req_bits_addr[5]), .Y(n1983) );
  AOI22X1 U1502 ( .A(reset), .B(io_resetVector[2]), .C(n2057), .D(
        io_cpu_resp_bits_pc[2]), .Y(n1289) );
  INVX8 U1503 ( .A(n1299), .Y(n2350) );
  AOI22X1 U1504 ( .A(reset), .B(io_resetVector[9]), .C(n1293), .D(n1350), .Y(
        n2002) );
  AND2X2 U1505 ( .A(n1857), .B(n1856), .Y(n2167) );
  INVX2 U1506 ( .A(n1349), .Y(n1350) );
  AND2X2 U1507 ( .A(n1359), .B(n1911), .Y(n2159) );
  INVX1 U1508 ( .A(n541), .Y(n1302) );
  XOR2X1 U1509 ( .A(n1039), .B(BTB_io_req_bits_addr[6]), .Y(n1312) );
  AOI21X1 U1510 ( .A(n1839), .B(n2198), .C(n1427), .Y(n1315) );
  INVX1 U1511 ( .A(s2_cacheable), .Y(n1317) );
  BUFX2 U1512 ( .A(n1052), .Y(n1322) );
  AND2X2 U1513 ( .A(n1052), .B(BTB_io_req_bits_addr[8]), .Y(n2001) );
  AND2X2 U1514 ( .A(n1052), .B(BTB_io_req_bits_addr[9]), .Y(n2004) );
  AND2X2 U1515 ( .A(n1052), .B(s1_speculative), .Y(n1968) );
  AOI22X1 U1516 ( .A(n1052), .B(\tlb_io_req_bits_vpn[3] ), .C(n1447), .D(n2021), .Y(n2022) );
  MUX2X1 U1517 ( .B(BTB_io_req_bits_addr[3]), .A(n1635), .S(n1323), .Y(n1958)
         );
  INVX2 U1518 ( .A(n1052), .Y(n1323) );
  AOI21X1 U1519 ( .A(n1052), .B(\tlb_io_req_bits_vpn[14] ), .C(n1496), .Y(
        n1326) );
  OAI21X1 U1520 ( .A(n1495), .B(n988), .C(n1327), .Y(n1379) );
  INVX1 U1521 ( .A(n1916), .Y(n1330) );
  NOR3X1 U1522 ( .A(n1336), .B(n799), .C(n801), .Y(n1335) );
  INVX1 U1523 ( .A(n2091), .Y(n1336) );
  AOI21X1 U1524 ( .A(n1052), .B(\tlb_io_req_bits_vpn[20] ), .C(n759), .Y(n1341) );
  AND2X2 U1525 ( .A(n1442), .B(n1347), .Y(n1547) );
  AOI21X1 U1526 ( .A(n1925), .B(n1247), .C(n1511), .Y(n1348) );
  INVX8 U1527 ( .A(n1522), .Y(n1351) );
  INVX1 U1528 ( .A(BTB_io_req_bits_addr[28]), .Y(n1360) );
  BUFX2 U1529 ( .A(n2415), .Y(BTB_io_req_bits_addr[6]) );
  BUFX2 U1530 ( .A(n2418), .Y(n1354) );
  INVX1 U1531 ( .A(n1536), .Y(n1365) );
  BUFX2 U1532 ( .A(n2417), .Y(n1357) );
  XNOR2X1 U1533 ( .A(n1846), .B(n1358), .Y(n1847) );
  OAI21X1 U1534 ( .A(n1362), .B(n1363), .C(n1513), .Y(n1361) );
  INVX1 U1535 ( .A(n1361), .Y(n1512) );
  INVX1 U1536 ( .A(BTB_io_req_bits_addr[29]), .Y(n1363) );
  OAI21X1 U1537 ( .A(n1365), .B(n1366), .C(n1535), .Y(\io_cpu_npc[9] ) );
  INVX1 U1538 ( .A(n2202), .Y(n1366) );
  AND2X2 U1539 ( .A(n1442), .B(n1347), .Y(n1372) );
  INVX1 U1540 ( .A(io_cpu_resp_bits_pc[27]), .Y(n1478) );
  INVX1 U1541 ( .A(n2026), .Y(n1517) );
  INVX1 U1542 ( .A(io_cpu_resp_bits_pc[16]), .Y(n1475) );
  INVX1 U1543 ( .A(BTB_io_req_bits_addr[9]), .Y(n1537) );
  INVX1 U1544 ( .A(io_cpu_resp_bits_pc[15]), .Y(n1473) );
  INVX1 U1545 ( .A(io_cpu_resp_bits_pc[24]), .Y(n1479) );
  INVX1 U1546 ( .A(io_cpu_resp_bits_pc[31]), .Y(n1489) );
  INVX1 U1547 ( .A(n1853), .Y(n1887) );
  INVX1 U1548 ( .A(io_cpu_resp_bits_pc[23]), .Y(n1480) );
  INVX1 U1549 ( .A(BTB_io_resp_bits_target[23]), .Y(n1521) );
  INVX1 U1550 ( .A(io_cpu_resp_bits_pc[18]), .Y(n1474) );
  INVX1 U1551 ( .A(io_cpu_resp_bits_pc[26]), .Y(n1477) );
  INVX1 U1552 ( .A(io_cpu_resp_bits_pc[28]), .Y(n1476) );
  INVX1 U1553 ( .A(io_cpu_resp_bits_pc[29]), .Y(n1482) );
  INVX1 U1554 ( .A(io_cpu_resp_bits_pc[30]), .Y(n1481) );
  INVX1 U1555 ( .A(n1905), .Y(n1505) );
  INVX1 U1556 ( .A(n1862), .Y(n1485) );
  INVX1 U1557 ( .A(n2129), .Y(n1622) );
  INVX1 U1558 ( .A(n2182), .Y(n1502) );
  INVX1 U1559 ( .A(n1516), .Y(n1515) );
  INVX1 U1560 ( .A(BTB_io_resp_bits_bridx), .Y(n1472) );
  INVX1 U1561 ( .A(n1829), .Y(\tlb_io_req_bits_vpn[11] ) );
  BUFX2 U1562 ( .A(n806), .Y(n1507) );
  AND2X1 U1563 ( .A(n1554), .B(n1655), .Y(io_cpu_npc_1) );
  AND2X1 U1564 ( .A(n1640), .B(n1654), .Y(io_cpu_npc_0) );
  INVX1 U1565 ( .A(s2_speculative), .Y(n1544) );
  INVX1 U1566 ( .A(BTB_io_req_bits_addr[14]), .Y(n1501) );
  INVX1 U1567 ( .A(n2033), .Y(\tlb_io_req_bits_vpn[5] ) );
  INVX1 U1568 ( .A(n2051), .Y(\tlb_io_req_bits_vpn[12] ) );
  BUFX2 U1569 ( .A(io_cpu_npc_24), .Y(n1376) );
  OR2X2 U1570 ( .A(n1475), .B(n485), .Y(n2024) );
  AND2X2 U1571 ( .A(n2202), .B(n1460), .Y(n1623) );
  INVX1 U1572 ( .A(n1623), .Y(n1408) );
  AND2X2 U1573 ( .A(n2202), .B(n1462), .Y(n1621) );
  AND2X2 U1574 ( .A(io_cpu_npc[7]), .B(n1463), .Y(n1615) );
  INVX1 U1575 ( .A(n1615), .Y(n1414) );
  INVX1 U1576 ( .A(n1487), .Y(n1419) );
  AND2X2 U1577 ( .A(BTB_io_req_bits_addr[4]), .B(n1052), .Y(n1519) );
  INVX1 U1578 ( .A(n824), .Y(n1423) );
  INVX1 U1579 ( .A(n813), .Y(n1425) );
  BUFX2 U1580 ( .A(n2196), .Y(n1427) );
  AND2X2 U1581 ( .A(n1551), .B(n1641), .Y(n1535) );
  BUFX2 U1582 ( .A(n1628), .Y(n1429) );
  INVX1 U1583 ( .A(n1525), .Y(n1430) );
  AND2X2 U1584 ( .A(n778), .B(n1528), .Y(n1459) );
  AND2X2 U1585 ( .A(n783), .B(n2201), .Y(n1634) );
  BUFX2 U1586 ( .A(n1636), .Y(\icache_io_req_bits_addr[2] ) );
  XOR2X1 U1587 ( .A(n1808), .B(n2124), .Y(n1462) );
  AND2X2 U1588 ( .A(n1822), .B(BTB_io_req_bits_addr[16]), .Y(n1709) );
  OAI21X1 U1589 ( .A(n805), .B(n2095), .C(n2094), .Y(n2096) );
  OAI21X1 U1590 ( .A(n2078), .B(n805), .C(n2077), .Y(n2079) );
  NAND3X1 U1591 ( .A(n1555), .B(n1692), .C(n1626), .Y(n1636) );
  OR2X2 U1592 ( .A(n969), .B(n1423), .Y(n336) );
  OR2X2 U1593 ( .A(n743), .B(n1425), .Y(n383) );
  AND2X2 U1594 ( .A(n1052), .B(BTB_io_req_bits_addr[0]), .Y(n1976) );
  AND2X2 U1595 ( .A(n1901), .B(n1296), .Y(n1492) );
  NAND3X1 U1596 ( .A(BTB_io_req_bits_addr[36]), .B(n1296), .C(n1454), .Y(n2069) );
  AOI21X1 U1597 ( .A(n2202), .B(n1486), .C(n975), .Y(io_cpu_npc_24) );
  XOR2X1 U1598 ( .A(n2159), .B(\tlb_io_req_bits_vpn[12] ), .Y(n1486) );
  NOR3X1 U1599 ( .A(n2055), .B(n1918), .C(n2161), .Y(n1496) );
  INVX2 U1600 ( .A(n2127), .Y(\tlb_io_req_bits_vpn[3] ) );
  OAI21X1 U1601 ( .A(n2184), .B(n1502), .C(n704), .Y(io_cpu_npc[35]) );
  OAI21X1 U1602 ( .A(\tlb_io_req_bits_vpn[11] ), .B(n1463), .C(n1505), .Y(
        n1504) );
  OR2X2 U1603 ( .A(n2031), .B(n972), .Y(n1629) );
  NOR3X1 U1604 ( .A(n1801), .B(n1322), .C(n1247), .Y(n1511) );
  AND2X2 U1605 ( .A(n1899), .B(n1898), .Y(n2176) );
  NAND3X1 U1606 ( .A(n1996), .B(n1995), .C(n1994), .Y(n1633) );
  NOR3X1 U1607 ( .A(n1020), .B(n2142), .C(n2141), .Y(n2143) );
  INVX8 U1608 ( .A(n1522), .Y(n1961) );
  AND2X2 U1609 ( .A(n1951), .B(n2099), .Y(n1529) );
  AND2X2 U1610 ( .A(n1543), .B(n1530), .Y(n1872) );
  MUX2X1 U1611 ( .B(\s1_pc_[39] ), .A(n1711), .S(n1507), .Y(n1531) );
  MUX2X1 U1612 ( .B(n1281), .A(n789), .S(n1323), .Y(n1534) );
  XNOR2X1 U1613 ( .A(n1843), .B(n1537), .Y(n1536) );
  OAI21X1 U1614 ( .A(n1687), .B(n1062), .C(n1539), .Y(n1707) );
  XNOR2X1 U1615 ( .A(n2019), .B(n2127), .Y(n2128) );
  INVX1 U1616 ( .A(n527), .Y(n1829) );
  AND2X2 U1617 ( .A(BTB_io_resp_valid), .B(BTB_io_resp_bits_taken), .Y(n1542)
         );
  INVX1 U1618 ( .A(n1876), .Y(n1611) );
  AND2X2 U1619 ( .A(io_cpu_npc[10]), .B(n1463), .Y(n2010) );
  INVX1 U1620 ( .A(BTB_io_req_bits_addr_17), .Y(n2033) );
  BUFX2 U1621 ( .A(n2412), .Y(BTB_io_req_bits_addr[9]) );
  BUFX2 U1622 ( .A(n1845), .Y(n1552) );
  BUFX2 U1623 ( .A(n1933), .Y(n1553) );
  BUFX2 U1624 ( .A(n1957), .Y(n1554) );
  BUFX2 U1625 ( .A(n1981), .Y(n1556) );
  BUFX2 U1626 ( .A(n1986), .Y(n1557) );
  BUFX2 U1627 ( .A(n1989), .Y(n1558) );
  BUFX2 U1628 ( .A(n1992), .Y(n1559) );
  BUFX2 U1629 ( .A(n2005), .Y(n1560) );
  BUFX2 U1630 ( .A(n2006), .Y(n1561) );
  BUFX2 U1631 ( .A(n2013), .Y(n1562) );
  BUFX2 U1632 ( .A(n2015), .Y(n1563) );
  BUFX2 U1633 ( .A(n2016), .Y(n1564) );
  BUFX2 U1634 ( .A(n2023), .Y(n1565) );
  BUFX2 U1635 ( .A(n2029), .Y(n1566) );
  BUFX2 U1636 ( .A(n2050), .Y(n1567) );
  BUFX2 U1637 ( .A(n2052), .Y(n1568) );
  BUFX2 U1638 ( .A(n2054), .Y(n1569) );
  BUFX2 U1639 ( .A(n2056), .Y(n1570) );
  BUFX2 U1640 ( .A(n2060), .Y(n1571) );
  BUFX2 U1641 ( .A(n2062), .Y(n1572) );
  BUFX2 U1642 ( .A(n1907), .Y(n1573) );
  BUFX2 U1643 ( .A(n2069), .Y(n1574) );
  INVX1 U1644 ( .A(n1988), .Y(n1580) );
  AND2X2 U1645 ( .A(n2118), .B(n1786), .Y(n1705) );
  AND2X2 U1646 ( .A(n2131), .B(n1408), .Y(io_cpu_npc_16) );
  INVX1 U1647 ( .A(io_cpu_npc_16), .Y(\icache_io_req_bits_addr[16] ) );
  AND2X2 U1648 ( .A(n832), .B(n2143), .Y(n2399) );
  INVX1 U1649 ( .A(n1376), .Y(\icache_io_req_bits_addr[24] ) );
  BUFX2 U1650 ( .A(n1920), .Y(n1590) );
  BUFX2 U1651 ( .A(n2025), .Y(n1594) );
  BUFX2 U1652 ( .A(n2041), .Y(n1595) );
  AND2X2 U1653 ( .A(BTB_io_resp_bits_target[35]), .B(n1961), .Y(n2065) );
  INVX1 U1654 ( .A(n2065), .Y(n1597) );
  BUFX2 U1655 ( .A(n1929), .Y(n1609) );
  BUFX2 U1656 ( .A(n1940), .Y(n1612) );
  BUFX2 U1657 ( .A(n2002), .Y(n1617) );
  AND2X2 U1658 ( .A(BTB_io_req_bits_addr[6]), .B(n1151), .Y(n1990) );
  INVX1 U1659 ( .A(n1990), .Y(n1619) );
  INVX1 U1660 ( .A(n2004), .Y(n1620) );
  BUFX2 U1661 ( .A(n1964), .Y(n1625) );
  INVX1 U1662 ( .A(n1960), .Y(n1626) );
  AND2X2 U1663 ( .A(n1548), .B(BTB_io_req_bits_addr[10]), .Y(n2120) );
  INVX1 U1664 ( .A(n2120), .Y(n1630) );
  BUFX2 U1665 ( .A(n1861), .Y(n1631) );
  INVX1 U1666 ( .A(n2403), .Y(icache_io_resp_ready) );
  OR2X1 U1667 ( .A(n2206), .B(n1064), .Y(n2398) );
  INVX1 U1668 ( .A(n2398), .Y(io_cpu_resp_bits_replay) );
  BUFX2 U1669 ( .A(n1955), .Y(n1640) );
  INVX1 U1670 ( .A(n1842), .Y(n1641) );
  INVX1 U1671 ( .A(n1998), .Y(n1642) );
  INVX1 U1672 ( .A(n2007), .Y(n1643) );
  INVX1 U1673 ( .A(n816), .Y(n1652) );
  INVX1 U1674 ( .A(n815), .Y(n1653) );
  INVX1 U1675 ( .A(n821), .Y(n1654) );
  INVX1 U1676 ( .A(n822), .Y(n1655) );
  INVX1 U1677 ( .A(n819), .Y(n1656) );
  INVX1 U1678 ( .A(n582), .Y(n1657) );
  INVX1 U1679 ( .A(n578), .Y(n1658) );
  AND2X1 U1680 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[34]), .Y(n1916)
         );
  INVX1 U1681 ( .A(n1976), .Y(n1659) );
  INVX1 U1682 ( .A(n1979), .Y(n1660) );
  INVX1 U1683 ( .A(n2010), .Y(n1661) );
  AND2X1 U1684 ( .A(BTB_io_resp_bits_bht_value[0]), .B(n2350), .Y(n2271) );
  AND2X1 U1685 ( .A(BTB_io_resp_bits_bht_history[0]), .B(n2350), .Y(n2274) );
  AND2X1 U1686 ( .A(BTB_io_resp_bits_bht_history[1]), .B(n2350), .Y(n2275) );
  INVX1 U1687 ( .A(n2278), .Y(n1662) );
  AND2X1 U1688 ( .A(BTB_io_resp_bits_bht_history[3]), .B(n2350), .Y(n2278) );
  INVX1 U1689 ( .A(n605), .Y(n1663) );
  INVX1 U1690 ( .A(n2348), .Y(n1664) );
  AND2X1 U1691 ( .A(BTB_io_resp_bits_target[26]), .B(n2350), .Y(n2348) );
  INVX1 U1692 ( .A(n2355), .Y(n1666) );
  AND2X1 U1693 ( .A(BTB_io_resp_bits_target[29]), .B(n2350), .Y(n2355) );
  INVX1 U1694 ( .A(n606), .Y(n1667) );
  INVX1 U1695 ( .A(n607), .Y(n1669) );
  INVX1 U1696 ( .A(n2363), .Y(n1670) );
  AND2X1 U1697 ( .A(BTB_io_resp_bits_target[33]), .B(n2350), .Y(n2363) );
  AND2X1 U1698 ( .A(BTB_io_resp_bits_target[35]), .B(n2350), .Y(n2365) );
  AND2X1 U1699 ( .A(BTB_io_resp_bits_target[36]), .B(n2350), .Y(n2366) );
  AND2X1 U1700 ( .A(BTB_io_resp_bits_target[37]), .B(n2350), .Y(n2367) );
  INVX1 U1701 ( .A(n2369), .Y(n1672) );
  AND2X1 U1702 ( .A(BTB_io_resp_bits_mask), .B(n2350), .Y(n2369) );
  INVX1 U1703 ( .A(n608), .Y(n1673) );
  INVX1 U1704 ( .A(n2032), .Y(n1674) );
  INVX1 U1705 ( .A(n1971), .Y(n1675) );
  INVX1 U1706 ( .A(n1974), .Y(n1676) );
  INVX1 U1707 ( .A(n1977), .Y(n1677) );
  INVX1 U1708 ( .A(n2375), .Y(n1678) );
  INVX1 U1709 ( .A(n1828), .Y(n1679) );
  AND2X1 U1710 ( .A(BTB_io_req_bits_addr[7]), .B(n2374), .Y(n1991) );
  INVX1 U1711 ( .A(n1991), .Y(n1680) );
  INVX1 U1712 ( .A(n2046), .Y(n1681) );
  INVX1 U1713 ( .A(n1683), .Y(n1684) );
  AND2X1 U1714 ( .A(BTB_io_req_bits_addr[34]), .B(n1053), .Y(n1915) );
  INVX1 U1715 ( .A(n818), .Y(n1685) );
  AND2X1 U1716 ( .A(n2094), .B(n2202), .Y(n2198) );
  INVX1 U1717 ( .A(n1709), .Y(n1686) );
  INVX1 U1718 ( .A(n1917), .Y(n1687) );
  INVX1 U1719 ( .A(n2061), .Y(n1688) );
  AND2X1 U1720 ( .A(n1831), .B(n2202), .Y(n2061) );
  INVX1 U1721 ( .A(n1689), .Y(n1690) );
  INVX1 U1722 ( .A(n1691), .Y(n1692) );
  INVX1 U1723 ( .A(n1693), .Y(n1694) );
  INVX1 U1724 ( .A(n1985), .Y(n1695) );
  INVX1 U1725 ( .A(n2156), .Y(n1696) );
  AND2X1 U1726 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[23]), .Y(n2156)
         );
  INVX1 U1727 ( .A(n2189), .Y(n1697) );
  AND2X1 U1728 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[37]), .Y(n2189)
         );
  INVX1 U1729 ( .A(n2195), .Y(n1698) );
  AND2X1 U1730 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[38]), .Y(n2195)
         );
  INVX1 U1731 ( .A(n2186), .Y(n1699) );
  INVX1 U1732 ( .A(n2192), .Y(n1700) );
  AND2X1 U1733 ( .A(n2109), .B(n2202), .Y(n2204) );
  OR2X1 U1734 ( .A(io_cpu_req_valid), .B(n2149), .Y(n1879) );
  INVX1 U1735 ( .A(n1963), .Y(n1701) );
  INVX1 U1736 ( .A(n1855), .Y(n1702) );
  INVX1 U1737 ( .A(n1891), .Y(n1703) );
  INVX1 U1738 ( .A(n2377), .Y(n1704) );
  INVX1 U1739 ( .A(n1707), .Y(n1708) );
  INVX1 U1740 ( .A(n2190), .Y(n1710) );
  AND2X1 U1741 ( .A(n2379), .B(n2378), .Y(n2402) );
  INVX1 U1742 ( .A(n2402), .Y(icache_io_s1_kill) );
  AND2X1 U1743 ( .A(io_cpu_req_valid), .B(n1904), .Y(n1905) );
  AND2X1 U1744 ( .A(io_cpu_req_valid), .B(n2080), .Y(n2081) );
  AND2X1 U1745 ( .A(io_cpu_req_valid), .B(n2097), .Y(n2098) );
  INVX1 U1746 ( .A(n598), .Y(n1716) );
  AND2X1 U1747 ( .A(BTB_io_resp_bits_target[27]), .B(n2350), .Y(n2351) );
  INVX1 U1748 ( .A(n2351), .Y(n1717) );
  AND2X1 U1749 ( .A(BTB_io_resp_bits_target[25]), .B(n2350), .Y(n2346) );
  INVX1 U1750 ( .A(n2346), .Y(n1718) );
  AND2X1 U1751 ( .A(BTB_io_resp_bits_target[24]), .B(n2350), .Y(n2344) );
  INVX1 U1752 ( .A(n2344), .Y(n1719) );
  AND2X1 U1753 ( .A(BTB_io_resp_bits_target[23]), .B(n2350), .Y(n2342) );
  INVX1 U1754 ( .A(n2342), .Y(n1720) );
  AND2X1 U1755 ( .A(BTB_io_resp_bits_target[22]), .B(n2350), .Y(n2340) );
  INVX1 U1756 ( .A(n2340), .Y(n1721) );
  AND2X1 U1757 ( .A(BTB_io_resp_bits_target[21]), .B(n2350), .Y(n2338) );
  INVX1 U1758 ( .A(n2338), .Y(n1722) );
  AND2X1 U1759 ( .A(BTB_io_resp_bits_target[20]), .B(n2350), .Y(n2336) );
  INVX1 U1760 ( .A(n2336), .Y(n1723) );
  AND2X1 U1761 ( .A(BTB_io_resp_bits_target[19]), .B(n2350), .Y(n2334) );
  INVX1 U1762 ( .A(n2334), .Y(n1724) );
  AND2X1 U1763 ( .A(BTB_io_resp_bits_target[18]), .B(n2350), .Y(n2332) );
  INVX1 U1764 ( .A(n2332), .Y(n1725) );
  AND2X1 U1765 ( .A(BTB_io_resp_bits_target[17]), .B(n2350), .Y(n2330) );
  INVX1 U1766 ( .A(n2330), .Y(n1726) );
  AND2X1 U1767 ( .A(BTB_io_resp_bits_target[16]), .B(n2350), .Y(n2328) );
  INVX1 U1768 ( .A(n2328), .Y(n1727) );
  AND2X1 U1769 ( .A(BTB_io_resp_bits_target[15]), .B(n2350), .Y(n2326) );
  INVX1 U1770 ( .A(n2326), .Y(n1728) );
  AND2X1 U1771 ( .A(BTB_io_resp_bits_target[14]), .B(n2350), .Y(n2324) );
  INVX1 U1772 ( .A(n2324), .Y(n1729) );
  AND2X1 U1773 ( .A(BTB_io_resp_bits_target[13]), .B(n2350), .Y(n2322) );
  INVX1 U1774 ( .A(n2322), .Y(n1730) );
  AND2X1 U1775 ( .A(BTB_io_resp_bits_target[12]), .B(n2350), .Y(n2320) );
  INVX1 U1776 ( .A(n2320), .Y(n1731) );
  AND2X1 U1777 ( .A(BTB_io_resp_bits_target[11]), .B(n2350), .Y(n2318) );
  INVX1 U1778 ( .A(n2318), .Y(n1732) );
  AND2X1 U1779 ( .A(BTB_io_resp_bits_target[10]), .B(n2350), .Y(n2316) );
  INVX1 U1780 ( .A(n2316), .Y(n1733) );
  AND2X1 U1781 ( .A(BTB_io_resp_bits_target[8]), .B(n2350), .Y(n2313) );
  INVX1 U1782 ( .A(n2313), .Y(n1734) );
  INVX1 U1783 ( .A(n594), .Y(n1735) );
  AND2X1 U1784 ( .A(BTB_io_resp_bits_target[6]), .B(n2350), .Y(n2309) );
  INVX1 U1785 ( .A(n2309), .Y(n1736) );
  AND2X1 U1786 ( .A(BTB_io_resp_bits_target[5]), .B(n2350), .Y(n2307) );
  INVX1 U1787 ( .A(n2307), .Y(n1737) );
  AND2X1 U1788 ( .A(BTB_io_resp_bits_target[4]), .B(n2350), .Y(n2305) );
  INVX1 U1789 ( .A(n2305), .Y(n1738) );
  INVX1 U1790 ( .A(n597), .Y(n1739) );
  AND2X1 U1791 ( .A(BTB_io_resp_bits_target[1]), .B(n2350), .Y(n2299) );
  INVX1 U1792 ( .A(n2299), .Y(n1740) );
  AND2X1 U1793 ( .A(BTB_io_resp_bits_entry[0]), .B(n2350), .Y(n2285) );
  INVX1 U1794 ( .A(n2285), .Y(n1741) );
  AND2X1 U1795 ( .A(BTB_io_resp_bits_entry[5]), .B(n2350), .Y(n2295) );
  INVX1 U1796 ( .A(n2295), .Y(n1742) );
  AND2X1 U1797 ( .A(BTB_io_resp_bits_entry[4]), .B(n2350), .Y(n2293) );
  INVX1 U1798 ( .A(n2293), .Y(n1743) );
  AND2X1 U1799 ( .A(BTB_io_resp_bits_entry[3]), .B(n2350), .Y(n2291) );
  INVX1 U1800 ( .A(n2291), .Y(n1744) );
  AND2X1 U1801 ( .A(BTB_io_resp_bits_entry[2]), .B(n2350), .Y(n2289) );
  INVX1 U1802 ( .A(n2289), .Y(n1745) );
  AND2X1 U1803 ( .A(BTB_io_resp_bits_entry[1]), .B(n2350), .Y(n2287) );
  INVX1 U1804 ( .A(n2287), .Y(n1746) );
  AND2X1 U1805 ( .A(BTB_io_resp_bits_bht_history[6]), .B(n2350), .Y(n2283) );
  INVX1 U1806 ( .A(n2283), .Y(n1747) );
  AND2X1 U1807 ( .A(BTB_io_resp_bits_bht_history[2]), .B(n2350), .Y(n2276) );
  INVX1 U1808 ( .A(n2276), .Y(n1748) );
  AND2X1 U1809 ( .A(BTB_io_resp_bits_bht_value[1]), .B(n2350), .Y(n2272) );
  INVX1 U1810 ( .A(n2272), .Y(n1749) );
  BUFX2 U1811 ( .A(n2012), .Y(n1750) );
  BUFX2 U1812 ( .A(n2038), .Y(n1751) );
  BUFX2 U1813 ( .A(n2058), .Y(n1752) );
  BUFX2 U1814 ( .A(n2067), .Y(n1753) );
  AND2X1 U1815 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[63]), .Y(n2269) );
  INVX1 U1816 ( .A(n2269), .Y(n1754) );
  AND2X1 U1817 ( .A(n2396), .B(icache_io_resp_bits_datablock[62]), .Y(n2267)
         );
  INVX1 U1818 ( .A(n2267), .Y(n1755) );
  AND2X1 U1819 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[61]), .Y(n2265) );
  INVX1 U1820 ( .A(n2265), .Y(n1756) );
  AND2X1 U1821 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[60]), .Y(n2263) );
  INVX1 U1822 ( .A(n2263), .Y(n1757) );
  AND2X1 U1823 ( .A(n2396), .B(icache_io_resp_bits_datablock[59]), .Y(n2261)
         );
  INVX1 U1824 ( .A(n2261), .Y(n1758) );
  AND2X1 U1825 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[58]), .Y(n2259) );
  INVX1 U1826 ( .A(n2259), .Y(n1759) );
  AND2X1 U1827 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[57]), .Y(n2257) );
  INVX1 U1828 ( .A(n2257), .Y(n1760) );
  AND2X1 U1829 ( .A(n2396), .B(icache_io_resp_bits_datablock[56]), .Y(n2255)
         );
  INVX1 U1830 ( .A(n2255), .Y(n1761) );
  AND2X1 U1831 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[55]), .Y(n2253) );
  INVX1 U1832 ( .A(n2253), .Y(n1762) );
  AND2X1 U1833 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[54]), .Y(n2251) );
  INVX1 U1834 ( .A(n2251), .Y(n1763) );
  AND2X1 U1835 ( .A(n2396), .B(icache_io_resp_bits_datablock[53]), .Y(n2249)
         );
  INVX1 U1836 ( .A(n2249), .Y(n1764) );
  AND2X1 U1837 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[52]), .Y(n2247) );
  INVX1 U1838 ( .A(n2247), .Y(n1765) );
  AND2X1 U1839 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[51]), .Y(n2245) );
  INVX1 U1840 ( .A(n2245), .Y(n1766) );
  AND2X1 U1841 ( .A(n2396), .B(icache_io_resp_bits_datablock[50]), .Y(n2243)
         );
  INVX1 U1842 ( .A(n2243), .Y(n1767) );
  AND2X1 U1843 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[49]), .Y(n2241) );
  INVX1 U1844 ( .A(n2241), .Y(n1768) );
  AND2X1 U1845 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[48]), .Y(n2239) );
  INVX1 U1846 ( .A(n2239), .Y(n1769) );
  AND2X1 U1847 ( .A(n2396), .B(icache_io_resp_bits_datablock[47]), .Y(n2237)
         );
  INVX1 U1848 ( .A(n2237), .Y(n1770) );
  AND2X1 U1849 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[46]), .Y(n2235) );
  INVX1 U1850 ( .A(n2235), .Y(n1771) );
  AND2X1 U1851 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[45]), .Y(n2233) );
  INVX1 U1852 ( .A(n2233), .Y(n1772) );
  AND2X1 U1853 ( .A(n2396), .B(icache_io_resp_bits_datablock[44]), .Y(n2231)
         );
  INVX1 U1854 ( .A(n2231), .Y(n1773) );
  AND2X1 U1855 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[43]), .Y(n2229) );
  INVX1 U1856 ( .A(n2229), .Y(n1774) );
  AND2X1 U1857 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[42]), .Y(n2227) );
  INVX1 U1858 ( .A(n2227), .Y(n1775) );
  AND2X1 U1859 ( .A(n2396), .B(icache_io_resp_bits_datablock[41]), .Y(n2225)
         );
  INVX1 U1860 ( .A(n2225), .Y(n1776) );
  AND2X1 U1861 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[40]), .Y(n2223) );
  INVX1 U1862 ( .A(n2223), .Y(n1777) );
  AND2X1 U1863 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[39]), .Y(n2221) );
  INVX1 U1864 ( .A(n2221), .Y(n1778) );
  AND2X1 U1865 ( .A(n2396), .B(icache_io_resp_bits_datablock[38]), .Y(n2219)
         );
  INVX1 U1866 ( .A(n2219), .Y(n1779) );
  AND2X1 U1867 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[37]), .Y(n2217) );
  INVX1 U1868 ( .A(n2217), .Y(n1780) );
  AND2X1 U1869 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[36]), .Y(n2215) );
  INVX1 U1870 ( .A(n2215), .Y(n1781) );
  AND2X1 U1871 ( .A(n2396), .B(icache_io_resp_bits_datablock[35]), .Y(n2213)
         );
  INVX1 U1872 ( .A(n2213), .Y(n1782) );
  AND2X1 U1873 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[34]), .Y(n2211) );
  INVX1 U1874 ( .A(n2211), .Y(n1783) );
  AND2X1 U1875 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[33]), .Y(n2209) );
  INVX1 U1876 ( .A(n2209), .Y(n1784) );
  AND2X1 U1877 ( .A(io_cpu_resp_bits_pc[2]), .B(
        icache_io_resp_bits_datablock[32]), .Y(n2207) );
  INVX1 U1878 ( .A(n2207), .Y(n1785) );
  INVX1 U1879 ( .A(n2122), .Y(n1786) );
  INVX1 U1880 ( .A(n2047), .Y(n1787) );
  AND2X1 U1881 ( .A(tlb_io_resp_cacheable), .B(n2374), .Y(n1972) );
  INVX1 U1882 ( .A(n1972), .Y(n1788) );
  INVX1 U1883 ( .A(n1968), .Y(n1789) );
  AND2X1 U1884 ( .A(io_cpu_resp_bits_xcpt_if), .B(n2057), .Y(n1973) );
  INVX1 U1885 ( .A(n1973), .Y(n1790) );
  AND2X1 U1886 ( .A(reset), .B(io_resetVector[0]), .Y(n1975) );
  INVX1 U1887 ( .A(n1975), .Y(n1791) );
  AND2X1 U1888 ( .A(reset), .B(io_resetVector[1]), .Y(n1978) );
  INVX1 U1889 ( .A(n1978), .Y(n1792) );
  INVX1 U1890 ( .A(n1980), .Y(n1793) );
  INVX1 U1891 ( .A(n1982), .Y(n1794) );
  INVX1 U1892 ( .A(n2001), .Y(n1795) );
  INVX1 U1893 ( .A(n2003), .Y(n1796) );
  AND2X1 U1894 ( .A(BTB_io_resp_valid), .B(n2374), .Y(n2376) );
  INVX1 U1895 ( .A(n2376), .Y(n1797) );
  INVX1 U1896 ( .A(n1826), .Y(n2031) );
  AND2X2 U1897 ( .A(n1643), .B(n1561), .Y(n2008) );
  AND2X2 U1898 ( .A(n1642), .B(n831), .Y(n1999) );
  INVX1 U1899 ( .A(n817), .Y(n1798) );
  INVX1 U1900 ( .A(n1542), .Y(n1799) );
  OR2X1 U1901 ( .A(n1831), .B(n1441), .Y(n1830) );
  OR2X1 U1902 ( .A(io_cpu_req_valid), .B(n1025), .Y(n1874) );
  OR2X1 U1903 ( .A(io_cpu_req_valid), .B(n1028), .Y(n1896) );
  AND2X1 U1904 ( .A(n1363), .B(n2202), .Y(n1926) );
  INVX1 U1905 ( .A(n1926), .Y(n1801) );
  AND2X1 U1906 ( .A(n1869), .B(n1053), .Y(n1870) );
  INVX1 U1907 ( .A(n1886), .Y(n1802) );
  AND2X2 U1908 ( .A(n1039), .B(n694), .Y(n1846) );
  AND2X2 U1909 ( .A(n825), .B(n2163), .Y(io_cpu_npc_26) );
  INVX1 U1910 ( .A(io_cpu_npc_26), .Y(\icache_io_req_bits_addr[26] ) );
  INVX1 U1911 ( .A(n2125), .Y(n1808) );
  AND2X2 U1912 ( .A(n1937), .B(n1652), .Y(n2118) );
  AND2X2 U1913 ( .A(n1945), .B(n1653), .Y(n2123) );
  INVX1 U1914 ( .A(n2123), .Y(n1810) );
  AND2X2 U1915 ( .A(n2035), .B(n1656), .Y(n2133) );
  AND2X2 U1916 ( .A(n1506), .B(n2154), .Y(n1827) );
  INVX1 U1917 ( .A(n1827), .Y(n1812) );
  AND2X2 U1918 ( .A(BTB_io_req_bits_addr[7]), .B(BTB_io_req_bits_addr[6]), .Y(
        n1994) );
  INVX1 U1919 ( .A(n1994), .Y(n1813) );
  AND2X2 U1920 ( .A(n1596), .B(n2202), .Y(n2135) );
  INVX1 U1921 ( .A(n2135), .Y(n1814) );
  INVX1 U1922 ( .A(n814), .Y(n1817) );
  INVX1 U1923 ( .A(io_cpu_npc_1), .Y(\icache_io_req_bits_addr[1] ) );
  INVX1 U1924 ( .A(io_cpu_npc_0), .Y(\icache_io_req_bits_addr[0] ) );
  BUFX2 U1925 ( .A(n1008), .Y(\io_cpu_npc[6] ) );
  BUFX2 U1926 ( .A(n2408), .Y(BTB_io_req_bits_addr[14]) );
  XNOR2X1 U1927 ( .A(n1825), .B(n2045), .Y(n1835) );
  OAI21X1 U1928 ( .A(n1458), .B(n1836), .C(n2178), .Y(io_cpu_npc[33]) );
  INVX1 U1929 ( .A(n1839), .Y(n1838) );
  XNOR2X1 U1930 ( .A(\tlb_io_req_bits_vpn[20] ), .B(n794), .Y(n1837) );
  AND2X2 U1931 ( .A(n2094), .B(n1052), .Y(n2102) );
  OAI21X1 U1932 ( .A(n1863), .B(n1139), .C(n1414), .Y(n420) );
  BUFX2 U1933 ( .A(n2413), .Y(BTB_io_req_bits_addr[8]) );
  XOR2X1 U1934 ( .A(n2168), .B(BTB_io_req_bits_addr[29]), .Y(n2169) );
  XOR2X1 U1935 ( .A(n795), .B(BTB_io_req_bits_addr[28]), .Y(n2166) );
  XOR2X1 U1936 ( .A(n2171), .B(\tlb_io_req_bits_vpn[18] ), .Y(n2172) );
  XNOR2X1 U1937 ( .A(n1889), .B(n2404), .Y(n1836) );
  AND2X2 U1938 ( .A(n783), .B(n2193), .Y(n1839) );
  INVX8 U1939 ( .A(n2374), .Y(n2068) );
  BUFX2 U1940 ( .A(n2407), .Y(BTB_io_req_bits_addr[16]) );
  INVX1 U1941 ( .A(\tlb_io_req_bits_vpn[14] ), .Y(n2055) );
  AND2X1 U1942 ( .A(n1952), .B(n2099), .Y(n1840) );
  INVX1 U1943 ( .A(BTB_io_req_bits_addr[8]), .Y(n1993) );
  NOR3X1 U1944 ( .A(n1993), .B(n1813), .C(n1987), .Y(n1843) );
  AOI22X1 U1945 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[12]), .C(
        BTB_io_resp_bits_target[12]), .D(n1961), .Y(n1845) );
  AOI22X1 U1946 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[30]), .C(
        BTB_io_resp_bits_target[30]), .D(n1351), .Y(n1852) );
  AND2X2 U1947 ( .A(n1852), .B(n1851), .Y(n2173) );
  INVX1 U1948 ( .A(BTB_io_req_bits_addr_30), .Y(n2059) );
  AOI22X1 U1949 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[28]), .C(
        BTB_io_resp_bits_target[28]), .D(n1470), .Y(n1857) );
  INVX1 U1950 ( .A(n1858), .Y(n1859) );
  INVX1 U1951 ( .A(BTB_io_req_bits_addr_24), .Y(n2051) );
  BUFX2 U1952 ( .A(n2414), .Y(BTB_io_req_bits_addr[7]) );
  NAND3X1 U1953 ( .A(BTB_io_req_bits_addr[6]), .B(n1996), .C(n1995), .Y(n1861)
         );
  XNOR2X1 U1954 ( .A(n1631), .B(BTB_io_req_bits_addr[7]), .Y(n1862) );
  INVX1 U1955 ( .A(BTB_io_req_bits_addr[7]), .Y(n1863) );
  INVX1 U1956 ( .A(BTB_io_req_bits_addr[27]), .Y(n1869) );
  AOI22X1 U1957 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[27]), .C(
        BTB_io_resp_bits_target[27]), .D(n1961), .Y(n1865) );
  AND2X1 U1958 ( .A(n1281), .B(n2202), .Y(n1867) );
  INVX1 U1959 ( .A(n1867), .Y(n1868) );
  INVX1 U1960 ( .A(BTB_io_req_bits_addr_19), .Y(n1892) );
  INVX1 U1961 ( .A(io_cpu_req_bits_pc[19]), .Y(n1875) );
  INVX1 U1962 ( .A(BTB_io_req_bits_addr_22), .Y(n1877) );
  INVX1 U1963 ( .A(n1877), .Y(\tlb_io_req_bits_vpn[10] ) );
  INVX1 U1964 ( .A(n2152), .Y(n1883) );
  INVX1 U1965 ( .A(io_cpu_req_bits_pc[22]), .Y(n1880) );
  OAI21X1 U1966 ( .A(n1203), .B(n1883), .C(n1658), .Y(n405) );
  AOI22X1 U1967 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[33]), .C(
        BTB_io_resp_bits_target[33]), .D(n1351), .Y(n1885) );
  INVX1 U1968 ( .A(\tlb_io_req_bits_vpn[20] ), .Y(n2063) );
  NOR3X1 U1969 ( .A(n1887), .B(n2063), .C(n1802), .Y(n1888) );
  AND2X1 U1970 ( .A(BTB_io_req_bits_addr[33]), .B(n1053), .Y(n1890) );
  INVX1 U1971 ( .A(n1892), .Y(\tlb_io_req_bits_vpn[7] ) );
  INVX1 U1972 ( .A(n800), .Y(n2045) );
  INVX1 U1973 ( .A(n2045), .Y(\tlb_io_req_bits_vpn[8] ) );
  INVX1 U1974 ( .A(BTB_io_req_bits_addr_21), .Y(n1894) );
  INVX1 U1975 ( .A(n1894), .Y(\tlb_io_req_bits_vpn[9] ) );
  AND2X1 U1976 ( .A(\tlb_io_req_bits_vpn[6] ), .B(\tlb_io_req_bits_vpn[7] ), 
        .Y(n2049) );
  INVX1 U1977 ( .A(io_cpu_req_bits_pc[21]), .Y(n1897) );
  AOI22X1 U1978 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[32]), .C(
        BTB_io_resp_bits_target[32]), .D(n1351), .Y(n1899) );
  INVX1 U1979 ( .A(io_cpu_req_bits_pc[23]), .Y(n1904) );
  AND2X1 U1980 ( .A(n1579), .B(n2099), .Y(n1906) );
  INVX1 U1981 ( .A(n1872), .Y(n2100) );
  NAND3X1 U1982 ( .A(n1906), .B(n2155), .C(n2100), .Y(n1907) );
  INVX1 U1983 ( .A(n1573), .Y(n1908) );
  INVX1 U1984 ( .A(n1913), .Y(n1914) );
  INVX1 U1985 ( .A(BTB_io_req_bits_addr_25), .Y(n2053) );
  AOI22X1 U1986 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[26]), .C(
        BTB_io_resp_bits_target[26]), .D(n1351), .Y(n1920) );
  AND2X2 U1987 ( .A(n1590), .B(n1919), .Y(n2163) );
  INVX1 U1988 ( .A(n2163), .Y(n1921) );
  AOI22X1 U1989 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[29]), .C(
        BTB_io_resp_bits_target[29]), .D(n1961), .Y(n1923) );
  AND2X1 U1990 ( .A(BTB_io_req_bits_addr[29]), .B(n2202), .Y(n1925) );
  INVX8 U1991 ( .A(n1441), .Y(n2202) );
  INVX1 U1992 ( .A(n2159), .Y(n1927) );
  AOI22X1 U1993 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[24]), .C(
        BTB_io_resp_bits_target[24]), .D(n1961), .Y(n1929) );
  XNOR2X1 U1994 ( .A(n995), .B(BTB_io_req_bits_addr[14]), .Y(n1931) );
  INVX1 U1995 ( .A(n1931), .Y(n1935) );
  AOI22X1 U1996 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[14]), .C(
        BTB_io_resp_bits_target[14]), .D(n1961), .Y(n1933) );
  OAI21X1 U1997 ( .A(n1458), .B(n1935), .C(n1934), .Y(\io_cpu_npc[14] ) );
  INVX1 U1998 ( .A(BTB_io_req_bits_addr[11]), .Y(n2119) );
  AND2X1 U1999 ( .A(n2119), .B(n2202), .Y(n1938) );
  AOI22X1 U2000 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[11]), .C(
        BTB_io_resp_bits_target[11]), .D(n1961), .Y(n1937) );
  AOI21X1 U2001 ( .A(n2120), .B(n1938), .C(n1809), .Y(n1941) );
  AND2X1 U2002 ( .A(n2124), .B(n2202), .Y(n1946) );
  AOI22X1 U2003 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[13]), .C(
        BTB_io_resp_bits_target[13]), .D(n1961), .Y(n1945) );
  AOI21X1 U2004 ( .A(n2125), .B(n1946), .C(n1810), .Y(n1949) );
  BUFX2 U2005 ( .A(n1354), .Y(BTB_io_req_bits_addr[3]) );
  INVX1 U2006 ( .A(n1959), .Y(BTB_io_req_bits_addr[2]) );
  INVX8 U2007 ( .A(n1953), .Y(io_cpu_resp_bits_pc[2]) );
  AOI22X1 U2008 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[0]), .C(
        BTB_io_resp_bits_target[0]), .D(n1351), .Y(n1955) );
  AOI22X1 U2009 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[1]), .C(
        BTB_io_resp_bits_target[1]), .D(n1961), .Y(n1957) );
  INVX1 U2010 ( .A(BTB_io_req_bits_addr_15), .Y(n2127) );
  XOR2X1 U2011 ( .A(BTB_io_req_bits_addr[2]), .B(BTB_io_req_bits_addr[3]), .Y(
        n2113) );
  INVX1 U2012 ( .A(n1958), .Y(n424) );
  BUFX2 U2013 ( .A(n1357), .Y(BTB_io_req_bits_addr[4]) );
  XOR2X1 U2014 ( .A(n1995), .B(BTB_io_req_bits_addr[4]), .Y(n1962) );
  BUFX2 U2015 ( .A(n781), .Y(\io_cpu_npc[12] ) );
  BUFX2 U2016 ( .A(n2416), .Y(BTB_io_req_bits_addr[5]) );
  OAI21X1 U2017 ( .A(n1544), .B(n1872), .C(n1298), .Y(n1966) );
  MUX2X1 U2018 ( .B(io_cpu_req_bits_speculative), .A(n1966), .S(n2099), .Y(
        n1967) );
  INVX1 U2019 ( .A(reset), .Y(n1969) );
  BUFX2 U2020 ( .A(s2_cacheable), .Y(n1970) );
  INVX1 U2021 ( .A(tlb_io_resp_miss), .Y(n2378) );
  OAI21X1 U2022 ( .A(io_cpu_npc_0), .B(n1370), .C(n1659), .Y(n427) );
  INVX1 U2023 ( .A(BTB_io_req_bits_addr[3]), .Y(n2114) );
  AOI22X1 U2024 ( .A(reset), .B(io_resetVector[3]), .C(io_cpu_resp_bits_pc[3]), 
        .D(n2057), .Y(n1981) );
  OAI21X1 U2025 ( .A(n2114), .B(n2068), .C(n1556), .Y(n464) );
  AOI22X1 U2026 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[6]), .C(
        io_cpu_resp_bits_pc[6]), .D(n2112), .Y(n1989) );
  AOI22X1 U2027 ( .A(reset), .B(io_resetVector[8]), .C(io_cpu_resp_bits_pc[8]), 
        .D(n2057), .Y(n1992) );
  OAI21X1 U2028 ( .A(n1993), .B(n2068), .C(n1559), .Y(n459) );
  XOR2X1 U2029 ( .A(n1633), .B(BTB_io_req_bits_addr[8]), .Y(n2000) );
  OAI21X1 U2030 ( .A(n2000), .B(n1458), .C(n1999), .Y(io_cpu_npc[8]) );
  INVX1 U2031 ( .A(BTB_io_req_bits_addr[10]), .Y(n2011) );
  AOI22X1 U2032 ( .A(reset), .B(io_resetVector[10]), .C(
        io_cpu_resp_bits_pc[10]), .D(n2057), .Y(n2005) );
  OAI21X1 U2033 ( .A(n2011), .B(n2068), .C(n1560), .Y(n457) );
  XOR2X1 U2034 ( .A(n1548), .B(n2011), .Y(n2009) );
  AOI22X1 U2035 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[10]), .C(
        io_cpu_resp_bits_pc[10]), .D(n2112), .Y(n2006) );
  OAI21X1 U2036 ( .A(n2009), .B(n1458), .C(n2008), .Y(io_cpu_npc[10]) );
  OAI21X1 U2037 ( .A(n2011), .B(n1139), .C(n1661), .Y(n417) );
  AOI22X1 U2038 ( .A(reset), .B(io_resetVector[11]), .C(
        io_cpu_resp_bits_pc[11]), .D(n2057), .Y(n2012) );
  OAI21X1 U2039 ( .A(n2119), .B(n2068), .C(n1750), .Y(n456) );
  INVX1 U2040 ( .A(BTB_io_req_bits_addr_12), .Y(n2014) );
  AOI22X1 U2041 ( .A(reset), .B(io_resetVector[12]), .C(
        io_cpu_resp_bits_pc[12]), .D(n2057), .Y(n2013) );
  OAI21X1 U2042 ( .A(n1358), .B(n2068), .C(n1562), .Y(n455) );
  AOI22X1 U2043 ( .A(reset), .B(io_resetVector[13]), .C(
        io_cpu_resp_bits_pc[13]), .D(n2057), .Y(n2015) );
  OAI21X1 U2044 ( .A(n2124), .B(n2068), .C(n1563), .Y(n454) );
  AOI22X1 U2045 ( .A(reset), .B(io_resetVector[14]), .C(
        io_cpu_resp_bits_pc[14]), .D(n2057), .Y(n2016) );
  OAI21X1 U2046 ( .A(n1501), .B(n2068), .C(n1564), .Y(n453) );
  AOI22X1 U2047 ( .A(reset), .B(io_resetVector[15]), .C(
        io_cpu_resp_bits_pc[15]), .D(n2057), .Y(n2017) );
  OAI21X1 U2048 ( .A(n2127), .B(n2068), .C(n840), .Y(n452) );
  AND2X1 U2049 ( .A(n2127), .B(n2202), .Y(n2020) );
  AND2X1 U2050 ( .A(\tlb_io_req_bits_vpn[3] ), .B(n1053), .Y(n2021) );
  INVX1 U2051 ( .A(BTB_io_req_bits_addr[16]), .Y(n2132) );
  AOI22X1 U2052 ( .A(reset), .B(io_resetVector[16]), .C(
        io_cpu_resp_bits_pc[16]), .D(n2057), .Y(n2023) );
  OAI21X1 U2053 ( .A(n2132), .B(n2068), .C(n1565), .Y(n451) );
  AND2X1 U2054 ( .A(n2132), .B(n1053), .Y(n2026) );
  AOI22X1 U2055 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[16]), .C(
        BTB_io_resp_bits_target[16]), .D(n1351), .Y(n2025) );
  AND2X1 U2056 ( .A(BTB_io_req_bits_addr[16]), .B(n1053), .Y(n2027) );
  AOI22X1 U2057 ( .A(n2027), .B(n1051), .C(BTB_io_req_bits_addr[16]), .D(n2071), .Y(n2028) );
  AOI22X1 U2058 ( .A(reset), .B(io_resetVector[17]), .C(
        io_cpu_resp_bits_pc[17]), .D(n2057), .Y(n2029) );
  OAI21X1 U2059 ( .A(n2033), .B(n2068), .C(n1566), .Y(n450) );
  AOI22X1 U2060 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[17]), .C(
        BTB_io_resp_bits_target[17]), .D(n1961), .Y(n2035) );
  MUX2X1 U2061 ( .B(n1811), .A(\tlb_io_req_bits_vpn[5] ), .S(n1052), .Y(n2036)
         );
  AOI22X1 U2062 ( .A(reset), .B(io_resetVector[18]), .C(
        io_cpu_resp_bits_pc[18]), .D(n2057), .Y(n2038) );
  OAI21X1 U2063 ( .A(n2039), .B(n2068), .C(n1751), .Y(n449) );
  AND2X1 U2064 ( .A(n2039), .B(n2202), .Y(n2042) );
  AOI22X1 U2065 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[18]), .C(
        BTB_io_resp_bits_target[18]), .D(n1351), .Y(n2041) );
  AND2X1 U2066 ( .A(\tlb_io_req_bits_vpn[6] ), .B(n1053), .Y(n2043) );
  INVX1 U2067 ( .A(io_cpu_req_bits_pc[20]), .Y(n2048) );
  AOI22X1 U2068 ( .A(reset), .B(io_resetVector[24]), .C(
        io_cpu_resp_bits_pc[24]), .D(n2057), .Y(n2050) );
  OAI21X1 U2069 ( .A(n2051), .B(n2068), .C(n1567), .Y(n443) );
  AOI22X1 U2070 ( .A(reset), .B(io_resetVector[25]), .C(
        io_cpu_resp_bits_pc[25]), .D(n2057), .Y(n2052) );
  OAI21X1 U2071 ( .A(n2053), .B(n2068), .C(n1568), .Y(n442) );
  AOI22X1 U2072 ( .A(reset), .B(io_resetVector[26]), .C(
        io_cpu_resp_bits_pc[26]), .D(n2057), .Y(n2054) );
  OAI21X1 U2073 ( .A(n2055), .B(n2068), .C(n1569), .Y(n441) );
  AOI22X1 U2074 ( .A(reset), .B(io_resetVector[28]), .C(
        io_cpu_resp_bits_pc[28]), .D(n2057), .Y(n2056) );
  OAI21X1 U2075 ( .A(n1360), .B(n2068), .C(n1570), .Y(n439) );
  AOI22X1 U2076 ( .A(reset), .B(io_resetVector[30]), .C(
        io_cpu_resp_bits_pc[30]), .D(n2057), .Y(n2058) );
  OAI21X1 U2077 ( .A(n2059), .B(n2068), .C(n1752), .Y(n437) );
  AOI22X1 U2078 ( .A(reset), .B(io_resetVector[31]), .C(
        io_cpu_resp_bits_pc[31]), .D(n2057), .Y(n2060) );
  OAI21X1 U2079 ( .A(n1831), .B(n2068), .C(n1571), .Y(n436) );
  AOI22X1 U2080 ( .A(reset), .B(io_resetVector[32]), .C(
        io_cpu_resp_bits_pc[32]), .D(n2057), .Y(n2062) );
  OAI21X1 U2081 ( .A(n2063), .B(n2068), .C(n1572), .Y(n435) );
  INVX1 U2082 ( .A(BTB_io_req_bits_addr[35]), .Y(n2064) );
  AOI22X1 U2083 ( .A(reset), .B(io_resetVector[36]), .C(
        io_cpu_resp_bits_pc[36]), .D(n2057), .Y(n2067) );
  OAI21X1 U2084 ( .A(n2070), .B(n2068), .C(n1753), .Y(n431) );
  AND2X1 U2085 ( .A(BTB_io_req_bits_addr[34]), .B(BTB_io_req_bits_addr[35]), 
        .Y(n2091) );
  INVX1 U2086 ( .A(BTB_io_req_bits_addr[36]), .Y(n2070) );
  MUX2X1 U2087 ( .B(n1019), .A(BTB_io_req_bits_addr[36]), .S(n2071), .Y(n2072)
         );
  INVX1 U2088 ( .A(n1575), .Y(n2075) );
  INVX1 U2089 ( .A(n579), .Y(n2089) );
  INVX1 U2090 ( .A(n2187), .Y(n2078) );
  INVX1 U2091 ( .A(BTB_io_req_bits_addr[37]), .Y(n2077) );
  INVX1 U2092 ( .A(n2079), .Y(n2088) );
  INVX1 U2093 ( .A(io_cpu_req_bits_pc[37]), .Y(n2080) );
  INVX1 U2094 ( .A(n2086), .Y(n2087) );
  OAI21X1 U2095 ( .A(n2088), .B(n2089), .C(n2087), .Y(n390) );
  AND2X1 U2096 ( .A(BTB_io_req_bits_addr[36]), .B(BTB_io_req_bits_addr[37]), 
        .Y(n2090) );
  AND2X1 U2097 ( .A(n2091), .B(n2090), .Y(n2107) );
  INVX1 U2098 ( .A(n1576), .Y(n2093) );
  INVX1 U2099 ( .A(n584), .Y(n2106) );
  INVX1 U2100 ( .A(n2193), .Y(n2095) );
  INVX1 U2101 ( .A(BTB_io_req_bits_addr[38]), .Y(n2094) );
  INVX1 U2102 ( .A(n2096), .Y(n2105) );
  INVX1 U2103 ( .A(io_cpu_req_bits_pc[38]), .Y(n2097) );
  INVX1 U2104 ( .A(n574), .Y(n2104) );
  OAI21X1 U2105 ( .A(n2106), .B(n2105), .C(n2104), .Y(n389) );
  AND2X1 U2106 ( .A(n2107), .B(BTB_io_req_bits_addr[38]), .Y(n2108) );
  INVX1 U2107 ( .A(n2201), .Y(n2110) );
  INVX1 U2108 ( .A(\s1_pc_[39] ), .Y(n2109) );
  OAI21X1 U2109 ( .A(n805), .B(n2110), .C(n2109), .Y(n2111) );
  OAI21X1 U2110 ( .A(n2113), .B(n2114), .C(n1053), .Y(n2199) );
  INVX1 U2111 ( .A(s1_same_block), .Y(n2115) );
  XOR2X1 U2112 ( .A(n1630), .B(n2119), .Y(n2121) );
  INVX1 U2113 ( .A(n1445), .Y(n2130) );
  OAI21X1 U2114 ( .A(n1814), .B(n2134), .C(n2133), .Y(io_cpu_npc[17]) );
  AND2X1 U2115 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[19]), .Y(n2138)
         );
  NOR3X1 U2116 ( .A(n2138), .B(n2137), .C(n1025), .Y(n2140) );
  AND2X1 U2117 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[20]), .Y(n2142)
         );
  AND2X1 U2118 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[21]), .Y(n2146)
         );
  NOR3X1 U2119 ( .A(n2146), .B(n2145), .C(n1028), .Y(n2148) );
  AND2X1 U2120 ( .A(io_cpu_req_valid), .B(io_cpu_req_bits_pc[22]), .Y(n2151)
         );
  NOR3X1 U2121 ( .A(n2151), .B(n2150), .C(n2149), .Y(n2153) );
  NAND3X1 U2122 ( .A(n1579), .B(n1696), .C(n2155), .Y(n2157) );
  XOR2X1 U2123 ( .A(n1546), .B(\tlb_io_req_bits_vpn[13] ), .Y(n2160) );
  XOR2X1 U2124 ( .A(n2161), .B(\tlb_io_req_bits_vpn[14] ), .Y(n2162) );
  XNOR2X1 U2125 ( .A(n1034), .B(\tlb_io_req_bits_vpn[19] ), .Y(n2174) );
  XOR2X1 U2126 ( .A(n2179), .B(BTB_io_req_bits_addr[34]), .Y(n2180) );
  AND2X1 U2127 ( .A(BTB_io_req_bits_addr[35]), .B(n1053), .Y(n2182) );
  AND2X1 U2128 ( .A(BTB_io_req_bits_addr[36]), .B(n1053), .Y(n2185) );
  OAI21X1 U2129 ( .A(n1023), .B(n1699), .C(n702), .Y(io_cpu_npc[36]) );
  AND2X1 U2130 ( .A(BTB_io_req_bits_addr[37]), .B(n1053), .Y(n2191) );
  OAI21X1 U2131 ( .A(n812), .B(n1700), .C(n1413), .Y(io_cpu_npc[37]) );
  AND2X1 U2132 ( .A(BTB_io_req_bits_addr[38]), .B(n1053), .Y(n2197) );
  NAND3X1 U2133 ( .A(n1714), .B(n1698), .C(n2194), .Y(n2196) );
  INVX1 U2134 ( .A(n2199), .Y(n2200) );
  AOI21X1 U2135 ( .A(n2200), .B(n1016), .C(n1042), .Y(icache_io_req_valid) );
  AND2X1 U2136 ( .A(\s1_pc_[39] ), .B(n1053), .Y(n2203) );
  INVX1 U2137 ( .A(icache_io_s2_kill), .Y(n2206) );
  INVX1 U2138 ( .A(icache_io_resp_bits_datablock[0]), .Y(n2208) );
  OAI21X1 U2139 ( .A(io_cpu_resp_bits_pc[2]), .B(n2208), .C(n1785), .Y(
        io_cpu_resp_bits_data[0]) );
  INVX1 U2140 ( .A(icache_io_resp_bits_datablock[1]), .Y(n2210) );
  OAI21X1 U2141 ( .A(io_cpu_resp_bits_pc[2]), .B(n2210), .C(n1784), .Y(
        io_cpu_resp_bits_data[1]) );
  INVX1 U2142 ( .A(icache_io_resp_bits_datablock[2]), .Y(n2212) );
  OAI21X1 U2143 ( .A(io_cpu_resp_bits_pc[2]), .B(n2212), .C(n1783), .Y(
        io_cpu_resp_bits_data[2]) );
  INVX1 U2144 ( .A(icache_io_resp_bits_datablock[3]), .Y(n2214) );
  OAI21X1 U2145 ( .A(n2396), .B(n2214), .C(n1782), .Y(io_cpu_resp_bits_data[3]) );
  INVX1 U2146 ( .A(icache_io_resp_bits_datablock[4]), .Y(n2216) );
  OAI21X1 U2147 ( .A(io_cpu_resp_bits_pc[2]), .B(n2216), .C(n1781), .Y(
        io_cpu_resp_bits_data[4]) );
  INVX1 U2148 ( .A(icache_io_resp_bits_datablock[5]), .Y(n2218) );
  OAI21X1 U2149 ( .A(io_cpu_resp_bits_pc[2]), .B(n2218), .C(n1780), .Y(
        io_cpu_resp_bits_data[5]) );
  INVX1 U2150 ( .A(icache_io_resp_bits_datablock[6]), .Y(n2220) );
  OAI21X1 U2151 ( .A(n2396), .B(n2220), .C(n1779), .Y(io_cpu_resp_bits_data[6]) );
  INVX1 U2152 ( .A(icache_io_resp_bits_datablock[7]), .Y(n2222) );
  OAI21X1 U2153 ( .A(io_cpu_resp_bits_pc[2]), .B(n2222), .C(n1778), .Y(
        io_cpu_resp_bits_data[7]) );
  INVX1 U2154 ( .A(icache_io_resp_bits_datablock[8]), .Y(n2224) );
  OAI21X1 U2155 ( .A(io_cpu_resp_bits_pc[2]), .B(n2224), .C(n1777), .Y(
        io_cpu_resp_bits_data[8]) );
  INVX1 U2156 ( .A(icache_io_resp_bits_datablock[9]), .Y(n2226) );
  OAI21X1 U2157 ( .A(n2396), .B(n2226), .C(n1776), .Y(io_cpu_resp_bits_data[9]) );
  INVX1 U2158 ( .A(icache_io_resp_bits_datablock[10]), .Y(n2228) );
  OAI21X1 U2159 ( .A(io_cpu_resp_bits_pc[2]), .B(n2228), .C(n1775), .Y(
        io_cpu_resp_bits_data[10]) );
  INVX1 U2160 ( .A(icache_io_resp_bits_datablock[11]), .Y(n2230) );
  OAI21X1 U2161 ( .A(io_cpu_resp_bits_pc[2]), .B(n2230), .C(n1774), .Y(
        io_cpu_resp_bits_data[11]) );
  INVX1 U2162 ( .A(icache_io_resp_bits_datablock[12]), .Y(n2232) );
  OAI21X1 U2163 ( .A(n2396), .B(n2232), .C(n1773), .Y(
        io_cpu_resp_bits_data[12]) );
  INVX1 U2164 ( .A(icache_io_resp_bits_datablock[13]), .Y(n2234) );
  OAI21X1 U2165 ( .A(io_cpu_resp_bits_pc[2]), .B(n2234), .C(n1772), .Y(
        io_cpu_resp_bits_data[13]) );
  INVX1 U2166 ( .A(icache_io_resp_bits_datablock[14]), .Y(n2236) );
  OAI21X1 U2167 ( .A(io_cpu_resp_bits_pc[2]), .B(n2236), .C(n1771), .Y(
        io_cpu_resp_bits_data[14]) );
  INVX1 U2168 ( .A(icache_io_resp_bits_datablock[15]), .Y(n2238) );
  OAI21X1 U2169 ( .A(n2396), .B(n2238), .C(n1770), .Y(
        io_cpu_resp_bits_data[15]) );
  INVX1 U2170 ( .A(icache_io_resp_bits_datablock[16]), .Y(n2240) );
  OAI21X1 U2171 ( .A(io_cpu_resp_bits_pc[2]), .B(n2240), .C(n1769), .Y(
        io_cpu_resp_bits_data[16]) );
  INVX1 U2172 ( .A(icache_io_resp_bits_datablock[17]), .Y(n2242) );
  OAI21X1 U2173 ( .A(io_cpu_resp_bits_pc[2]), .B(n2242), .C(n1768), .Y(
        io_cpu_resp_bits_data[17]) );
  INVX1 U2174 ( .A(icache_io_resp_bits_datablock[18]), .Y(n2244) );
  OAI21X1 U2175 ( .A(n2396), .B(n2244), .C(n1767), .Y(
        io_cpu_resp_bits_data[18]) );
  INVX1 U2176 ( .A(icache_io_resp_bits_datablock[19]), .Y(n2246) );
  OAI21X1 U2177 ( .A(io_cpu_resp_bits_pc[2]), .B(n2246), .C(n1766), .Y(
        io_cpu_resp_bits_data[19]) );
  INVX1 U2178 ( .A(icache_io_resp_bits_datablock[20]), .Y(n2248) );
  OAI21X1 U2179 ( .A(io_cpu_resp_bits_pc[2]), .B(n2248), .C(n1765), .Y(
        io_cpu_resp_bits_data[20]) );
  INVX1 U2180 ( .A(icache_io_resp_bits_datablock[21]), .Y(n2250) );
  OAI21X1 U2181 ( .A(n2396), .B(n2250), .C(n1764), .Y(
        io_cpu_resp_bits_data[21]) );
  INVX1 U2182 ( .A(icache_io_resp_bits_datablock[22]), .Y(n2252) );
  OAI21X1 U2183 ( .A(io_cpu_resp_bits_pc[2]), .B(n2252), .C(n1763), .Y(
        io_cpu_resp_bits_data[22]) );
  INVX1 U2184 ( .A(icache_io_resp_bits_datablock[23]), .Y(n2254) );
  OAI21X1 U2185 ( .A(io_cpu_resp_bits_pc[2]), .B(n2254), .C(n1762), .Y(
        io_cpu_resp_bits_data[23]) );
  INVX1 U2186 ( .A(icache_io_resp_bits_datablock[24]), .Y(n2256) );
  OAI21X1 U2187 ( .A(n2396), .B(n2256), .C(n1761), .Y(
        io_cpu_resp_bits_data[24]) );
  INVX1 U2188 ( .A(icache_io_resp_bits_datablock[25]), .Y(n2258) );
  OAI21X1 U2189 ( .A(io_cpu_resp_bits_pc[2]), .B(n2258), .C(n1760), .Y(
        io_cpu_resp_bits_data[25]) );
  INVX1 U2190 ( .A(icache_io_resp_bits_datablock[26]), .Y(n2260) );
  OAI21X1 U2191 ( .A(io_cpu_resp_bits_pc[2]), .B(n2260), .C(n1759), .Y(
        io_cpu_resp_bits_data[26]) );
  INVX1 U2192 ( .A(icache_io_resp_bits_datablock[27]), .Y(n2262) );
  OAI21X1 U2193 ( .A(n2396), .B(n2262), .C(n1758), .Y(
        io_cpu_resp_bits_data[27]) );
  INVX1 U2194 ( .A(icache_io_resp_bits_datablock[28]), .Y(n2264) );
  OAI21X1 U2195 ( .A(io_cpu_resp_bits_pc[2]), .B(n2264), .C(n1757), .Y(
        io_cpu_resp_bits_data[28]) );
  INVX1 U2196 ( .A(icache_io_resp_bits_datablock[29]), .Y(n2266) );
  OAI21X1 U2197 ( .A(io_cpu_resp_bits_pc[2]), .B(n2266), .C(n1756), .Y(
        io_cpu_resp_bits_data[29]) );
  INVX1 U2198 ( .A(icache_io_resp_bits_datablock[30]), .Y(n2268) );
  OAI21X1 U2199 ( .A(n2396), .B(n2268), .C(n1755), .Y(
        io_cpu_resp_bits_data[30]) );
  INVX1 U2200 ( .A(icache_io_resp_bits_datablock[31]), .Y(n2270) );
  OAI21X1 U2201 ( .A(io_cpu_resp_bits_pc[2]), .B(n2270), .C(n1754), .Y(
        io_cpu_resp_bits_data[31]) );
  INVX1 U2202 ( .A(io_cpu_resp_bits_btb_bits_bht_value[1]), .Y(n2273) );
  INVX1 U2203 ( .A(io_cpu_resp_bits_btb_bits_bht_history[2]), .Y(n2277) );
  INVX1 U2204 ( .A(io_cpu_resp_bits_btb_bits_bht_history[3]), .Y(n2279) );
  INVX1 U2205 ( .A(io_cpu_resp_bits_btb_bits_bht_history[5]), .Y(n2282) );
  INVX1 U2206 ( .A(io_cpu_resp_bits_btb_bits_bht_history[6]), .Y(n2284) );
  INVX1 U2207 ( .A(io_cpu_resp_bits_btb_bits_entry[0]), .Y(n2286) );
  INVX1 U2208 ( .A(io_cpu_resp_bits_btb_bits_entry[1]), .Y(n2288) );
  OAI21X1 U2209 ( .A(n2288), .B(n2350), .C(n1746), .Y(n340) );
  INVX1 U2210 ( .A(io_cpu_resp_bits_btb_bits_entry[2]), .Y(n2290) );
  INVX1 U2211 ( .A(io_cpu_resp_bits_btb_bits_entry[3]), .Y(n2292) );
  INVX1 U2212 ( .A(io_cpu_resp_bits_btb_bits_entry[4]), .Y(n2294) );
  OAI21X1 U2213 ( .A(n2294), .B(n2350), .C(n1743), .Y(n343) );
  INVX1 U2214 ( .A(io_cpu_resp_bits_btb_bits_entry[5]), .Y(n2296) );
  INVX1 U2215 ( .A(io_cpu_resp_bits_btb_bits_target[0]), .Y(n2298) );
  INVX1 U2216 ( .A(io_cpu_resp_bits_btb_bits_target[1]), .Y(n2301) );
  INVX1 U2217 ( .A(io_cpu_resp_bits_btb_bits_target[3]), .Y(n2304) );
  INVX1 U2218 ( .A(io_cpu_resp_bits_btb_bits_target[4]), .Y(n2306) );
  INVX1 U2219 ( .A(io_cpu_resp_bits_btb_bits_target[5]), .Y(n2308) );
  OAI21X1 U2220 ( .A(n2308), .B(n2350), .C(n1737), .Y(n350) );
  INVX1 U2221 ( .A(io_cpu_resp_bits_btb_bits_target[6]), .Y(n2310) );
  OAI21X1 U2222 ( .A(n2310), .B(n2350), .C(n1736), .Y(n351) );
  INVX1 U2223 ( .A(io_cpu_resp_bits_btb_bits_target[7]), .Y(n2312) );
  INVX1 U2224 ( .A(io_cpu_resp_bits_btb_bits_target[8]), .Y(n2314) );
  OAI21X1 U2225 ( .A(n2314), .B(n2350), .C(n1734), .Y(n353) );
  INVX1 U2226 ( .A(io_cpu_resp_bits_btb_bits_target[10]), .Y(n2317) );
  OAI21X1 U2227 ( .A(n2317), .B(n2350), .C(n1733), .Y(n355) );
  INVX1 U2228 ( .A(io_cpu_resp_bits_btb_bits_target[11]), .Y(n2319) );
  INVX1 U2229 ( .A(io_cpu_resp_bits_btb_bits_target[12]), .Y(n2321) );
  OAI21X1 U2230 ( .A(n2321), .B(n2350), .C(n1731), .Y(n357) );
  INVX1 U2231 ( .A(io_cpu_resp_bits_btb_bits_target[13]), .Y(n2323) );
  INVX1 U2232 ( .A(io_cpu_resp_bits_btb_bits_target[14]), .Y(n2325) );
  INVX1 U2233 ( .A(io_cpu_resp_bits_btb_bits_target[15]), .Y(n2327) );
  OAI21X1 U2234 ( .A(n2327), .B(n2350), .C(n1728), .Y(n360) );
  INVX1 U2235 ( .A(io_cpu_resp_bits_btb_bits_target[16]), .Y(n2329) );
  OAI21X1 U2236 ( .A(n2329), .B(n2350), .C(n1727), .Y(n361) );
  INVX1 U2237 ( .A(io_cpu_resp_bits_btb_bits_target[17]), .Y(n2331) );
  OAI21X1 U2238 ( .A(n2331), .B(n2350), .C(n1726), .Y(n362) );
  INVX1 U2239 ( .A(io_cpu_resp_bits_btb_bits_target[18]), .Y(n2333) );
  OAI21X1 U2240 ( .A(n2333), .B(n2350), .C(n1725), .Y(n363) );
  INVX1 U2241 ( .A(io_cpu_resp_bits_btb_bits_target[19]), .Y(n2335) );
  INVX1 U2242 ( .A(io_cpu_resp_bits_btb_bits_target[20]), .Y(n2337) );
  OAI21X1 U2243 ( .A(n2337), .B(n2350), .C(n1723), .Y(n365) );
  INVX1 U2244 ( .A(io_cpu_resp_bits_btb_bits_target[21]), .Y(n2339) );
  OAI21X1 U2245 ( .A(n2339), .B(n2350), .C(n1722), .Y(n366) );
  INVX1 U2246 ( .A(io_cpu_resp_bits_btb_bits_target[22]), .Y(n2341) );
  OAI21X1 U2247 ( .A(n2341), .B(n2350), .C(n1721), .Y(n367) );
  INVX1 U2248 ( .A(io_cpu_resp_bits_btb_bits_target[23]), .Y(n2343) );
  OAI21X1 U2249 ( .A(n2343), .B(n2350), .C(n1720), .Y(n368) );
  INVX1 U2250 ( .A(io_cpu_resp_bits_btb_bits_target[24]), .Y(n2345) );
  INVX1 U2251 ( .A(io_cpu_resp_bits_btb_bits_target[25]), .Y(n2347) );
  INVX1 U2252 ( .A(io_cpu_resp_bits_btb_bits_target[26]), .Y(n2349) );
  OAI21X1 U2253 ( .A(n2349), .B(n2350), .C(n1664), .Y(n371) );
  INVX1 U2254 ( .A(io_cpu_resp_bits_btb_bits_target[27]), .Y(n2352) );
  INVX1 U2255 ( .A(io_cpu_resp_bits_btb_bits_target[28]), .Y(n2354) );
  OAI21X1 U2256 ( .A(n2354), .B(n2350), .C(n2353), .Y(n373) );
  INVX1 U2257 ( .A(io_cpu_resp_bits_btb_bits_target[29]), .Y(n2356) );
  OAI21X1 U2258 ( .A(n2356), .B(n2350), .C(n1666), .Y(n374) );
  INVX1 U2259 ( .A(io_cpu_resp_bits_btb_bits_target[30]), .Y(n2358) );
  OAI21X1 U2260 ( .A(n2358), .B(n2350), .C(n1667), .Y(n375) );
  INVX1 U2261 ( .A(io_cpu_resp_bits_btb_bits_target[31]), .Y(n2360) );
  OAI21X1 U2262 ( .A(n2360), .B(n2350), .C(n2359), .Y(n376) );
  INVX1 U2263 ( .A(io_cpu_resp_bits_btb_bits_target[32]), .Y(n2362) );
  INVX1 U2264 ( .A(io_cpu_resp_bits_btb_bits_target[33]), .Y(n2364) );
  OAI21X1 U2265 ( .A(n2364), .B(n2350), .C(n1670), .Y(n378) );
  INVX1 U2266 ( .A(io_cpu_resp_bits_btb_bits_mask), .Y(n2370) );
  INVX1 U2267 ( .A(io_cpu_resp_bits_btb_bits_taken), .Y(n2373) );
  NOR3X1 U2268 ( .A(tlb_io_resp_xcpt_if), .B(io_cpu_flush_tlb), .C(n1704), .Y(
        n2379) );
endmodule

