module BoomFrontend( // @[:boom.system.TestHarness.SmallBoomConfig.fir@124747.2]
  input         clock, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124748.4]
  input         reset, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124749.4]
  input         auto_icache_master_out_a_ready, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  output        auto_icache_master_out_a_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  output [31:0] auto_icache_master_out_a_bits_address, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  input         auto_icache_master_out_d_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  input  [2:0]  auto_icache_master_out_d_bits_opcode, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  input  [3:0]  auto_icache_master_out_d_bits_size, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  input  [63:0] auto_icache_master_out_d_bits_data, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  input         auto_icache_master_out_d_bits_error, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124750.4]
  input  [31:0] io_reset_vector, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_fetchpacket_ready, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [31:0] io_cpu_fetchpacket_bits_uops_0_inst, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [39:0] io_cpu_fetchpacket_bits_uops_0_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_br_prediction_btb_blame, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_br_prediction_btb_hit, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_br_prediction_btb_taken, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_blame, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_hit, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_taken, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [3:0]  io_cpu_fetchpacket_bits_uops_0_br_prediction_bim_resp_rowdata, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [9:0]  io_cpu_fetchpacket_bits_uops_0_br_prediction_bim_resp_entry_idx, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [1:0]  io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_resp_takens, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [7:0]  io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_resp_history, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_resp_info, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_stat_brjmp_mispredicted, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_stat_btb_made_pred, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_stat_btb_mispredicted, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_stat_bpd_made_pred, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_stat_bpd_mispredicted, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [3:0]  io_cpu_fetchpacket_bits_uops_0_ftq_idx, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [5:0]  io_cpu_fetchpacket_bits_uops_0_pc_lob, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [11:0] io_cpu_fetchpacket_bits_uops_0_csr_addr, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [5:0]  io_cpu_fetchpacket_bits_uops_0_pdst, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_xcpt_pf_if, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_xcpt_ae_if, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_replay_if, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_fetchpacket_bits_uops_0_xcpt_ma_if, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [63:0] io_cpu_fetchpacket_bits_uops_0_debug_wdata, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [31:0] io_cpu_fetchpacket_bits_uops_0_debug_events_fetch_seq, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_br_unit_take_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [39:0] io_cpu_br_unit_target, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_br_unit_brinfo_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_br_unit_brinfo_mispredict, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [2:0]  io_cpu_br_unit_brinfo_pc_lob, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [15:0] io_cpu_br_unit_brinfo_ftq_idx, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_br_unit_brinfo_taken, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [2:0]  io_cpu_br_unit_brinfo_cfi_type, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_br_unit_btb_update_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [38:0] io_cpu_br_unit_btb_update_bits_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [38:0] io_cpu_br_unit_btb_update_bits_target, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [38:0] io_cpu_br_unit_btb_update_bits_cfi_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [2:0]  io_cpu_br_unit_btb_update_bits_bpd_type, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [2:0]  io_cpu_br_unit_btb_update_bits_cfi_type, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [3:0]  io_cpu_get_pc_ftq_idx, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [39:0] io_cpu_get_pc_fetch_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_get_pc_next_val, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [39:0] io_cpu_get_pc_next_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_sfence_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_sfence_bits_rs1, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_sfence_bits_rs2, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [38:0] io_cpu_sfence_bits_addr, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_sfence_take_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [39:0] io_cpu_sfence_addr, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_commit_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [15:0] io_cpu_commit_bits, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_flush_info_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [3:0]  io_cpu_flush_info_bits_ftq_idx, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [5:0]  io_cpu_flush_info_bits_pc_lob, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [2:0]  io_cpu_flush_info_bits_flush_typ, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_flush_take_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [39:0] io_cpu_flush_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_flush_icache, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [3:0]  io_cpu_com_ftq_idx, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [39:0] io_cpu_com_fetch_pc, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_flush, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_clear_fetchbuffer, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_cpu_status_debug, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_perf_acquire, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_cpu_perf_tlbMiss, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_req_ready, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output        io_ptw_req_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  output [26:0] io_ptw_req_bits_addr, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_valid, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_ae, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [53:0] io_ptw_resp_bits_pte_ppn, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_d, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_a, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_g, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_u, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_x, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_w, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_r, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_pte_v, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [1:0]  io_ptw_resp_bits_level, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input         io_ptw_resp_bits_homogeneous, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [3:0]  io_ptw_ptbr_mode, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]
  input  [1:0]  io_ptw_status_prv, // @[:boom.system.TestHarness.SmallBoomConfig.fir@124751.4]

  input canary_in,
  output alarm
);
  assign alarm = canary_in;
  wire  icache_clock; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_reset; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_auto_master_out_a_ready; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_auto_master_out_a_valid; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [31:0] icache_auto_master_out_a_bits_address; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_auto_master_out_d_valid; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [2:0] icache_auto_master_out_d_bits_opcode; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [3:0] icache_auto_master_out_d_bits_size; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [63:0] icache_auto_master_out_d_bits_data; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_auto_master_out_d_bits_error; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_req_ready; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_req_valid; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [38:0] icache_io_req_bits_addr; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [38:0] icache_io_s1_vaddr; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [31:0] icache_io_s1_paddr; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_s1_kill; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_s2_kill; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_resp_valid; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire [63:0] icache_io_resp_bits_data; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_resp_bits_ae; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_invalidate; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  icache_io_perf_acquire; // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
  wire  tlb_clock; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_reset; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_req_ready; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_req_valid; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [39:0] tlb_io_req_bits_vaddr; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_req_bits_sfence_valid; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_req_bits_sfence_bits_rs1; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_req_bits_sfence_bits_rs2; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [38:0] tlb_io_req_bits_sfence_bits_addr; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_resp_miss; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [31:0] tlb_io_resp_paddr; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_resp_pf_inst; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_resp_ae_inst; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_resp_cacheable; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_req_ready; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_req_valid; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [26:0] tlb_io_ptw_req_bits_addr; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_valid; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_ae; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [53:0] tlb_io_ptw_resp_bits_pte_ppn; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_d; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_a; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_g; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_u; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_x; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_w; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_r; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_pte_v; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [1:0] tlb_io_ptw_resp_bits_level; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  tlb_io_ptw_resp_bits_homogeneous; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [3:0] tlb_io_ptw_ptbr_mode; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire [1:0] tlb_io_ptw_status_prv; // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
  wire  fetch_controller_clock; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_reset; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_imem_req_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_imem_req_bits_pc"
  wire [39:0] fetch_controller_io_imem_req_bits_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_imem_req_bits_speculative; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_imem_resp_ready; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_imem_resp_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_imem_resp_bits_pc"
  secure wire [39:0] fetch_controller_io_imem_resp_bits_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [63:0] fetch_controller_io_imem_resp_bits_data; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [1:0] fetch_controller_io_imem_resp_bits_mask; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_imem_resp_bits_xcpt_pf_inst; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_imem_resp_bits_xcpt_ae_inst; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_imem_resp_bits_replay; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f2_btb_resp_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f2_btb_resp_bits_taken; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [38:0] fetch_controller_io_f2_btb_resp_bits_target; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [1:0] fetch_controller_io_f2_btb_resp_bits_mask; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f2_btb_resp_bits_cfi_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [2:0] fetch_controller_io_f2_btb_resp_bits_bpd_type; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [2:0] fetch_controller_io_f2_btb_resp_bits_cfi_type; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f2_btb_resp_bits_bim_resp_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [3:0] fetch_controller_io_f2_btb_resp_bits_bim_resp_bits_rowdata; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [9:0] fetch_controller_io_f2_btb_resp_bits_bim_resp_bits_entry_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f3_bpd_resp_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [1:0] fetch_controller_io_f3_bpd_resp_bits_takens; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [7:0] fetch_controller_io_f3_bpd_resp_bits_history; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f3_btb_update_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_f3_btb_update_bits_pc"
  secure wire [38:0] fetch_controller_io_f3_btb_update_bits_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [38:0] fetch_controller_io_f3_btb_update_bits_target; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_f3_btb_update_bits_cfi_pc"
  secure wire [38:0] fetch_controller_io_f3_btb_update_bits_cfi_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [2:0] fetch_controller_io_f3_btb_update_bits_bpd_type; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [2:0] fetch_controller_io_f3_btb_update_bits_cfi_type; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f2_redirect; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f3_stall; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_f4_redirect; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_f4_taken"
  secure wire  fetch_controller_io_f4_taken; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_bim_update_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [9:0] fetch_controller_io_bim_update_bits_entry_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_bim_update_bits_cfi_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [1:0] fetch_controller_io_bim_update_bits_cntr_value; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_bim_update_bits_mispredicted; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_bim_update_bits_taken; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_ftq_restore_history_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [7:0] fetch_controller_io_ftq_restore_history_bits_history; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_ftq_restore_history_bits_taken; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_br_unit_take_pc"
  secure wire  fetch_controller_io_br_unit_take_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [39:0] fetch_controller_io_br_unit_target; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_br_unit_brinfo_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_br_unit_brinfo_mispredict; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [2:0] fetch_controller_io_br_unit_brinfo_pc_lob; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [15:0] fetch_controller_io_br_unit_brinfo_ftq_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_br_unit_brinfo_taken; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [2:0] fetch_controller_io_br_unit_brinfo_cfi_type; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [3:0] fetch_controller_io_get_pc_ftq_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_get_pc_fetch_pc"
  secure wire [39:0] fetch_controller_io_get_pc_fetch_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_get_pc_next_val"
  secure wire  fetch_controller_io_get_pc_next_val; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_get_pc_next_pc"
  secure wire [39:0] fetch_controller_io_get_pc_next_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_clear_fetchbuffer; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_commit_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [15:0] fetch_controller_io_commit_bits; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_flush_info_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [3:0] fetch_controller_io_flush_info_bits_ftq_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [5:0] fetch_controller_io_flush_info_bits_pc_lob; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [2:0] fetch_controller_io_flush_info_bits_flush_typ; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_flush_take_pc"
  secure wire  fetch_controller_io_flush_take_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_flush_pc"
  secure wire [39:0] fetch_controller_io_flush_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [3:0] fetch_controller_io_com_ftq_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_com_fetch_pc"
  secure wire [39:0] fetch_controller_io_com_fetch_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_sfence_take_pc"
  secure wire  fetch_controller_io_sfence_take_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [39:0] fetch_controller_io_sfence_addr; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_ready; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_valid; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [31:0] fetch_controller_io_fetchpacket_bits_uops_0_inst; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_fetchpacket_bits_uops_0_pc"
  secure wire [39:0] fetch_controller_io_fetchpacket_bits_uops_0_pc; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_blame; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_hit; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_taken; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_blame; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_hit; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_taken; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [3:0] fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bim_resp_rowdata; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [9:0] fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bim_resp_entry_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [1:0] fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_takens; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [7:0] fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_history; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_info; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_fetchpacket_bits_uops_0_stat_brjmp_mispredicted"
  secure wire  fetch_controller_io_fetchpacket_bits_uops_0_stat_brjmp_mispredicted; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_stat_btb_made_pred; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  // synopsys keep_signal_name "fetch_controller_io_fetchpacket_bits_uops_0_stat_btb_mispredicted"
  secure wire  fetch_controller_io_fetchpacket_bits_uops_0_stat_btb_mispredicted; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_stat_bpd_made_pred; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_stat_bpd_mispredicted; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [3:0] fetch_controller_io_fetchpacket_bits_uops_0_ftq_idx; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [5:0] fetch_controller_io_fetchpacket_bits_uops_0_pc_lob; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [11:0] fetch_controller_io_fetchpacket_bits_uops_0_csr_addr; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [5:0] fetch_controller_io_fetchpacket_bits_uops_0_pdst; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_xcpt_pf_if; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_xcpt_ae_if; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_replay_if; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  fetch_controller_io_fetchpacket_bits_uops_0_xcpt_ma_if; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [63:0] fetch_controller_io_fetchpacket_bits_uops_0_debug_wdata; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire [31:0] fetch_controller_io_fetchpacket_bits_uops_0_debug_events_fetch_seq; // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
  wire  bpdpipeline_clock; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_reset; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_s0_req_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [38:0] bpdpipeline_io_s0_req_bits_addr; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [39:0] bpdpipeline_io_debug_imemresp_pc; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_btb_resp_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_btb_resp_bits_taken; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [38:0] bpdpipeline_io_f2_btb_resp_bits_target; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [1:0] bpdpipeline_io_f2_btb_resp_bits_mask; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_btb_resp_bits_cfi_idx; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [2:0] bpdpipeline_io_f2_btb_resp_bits_bpd_type; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [2:0] bpdpipeline_io_f2_btb_resp_bits_cfi_type; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_btb_resp_bits_bim_resp_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [3:0] bpdpipeline_io_f2_btb_resp_bits_bim_resp_bits_rowdata; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [9:0] bpdpipeline_io_f2_btb_resp_bits_bim_resp_bits_entry_idx; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_stall; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_replay; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f2_redirect; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f3_bpd_resp_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [1:0] bpdpipeline_io_f3_bpd_resp_bits_takens; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [7:0] bpdpipeline_io_f3_bpd_resp_bits_history; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f3_btb_update_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [38:0] bpdpipeline_io_f3_btb_update_bits_pc; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [38:0] bpdpipeline_io_f3_btb_update_bits_target; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [38:0] bpdpipeline_io_f3_btb_update_bits_cfi_pc; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [2:0] bpdpipeline_io_f3_btb_update_bits_bpd_type; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [2:0] bpdpipeline_io_f3_btb_update_bits_cfi_type; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f3_stall; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f4_redirect; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_f4_taken; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_bim_update_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [9:0] bpdpipeline_io_bim_update_bits_entry_idx; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_bim_update_bits_cfi_idx; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [1:0] bpdpipeline_io_bim_update_bits_cntr_value; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_bim_update_bits_mispredicted; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_bim_update_bits_taken; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_br_unit_btb_update_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  // synopsys keep_signal_name "bpdpipeline_io_br_unit_btb_update_bits_pc"
  secure wire [38:0] bpdpipeline_io_br_unit_btb_update_bits_pc; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [38:0] bpdpipeline_io_br_unit_btb_update_bits_target; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [38:0] bpdpipeline_io_br_unit_btb_update_bits_cfi_pc; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [2:0] bpdpipeline_io_br_unit_btb_update_bits_bpd_type; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [2:0] bpdpipeline_io_br_unit_btb_update_bits_cfi_type; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_fe_clear; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_ftq_restore_valid; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire [7:0] bpdpipeline_io_ftq_restore_bits_history; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_ftq_restore_bits_taken; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_flush; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_redirect; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  wire  bpdpipeline_io_status_debug; // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
  reg  s1_valid; // @[frontend.scala 171:25:boom.system.TestHarness.SmallBoomConfig.fir@124776.4]
  reg [31:0] _RAND_0;
  reg [39:0] s1_pc; // @[frontend.scala 172:18:boom.system.TestHarness.SmallBoomConfig.fir@124778.4]
  reg [63:0] _RAND_1;
  reg  s1_speculative; // @[frontend.scala 173:27:boom.system.TestHarness.SmallBoomConfig.fir@124779.4]
  reg [31:0] _RAND_2;
  reg  s2_valid; // @[frontend.scala 174:25:boom.system.TestHarness.SmallBoomConfig.fir@124780.4]
  reg [31:0] _RAND_3;
  reg [39:0] s2_pc; // @[frontend.scala 175:22:boom.system.TestHarness.SmallBoomConfig.fir@124784.4]
  reg [63:0] _RAND_4;
  reg  s2_tlb_resp_miss; // @[frontend.scala 179:24:boom.system.TestHarness.SmallBoomConfig.fir@124787.4]
  reg [31:0] _RAND_5;
  reg  s2_tlb_resp_pf_inst; // @[frontend.scala 179:24:boom.system.TestHarness.SmallBoomConfig.fir@124787.4]
  reg [31:0] _RAND_6;
  reg  s2_tlb_resp_ae_inst; // @[frontend.scala 179:24:boom.system.TestHarness.SmallBoomConfig.fir@124787.4]
  reg [31:0] _RAND_7;
  reg  s2_tlb_resp_cacheable; // @[frontend.scala 179:24:boom.system.TestHarness.SmallBoomConfig.fir@124787.4]
  reg [31:0] _RAND_8;
  reg  s2_speculative; // @[frontend.scala 181:27:boom.system.TestHarness.SmallBoomConfig.fir@124789.4]
  reg [31:0] _RAND_9;
  reg  _T_613; // @[frontend.scala 192:78:boom.system.TestHarness.SmallBoomConfig.fir@124811.4]
  reg [31:0] _RAND_10;
  reg  _T_639; // @[frontend.scala 231:49:boom.system.TestHarness.SmallBoomConfig.fir@124859.4]
  reg [31:0] _RAND_11;
  wire  s0_valid; // @[frontend.scala 170:53:boom.system.TestHarness.SmallBoomConfig.fir@124775.4]
  wire [31:0] _T_584; // @[frontend.scala 314:29:boom.system.TestHarness.SmallBoomConfig.fir@124781.4]
  wire [31:0] _T_586; // @[frontend.scala 314:33:boom.system.TestHarness.SmallBoomConfig.fir@124782.4]
  wire [31:0] _T_587; // @[frontend.scala 314:27:boom.system.TestHarness.SmallBoomConfig.fir@124783.4]
  wire  s2_xcpt; // @[frontend.scala 180:37:boom.system.TestHarness.SmallBoomConfig.fir@124788.4]
  wire [39:0] _T_595; // @[frontend.scala 69:12:boom.system.TestHarness.SmallBoomConfig.fir@124793.4]
  wire [39:0] _T_597; // @[frontend.scala 69:18:boom.system.TestHarness.SmallBoomConfig.fir@124794.4]
  wire [39:0] s1_base_pc; // @[frontend.scala 69:10:boom.system.TestHarness.SmallBoomConfig.fir@124795.4]
  wire [40:0] _T_599; // @[frontend.scala 81:12:boom.system.TestHarness.SmallBoomConfig.fir@124796.4]
  wire [39:0] ntpc; // @[frontend.scala 81:12:boom.system.TestHarness.SmallBoomConfig.fir@124797.4]
  wire  _T_604; // @[Decoupled.scala 37:37:boom.system.TestHarness.SmallBoomConfig.fir@124806.4]
  wire  _T_606; // @[frontend.scala 192:29:boom.system.TestHarness.SmallBoomConfig.fir@124807.4]
  wire  _T_607; // @[frontend.scala 192:26:boom.system.TestHarness.SmallBoomConfig.fir@124808.4]
  wire  _T_609; // @[frontend.scala 192:92:boom.system.TestHarness.SmallBoomConfig.fir@124809.4]
  wire  s2_replay; // @[frontend.scala 192:68:boom.system.TestHarness.SmallBoomConfig.fir@124813.4]
  wire  _T_610; // @[frontend.scala 192:89:boom.system.TestHarness.SmallBoomConfig.fir@124810.4]
  wire [39:0] npc; // @[frontend.scala 193:16:boom.system.TestHarness.SmallBoomConfig.fir@124815.4]
  wire  _T_620; // @[frontend.scala 205:9:boom.system.TestHarness.SmallBoomConfig.fir@124824.4]
  wire  s2_redirect; // @[:boom.system.TestHarness.SmallBoomConfig.fir@124820.4]
  wire  _T_622; // @[frontend.scala 206:17:boom.system.TestHarness.SmallBoomConfig.fir@124826.6]
  wire  _T_627; // @[frontend.scala 226:36:boom.system.TestHarness.SmallBoomConfig.fir@124846.4]
  wire  _T_630; // @[frontend.scala 227:42:boom.system.TestHarness.SmallBoomConfig.fir@124849.4]
  wire  _T_631; // @[frontend.scala 227:39:boom.system.TestHarness.SmallBoomConfig.fir@124850.4]
  wire [39:0] _T_633; // @[frontend.scala 230:23:boom.system.TestHarness.SmallBoomConfig.fir@124854.4]
  wire [39:0] _T_634; // @[frontend.scala 314:29:boom.system.TestHarness.SmallBoomConfig.fir@124855.4]
  wire [39:0] _T_636; // @[frontend.scala 314:33:boom.system.TestHarness.SmallBoomConfig.fir@124856.4]
  wire [39:0] s0_pc; // @[frontend.scala 314:27:boom.system.TestHarness.SmallBoomConfig.fir@124857.4]
  wire  _T_640; // @[frontend.scala 231:60:boom.system.TestHarness.SmallBoomConfig.fir@124861.4]
  wire  _T_642; // @[frontend.scala 231:100:boom.system.TestHarness.SmallBoomConfig.fir@124862.4]
  wire  _T_643; // @[frontend.scala 231:118:boom.system.TestHarness.SmallBoomConfig.fir@124863.4]
  wire  _T_644; // @[frontend.scala 231:97:boom.system.TestHarness.SmallBoomConfig.fir@124864.4]
  wire  _T_646; // @[package.scala 109:13:boom.system.TestHarness.SmallBoomConfig.fir@124869.4]
  wire [2:0] _T_648; // @[frontend.scala 96:31:boom.system.TestHarness.SmallBoomConfig.fir@124870.4]
  wire  _T_650; // @[frontend.scala 238:99:boom.system.TestHarness.SmallBoomConfig.fir@124872.4]
  wire  _T_651; // @[frontend.scala 238:96:boom.system.TestHarness.SmallBoomConfig.fir@124873.4]
  wire  _T_653; // @[frontend.scala 238:124:boom.system.TestHarness.SmallBoomConfig.fir@124874.4]
  wire  _T_656; // @[frontend.scala 242:30:boom.system.TestHarness.SmallBoomConfig.fir@124881.4]
  ICache icache ( // @[frontend.scala 141:26:boom.system.TestHarness.SmallBoomConfig.fir@124753.4]
    .clock(icache_clock),
    .reset(icache_reset),
    .auto_master_out_a_ready(icache_auto_master_out_a_ready),
    .auto_master_out_a_valid(icache_auto_master_out_a_valid),
    .auto_master_out_a_bits_address(icache_auto_master_out_a_bits_address),
    .auto_master_out_d_valid(icache_auto_master_out_d_valid),
    .auto_master_out_d_bits_opcode(icache_auto_master_out_d_bits_opcode),
    .auto_master_out_d_bits_size(icache_auto_master_out_d_bits_size),
    .auto_master_out_d_bits_data(icache_auto_master_out_d_bits_data),
    .auto_master_out_d_bits_error(icache_auto_master_out_d_bits_error),
    .io_req_ready(icache_io_req_ready),
    .io_req_valid(icache_io_req_valid),
    .io_req_bits_addr(icache_io_req_bits_addr),
    .io_s1_vaddr(icache_io_s1_vaddr),
    .io_s1_paddr(icache_io_s1_paddr),
    .io_s1_kill(icache_io_s1_kill),
    .io_s2_kill(icache_io_s2_kill),
    .io_resp_valid(icache_io_resp_valid),
    .io_resp_bits_data(icache_io_resp_bits_data),
    .io_resp_bits_ae(icache_io_resp_bits_ae),
    .io_invalidate(icache_io_invalidate),
    .io_perf_acquire(icache_io_perf_acquire)
  );
  TLB_1 tlb ( // @[frontend.scala 163:19:boom.system.TestHarness.SmallBoomConfig.fir@124761.4]
    .clock(tlb_clock),
    .reset(tlb_reset),
    .io_req_ready(tlb_io_req_ready),
    .io_req_valid(tlb_io_req_valid),
    .io_req_bits_vaddr(tlb_io_req_bits_vaddr),
    .io_req_bits_sfence_valid(tlb_io_req_bits_sfence_valid),
    .io_req_bits_sfence_bits_rs1(tlb_io_req_bits_sfence_bits_rs1),
    .io_req_bits_sfence_bits_rs2(tlb_io_req_bits_sfence_bits_rs2),
    .io_req_bits_sfence_bits_addr(tlb_io_req_bits_sfence_bits_addr),
    .io_resp_miss(tlb_io_resp_miss),
    .io_resp_paddr(tlb_io_resp_paddr),
    .io_resp_pf_inst(tlb_io_resp_pf_inst),
    .io_resp_ae_inst(tlb_io_resp_ae_inst),
    .io_resp_cacheable(tlb_io_resp_cacheable),
    .io_ptw_req_ready(tlb_io_ptw_req_ready),
    .io_ptw_req_valid(tlb_io_ptw_req_valid),
    .io_ptw_req_bits_addr(tlb_io_ptw_req_bits_addr),
    .io_ptw_resp_valid(tlb_io_ptw_resp_valid),
    .io_ptw_resp_bits_ae(tlb_io_ptw_resp_bits_ae),
    .io_ptw_resp_bits_pte_ppn(tlb_io_ptw_resp_bits_pte_ppn),
    .io_ptw_resp_bits_pte_d(tlb_io_ptw_resp_bits_pte_d),
    .io_ptw_resp_bits_pte_a(tlb_io_ptw_resp_bits_pte_a),
    .io_ptw_resp_bits_pte_g(tlb_io_ptw_resp_bits_pte_g),
    .io_ptw_resp_bits_pte_u(tlb_io_ptw_resp_bits_pte_u),
    .io_ptw_resp_bits_pte_x(tlb_io_ptw_resp_bits_pte_x),
    .io_ptw_resp_bits_pte_w(tlb_io_ptw_resp_bits_pte_w),
    .io_ptw_resp_bits_pte_r(tlb_io_ptw_resp_bits_pte_r),
    .io_ptw_resp_bits_pte_v(tlb_io_ptw_resp_bits_pte_v),
    .io_ptw_resp_bits_level(tlb_io_ptw_resp_bits_level),
    .io_ptw_resp_bits_homogeneous(tlb_io_ptw_resp_bits_homogeneous),
    .io_ptw_ptbr_mode(tlb_io_ptw_ptbr_mode),
    .io_ptw_status_prv(tlb_io_ptw_status_prv)
  );
  FetchControlUnit fetch_controller ( // @[frontend.scala 164:32:boom.system.TestHarness.SmallBoomConfig.fir@124765.4]
    .clock(fetch_controller_clock),
    .reset(fetch_controller_reset),
    .io_imem_req_valid(fetch_controller_io_imem_req_valid),
    .io_imem_req_bits_pc(fetch_controller_io_imem_req_bits_pc),
    .io_imem_req_bits_speculative(fetch_controller_io_imem_req_bits_speculative),
    .io_imem_resp_ready(fetch_controller_io_imem_resp_ready),
    .io_imem_resp_valid(fetch_controller_io_imem_resp_valid),
    .io_imem_resp_bits_pc(fetch_controller_io_imem_resp_bits_pc),
    .io_imem_resp_bits_data(fetch_controller_io_imem_resp_bits_data),
    .io_imem_resp_bits_mask(fetch_controller_io_imem_resp_bits_mask),
    .io_imem_resp_bits_xcpt_pf_inst(fetch_controller_io_imem_resp_bits_xcpt_pf_inst),
    .io_imem_resp_bits_xcpt_ae_inst(fetch_controller_io_imem_resp_bits_xcpt_ae_inst),
    .io_imem_resp_bits_replay(fetch_controller_io_imem_resp_bits_replay),
    .io_f2_btb_resp_valid(fetch_controller_io_f2_btb_resp_valid),
    .io_f2_btb_resp_bits_taken(fetch_controller_io_f2_btb_resp_bits_taken),
    .io_f2_btb_resp_bits_target(fetch_controller_io_f2_btb_resp_bits_target),
    .io_f2_btb_resp_bits_mask(fetch_controller_io_f2_btb_resp_bits_mask),
    .io_f2_btb_resp_bits_cfi_idx(fetch_controller_io_f2_btb_resp_bits_cfi_idx),
    .io_f2_btb_resp_bits_bpd_type(fetch_controller_io_f2_btb_resp_bits_bpd_type),
    .io_f2_btb_resp_bits_cfi_type(fetch_controller_io_f2_btb_resp_bits_cfi_type),
    .io_f2_btb_resp_bits_bim_resp_valid(fetch_controller_io_f2_btb_resp_bits_bim_resp_valid),
    .io_f2_btb_resp_bits_bim_resp_bits_rowdata(fetch_controller_io_f2_btb_resp_bits_bim_resp_bits_rowdata),
    .io_f2_btb_resp_bits_bim_resp_bits_entry_idx(fetch_controller_io_f2_btb_resp_bits_bim_resp_bits_entry_idx),
    .io_f3_bpd_resp_valid(fetch_controller_io_f3_bpd_resp_valid),
    .io_f3_bpd_resp_bits_takens(fetch_controller_io_f3_bpd_resp_bits_takens),
    .io_f3_bpd_resp_bits_history(fetch_controller_io_f3_bpd_resp_bits_history),
    .io_f3_btb_update_valid(fetch_controller_io_f3_btb_update_valid),
    .io_f3_btb_update_bits_pc(fetch_controller_io_f3_btb_update_bits_pc),
    .io_f3_btb_update_bits_target(fetch_controller_io_f3_btb_update_bits_target),
    .io_f3_btb_update_bits_cfi_pc(fetch_controller_io_f3_btb_update_bits_cfi_pc),
    .io_f3_btb_update_bits_bpd_type(fetch_controller_io_f3_btb_update_bits_bpd_type),
    .io_f3_btb_update_bits_cfi_type(fetch_controller_io_f3_btb_update_bits_cfi_type),
    .io_f2_redirect(fetch_controller_io_f2_redirect),
    .io_f3_stall(fetch_controller_io_f3_stall),
    .io_f4_redirect(fetch_controller_io_f4_redirect),
    .io_f4_taken(fetch_controller_io_f4_taken),
    .io_bim_update_valid(fetch_controller_io_bim_update_valid),
    .io_bim_update_bits_entry_idx(fetch_controller_io_bim_update_bits_entry_idx),
    .io_bim_update_bits_cfi_idx(fetch_controller_io_bim_update_bits_cfi_idx),
    .io_bim_update_bits_cntr_value(fetch_controller_io_bim_update_bits_cntr_value),
    .io_bim_update_bits_mispredicted(fetch_controller_io_bim_update_bits_mispredicted),
    .io_bim_update_bits_taken(fetch_controller_io_bim_update_bits_taken),
    .io_ftq_restore_history_valid(fetch_controller_io_ftq_restore_history_valid),
    .io_ftq_restore_history_bits_history(fetch_controller_io_ftq_restore_history_bits_history),
    .io_ftq_restore_history_bits_taken(fetch_controller_io_ftq_restore_history_bits_taken),
    .io_br_unit_take_pc(fetch_controller_io_br_unit_take_pc),
    .io_br_unit_target(fetch_controller_io_br_unit_target),
    .io_br_unit_brinfo_valid(fetch_controller_io_br_unit_brinfo_valid),
    .io_br_unit_brinfo_mispredict(fetch_controller_io_br_unit_brinfo_mispredict),
    .io_br_unit_brinfo_pc_lob(fetch_controller_io_br_unit_brinfo_pc_lob),
    .io_br_unit_brinfo_ftq_idx(fetch_controller_io_br_unit_brinfo_ftq_idx),
    .io_br_unit_brinfo_taken(fetch_controller_io_br_unit_brinfo_taken),
    .io_br_unit_brinfo_cfi_type(fetch_controller_io_br_unit_brinfo_cfi_type),
    .io_get_pc_ftq_idx(fetch_controller_io_get_pc_ftq_idx),
    .io_get_pc_fetch_pc(fetch_controller_io_get_pc_fetch_pc),
    .io_get_pc_next_val(fetch_controller_io_get_pc_next_val),
    .io_get_pc_next_pc(fetch_controller_io_get_pc_next_pc),
    .io_clear_fetchbuffer(fetch_controller_io_clear_fetchbuffer),
    .io_commit_valid(fetch_controller_io_commit_valid),
    .io_commit_bits(fetch_controller_io_commit_bits),
    .io_flush_info_valid(fetch_controller_io_flush_info_valid),
    .io_flush_info_bits_ftq_idx(fetch_controller_io_flush_info_bits_ftq_idx),
    .io_flush_info_bits_pc_lob(fetch_controller_io_flush_info_bits_pc_lob),
    .io_flush_info_bits_flush_typ(fetch_controller_io_flush_info_bits_flush_typ),
    .io_flush_take_pc(fetch_controller_io_flush_take_pc),
    .io_flush_pc(fetch_controller_io_flush_pc),
    .io_com_ftq_idx(fetch_controller_io_com_ftq_idx),
    .io_com_fetch_pc(fetch_controller_io_com_fetch_pc),
    .io_sfence_take_pc(fetch_controller_io_sfence_take_pc),
    .io_sfence_addr(fetch_controller_io_sfence_addr),
    .io_fetchpacket_ready(fetch_controller_io_fetchpacket_ready),
    .io_fetchpacket_valid(fetch_controller_io_fetchpacket_valid),
    .io_fetchpacket_bits_uops_0_valid(fetch_controller_io_fetchpacket_bits_uops_0_valid),
    .io_fetchpacket_bits_uops_0_inst(fetch_controller_io_fetchpacket_bits_uops_0_inst),
    .io_fetchpacket_bits_uops_0_pc(fetch_controller_io_fetchpacket_bits_uops_0_pc),
    .io_fetchpacket_bits_uops_0_br_prediction_btb_blame(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_blame),
    .io_fetchpacket_bits_uops_0_br_prediction_btb_hit(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_hit),
    .io_fetchpacket_bits_uops_0_br_prediction_btb_taken(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_taken),
    .io_fetchpacket_bits_uops_0_br_prediction_bpd_blame(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_blame),
    .io_fetchpacket_bits_uops_0_br_prediction_bpd_hit(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_hit),
    .io_fetchpacket_bits_uops_0_br_prediction_bpd_taken(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_taken),
    .io_fetchpacket_bits_uops_0_br_prediction_bim_resp_rowdata(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bim_resp_rowdata),
    .io_fetchpacket_bits_uops_0_br_prediction_bim_resp_entry_idx(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bim_resp_entry_idx),
    .io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_takens(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_takens),
    .io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_history(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_history),
    .io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_info(fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_info),
    .io_fetchpacket_bits_uops_0_stat_brjmp_mispredicted(fetch_controller_io_fetchpacket_bits_uops_0_stat_brjmp_mispredicted),
    .io_fetchpacket_bits_uops_0_stat_btb_made_pred(fetch_controller_io_fetchpacket_bits_uops_0_stat_btb_made_pred),
    .io_fetchpacket_bits_uops_0_stat_btb_mispredicted(fetch_controller_io_fetchpacket_bits_uops_0_stat_btb_mispredicted),
    .io_fetchpacket_bits_uops_0_stat_bpd_made_pred(fetch_controller_io_fetchpacket_bits_uops_0_stat_bpd_made_pred),
    .io_fetchpacket_bits_uops_0_stat_bpd_mispredicted(fetch_controller_io_fetchpacket_bits_uops_0_stat_bpd_mispredicted),
    .io_fetchpacket_bits_uops_0_ftq_idx(fetch_controller_io_fetchpacket_bits_uops_0_ftq_idx),
    .io_fetchpacket_bits_uops_0_pc_lob(fetch_controller_io_fetchpacket_bits_uops_0_pc_lob),
    .io_fetchpacket_bits_uops_0_csr_addr(fetch_controller_io_fetchpacket_bits_uops_0_csr_addr),
    .io_fetchpacket_bits_uops_0_pdst(fetch_controller_io_fetchpacket_bits_uops_0_pdst),
    .io_fetchpacket_bits_uops_0_xcpt_pf_if(fetch_controller_io_fetchpacket_bits_uops_0_xcpt_pf_if),
    .io_fetchpacket_bits_uops_0_xcpt_ae_if(fetch_controller_io_fetchpacket_bits_uops_0_xcpt_ae_if),
    .io_fetchpacket_bits_uops_0_replay_if(fetch_controller_io_fetchpacket_bits_uops_0_replay_if),
    .io_fetchpacket_bits_uops_0_xcpt_ma_if(fetch_controller_io_fetchpacket_bits_uops_0_xcpt_ma_if),
    .io_fetchpacket_bits_uops_0_debug_wdata(fetch_controller_io_fetchpacket_bits_uops_0_debug_wdata),
    .io_fetchpacket_bits_uops_0_debug_events_fetch_seq(fetch_controller_io_fetchpacket_bits_uops_0_debug_events_fetch_seq)
  );
  BranchPredictionStage bpdpipeline ( // @[frontend.scala 165:27:boom.system.TestHarness.SmallBoomConfig.fir@124769.4]
    .clock(bpdpipeline_clock),
    .reset(bpdpipeline_reset),
    .io_s0_req_valid(bpdpipeline_io_s0_req_valid),
    .io_s0_req_bits_addr(bpdpipeline_io_s0_req_bits_addr),
    .io_debug_imemresp_pc(bpdpipeline_io_debug_imemresp_pc),
    .io_f2_valid(bpdpipeline_io_f2_valid),
    .io_f2_btb_resp_valid(bpdpipeline_io_f2_btb_resp_valid),
    .io_f2_btb_resp_bits_taken(bpdpipeline_io_f2_btb_resp_bits_taken),
    .io_f2_btb_resp_bits_target(bpdpipeline_io_f2_btb_resp_bits_target),
    .io_f2_btb_resp_bits_mask(bpdpipeline_io_f2_btb_resp_bits_mask),
    .io_f2_btb_resp_bits_cfi_idx(bpdpipeline_io_f2_btb_resp_bits_cfi_idx),
    .io_f2_btb_resp_bits_bpd_type(bpdpipeline_io_f2_btb_resp_bits_bpd_type),
    .io_f2_btb_resp_bits_cfi_type(bpdpipeline_io_f2_btb_resp_bits_cfi_type),
    .io_f2_btb_resp_bits_bim_resp_valid(bpdpipeline_io_f2_btb_resp_bits_bim_resp_valid),
    .io_f2_btb_resp_bits_bim_resp_bits_rowdata(bpdpipeline_io_f2_btb_resp_bits_bim_resp_bits_rowdata),
    .io_f2_btb_resp_bits_bim_resp_bits_entry_idx(bpdpipeline_io_f2_btb_resp_bits_bim_resp_bits_entry_idx),
    .io_f2_stall(bpdpipeline_io_f2_stall),
    .io_f2_replay(bpdpipeline_io_f2_replay),
    .io_f2_redirect(bpdpipeline_io_f2_redirect),
    .io_f3_bpd_resp_valid(bpdpipeline_io_f3_bpd_resp_valid),
    .io_f3_bpd_resp_bits_takens(bpdpipeline_io_f3_bpd_resp_bits_takens),
    .io_f3_bpd_resp_bits_history(bpdpipeline_io_f3_bpd_resp_bits_history),
    .io_f3_btb_update_valid(bpdpipeline_io_f3_btb_update_valid),
    .io_f3_btb_update_bits_pc(bpdpipeline_io_f3_btb_update_bits_pc),
    .io_f3_btb_update_bits_target(bpdpipeline_io_f3_btb_update_bits_target),
    .io_f3_btb_update_bits_cfi_pc(bpdpipeline_io_f3_btb_update_bits_cfi_pc),
    .io_f3_btb_update_bits_bpd_type(bpdpipeline_io_f3_btb_update_bits_bpd_type),
    .io_f3_btb_update_bits_cfi_type(bpdpipeline_io_f3_btb_update_bits_cfi_type),
    .io_f3_stall(bpdpipeline_io_f3_stall),
    .io_f4_redirect(bpdpipeline_io_f4_redirect),
    .io_f4_taken(bpdpipeline_io_f4_taken),
    .io_bim_update_valid(bpdpipeline_io_bim_update_valid),
    .io_bim_update_bits_entry_idx(bpdpipeline_io_bim_update_bits_entry_idx),
    .io_bim_update_bits_cfi_idx(bpdpipeline_io_bim_update_bits_cfi_idx),
    .io_bim_update_bits_cntr_value(bpdpipeline_io_bim_update_bits_cntr_value),
    .io_bim_update_bits_mispredicted(bpdpipeline_io_bim_update_bits_mispredicted),
    .io_bim_update_bits_taken(bpdpipeline_io_bim_update_bits_taken),
    .io_br_unit_btb_update_valid(bpdpipeline_io_br_unit_btb_update_valid),
    .io_br_unit_btb_update_bits_pc(bpdpipeline_io_br_unit_btb_update_bits_pc),
    .io_br_unit_btb_update_bits_target(bpdpipeline_io_br_unit_btb_update_bits_target),
    .io_br_unit_btb_update_bits_cfi_pc(bpdpipeline_io_br_unit_btb_update_bits_cfi_pc),
    .io_br_unit_btb_update_bits_bpd_type(bpdpipeline_io_br_unit_btb_update_bits_bpd_type),
    .io_br_unit_btb_update_bits_cfi_type(bpdpipeline_io_br_unit_btb_update_bits_cfi_type),
    .io_fe_clear(bpdpipeline_io_fe_clear),
    .io_ftq_restore_valid(bpdpipeline_io_ftq_restore_valid),
    .io_ftq_restore_bits_history(bpdpipeline_io_ftq_restore_bits_history),
    .io_ftq_restore_bits_taken(bpdpipeline_io_ftq_restore_bits_taken),
    .io_flush(bpdpipeline_io_flush),
    .io_redirect(bpdpipeline_io_redirect),
    .io_status_debug(bpdpipeline_io_status_debug)
  );
  assign s0_valid = fetch_controller_io_imem_req_valid | fetch_controller_io_imem_resp_ready; // @[frontend.scala 170:53:boom.system.TestHarness.SmallBoomConfig.fir@124775.4]
  assign _T_584 = ~ io_reset_vector; // @[frontend.scala 314:29:boom.system.TestHarness.SmallBoomConfig.fir@124781.4]
  assign _T_586 = _T_584 | 32'h3; // @[frontend.scala 314:33:boom.system.TestHarness.SmallBoomConfig.fir@124782.4]
  assign _T_587 = ~ _T_586; // @[frontend.scala 314:27:boom.system.TestHarness.SmallBoomConfig.fir@124783.4]
  assign s2_xcpt = s2_tlb_resp_ae_inst | s2_tlb_resp_pf_inst; // @[frontend.scala 180:37:boom.system.TestHarness.SmallBoomConfig.fir@124788.4]
  assign _T_595 = ~ s1_pc; // @[frontend.scala 69:12:boom.system.TestHarness.SmallBoomConfig.fir@124793.4]
  assign _T_597 = _T_595 | 40'h7; // @[frontend.scala 69:18:boom.system.TestHarness.SmallBoomConfig.fir@124794.4]
  assign s1_base_pc = ~ _T_597; // @[frontend.scala 69:10:boom.system.TestHarness.SmallBoomConfig.fir@124795.4]
  assign _T_599 = s1_base_pc + 40'h8; // @[frontend.scala 81:12:boom.system.TestHarness.SmallBoomConfig.fir@124796.4]
  assign ntpc = _T_599[39:0]; // @[frontend.scala 81:12:boom.system.TestHarness.SmallBoomConfig.fir@124797.4]
  assign _T_604 = fetch_controller_io_imem_resp_ready & fetch_controller_io_imem_resp_valid; // @[Decoupled.scala 37:37:boom.system.TestHarness.SmallBoomConfig.fir@124806.4]
  assign _T_606 = _T_604 == 1'h0; // @[frontend.scala 192:29:boom.system.TestHarness.SmallBoomConfig.fir@124807.4]
  assign _T_607 = s2_valid & _T_606; // @[frontend.scala 192:26:boom.system.TestHarness.SmallBoomConfig.fir@124808.4]
  assign _T_609 = s0_valid == 1'h0; // @[frontend.scala 192:92:boom.system.TestHarness.SmallBoomConfig.fir@124809.4]
  assign s2_replay = _T_607 | _T_613; // @[frontend.scala 192:68:boom.system.TestHarness.SmallBoomConfig.fir@124813.4]
  assign _T_610 = s2_replay & _T_609; // @[frontend.scala 192:89:boom.system.TestHarness.SmallBoomConfig.fir@124810.4]
  assign npc = s2_replay ? s2_pc : ntpc; // @[frontend.scala 193:16:boom.system.TestHarness.SmallBoomConfig.fir@124815.4]
  assign _T_620 = s2_replay == 1'h0; // @[frontend.scala 205:9:boom.system.TestHarness.SmallBoomConfig.fir@124824.4]
  assign s2_redirect = fetch_controller_io_imem_req_valid; // @[:boom.system.TestHarness.SmallBoomConfig.fir@124820.4]
  assign _T_622 = s2_redirect == 1'h0; // @[frontend.scala 206:17:boom.system.TestHarness.SmallBoomConfig.fir@124826.6]
  assign _T_627 = s2_redirect | tlb_io_resp_miss; // @[frontend.scala 226:36:boom.system.TestHarness.SmallBoomConfig.fir@124846.4]
  assign _T_630 = s2_tlb_resp_cacheable == 1'h0; // @[frontend.scala 227:42:boom.system.TestHarness.SmallBoomConfig.fir@124849.4]
  assign _T_631 = s2_speculative & _T_630; // @[frontend.scala 227:39:boom.system.TestHarness.SmallBoomConfig.fir@124850.4]
  assign _T_633 = fetch_controller_io_imem_req_valid ? fetch_controller_io_imem_req_bits_pc : npc; // @[frontend.scala 230:23:boom.system.TestHarness.SmallBoomConfig.fir@124854.4]
  assign _T_634 = ~ _T_633; // @[frontend.scala 314:29:boom.system.TestHarness.SmallBoomConfig.fir@124855.4]
  assign _T_636 = _T_634 | 40'h3; // @[frontend.scala 314:33:boom.system.TestHarness.SmallBoomConfig.fir@124856.4]
  assign s0_pc = ~ _T_636; // @[frontend.scala 314:27:boom.system.TestHarness.SmallBoomConfig.fir@124857.4]
  assign _T_640 = _T_639 & s2_valid; // @[frontend.scala 231:60:boom.system.TestHarness.SmallBoomConfig.fir@124861.4]
  assign _T_642 = s2_tlb_resp_miss == 1'h0; // @[frontend.scala 231:100:boom.system.TestHarness.SmallBoomConfig.fir@124862.4]
  assign _T_643 = _T_642 & icache_io_s2_kill; // @[frontend.scala 231:118:boom.system.TestHarness.SmallBoomConfig.fir@124863.4]
  assign _T_644 = icache_io_resp_valid | _T_643; // @[frontend.scala 231:97:boom.system.TestHarness.SmallBoomConfig.fir@124864.4]
  assign _T_646 = s2_pc[2]; // @[package.scala 109:13:boom.system.TestHarness.SmallBoomConfig.fir@124869.4]
  assign _T_648 = 3'h3 << _T_646; // @[frontend.scala 96:31:boom.system.TestHarness.SmallBoomConfig.fir@124870.4]
  assign _T_650 = icache_io_resp_valid == 1'h0; // @[frontend.scala 238:99:boom.system.TestHarness.SmallBoomConfig.fir@124872.4]
  assign _T_651 = icache_io_s2_kill & _T_650; // @[frontend.scala 238:96:boom.system.TestHarness.SmallBoomConfig.fir@124873.4]
  assign _T_653 = s2_xcpt == 1'h0; // @[frontend.scala 238:124:boom.system.TestHarness.SmallBoomConfig.fir@124874.4]
  assign _T_656 = icache_io_resp_valid & icache_io_resp_bits_ae; // @[frontend.scala 242:30:boom.system.TestHarness.SmallBoomConfig.fir@124881.4]
  assign auto_icache_master_out_a_valid = icache_auto_master_out_a_valid;
  assign auto_icache_master_out_a_bits_address = icache_auto_master_out_a_bits_address;
  assign io_cpu_fetchpacket_valid = fetch_controller_io_fetchpacket_valid;
  assign io_cpu_fetchpacket_bits_uops_0_valid = fetch_controller_io_fetchpacket_bits_uops_0_valid;
  assign io_cpu_fetchpacket_bits_uops_0_inst = fetch_controller_io_fetchpacket_bits_uops_0_inst;
  assign io_cpu_fetchpacket_bits_uops_0_pc = fetch_controller_io_fetchpacket_bits_uops_0_pc;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_btb_blame = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_blame;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_btb_hit = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_hit;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_btb_taken = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_btb_taken;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_blame = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_blame;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_hit = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_hit;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_taken = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_taken;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bim_resp_rowdata = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bim_resp_rowdata;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bim_resp_entry_idx = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bim_resp_entry_idx;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_resp_takens = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_takens;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_resp_history = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_history;
  assign io_cpu_fetchpacket_bits_uops_0_br_prediction_bpd_resp_info = fetch_controller_io_fetchpacket_bits_uops_0_br_prediction_bpd_resp_info;
  assign io_cpu_fetchpacket_bits_uops_0_stat_brjmp_mispredicted = fetch_controller_io_fetchpacket_bits_uops_0_stat_brjmp_mispredicted;
  assign io_cpu_fetchpacket_bits_uops_0_stat_btb_made_pred = fetch_controller_io_fetchpacket_bits_uops_0_stat_btb_made_pred;
  assign io_cpu_fetchpacket_bits_uops_0_stat_btb_mispredicted = fetch_controller_io_fetchpacket_bits_uops_0_stat_btb_mispredicted;
  assign io_cpu_fetchpacket_bits_uops_0_stat_bpd_made_pred = fetch_controller_io_fetchpacket_bits_uops_0_stat_bpd_made_pred;
  assign io_cpu_fetchpacket_bits_uops_0_stat_bpd_mispredicted = fetch_controller_io_fetchpacket_bits_uops_0_stat_bpd_mispredicted;
  assign io_cpu_fetchpacket_bits_uops_0_ftq_idx = fetch_controller_io_fetchpacket_bits_uops_0_ftq_idx;
  assign io_cpu_fetchpacket_bits_uops_0_pc_lob = fetch_controller_io_fetchpacket_bits_uops_0_pc_lob;
  assign io_cpu_fetchpacket_bits_uops_0_csr_addr = fetch_controller_io_fetchpacket_bits_uops_0_csr_addr;
  assign io_cpu_fetchpacket_bits_uops_0_pdst = fetch_controller_io_fetchpacket_bits_uops_0_pdst;
  assign io_cpu_fetchpacket_bits_uops_0_xcpt_pf_if = fetch_controller_io_fetchpacket_bits_uops_0_xcpt_pf_if;
  assign io_cpu_fetchpacket_bits_uops_0_xcpt_ae_if = fetch_controller_io_fetchpacket_bits_uops_0_xcpt_ae_if;
  assign io_cpu_fetchpacket_bits_uops_0_replay_if = fetch_controller_io_fetchpacket_bits_uops_0_replay_if;
  assign io_cpu_fetchpacket_bits_uops_0_xcpt_ma_if = fetch_controller_io_fetchpacket_bits_uops_0_xcpt_ma_if;
  assign io_cpu_fetchpacket_bits_uops_0_debug_wdata = fetch_controller_io_fetchpacket_bits_uops_0_debug_wdata;
  assign io_cpu_fetchpacket_bits_uops_0_debug_events_fetch_seq = fetch_controller_io_fetchpacket_bits_uops_0_debug_events_fetch_seq;
  assign io_cpu_get_pc_fetch_pc = fetch_controller_io_get_pc_fetch_pc;
  assign io_cpu_get_pc_next_val = fetch_controller_io_get_pc_next_val;
  assign io_cpu_get_pc_next_pc = fetch_controller_io_get_pc_next_pc;
  assign io_cpu_com_fetch_pc = fetch_controller_io_com_fetch_pc;
  assign io_cpu_perf_acquire = icache_io_perf_acquire;
  assign io_cpu_perf_tlbMiss = io_ptw_req_ready & io_ptw_req_valid;
  assign io_ptw_req_valid = tlb_io_ptw_req_valid;
  assign io_ptw_req_bits_addr = tlb_io_ptw_req_bits_addr;
  assign icache_clock = clock;
  assign icache_reset = reset;
  assign icache_auto_master_out_a_ready = auto_icache_master_out_a_ready;
  assign icache_auto_master_out_d_valid = auto_icache_master_out_d_valid;
  assign icache_auto_master_out_d_bits_opcode = auto_icache_master_out_d_bits_opcode;
  assign icache_auto_master_out_d_bits_size = auto_icache_master_out_d_bits_size;
  assign icache_auto_master_out_d_bits_data = auto_icache_master_out_d_bits_data;
  assign icache_auto_master_out_d_bits_error = auto_icache_master_out_d_bits_error;
  assign icache_io_req_valid = fetch_controller_io_imem_req_valid | fetch_controller_io_imem_resp_ready;
  assign icache_io_req_bits_addr = s0_pc[38:0];
  assign icache_io_s1_vaddr = s1_pc[38:0];
  assign icache_io_s1_paddr = tlb_io_resp_paddr;
  assign icache_io_s1_kill = _T_627 | s2_replay;
  assign icache_io_s2_kill = _T_631 | s2_xcpt;
  assign icache_io_invalidate = io_cpu_flush_icache;
  assign tlb_clock = clock;
  assign tlb_reset = reset;
  assign tlb_io_req_valid = s2_replay == 1'h0;
  assign tlb_io_req_bits_vaddr = s1_pc;
  assign tlb_io_req_bits_sfence_valid = io_cpu_sfence_valid;
  assign tlb_io_req_bits_sfence_bits_rs1 = io_cpu_sfence_bits_rs1;
  assign tlb_io_req_bits_sfence_bits_rs2 = io_cpu_sfence_bits_rs2;
  assign tlb_io_req_bits_sfence_bits_addr = io_cpu_sfence_bits_addr;
  assign tlb_io_ptw_req_ready = io_ptw_req_ready;
  assign tlb_io_ptw_resp_valid = io_ptw_resp_valid;
  assign tlb_io_ptw_resp_bits_ae = io_ptw_resp_bits_ae;
  assign tlb_io_ptw_resp_bits_pte_ppn = io_ptw_resp_bits_pte_ppn;
  assign tlb_io_ptw_resp_bits_pte_d = io_ptw_resp_bits_pte_d;
  assign tlb_io_ptw_resp_bits_pte_a = io_ptw_resp_bits_pte_a;
  assign tlb_io_ptw_resp_bits_pte_g = io_ptw_resp_bits_pte_g;
  assign tlb_io_ptw_resp_bits_pte_u = io_ptw_resp_bits_pte_u;
  assign tlb_io_ptw_resp_bits_pte_x = io_ptw_resp_bits_pte_x;
  assign tlb_io_ptw_resp_bits_pte_w = io_ptw_resp_bits_pte_w;
  assign tlb_io_ptw_resp_bits_pte_r = io_ptw_resp_bits_pte_r;
  assign tlb_io_ptw_resp_bits_pte_v = io_ptw_resp_bits_pte_v;
  assign tlb_io_ptw_resp_bits_level = io_ptw_resp_bits_level;
  assign tlb_io_ptw_resp_bits_homogeneous = io_ptw_resp_bits_homogeneous;
  assign tlb_io_ptw_ptbr_mode = io_ptw_ptbr_mode;
  assign tlb_io_ptw_status_prv = io_ptw_status_prv;
  assign fetch_controller_clock = clock;
  assign fetch_controller_reset = reset;
  assign fetch_controller_io_imem_resp_valid = _T_640 & _T_644;
  assign fetch_controller_io_imem_resp_bits_pc = s2_pc;
  assign fetch_controller_io_imem_resp_bits_data = icache_io_resp_bits_data;
  assign fetch_controller_io_imem_resp_bits_mask = _T_648[1:0];
  assign fetch_controller_io_imem_resp_bits_xcpt_pf_inst = s2_tlb_resp_pf_inst;
  assign fetch_controller_io_imem_resp_bits_xcpt_ae_inst = _T_656 ? 1'h1 : s2_tlb_resp_ae_inst;
  assign fetch_controller_io_imem_resp_bits_replay = _T_651 & _T_653;
  assign fetch_controller_io_f2_btb_resp_valid = bpdpipeline_io_f2_btb_resp_valid;
  assign fetch_controller_io_f2_btb_resp_bits_taken = bpdpipeline_io_f2_btb_resp_bits_taken;
  assign fetch_controller_io_f2_btb_resp_bits_target = bpdpipeline_io_f2_btb_resp_bits_target;
  assign fetch_controller_io_f2_btb_resp_bits_mask = bpdpipeline_io_f2_btb_resp_bits_mask;
  assign fetch_controller_io_f2_btb_resp_bits_cfi_idx = bpdpipeline_io_f2_btb_resp_bits_cfi_idx;
  assign fetch_controller_io_f2_btb_resp_bits_bpd_type = bpdpipeline_io_f2_btb_resp_bits_bpd_type;
  assign fetch_controller_io_f2_btb_resp_bits_cfi_type = bpdpipeline_io_f2_btb_resp_bits_cfi_type;
  assign fetch_controller_io_f2_btb_resp_bits_bim_resp_valid = bpdpipeline_io_f2_btb_resp_bits_bim_resp_valid;
  assign fetch_controller_io_f2_btb_resp_bits_bim_resp_bits_rowdata = bpdpipeline_io_f2_btb_resp_bits_bim_resp_bits_rowdata;
  assign fetch_controller_io_f2_btb_resp_bits_bim_resp_bits_entry_idx = bpdpipeline_io_f2_btb_resp_bits_bim_resp_bits_entry_idx;
  assign fetch_controller_io_f3_bpd_resp_valid = bpdpipeline_io_f3_bpd_resp_valid;
  assign fetch_controller_io_f3_bpd_resp_bits_takens = bpdpipeline_io_f3_bpd_resp_bits_takens;
  assign fetch_controller_io_f3_bpd_resp_bits_history = bpdpipeline_io_f3_bpd_resp_bits_history;
  assign fetch_controller_io_br_unit_take_pc = io_cpu_br_unit_take_pc;
  assign fetch_controller_io_br_unit_target = io_cpu_br_unit_target;
  assign fetch_controller_io_br_unit_brinfo_valid = io_cpu_br_unit_brinfo_valid;
  assign fetch_controller_io_br_unit_brinfo_mispredict = io_cpu_br_unit_brinfo_mispredict;
  assign fetch_controller_io_br_unit_brinfo_pc_lob = io_cpu_br_unit_brinfo_pc_lob;
  assign fetch_controller_io_br_unit_brinfo_ftq_idx = io_cpu_br_unit_brinfo_ftq_idx;
  assign fetch_controller_io_br_unit_brinfo_taken = io_cpu_br_unit_brinfo_taken;
  assign fetch_controller_io_br_unit_brinfo_cfi_type = io_cpu_br_unit_brinfo_cfi_type;
  assign fetch_controller_io_get_pc_ftq_idx = io_cpu_get_pc_ftq_idx;
  assign fetch_controller_io_clear_fetchbuffer = io_cpu_clear_fetchbuffer;
  assign fetch_controller_io_commit_valid = io_cpu_commit_valid;
  assign fetch_controller_io_commit_bits = io_cpu_commit_bits;
  assign fetch_controller_io_flush_info_valid = io_cpu_flush_info_valid;
  assign fetch_controller_io_flush_info_bits_ftq_idx = io_cpu_flush_info_bits_ftq_idx;
  assign fetch_controller_io_flush_info_bits_pc_lob = io_cpu_flush_info_bits_pc_lob;
  assign fetch_controller_io_flush_info_bits_flush_typ = io_cpu_flush_info_bits_flush_typ;
  assign fetch_controller_io_flush_take_pc = io_cpu_flush_take_pc;
  assign fetch_controller_io_flush_pc = io_cpu_flush_pc;
  assign fetch_controller_io_com_ftq_idx = io_cpu_com_ftq_idx;
  assign fetch_controller_io_sfence_take_pc = io_cpu_sfence_take_pc;
  assign fetch_controller_io_sfence_addr = io_cpu_sfence_addr;
  assign fetch_controller_io_fetchpacket_ready = io_cpu_fetchpacket_ready;
  assign bpdpipeline_clock = clock;
  assign bpdpipeline_reset = reset;
  assign bpdpipeline_io_s0_req_valid = fetch_controller_io_imem_req_valid | fetch_controller_io_imem_resp_ready;
  assign bpdpipeline_io_s0_req_bits_addr = s0_pc[38:0];
  assign bpdpipeline_io_debug_imemresp_pc = fetch_controller_io_imem_resp_bits_pc;
  assign bpdpipeline_io_f2_valid = fetch_controller_io_imem_resp_valid;
  assign bpdpipeline_io_f2_stall = fetch_controller_io_imem_resp_ready == 1'h0;
  assign bpdpipeline_io_f2_replay = _T_607 | _T_613;
  assign bpdpipeline_io_f2_redirect = fetch_controller_io_f2_redirect;
  assign bpdpipeline_io_f3_btb_update_valid = fetch_controller_io_f3_btb_update_valid;
  assign bpdpipeline_io_f3_btb_update_bits_pc = fetch_controller_io_f3_btb_update_bits_pc;
  assign bpdpipeline_io_f3_btb_update_bits_target = fetch_controller_io_f3_btb_update_bits_target;
  assign bpdpipeline_io_f3_btb_update_bits_cfi_pc = fetch_controller_io_f3_btb_update_bits_cfi_pc;
  assign bpdpipeline_io_f3_btb_update_bits_bpd_type = fetch_controller_io_f3_btb_update_bits_bpd_type;
  assign bpdpipeline_io_f3_btb_update_bits_cfi_type = fetch_controller_io_f3_btb_update_bits_cfi_type;
  assign bpdpipeline_io_f3_stall = fetch_controller_io_f3_stall;
  assign bpdpipeline_io_f4_redirect = fetch_controller_io_f4_redirect;
  assign bpdpipeline_io_f4_taken = fetch_controller_io_f4_taken;
  assign bpdpipeline_io_bim_update_valid = fetch_controller_io_bim_update_valid;
  assign bpdpipeline_io_bim_update_bits_entry_idx = fetch_controller_io_bim_update_bits_entry_idx;
  assign bpdpipeline_io_bim_update_bits_cfi_idx = fetch_controller_io_bim_update_bits_cfi_idx;
  assign bpdpipeline_io_bim_update_bits_cntr_value = fetch_controller_io_bim_update_bits_cntr_value;
  assign bpdpipeline_io_bim_update_bits_mispredicted = fetch_controller_io_bim_update_bits_mispredicted;
  assign bpdpipeline_io_bim_update_bits_taken = fetch_controller_io_bim_update_bits_taken;
  assign bpdpipeline_io_br_unit_btb_update_valid = io_cpu_br_unit_btb_update_valid;
  assign bpdpipeline_io_br_unit_btb_update_bits_pc = io_cpu_br_unit_btb_update_bits_pc;
  assign bpdpipeline_io_br_unit_btb_update_bits_target = io_cpu_br_unit_btb_update_bits_target;
  assign bpdpipeline_io_br_unit_btb_update_bits_cfi_pc = io_cpu_br_unit_btb_update_bits_cfi_pc;
  assign bpdpipeline_io_br_unit_btb_update_bits_bpd_type = io_cpu_br_unit_btb_update_bits_bpd_type;
  assign bpdpipeline_io_br_unit_btb_update_bits_cfi_type = io_cpu_br_unit_btb_update_bits_cfi_type;
  assign bpdpipeline_io_fe_clear = fetch_controller_io_clear_fetchbuffer;
  assign bpdpipeline_io_ftq_restore_valid = fetch_controller_io_ftq_restore_history_valid;
  assign bpdpipeline_io_ftq_restore_bits_history = fetch_controller_io_ftq_restore_history_bits_history;
  assign bpdpipeline_io_ftq_restore_bits_taken = fetch_controller_io_ftq_restore_history_bits_taken;
  assign bpdpipeline_io_flush = io_cpu_flush;
  assign bpdpipeline_io_redirect = fetch_controller_io_imem_req_valid;
  assign bpdpipeline_io_status_debug = io_cpu_status_debug;
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE
  integer initvar;
  initial begin
    `ifndef verilator
      #0.002 begin end
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{$random}};
  s1_valid = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {2{$random}};
  s1_pc = _RAND_1[39:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{$random}};
  s1_speculative = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{$random}};
  s2_valid = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {2{$random}};
  s2_pc = _RAND_4[39:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{$random}};
  s2_tlb_resp_miss = _RAND_5[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{$random}};
  s2_tlb_resp_pf_inst = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{$random}};
  s2_tlb_resp_ae_inst = _RAND_7[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{$random}};
  s2_tlb_resp_cacheable = _RAND_8[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{$random}};
  s2_speculative = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{$random}};
  _T_613 = _RAND_10[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {1{$random}};
  _T_639 = _RAND_11[0:0];
  `endif // RANDOMIZE_REG_INIT
  end
`endif // RANDOMIZE
  always @(posedge clock) begin
    s1_valid <= fetch_controller_io_imem_req_valid | fetch_controller_io_imem_resp_ready;
    s1_pc <= ~ _T_636;
    if (fetch_controller_io_imem_req_valid) begin
      s1_speculative <= fetch_controller_io_imem_req_bits_speculative;
    end else begin
      if (s2_replay) begin
        s1_speculative <= s2_speculative;
      end else begin
        s1_speculative <= 1'h1;
      end
    end
    if (reset) begin
      s2_valid <= 1'h0;
    end else begin
      if (_T_620) begin
        s2_valid <= _T_622;
      end else begin
        s2_valid <= 1'h0;
      end
    end
    if (reset) begin
      s2_pc <= {{8'd0}, _T_587};
    end else begin
      if (_T_620) begin
        s2_pc <= s1_pc;
      end
    end
    if (_T_620) begin
      s2_tlb_resp_miss <= tlb_io_resp_miss;
    end
    if (_T_620) begin
      s2_tlb_resp_pf_inst <= tlb_io_resp_pf_inst;
    end
    if (_T_620) begin
      s2_tlb_resp_ae_inst <= tlb_io_resp_ae_inst;
    end
    if (_T_620) begin
      s2_tlb_resp_cacheable <= tlb_io_resp_cacheable;
    end
    if (reset) begin
      s2_speculative <= 1'h0;
    end else begin
      if (_T_620) begin
        s2_speculative <= s1_speculative;
      end
    end
    if (reset) begin
      _T_613 <= 1'h1;
    end else begin
      _T_613 <= _T_610;
    end
    _T_639 <= s1_valid;
  end
endmodule