module m2 (
	input IN40, CLK, IN22, IN21, 
	output OUT30, OUT20, OUT40, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	MUX2X1 U2 ( .B(OUT10), .Y(OUT20), .A(IN21), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	XOR2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
endmodule
module m1 (
	input IN40, CLK, IN22, IN21, 
	output OUT30, OUT20, OUT40, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	MUX2X1 U2 ( .B(OUT10), .Y(OUT20), .A(IN21), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
	XNOR2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
endmodule
module m8 (
	input CLK, IN22, IN40, IN30, IN21, 
	output OUT30, OUT20, OUT40, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	MUX2X1 U2 ( .B(OUT10), .Y(OUT20), .A(IN21), .S(IN22) );
	DFFPOSX1 U3 ( .Q(OUT30), .D(IN30), .CLK(CLK) );
	XOR2X1 U4 ( .A(OUT30), .Y(OUT40), .B(IN40) );
endmodule
module m3 (
	input CLK, IN22, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(OUT30) );
	MUX2X1 U2 ( .B(OUT10), .Y(OUT20), .A(IN21), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m6 (
	input CLK, IN10, IN22, IN21, 
	output OUT30, OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	MUX2X1 U2 ( .B(OUT10), .Y(OUT20), .A(IN21), .S(IN22) );
	DFFPOSX1 U3 ( .D(OUT20), .Q(OUT30), .CLK(CLK) );
endmodule
module m23 (
	input IN11, IN10, IN10_1, IN11_1, 
	output OUT20, OUT10
);
	AOI22X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1), .C(IN11), .D(IN11_1) );
	BUFX2 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m0 (
	input IN10, 
	output OUT20, OUT10
);
	INVX1 U1 ( .Y(OUT10), .A(IN10) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m22 (
	input IN10, IN10_1, 
	output OUT20, OUT10
);
	AND2X1 U1 ( .Y(OUT10), .A(IN10), .B(IN10_1) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
module m16 (
	input CLK, IN10, 
	output OUT20, OUT10
);
	DFFPOSX1 U1 ( .Q(OUT10), .D(IN10), .CLK(CLK) );
	INVX1 U2 ( .A(OUT10), .Y(OUT20) );
endmodule
