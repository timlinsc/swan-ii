module Frontend_frontend(
  input   clock,
  input   reset,
  input   io_mem_0_a_ready,
  output  io_mem_0_a_valid,
  output [2:0] io_mem_0_a_bits_opcode,
  output [2:0] io_mem_0_a_bits_param,
  output [3:0] io_mem_0_a_bits_size,
  output  io_mem_0_a_bits_source,
  output [31:0] io_mem_0_a_bits_address,
  output [7:0] io_mem_0_a_bits_mask,
  output [63:0] io_mem_0_a_bits_data,
  output  io_mem_0_b_ready,
  input   io_mem_0_b_valid,
  input  [2:0] io_mem_0_b_bits_opcode,
  input  [1:0] io_mem_0_b_bits_param,
  input  [3:0] io_mem_0_b_bits_size,
  input   io_mem_0_b_bits_source,
  input  [31:0] io_mem_0_b_bits_address,
  input  [7:0] io_mem_0_b_bits_mask,
  input  [63:0] io_mem_0_b_bits_data,
  input   io_mem_0_c_ready,
  output  io_mem_0_c_valid,
  output [2:0] io_mem_0_c_bits_opcode,
  output [2:0] io_mem_0_c_bits_param,
  output [3:0] io_mem_0_c_bits_size,
  output  io_mem_0_c_bits_source,
  output [31:0] io_mem_0_c_bits_address,
  output [63:0] io_mem_0_c_bits_data,
  output  io_mem_0_c_bits_error,
  output  io_mem_0_d_ready,
  input   io_mem_0_d_valid,
  input  [2:0] io_mem_0_d_bits_opcode,
  input  [1:0] io_mem_0_d_bits_param,
  input  [3:0] io_mem_0_d_bits_size,
  input   io_mem_0_d_bits_source,
  input  [3:0] io_mem_0_d_bits_sink,
  input  [2:0] io_mem_0_d_bits_addr_lo,
  input  [63:0] io_mem_0_d_bits_data,
  input   io_mem_0_d_bits_error,
  input   io_mem_0_e_ready,
  output  io_mem_0_e_valid,
  output [3:0] io_mem_0_e_bits_sink,
  input   io_cpu_req_valid,
  input  [39:0] io_cpu_req_bits_pc,
  input   io_cpu_req_bits_speculative,
  input   io_cpu_resp_ready,
  output  io_cpu_resp_valid,
  output  io_cpu_resp_bits_btb_valid,
  output  io_cpu_resp_bits_btb_bits_taken,
  output  io_cpu_resp_bits_btb_bits_mask,
  output  io_cpu_resp_bits_btb_bits_bridx,
  output [38:0] io_cpu_resp_bits_btb_bits_target,
  output [5:0] io_cpu_resp_bits_btb_bits_entry,
  output [6:0] io_cpu_resp_bits_btb_bits_bht_history,
  output [1:0] io_cpu_resp_bits_btb_bits_bht_value,
  output [39:0] io_cpu_resp_bits_pc,
  output [31:0] io_cpu_resp_bits_data,
  output  io_cpu_resp_bits_mask,
  output  io_cpu_resp_bits_xcpt_if,
  output  io_cpu_resp_bits_replay,
  input   io_cpu_btb_update_valid,
  input   io_cpu_btb_update_bits_prediction_valid,
  input   io_cpu_btb_update_bits_prediction_bits_taken,
  input   io_cpu_btb_update_bits_prediction_bits_mask,
  input   io_cpu_btb_update_bits_prediction_bits_bridx,
  input  [38:0] io_cpu_btb_update_bits_prediction_bits_target,
  input  [5:0] io_cpu_btb_update_bits_prediction_bits_entry,
  input  [6:0] io_cpu_btb_update_bits_prediction_bits_bht_history,
  input  [1:0] io_cpu_btb_update_bits_prediction_bits_bht_value,
  input  [38:0] io_cpu_btb_update_bits_pc,
  input  [38:0] io_cpu_btb_update_bits_target,
  input   io_cpu_btb_update_bits_taken,
  input   io_cpu_btb_update_bits_isValid,
  input   io_cpu_btb_update_bits_isJump,
  input   io_cpu_btb_update_bits_isReturn,
  input  [38:0] io_cpu_btb_update_bits_br_pc,
  input   io_cpu_bht_update_valid,
  input   io_cpu_bht_update_bits_prediction_valid,
  input   io_cpu_bht_update_bits_prediction_bits_taken,
  input   io_cpu_bht_update_bits_prediction_bits_mask,
  input   io_cpu_bht_update_bits_prediction_bits_bridx,
  input  [38:0] io_cpu_bht_update_bits_prediction_bits_target,
  input  [5:0] io_cpu_bht_update_bits_prediction_bits_entry,
  input  [6:0] io_cpu_bht_update_bits_prediction_bits_bht_history,
  input  [1:0] io_cpu_bht_update_bits_prediction_bits_bht_value,
  input  [38:0] io_cpu_bht_update_bits_pc,
  input   io_cpu_bht_update_bits_taken,
  input   io_cpu_bht_update_bits_mispredict,
  input   io_cpu_ras_update_valid,
  input   io_cpu_ras_update_bits_isCall,
  input   io_cpu_ras_update_bits_isReturn,
  input  [38:0] io_cpu_ras_update_bits_returnAddr,
  input   io_cpu_ras_update_bits_prediction_valid,
  input   io_cpu_ras_update_bits_prediction_bits_taken,
  input   io_cpu_ras_update_bits_prediction_bits_mask,
  input   io_cpu_ras_update_bits_prediction_bits_bridx,
  input  [38:0] io_cpu_ras_update_bits_prediction_bits_target,
  input  [5:0] io_cpu_ras_update_bits_prediction_bits_entry,
  input  [6:0] io_cpu_ras_update_bits_prediction_bits_bht_history,
  input  [1:0] io_cpu_ras_update_bits_prediction_bits_bht_value,
  input   io_cpu_flush_icache,
  input   io_cpu_flush_tlb,
  output [39:0] io_cpu_npc,
  input   io_ptw_req_ready,
  output  io_ptw_req_valid,
  output [1:0] io_ptw_req_bits_prv,
  output  io_ptw_req_bits_pum,
  output  io_ptw_req_bits_mxr,
  output [26:0] io_ptw_req_bits_addr,
  output  io_ptw_req_bits_store,
  output  io_ptw_req_bits_fetch,
  input   io_ptw_resp_valid,
  input  [15:0] io_ptw_resp_bits_pte_reserved_for_hardware,
  input  [37:0] io_ptw_resp_bits_pte_ppn,
  input  [1:0] io_ptw_resp_bits_pte_reserved_for_software,
  input   io_ptw_resp_bits_pte_d,
  input   io_ptw_resp_bits_pte_a,
  input   io_ptw_resp_bits_pte_g,
  input   io_ptw_resp_bits_pte_u,
  input   io_ptw_resp_bits_pte_x,
  input   io_ptw_resp_bits_pte_w,
  input   io_ptw_resp_bits_pte_r,
  input   io_ptw_resp_bits_pte_v,
  input  [6:0] io_ptw_ptbr_asid,
  input  [37:0] io_ptw_ptbr_ppn,
  input   io_ptw_invalidate,
  input   io_ptw_status_debug,
  input  [31:0] io_ptw_status_isa,
  input  [1:0] io_ptw_status_prv,
  input   io_ptw_status_sd,
  input  [30:0] io_ptw_status_zero3,
  input   io_ptw_status_sd_rv32,
  input  [1:0] io_ptw_status_zero2,
  input  [4:0] io_ptw_status_vm,
  input  [3:0] io_ptw_status_zero1,
  input   io_ptw_status_mxr,
  input   io_ptw_status_pum,
  input   io_ptw_status_mprv,
  input  [1:0] io_ptw_status_xs,
  input  [1:0] io_ptw_status_fs,
  input  [1:0] io_ptw_status_mpp,
  input  [1:0] io_ptw_status_hpp,
  input   io_ptw_status_spp,
  input   io_ptw_status_mpie,
  input   io_ptw_status_hpie,
  input   io_ptw_status_spie,
  input   io_ptw_status_upie,
  input   io_ptw_status_mie,
  input   io_ptw_status_hie,
  input   io_ptw_status_sie,
  input   io_ptw_status_uie,
  input  [39:0] io_resetVector,

  output  icache_clock,
  output  icache_reset,
  output  icache_io_req_valid,
  output [38:0] icache_io_req_bits_addr,
  output [19:0] icache_io_s1_ppn,
  output  icache_io_s1_kill,
  output  icache_io_s2_kill,
  output  icache_io_resp_ready,
  input  icache_io_resp_valid,
  input [31:0] icache_io_resp_bits_data,
  input [63:0] icache_io_resp_bits_datablock,
  output  icache_io_invalidate,
  output  icache_io_mem_0_a_ready,
  input  icache_io_mem_0_a_valid,
  input [2:0] icache_io_mem_0_a_bits_opcode,
  input [2:0] icache_io_mem_0_a_bits_param,
  input [3:0] icache_io_mem_0_a_bits_size,
  input  icache_io_mem_0_a_bits_source,
  input [31:0] icache_io_mem_0_a_bits_address,
  input [7:0] icache_io_mem_0_a_bits_mask,
  input [63:0] icache_io_mem_0_a_bits_data,
  input  icache_io_mem_0_b_ready,
  output  icache_io_mem_0_b_valid,
  output [2:0] icache_io_mem_0_b_bits_opcode,
  output [1:0] icache_io_mem_0_b_bits_param,
  output [3:0] icache_io_mem_0_b_bits_size,
  output  icache_io_mem_0_b_bits_source,
  output [31:0] icache_io_mem_0_b_bits_address,
  output [7:0] icache_io_mem_0_b_bits_mask,
  output [63:0] icache_io_mem_0_b_bits_data,
  output  icache_io_mem_0_c_ready,
  input  icache_io_mem_0_c_valid,
  input [2:0] icache_io_mem_0_c_bits_opcode,
  input [2:0] icache_io_mem_0_c_bits_param,
  input [3:0] icache_io_mem_0_c_bits_size,
  input  icache_io_mem_0_c_bits_source,
  input [31:0] icache_io_mem_0_c_bits_address,
  input [63:0] icache_io_mem_0_c_bits_data,
  input  icache_io_mem_0_c_bits_error,
  input  icache_io_mem_0_d_ready,
  output  icache_io_mem_0_d_valid,
  output [2:0] icache_io_mem_0_d_bits_opcode,
  output [1:0] icache_io_mem_0_d_bits_param,
  output [3:0] icache_io_mem_0_d_bits_size,
  output  icache_io_mem_0_d_bits_source,
  output [3:0] icache_io_mem_0_d_bits_sink,
  output [2:0] icache_io_mem_0_d_bits_addr_lo,
  output [63:0] icache_io_mem_0_d_bits_data,
  output  icache_io_mem_0_d_bits_error,
  output  icache_io_mem_0_e_ready,
  input  icache_io_mem_0_e_valid,
  input [3:0] icache_io_mem_0_e_bits_sink,
  output  tlb_clock,
  output  tlb_reset,
  input  tlb_io_req_ready,
  output  tlb_io_req_valid,
  output [27:0] tlb_io_req_bits_vpn,
  output  tlb_io_req_bits_passthrough,
  output  tlb_io_req_bits_instruction,
  output  tlb_io_req_bits_store,
  input  tlb_io_resp_miss,
  input [19:0] tlb_io_resp_ppn,
  input  tlb_io_resp_xcpt_ld,
  input  tlb_io_resp_xcpt_st,
  input  tlb_io_resp_xcpt_if,
  input  tlb_io_resp_cacheable,
  output  tlb_io_ptw_req_ready,
  input  tlb_io_ptw_req_valid,
  input [1:0] tlb_io_ptw_req_bits_prv,
  input  tlb_io_ptw_req_bits_pum,
  input  tlb_io_ptw_req_bits_mxr,
  input [26:0] tlb_io_ptw_req_bits_addr,
  input  tlb_io_ptw_req_bits_store,
  input  tlb_io_ptw_req_bits_fetch,
  output  tlb_io_ptw_resp_valid,
  output [15:0] tlb_io_ptw_resp_bits_pte_reserved_for_hardware,
  output [37:0] tlb_io_ptw_resp_bits_pte_ppn,
  output [1:0] tlb_io_ptw_resp_bits_pte_reserved_for_software,
  output  tlb_io_ptw_resp_bits_pte_d,
  output  tlb_io_ptw_resp_bits_pte_a,
  output  tlb_io_ptw_resp_bits_pte_g,
  output  tlb_io_ptw_resp_bits_pte_u,
  output  tlb_io_ptw_resp_bits_pte_x,
  output  tlb_io_ptw_resp_bits_pte_w,
  output  tlb_io_ptw_resp_bits_pte_r,
  output  tlb_io_ptw_resp_bits_pte_v,
  output [6:0] tlb_io_ptw_ptbr_asid,
  output [37:0] tlb_io_ptw_ptbr_ppn,
  output  tlb_io_ptw_invalidate,
  output  tlb_io_ptw_status_debug,
  output [31:0] tlb_io_ptw_status_isa,
  output [1:0] tlb_io_ptw_status_prv,
  output  tlb_io_ptw_status_sd,
  output [30:0] tlb_io_ptw_status_zero3,
  output  tlb_io_ptw_status_sd_rv32,
  output [1:0] tlb_io_ptw_status_zero2,
  output [4:0] tlb_io_ptw_status_vm,
  output [3:0] tlb_io_ptw_status_zero1,
  output  tlb_io_ptw_status_mxr,
  output  tlb_io_ptw_status_pum,
  output  tlb_io_ptw_status_mprv,
  output [1:0] tlb_io_ptw_status_xs,
  output [1:0] tlb_io_ptw_status_fs,
  output [1:0] tlb_io_ptw_status_mpp,
  output [1:0] tlb_io_ptw_status_hpp,
  output  tlb_io_ptw_status_spp,
  output  tlb_io_ptw_status_mpie,
  output  tlb_io_ptw_status_hpie,
  output  tlb_io_ptw_status_spie,
  output  tlb_io_ptw_status_upie,
  output  tlb_io_ptw_status_mie,
  output  tlb_io_ptw_status_hie,
  output  tlb_io_ptw_status_sie,
  output  tlb_io_ptw_status_uie,
  output  BTB_clock,
  output  BTB_reset,
  output  BTB_io_req_valid,
  output [38:0] BTB_io_req_bits_addr,
  input  BTB_io_resp_valid,
  input  BTB_io_resp_bits_taken,
  input  BTB_io_resp_bits_mask,
  input  BTB_io_resp_bits_bridx,
  input [38:0] BTB_io_resp_bits_target,
  input [5:0] BTB_io_resp_bits_entry,
  input [6:0] BTB_io_resp_bits_bht_history,
  input [1:0] BTB_io_resp_bits_bht_value,
  output  BTB_io_btb_update_valid,
  output  BTB_io_btb_update_bits_prediction_valid,
  output  BTB_io_btb_update_bits_prediction_bits_taken,
  output  BTB_io_btb_update_bits_prediction_bits_mask,
  output  BTB_io_btb_update_bits_prediction_bits_bridx,
  output [38:0] BTB_io_btb_update_bits_prediction_bits_target,
  output [5:0] BTB_io_btb_update_bits_prediction_bits_entry,
  output [6:0] BTB_io_btb_update_bits_prediction_bits_bht_history,
  output [1:0] BTB_io_btb_update_bits_prediction_bits_bht_value,
  output [38:0] BTB_io_btb_update_bits_pc,
  output [38:0] BTB_io_btb_update_bits_target,
  output  BTB_io_btb_update_bits_taken,
  output  BTB_io_btb_update_bits_isValid,
  output  BTB_io_btb_update_bits_isJump,
  output  BTB_io_btb_update_bits_isReturn,
  output [38:0] BTB_io_btb_update_bits_br_pc,
  output  BTB_io_bht_update_valid,
  output  BTB_io_bht_update_bits_prediction_valid,
  output  BTB_io_bht_update_bits_prediction_bits_taken,
  output  BTB_io_bht_update_bits_prediction_bits_mask,
  output  BTB_io_bht_update_bits_prediction_bits_bridx,
  output [38:0] BTB_io_bht_update_bits_prediction_bits_target,
  output [5:0] BTB_io_bht_update_bits_prediction_bits_entry,
  output [6:0] BTB_io_bht_update_bits_prediction_bits_bht_history,
  output [1:0] BTB_io_bht_update_bits_prediction_bits_bht_value,
  output [38:0] BTB_io_bht_update_bits_pc,
  output  BTB_io_bht_update_bits_taken,
  output  BTB_io_bht_update_bits_mispredict,
  output  BTB_io_ras_update_valid,
  output  BTB_io_ras_update_bits_isCall,
  output  BTB_io_ras_update_bits_isReturn,
  output [38:0] BTB_io_ras_update_bits_returnAddr,
  output  BTB_io_ras_update_bits_prediction_valid,
  output  BTB_io_ras_update_bits_prediction_bits_taken,
  output  BTB_io_ras_update_bits_prediction_bits_mask,
  output  BTB_io_ras_update_bits_prediction_bits_bridx,
  output [38:0] BTB_io_ras_update_bits_prediction_bits_target,
  output [5:0] BTB_io_ras_update_bits_prediction_bits_entry,
  output [6:0] BTB_io_ras_update_bits_prediction_bits_bht_history,
  output [1:0] BTB_io_ras_update_bits_prediction_bits_bht_value,
  canary input canary_in,
  output alarm
);
  assign alarm = canary_in;
  wire  icache_clock;
  wire  icache_reset;
  wire  icache_io_req_valid;
  wire [38:0] icache_io_req_bits_addr;
  wire [19:0] icache_io_s1_ppn;
  wire  icache_io_s1_kill;
  wire  icache_io_s2_kill;
  wire  icache_io_resp_ready;
  wire  icache_io_resp_valid;
  wire [31:0] icache_io_resp_bits_data;
  wire [63:0] icache_io_resp_bits_datablock;
  wire  icache_io_invalidate;
  wire  icache_io_mem_0_a_ready;
  wire  icache_io_mem_0_a_valid;
  wire [2:0] icache_io_mem_0_a_bits_opcode;
  wire [2:0] icache_io_mem_0_a_bits_param;
  wire [3:0] icache_io_mem_0_a_bits_size;
  wire  icache_io_mem_0_a_bits_source;
  wire [31:0] icache_io_mem_0_a_bits_address;
  wire [7:0] icache_io_mem_0_a_bits_mask;
  wire [63:0] icache_io_mem_0_a_bits_data;
  wire  icache_io_mem_0_b_ready;
  wire  icache_io_mem_0_b_valid;
  wire [2:0] icache_io_mem_0_b_bits_opcode;
  wire [1:0] icache_io_mem_0_b_bits_param;
  wire [3:0] icache_io_mem_0_b_bits_size;
  wire  icache_io_mem_0_b_bits_source;
  wire [31:0] icache_io_mem_0_b_bits_address;
  wire [7:0] icache_io_mem_0_b_bits_mask;
  wire [63:0] icache_io_mem_0_b_bits_data;
  wire  icache_io_mem_0_c_ready;
  wire  icache_io_mem_0_c_valid;
  wire [2:0] icache_io_mem_0_c_bits_opcode;
  wire [2:0] icache_io_mem_0_c_bits_param;
  wire [3:0] icache_io_mem_0_c_bits_size;
  wire  icache_io_mem_0_c_bits_source;
  wire [31:0] icache_io_mem_0_c_bits_address;
  wire [63:0] icache_io_mem_0_c_bits_data;
  wire  icache_io_mem_0_c_bits_error;
  wire  icache_io_mem_0_d_ready;
  wire  icache_io_mem_0_d_valid;
  wire [2:0] icache_io_mem_0_d_bits_opcode;
  wire [1:0] icache_io_mem_0_d_bits_param;
  wire [3:0] icache_io_mem_0_d_bits_size;
  wire  icache_io_mem_0_d_bits_source;
  wire [3:0] icache_io_mem_0_d_bits_sink;
  wire [2:0] icache_io_mem_0_d_bits_addr_lo;
  wire [63:0] icache_io_mem_0_d_bits_data;
  wire  icache_io_mem_0_d_bits_error;
  wire  icache_io_mem_0_e_ready;
  wire  icache_io_mem_0_e_valid;
  wire [3:0] icache_io_mem_0_e_bits_sink;
  wire  tlb_clock;
  wire  tlb_reset;
  wire  tlb_io_req_ready;
  wire  tlb_io_req_valid;
  wire [27:0] tlb_io_req_bits_vpn;
  wire  tlb_io_req_bits_passthrough;
  wire  tlb_io_req_bits_instruction;
  wire  tlb_io_req_bits_store;
  wire  tlb_io_resp_miss;
  wire [19:0] tlb_io_resp_ppn;
  wire  tlb_io_resp_xcpt_ld;
  wire  tlb_io_resp_xcpt_st;
  wire  tlb_io_resp_xcpt_if;
  wire  tlb_io_resp_cacheable;
  wire  tlb_io_ptw_req_ready;
  wire  tlb_io_ptw_req_valid;
  wire [1:0] tlb_io_ptw_req_bits_prv;
  wire  tlb_io_ptw_req_bits_pum;
  wire  tlb_io_ptw_req_bits_mxr;
  wire [26:0] tlb_io_ptw_req_bits_addr;
  wire  tlb_io_ptw_req_bits_store;
  wire  tlb_io_ptw_req_bits_fetch;
  wire  tlb_io_ptw_resp_valid;
  wire [15:0] tlb_io_ptw_resp_bits_pte_reserved_for_hardware;
  wire [37:0] tlb_io_ptw_resp_bits_pte_ppn;
  wire [1:0] tlb_io_ptw_resp_bits_pte_reserved_for_software;
  wire  tlb_io_ptw_resp_bits_pte_d;
  wire  tlb_io_ptw_resp_bits_pte_a;
  wire  tlb_io_ptw_resp_bits_pte_g;
  wire  tlb_io_ptw_resp_bits_pte_u;
  wire  tlb_io_ptw_resp_bits_pte_x;
  wire  tlb_io_ptw_resp_bits_pte_w;
  wire  tlb_io_ptw_resp_bits_pte_r;
  wire  tlb_io_ptw_resp_bits_pte_v;
  wire [6:0] tlb_io_ptw_ptbr_asid;
  wire [37:0] tlb_io_ptw_ptbr_ppn;
  wire  tlb_io_ptw_invalidate;
  wire  tlb_io_ptw_status_debug;
  wire [31:0] tlb_io_ptw_status_isa;
  wire [1:0] tlb_io_ptw_status_prv;
  wire  tlb_io_ptw_status_sd;
  wire [30:0] tlb_io_ptw_status_zero3;
  wire  tlb_io_ptw_status_sd_rv32;
  wire [1:0] tlb_io_ptw_status_zero2;
  wire [4:0] tlb_io_ptw_status_vm;
  wire [3:0] tlb_io_ptw_status_zero1;
  wire  tlb_io_ptw_status_mxr;
  wire  tlb_io_ptw_status_pum;
  wire  tlb_io_ptw_status_mprv;
  wire [1:0] tlb_io_ptw_status_xs;
  wire [1:0] tlb_io_ptw_status_fs;
  wire [1:0] tlb_io_ptw_status_mpp;
  wire [1:0] tlb_io_ptw_status_hpp;
  wire  tlb_io_ptw_status_spp;
  wire  tlb_io_ptw_status_mpie;
  wire  tlb_io_ptw_status_hpie;
  wire  tlb_io_ptw_status_spie;
  wire  tlb_io_ptw_status_upie;
  wire  tlb_io_ptw_status_mie;
  wire  tlb_io_ptw_status_hie;
  wire  tlb_io_ptw_status_sie;
  wire  tlb_io_ptw_status_uie;
  reg [39:0] s1_pc_;
  reg [63:0] _GEN_16;
  wire [39:0] _T_1760;
  wire [39:0] _T_1762;
  wire [39:0] s1_pc;
  secure reg  s1_speculative;
  // synopsys keep_signal_name "s1_speculative"
  reg [31:0] _GEN_26;
  reg  s1_same_block;
  reg [31:0] _GEN_28;
  reg  s2_valid;
  reg [31:0] _GEN_29;
  reg [39:0] s2_pc;
  reg [63:0] _GEN_30;
  reg  s2_btb_resp_valid;
  reg [31:0] _GEN_31;
  reg  s2_btb_resp_bits_taken;
  reg [31:0] _GEN_32;
  reg  s2_btb_resp_bits_mask;
  reg [31:0] _GEN_33;
  reg  s2_btb_resp_bits_bridx;
  reg [31:0] _GEN_34;
  reg [38:0] s2_btb_resp_bits_target;
  reg [63:0] _GEN_35;
  reg [5:0] s2_btb_resp_bits_entry;
  reg [31:0] _GEN_36;
  reg [6:0] s2_btb_resp_bits_bht_history;
  reg [31:0] _GEN_37;
  reg [1:0] s2_btb_resp_bits_bht_value;
  reg [31:0] _GEN_38;
  reg  s2_xcpt_if;
  reg [31:0] _GEN_39;
  secure reg  s2_speculative;
  // synopsys keep_signal_name "s2_speculative"
  reg [31:0] _GEN_40;
  reg  s2_cacheable;
  reg [31:0] _GEN_41;
  wire [39:0] _T_1787;
  wire [39:0] _T_1789;
  wire [39:0] _T_1790;
  wire [40:0] _T_1792;
  wire [39:0] ntpc;
  wire [39:0] _T_1794;
  wire [39:0] _T_1796;
 secure wire  ntpc_same_block;
  // synopsys keep_signal_name "ntpc_same_block"
  secure wire [39:0] predicted_npc;
  // synopsys keep_signal_name "predicted_npc"
  secure wire  predicted_taken;
  // synopsys keep_signal_name "predicted_taken"
  wire  _T_1799;
  wire  icmiss;
  secure wire [39:0] npc;
  // synopsys keep_signal_name "npc"
  wire  _T_1801;
  wire  _T_1803;
  wire  _T_1804;
  wire  _T_1806;
  wire  _T_1807;
  wire  s0_same_block;
  wire  _T_1809;
  wire  stall;
  wire  _T_1811;
  wire  _T_1813;
  wire  _T_1814;
  wire  _T_1816;
  wire  _T_1823;
  wire [39:0] _GEN_0;
  wire  _GEN_1;
  wire  _GEN_2;
  wire  _GEN_3;
  wire  _GEN_4;
  wire [39:0] _GEN_5;
  wire  _GEN_6;
  wire  _GEN_7;
  wire [39:0] _GEN_8;
  wire  _GEN_9;
  wire  _GEN_10;
  wire  _GEN_11;
  wire  _GEN_12;
  wire [39:0] _GEN_13;
  wire  _GEN_14;
  wire  _GEN_15;
  wire  BTB_clock;
  wire  BTB_reset;
  wire  BTB_io_req_valid;
  wire [38:0] BTB_io_req_bits_addr;
  wire  BTB_io_resp_valid;
  wire  BTB_io_resp_bits_taken;
  wire  BTB_io_resp_bits_mask;
  wire  BTB_io_resp_bits_bridx;
  wire [38:0] BTB_io_resp_bits_target;
  wire [5:0] BTB_io_resp_bits_entry;
  wire [6:0] BTB_io_resp_bits_bht_history;
  wire [1:0] BTB_io_resp_bits_bht_value;
  wire  BTB_io_btb_update_valid;
  wire  BTB_io_btb_update_bits_prediction_valid;
  wire  BTB_io_btb_update_bits_prediction_bits_taken;
  wire  BTB_io_btb_update_bits_prediction_bits_mask;
  wire  BTB_io_btb_update_bits_prediction_bits_bridx;
  wire [38:0] BTB_io_btb_update_bits_prediction_bits_target;
  wire [5:0] BTB_io_btb_update_bits_prediction_bits_entry;
  wire [6:0] BTB_io_btb_update_bits_prediction_bits_bht_history;
  wire [1:0] BTB_io_btb_update_bits_prediction_bits_bht_value;
  wire [38:0] BTB_io_btb_update_bits_pc;
  wire [38:0] BTB_io_btb_update_bits_target;
  wire  BTB_io_btb_update_bits_taken;
  wire  BTB_io_btb_update_bits_isValid;
  secure wire  BTB_io_btb_update_bits_isJump;
  // synopsys keep_signal_name "BTB_io_btb_update_bits_isJump"
  secure wire  BTB_io_btb_update_bits_isReturn;
  // synopsys keep_signal_name "BTB_io_btb_update_bits_isReturn"
  wire [38:0] BTB_io_btb_update_bits_br_pc;
  wire  BTB_io_bht_update_valid;
  wire  BTB_io_bht_update_bits_prediction_valid;
  wire  BTB_io_bht_update_bits_prediction_bits_taken;
  wire  BTB_io_bht_update_bits_prediction_bits_mask;
  wire  BTB_io_bht_update_bits_prediction_bits_bridx;
  wire [38:0] BTB_io_bht_update_bits_prediction_bits_target;
  wire [5:0] BTB_io_bht_update_bits_prediction_bits_entry;
  wire [6:0] BTB_io_bht_update_bits_prediction_bits_bht_history;
  wire [1:0] BTB_io_bht_update_bits_prediction_bits_bht_value;
  wire [38:0] BTB_io_bht_update_bits_pc;
  wire  BTB_io_bht_update_bits_taken;
  secure wire  BTB_io_bht_update_bits_mispredict;
  // synopsys keep_signal_name "BTB_io_bht_update_bits_mispredict"
  wire  BTB_io_ras_update_valid;
  secure wire  BTB_io_ras_update_bits_isCall;
  // synopsys keep_signal_name "BTB_io_ras_update_bits_isCall"
  secure wire  BTB_io_ras_update_bits_isReturn;
  // synopsys keep_signal_name "BTB_io_ras_update_bits_isReturn"
  wire [38:0] BTB_io_ras_update_bits_returnAddr;
  wire  BTB_io_ras_update_bits_prediction_valid;
  wire  BTB_io_ras_update_bits_prediction_bits_taken;
  wire  BTB_io_ras_update_bits_prediction_bits_mask;
  wire  BTB_io_ras_update_bits_prediction_bits_bridx;
  wire [38:0] BTB_io_ras_update_bits_prediction_bits_target;
  wire [5:0] BTB_io_ras_update_bits_prediction_bits_entry;
  wire [6:0] BTB_io_ras_update_bits_prediction_bits_bht_history;
  wire [1:0] BTB_io_ras_update_bits_prediction_bits_bht_value;
  wire  _T_1831;
  wire  _GEN_17;
  wire  _GEN_18;
  wire  _GEN_19;
  wire  _GEN_20;
  wire [38:0] _GEN_21;
  wire [5:0] _GEN_22;
  wire [6:0] _GEN_23;
  wire [1:0] _GEN_24;
  wire  _T_1833;
  wire  _T_1834;
  wire [39:0] _T_1835;
  wire [39:0] _GEN_25;
  wire [27:0] _T_1842;
  wire  _T_1849;
  wire  _T_1850;
  wire  _T_1851;
  wire  _T_1852;
  wire  _T_1853;
  wire  _T_1854;
  wire  _T_1856;
  wire  _T_1857;
  wire  _T_1861;
  wire  _T_1862;
  wire  _T_1863;
  wire  _T_1864;
  wire  _T_1865;
  wire [39:0] _T_1866;
  wire  _T_1867;
  wire [5:0] _GEN_27;
  wire [5:0] _T_1868;
  wire [63:0] _T_1869;
  wire [1:0] _T_1872;
  wire  _T_1875;
  wire  _T_1877;
  wire  _T_1878;
  /*ICache_icache icache (
    .clock(icache_clock),
    .reset(icache_reset),
    .io_req_valid(icache_io_req_valid),
    .io_req_bits_addr(icache_io_req_bits_addr),
    .io_s1_ppn(icache_io_s1_ppn),
    .io_s1_kill(icache_io_s1_kill),
    .io_s2_kill(icache_io_s2_kill),
    .io_resp_ready(icache_io_resp_ready),
    .io_resp_valid(icache_io_resp_valid),
    .io_resp_bits_data(icache_io_resp_bits_data),
    .io_resp_bits_datablock(icache_io_resp_bits_datablock),
    .io_invalidate(icache_io_invalidate),
    .io_mem_0_a_ready(icache_io_mem_0_a_ready),
    .io_mem_0_a_valid(icache_io_mem_0_a_valid),
    .io_mem_0_a_bits_opcode(icache_io_mem_0_a_bits_opcode),
    .io_mem_0_a_bits_param(icache_io_mem_0_a_bits_param),
    .io_mem_0_a_bits_size(icache_io_mem_0_a_bits_size),
    .io_mem_0_a_bits_source(icache_io_mem_0_a_bits_source),
    .io_mem_0_a_bits_address(icache_io_mem_0_a_bits_address),
    .io_mem_0_a_bits_mask(icache_io_mem_0_a_bits_mask),
    .io_mem_0_a_bits_data(icache_io_mem_0_a_bits_data),
    .io_mem_0_b_ready(icache_io_mem_0_b_ready),
    .io_mem_0_b_valid(icache_io_mem_0_b_valid),
    .io_mem_0_b_bits_opcode(icache_io_mem_0_b_bits_opcode),
    .io_mem_0_b_bits_param(icache_io_mem_0_b_bits_param),
    .io_mem_0_b_bits_size(icache_io_mem_0_b_bits_size),
    .io_mem_0_b_bits_source(icache_io_mem_0_b_bits_source),
    .io_mem_0_b_bits_address(icache_io_mem_0_b_bits_address),
    .io_mem_0_b_bits_mask(icache_io_mem_0_b_bits_mask),
    .io_mem_0_b_bits_data(icache_io_mem_0_b_bits_data),
    .io_mem_0_c_ready(icache_io_mem_0_c_ready),
    .io_mem_0_c_valid(icache_io_mem_0_c_valid),
    .io_mem_0_c_bits_opcode(icache_io_mem_0_c_bits_opcode),
    .io_mem_0_c_bits_param(icache_io_mem_0_c_bits_param),
    .io_mem_0_c_bits_size(icache_io_mem_0_c_bits_size),
    .io_mem_0_c_bits_source(icache_io_mem_0_c_bits_source),
    .io_mem_0_c_bits_address(icache_io_mem_0_c_bits_address),
    .io_mem_0_c_bits_data(icache_io_mem_0_c_bits_data),
    .io_mem_0_c_bits_error(icache_io_mem_0_c_bits_error),
    .io_mem_0_d_ready(icache_io_mem_0_d_ready),
    .io_mem_0_d_valid(icache_io_mem_0_d_valid),
    .io_mem_0_d_bits_opcode(icache_io_mem_0_d_bits_opcode),
    .io_mem_0_d_bits_param(icache_io_mem_0_d_bits_param),
    .io_mem_0_d_bits_size(icache_io_mem_0_d_bits_size),
    .io_mem_0_d_bits_source(icache_io_mem_0_d_bits_source),
    .io_mem_0_d_bits_sink(icache_io_mem_0_d_bits_sink),
    .io_mem_0_d_bits_addr_lo(icache_io_mem_0_d_bits_addr_lo),
    .io_mem_0_d_bits_data(icache_io_mem_0_d_bits_data),
    .io_mem_0_d_bits_error(icache_io_mem_0_d_bits_error),
    .io_mem_0_e_ready(icache_io_mem_0_e_ready),
    .io_mem_0_e_valid(icache_io_mem_0_e_valid),
    .io_mem_0_e_bits_sink(icache_io_mem_0_e_bits_sink)
  );
  TLB tlb (
    .clock(tlb_clock),
    .reset(tlb_reset),
    .io_req_ready(tlb_io_req_ready),
    .io_req_valid(tlb_io_req_valid),
    .io_req_bits_vpn(tlb_io_req_bits_vpn),
    .io_req_bits_passthrough(tlb_io_req_bits_passthrough),
    .io_req_bits_instruction(tlb_io_req_bits_instruction),
    .io_req_bits_store(tlb_io_req_bits_store),
    .io_resp_miss(tlb_io_resp_miss),
    .io_resp_ppn(tlb_io_resp_ppn),
    .io_resp_xcpt_ld(tlb_io_resp_xcpt_ld),
    .io_resp_xcpt_st(tlb_io_resp_xcpt_st),
    .io_resp_xcpt_if(tlb_io_resp_xcpt_if),
    .io_resp_cacheable(tlb_io_resp_cacheable),
    .io_ptw_req_ready(tlb_io_ptw_req_ready),
    .io_ptw_req_valid(tlb_io_ptw_req_valid),
    .io_ptw_req_bits_prv(tlb_io_ptw_req_bits_prv),
    .io_ptw_req_bits_pum(tlb_io_ptw_req_bits_pum),
    .io_ptw_req_bits_mxr(tlb_io_ptw_req_bits_mxr),
    .io_ptw_req_bits_addr(tlb_io_ptw_req_bits_addr),
    .io_ptw_req_bits_store(tlb_io_ptw_req_bits_store),
    .io_ptw_req_bits_fetch(tlb_io_ptw_req_bits_fetch),
    .io_ptw_resp_valid(tlb_io_ptw_resp_valid),
    .io_ptw_resp_bits_pte_reserved_for_hardware(tlb_io_ptw_resp_bits_pte_reserved_for_hardware),
    .io_ptw_resp_bits_pte_ppn(tlb_io_ptw_resp_bits_pte_ppn),
    .io_ptw_resp_bits_pte_reserved_for_software(tlb_io_ptw_resp_bits_pte_reserved_for_software),
    .io_ptw_resp_bits_pte_d(tlb_io_ptw_resp_bits_pte_d),
    .io_ptw_resp_bits_pte_a(tlb_io_ptw_resp_bits_pte_a),
    .io_ptw_resp_bits_pte_g(tlb_io_ptw_resp_bits_pte_g),
    .io_ptw_resp_bits_pte_u(tlb_io_ptw_resp_bits_pte_u),
    .io_ptw_resp_bits_pte_x(tlb_io_ptw_resp_bits_pte_x),
    .io_ptw_resp_bits_pte_w(tlb_io_ptw_resp_bits_pte_w),
    .io_ptw_resp_bits_pte_r(tlb_io_ptw_resp_bits_pte_r),
    .io_ptw_resp_bits_pte_v(tlb_io_ptw_resp_bits_pte_v),
    .io_ptw_ptbr_asid(tlb_io_ptw_ptbr_asid),
    .io_ptw_ptbr_ppn(tlb_io_ptw_ptbr_ppn),
    .io_ptw_invalidate(tlb_io_ptw_invalidate),
    .io_ptw_status_debug(tlb_io_ptw_status_debug),
    .io_ptw_status_isa(tlb_io_ptw_status_isa),
    .io_ptw_status_prv(tlb_io_ptw_status_prv),
    .io_ptw_status_sd(tlb_io_ptw_status_sd),
    .io_ptw_status_zero3(tlb_io_ptw_status_zero3),
    .io_ptw_status_sd_rv32(tlb_io_ptw_status_sd_rv32),
    .io_ptw_status_zero2(tlb_io_ptw_status_zero2),
    .io_ptw_status_vm(tlb_io_ptw_status_vm),
    .io_ptw_status_zero1(tlb_io_ptw_status_zero1),
    .io_ptw_status_mxr(tlb_io_ptw_status_mxr),
    .io_ptw_status_pum(tlb_io_ptw_status_pum),
    .io_ptw_status_mprv(tlb_io_ptw_status_mprv),
    .io_ptw_status_xs(tlb_io_ptw_status_xs),
    .io_ptw_status_fs(tlb_io_ptw_status_fs),
    .io_ptw_status_mpp(tlb_io_ptw_status_mpp),
    .io_ptw_status_hpp(tlb_io_ptw_status_hpp),
    .io_ptw_status_spp(tlb_io_ptw_status_spp),
    .io_ptw_status_mpie(tlb_io_ptw_status_mpie),
    .io_ptw_status_hpie(tlb_io_ptw_status_hpie),
    .io_ptw_status_spie(tlb_io_ptw_status_spie),
    .io_ptw_status_upie(tlb_io_ptw_status_upie),
    .io_ptw_status_mie(tlb_io_ptw_status_mie),
    .io_ptw_status_hie(tlb_io_ptw_status_hie),
    .io_ptw_status_sie(tlb_io_ptw_status_sie),
    .io_ptw_status_uie(tlb_io_ptw_status_uie)
  );
  BTB BTB (
    .clock(BTB_clock),
    .reset(BTB_reset),
    .io_req_valid(BTB_io_req_valid),
    .io_req_bits_addr(BTB_io_req_bits_addr),
    .io_resp_valid(BTB_io_resp_valid),
    .io_resp_bits_taken(BTB_io_resp_bits_taken),
    .io_resp_bits_mask(BTB_io_resp_bits_mask),
    .io_resp_bits_bridx(BTB_io_resp_bits_bridx),
    .io_resp_bits_target(BTB_io_resp_bits_target),
    .io_resp_bits_entry(BTB_io_resp_bits_entry),
    .io_resp_bits_bht_history(BTB_io_resp_bits_bht_history),
    .io_resp_bits_bht_value(BTB_io_resp_bits_bht_value),
    .io_btb_update_valid(BTB_io_btb_update_valid),
    .io_btb_update_bits_prediction_valid(BTB_io_btb_update_bits_prediction_valid),
    .io_btb_update_bits_prediction_bits_taken(BTB_io_btb_update_bits_prediction_bits_taken),
    .io_btb_update_bits_prediction_bits_mask(BTB_io_btb_update_bits_prediction_bits_mask),
    .io_btb_update_bits_prediction_bits_bridx(BTB_io_btb_update_bits_prediction_bits_bridx),
    .io_btb_update_bits_prediction_bits_target(BTB_io_btb_update_bits_prediction_bits_target),
    .io_btb_update_bits_prediction_bits_entry(BTB_io_btb_update_bits_prediction_bits_entry),
    .io_btb_update_bits_prediction_bits_bht_history(BTB_io_btb_update_bits_prediction_bits_bht_history),
    .io_btb_update_bits_prediction_bits_bht_value(BTB_io_btb_update_bits_prediction_bits_bht_value),
    .io_btb_update_bits_pc(BTB_io_btb_update_bits_pc),
    .io_btb_update_bits_target(BTB_io_btb_update_bits_target),
    .io_btb_update_bits_taken(BTB_io_btb_update_bits_taken),
    .io_btb_update_bits_isValid(BTB_io_btb_update_bits_isValid),
    .io_btb_update_bits_isJump(BTB_io_btb_update_bits_isJump),
    .io_btb_update_bits_isReturn(BTB_io_btb_update_bits_isReturn),
    .io_btb_update_bits_br_pc(BTB_io_btb_update_bits_br_pc),
    .io_bht_update_valid(BTB_io_bht_update_valid),
    .io_bht_update_bits_prediction_valid(BTB_io_bht_update_bits_prediction_valid),
    .io_bht_update_bits_prediction_bits_taken(BTB_io_bht_update_bits_prediction_bits_taken),
    .io_bht_update_bits_prediction_bits_mask(BTB_io_bht_update_bits_prediction_bits_mask),
    .io_bht_update_bits_prediction_bits_bridx(BTB_io_bht_update_bits_prediction_bits_bridx),
    .io_bht_update_bits_prediction_bits_target(BTB_io_bht_update_bits_prediction_bits_target),
    .io_bht_update_bits_prediction_bits_entry(BTB_io_bht_update_bits_prediction_bits_entry),
    .io_bht_update_bits_prediction_bits_bht_history(BTB_io_bht_update_bits_prediction_bits_bht_history),
    .io_bht_update_bits_prediction_bits_bht_value(BTB_io_bht_update_bits_prediction_bits_bht_value),
    .io_bht_update_bits_pc(BTB_io_bht_update_bits_pc),
    .io_bht_update_bits_taken(BTB_io_bht_update_bits_taken),
    .io_bht_update_bits_mispredict(BTB_io_bht_update_bits_mispredict),
    .io_ras_update_valid(BTB_io_ras_update_valid),
    .io_ras_update_bits_isCall(BTB_io_ras_update_bits_isCall),
    .io_ras_update_bits_isReturn(BTB_io_ras_update_bits_isReturn),
    .io_ras_update_bits_returnAddr(BTB_io_ras_update_bits_returnAddr),
    .io_ras_update_bits_prediction_valid(BTB_io_ras_update_bits_prediction_valid),
    .io_ras_update_bits_prediction_bits_taken(BTB_io_ras_update_bits_prediction_bits_taken),
    .io_ras_update_bits_prediction_bits_mask(BTB_io_ras_update_bits_prediction_bits_mask),
    .io_ras_update_bits_prediction_bits_bridx(BTB_io_ras_update_bits_prediction_bits_bridx),
    .io_ras_update_bits_prediction_bits_target(BTB_io_ras_update_bits_prediction_bits_target),
    .io_ras_update_bits_prediction_bits_entry(BTB_io_ras_update_bits_prediction_bits_entry),
    .io_ras_update_bits_prediction_bits_bht_history(BTB_io_ras_update_bits_prediction_bits_bht_history),
    .io_ras_update_bits_prediction_bits_bht_value(BTB_io_ras_update_bits_prediction_bits_bht_value)
  );*/
  assign io_mem_0_a_valid = icache_io_mem_0_a_valid;
  assign io_mem_0_a_bits_opcode = icache_io_mem_0_a_bits_opcode;
  assign io_mem_0_a_bits_param = icache_io_mem_0_a_bits_param;
  assign io_mem_0_a_bits_size = icache_io_mem_0_a_bits_size;
  assign io_mem_0_a_bits_source = icache_io_mem_0_a_bits_source;
  assign io_mem_0_a_bits_address = icache_io_mem_0_a_bits_address;
  assign io_mem_0_a_bits_mask = icache_io_mem_0_a_bits_mask;
  assign io_mem_0_a_bits_data = icache_io_mem_0_a_bits_data;
  assign io_mem_0_b_ready = icache_io_mem_0_b_ready;
  assign io_mem_0_c_valid = icache_io_mem_0_c_valid;
  assign io_mem_0_c_bits_opcode = icache_io_mem_0_c_bits_opcode;
  assign io_mem_0_c_bits_param = icache_io_mem_0_c_bits_param;
  assign io_mem_0_c_bits_size = icache_io_mem_0_c_bits_size;
  assign io_mem_0_c_bits_source = icache_io_mem_0_c_bits_source;
  assign io_mem_0_c_bits_address = icache_io_mem_0_c_bits_address;
  assign io_mem_0_c_bits_data = icache_io_mem_0_c_bits_data;
  assign io_mem_0_c_bits_error = icache_io_mem_0_c_bits_error;
  assign io_mem_0_d_ready = icache_io_mem_0_d_ready;
  assign io_mem_0_e_valid = icache_io_mem_0_e_valid;
  assign io_mem_0_e_bits_sink = icache_io_mem_0_e_bits_sink;
  assign io_cpu_resp_valid = _T_1865;
  assign io_cpu_resp_bits_btb_valid = s2_btb_resp_valid;
  assign io_cpu_resp_bits_btb_bits_taken = s2_btb_resp_bits_taken;
  assign io_cpu_resp_bits_btb_bits_mask = s2_btb_resp_bits_mask;
  assign io_cpu_resp_bits_btb_bits_bridx = s2_btb_resp_bits_bridx;
  assign io_cpu_resp_bits_btb_bits_target = s2_btb_resp_bits_target;
  assign io_cpu_resp_bits_btb_bits_entry = s2_btb_resp_bits_entry;
  assign io_cpu_resp_bits_btb_bits_bht_history = s2_btb_resp_bits_bht_history;
  assign io_cpu_resp_bits_btb_bits_bht_value = s2_btb_resp_bits_bht_value;
  assign io_cpu_resp_bits_pc = s2_pc;
  assign io_cpu_resp_bits_data = _T_1869[31:0];
  assign io_cpu_resp_bits_mask = _T_1872[0];
  assign io_cpu_resp_bits_xcpt_if = s2_xcpt_if;
  assign io_cpu_resp_bits_replay = _T_1878;
  assign io_cpu_npc = _T_1866;
  assign io_ptw_req_valid = tlb_io_ptw_req_valid;
  assign io_ptw_req_bits_prv = tlb_io_ptw_req_bits_prv;
  assign io_ptw_req_bits_pum = tlb_io_ptw_req_bits_pum;
  assign io_ptw_req_bits_mxr = tlb_io_ptw_req_bits_mxr;
  assign io_ptw_req_bits_addr = tlb_io_ptw_req_bits_addr;
  assign io_ptw_req_bits_store = tlb_io_ptw_req_bits_store;
  assign io_ptw_req_bits_fetch = tlb_io_ptw_req_bits_fetch;
  assign icache_clock = clock;
  assign icache_reset = reset;
  assign icache_io_req_valid = _T_1850;
  assign icache_io_req_bits_addr = io_cpu_npc[38:0];
  assign icache_io_s1_ppn = tlb_io_resp_ppn;
  assign icache_io_s1_kill = _T_1854;
  assign icache_io_s2_kill = _T_1857;
  assign icache_io_resp_ready = _T_1862;
  assign icache_io_invalidate = io_cpu_flush_icache;
  assign icache_io_mem_0_a_ready = io_mem_0_a_ready;
  assign icache_io_mem_0_b_valid = io_mem_0_b_valid;
  assign icache_io_mem_0_b_bits_opcode = io_mem_0_b_bits_opcode;
  assign icache_io_mem_0_b_bits_param = io_mem_0_b_bits_param;
  assign icache_io_mem_0_b_bits_size = io_mem_0_b_bits_size;
  assign icache_io_mem_0_b_bits_source = io_mem_0_b_bits_source;
  assign icache_io_mem_0_b_bits_address = io_mem_0_b_bits_address;
  assign icache_io_mem_0_b_bits_mask = io_mem_0_b_bits_mask;
  assign icache_io_mem_0_b_bits_data = io_mem_0_b_bits_data;
  assign icache_io_mem_0_c_ready = io_mem_0_c_ready;
  assign icache_io_mem_0_d_valid = io_mem_0_d_valid;
  assign icache_io_mem_0_d_bits_opcode = io_mem_0_d_bits_opcode;
  assign icache_io_mem_0_d_bits_param = io_mem_0_d_bits_param;
  assign icache_io_mem_0_d_bits_size = io_mem_0_d_bits_size;
  assign icache_io_mem_0_d_bits_source = io_mem_0_d_bits_source;
  assign icache_io_mem_0_d_bits_sink = io_mem_0_d_bits_sink;
  assign icache_io_mem_0_d_bits_addr_lo = io_mem_0_d_bits_addr_lo;
  assign icache_io_mem_0_d_bits_data = io_mem_0_d_bits_data;
  assign icache_io_mem_0_d_bits_error = io_mem_0_d_bits_error;
  assign icache_io_mem_0_e_ready = io_mem_0_e_ready;
  assign tlb_clock = clock;
  assign tlb_reset = reset;
  assign tlb_io_req_valid = _T_1831;
  assign tlb_io_req_bits_vpn = _T_1842;
  assign tlb_io_req_bits_passthrough = 1'h0;
  assign tlb_io_req_bits_instruction = 1'h1;
  assign tlb_io_req_bits_store = 1'h0;
  assign tlb_io_ptw_req_ready = io_ptw_req_ready;
  assign tlb_io_ptw_resp_valid = io_ptw_resp_valid;
  assign tlb_io_ptw_resp_bits_pte_reserved_for_hardware = io_ptw_resp_bits_pte_reserved_for_hardware;
  assign tlb_io_ptw_resp_bits_pte_ppn = io_ptw_resp_bits_pte_ppn;
  assign tlb_io_ptw_resp_bits_pte_reserved_for_software = io_ptw_resp_bits_pte_reserved_for_software;
  assign tlb_io_ptw_resp_bits_pte_d = io_ptw_resp_bits_pte_d;
  assign tlb_io_ptw_resp_bits_pte_a = io_ptw_resp_bits_pte_a;
  assign tlb_io_ptw_resp_bits_pte_g = io_ptw_resp_bits_pte_g;
  assign tlb_io_ptw_resp_bits_pte_u = io_ptw_resp_bits_pte_u;
  assign tlb_io_ptw_resp_bits_pte_x = io_ptw_resp_bits_pte_x;
  assign tlb_io_ptw_resp_bits_pte_w = io_ptw_resp_bits_pte_w;
  assign tlb_io_ptw_resp_bits_pte_r = io_ptw_resp_bits_pte_r;
  assign tlb_io_ptw_resp_bits_pte_v = io_ptw_resp_bits_pte_v;
  assign tlb_io_ptw_ptbr_asid = io_ptw_ptbr_asid;
  assign tlb_io_ptw_ptbr_ppn = io_ptw_ptbr_ppn;
  assign tlb_io_ptw_invalidate = io_ptw_invalidate;
  assign tlb_io_ptw_status_debug = io_ptw_status_debug;
  assign tlb_io_ptw_status_isa = io_ptw_status_isa;
  assign tlb_io_ptw_status_prv = io_ptw_status_prv;
  assign tlb_io_ptw_status_sd = io_ptw_status_sd;
  assign tlb_io_ptw_status_zero3 = io_ptw_status_zero3;
  assign tlb_io_ptw_status_sd_rv32 = io_ptw_status_sd_rv32;
  assign tlb_io_ptw_status_zero2 = io_ptw_status_zero2;
  assign tlb_io_ptw_status_vm = io_ptw_status_vm;
  assign tlb_io_ptw_status_zero1 = io_ptw_status_zero1;
  assign tlb_io_ptw_status_mxr = io_ptw_status_mxr;
  assign tlb_io_ptw_status_pum = io_ptw_status_pum;
  assign tlb_io_ptw_status_mprv = io_ptw_status_mprv;
  assign tlb_io_ptw_status_xs = io_ptw_status_xs;
  assign tlb_io_ptw_status_fs = io_ptw_status_fs;
  assign tlb_io_ptw_status_mpp = io_ptw_status_mpp;
  assign tlb_io_ptw_status_hpp = io_ptw_status_hpp;
  assign tlb_io_ptw_status_spp = io_ptw_status_spp;
  assign tlb_io_ptw_status_mpie = io_ptw_status_mpie;
  assign tlb_io_ptw_status_hpie = io_ptw_status_hpie;
  assign tlb_io_ptw_status_spie = io_ptw_status_spie;
  assign tlb_io_ptw_status_upie = io_ptw_status_upie;
  assign tlb_io_ptw_status_mie = io_ptw_status_mie;
  assign tlb_io_ptw_status_hie = io_ptw_status_hie;
  assign tlb_io_ptw_status_sie = io_ptw_status_sie;
  assign tlb_io_ptw_status_uie = io_ptw_status_uie;
  assign _T_1760 = ~ s1_pc_;
  assign _T_1762 = _T_1760 | 40'h3;
  assign s1_pc = ~ _T_1762;
  assign _T_1787 = ~ s1_pc;
  assign _T_1789 = _T_1787 | 40'h3;
  assign _T_1790 = ~ _T_1789;
  assign _T_1792 = _T_1790 + 40'h4;
  assign ntpc = _T_1792[39:0];
  assign _T_1794 = ntpc & 40'h8;
  assign _T_1796 = s1_pc & 40'h8;
  assign ntpc_same_block = _T_1794 == _T_1796;
  assign predicted_npc = _GEN_25;
  assign predicted_taken = _T_1833;
  assign _T_1799 = icache_io_resp_valid == 1'h0;
  assign icmiss = s2_valid & _T_1799;
  assign npc = icmiss ? s2_pc : predicted_npc;
  assign _T_1801 = predicted_taken == 1'h0;
  assign _T_1803 = icmiss == 1'h0;
  assign _T_1804 = _T_1801 & _T_1803;
  assign _T_1806 = io_cpu_req_valid == 1'h0;
  assign _T_1807 = _T_1804 & _T_1806;
  assign s0_same_block = _T_1807 & ntpc_same_block;
  assign _T_1809 = io_cpu_resp_ready == 1'h0;
  assign stall = io_cpu_resp_valid & _T_1809;
  assign _T_1811 = stall == 1'h0;
  assign _T_1813 = tlb_io_resp_miss == 1'h0;
  assign _T_1814 = s0_same_block & _T_1813;
  assign _T_1816 = icmiss ? s2_speculative : 1'h1;
  assign _T_1823 = tlb_io_resp_xcpt_if & _T_1813;
  assign _GEN_0 = _T_1803 ? s1_pc : s2_pc;
  assign _GEN_1 = _T_1803 ? s1_speculative : s2_speculative;
  assign _GEN_2 = _T_1803 ? tlb_io_resp_cacheable : s2_cacheable;
  assign _GEN_3 = _T_1803 ? _T_1823 : s2_xcpt_if;
  assign _GEN_4 = _T_1811 ? _T_1814 : s1_same_block;
  assign _GEN_5 = _T_1811 ? io_cpu_npc : s1_pc_;
  assign _GEN_6 = _T_1811 ? _T_1816 : s1_speculative;
  assign _GEN_7 = _T_1811 ? _T_1803 : s2_valid;
  assign _GEN_8 = _T_1811 ? _GEN_0 : s2_pc;
  assign _GEN_9 = _T_1811 ? _GEN_1 : s2_speculative;
  assign _GEN_10 = _T_1811 ? _GEN_2 : s2_cacheable;
  assign _GEN_11 = _T_1811 ? _GEN_3 : s2_xcpt_if;
  assign _GEN_12 = io_cpu_req_valid ? 1'h0 : _GEN_4;
  assign _GEN_13 = io_cpu_req_valid ? io_cpu_npc : _GEN_5;
  assign _GEN_14 = io_cpu_req_valid ? io_cpu_req_bits_speculative : _GEN_6;
  assign _GEN_15 = io_cpu_req_valid ? 1'h0 : _GEN_7;
  assign BTB_clock = clock;
  assign BTB_reset = reset;
  assign BTB_io_req_valid = _T_1831;
  assign BTB_io_req_bits_addr = s1_pc_[38:0];
  assign BTB_io_btb_update_valid = io_cpu_btb_update_valid;
  assign BTB_io_btb_update_bits_prediction_valid = io_cpu_btb_update_bits_prediction_valid;
  assign BTB_io_btb_update_bits_prediction_bits_taken = io_cpu_btb_update_bits_prediction_bits_taken;
  assign BTB_io_btb_update_bits_prediction_bits_mask = io_cpu_btb_update_bits_prediction_bits_mask;
  assign BTB_io_btb_update_bits_prediction_bits_bridx = io_cpu_btb_update_bits_prediction_bits_bridx;
  assign BTB_io_btb_update_bits_prediction_bits_target = io_cpu_btb_update_bits_prediction_bits_target;
  assign BTB_io_btb_update_bits_prediction_bits_entry = io_cpu_btb_update_bits_prediction_bits_entry;
  assign BTB_io_btb_update_bits_prediction_bits_bht_history = io_cpu_btb_update_bits_prediction_bits_bht_history;
  assign BTB_io_btb_update_bits_prediction_bits_bht_value = io_cpu_btb_update_bits_prediction_bits_bht_value;
  assign BTB_io_btb_update_bits_pc = io_cpu_btb_update_bits_pc;
  assign BTB_io_btb_update_bits_target = io_cpu_btb_update_bits_target;
  assign BTB_io_btb_update_bits_taken = io_cpu_btb_update_bits_taken;
  assign BTB_io_btb_update_bits_isValid = io_cpu_btb_update_bits_isValid;
  assign BTB_io_btb_update_bits_isJump = io_cpu_btb_update_bits_isJump;
  assign BTB_io_btb_update_bits_isReturn = io_cpu_btb_update_bits_isReturn;
  assign BTB_io_btb_update_bits_br_pc = io_cpu_btb_update_bits_br_pc;
  assign BTB_io_bht_update_valid = io_cpu_bht_update_valid;
  assign BTB_io_bht_update_bits_prediction_valid = io_cpu_bht_update_bits_prediction_valid;
  assign BTB_io_bht_update_bits_prediction_bits_taken = io_cpu_bht_update_bits_prediction_bits_taken;
  assign BTB_io_bht_update_bits_prediction_bits_mask = io_cpu_bht_update_bits_prediction_bits_mask;
  assign BTB_io_bht_update_bits_prediction_bits_bridx = io_cpu_bht_update_bits_prediction_bits_bridx;
  assign BTB_io_bht_update_bits_prediction_bits_target = io_cpu_bht_update_bits_prediction_bits_target;
  assign BTB_io_bht_update_bits_prediction_bits_entry = io_cpu_bht_update_bits_prediction_bits_entry;
  assign BTB_io_bht_update_bits_prediction_bits_bht_history = io_cpu_bht_update_bits_prediction_bits_bht_history;
  assign BTB_io_bht_update_bits_prediction_bits_bht_value = io_cpu_bht_update_bits_prediction_bits_bht_value;
  assign BTB_io_bht_update_bits_pc = io_cpu_bht_update_bits_pc;
  assign BTB_io_bht_update_bits_taken = io_cpu_bht_update_bits_taken;
  assign BTB_io_bht_update_bits_mispredict = io_cpu_bht_update_bits_mispredict;
  assign BTB_io_ras_update_valid = io_cpu_ras_update_valid;
  assign BTB_io_ras_update_bits_isCall = io_cpu_ras_update_bits_isCall;
  assign BTB_io_ras_update_bits_isReturn = io_cpu_ras_update_bits_isReturn;
  assign BTB_io_ras_update_bits_returnAddr = io_cpu_ras_update_bits_returnAddr;
  assign BTB_io_ras_update_bits_prediction_valid = io_cpu_ras_update_bits_prediction_valid;
  assign BTB_io_ras_update_bits_prediction_bits_taken = io_cpu_ras_update_bits_prediction_bits_taken;
  assign BTB_io_ras_update_bits_prediction_bits_mask = io_cpu_ras_update_bits_prediction_bits_mask;
  assign BTB_io_ras_update_bits_prediction_bits_bridx = io_cpu_ras_update_bits_prediction_bits_bridx;
  assign BTB_io_ras_update_bits_prediction_bits_target = io_cpu_ras_update_bits_prediction_bits_target;
  assign BTB_io_ras_update_bits_prediction_bits_entry = io_cpu_ras_update_bits_prediction_bits_entry;
  assign BTB_io_ras_update_bits_prediction_bits_bht_history = io_cpu_ras_update_bits_prediction_bits_bht_history;
  assign BTB_io_ras_update_bits_prediction_bits_bht_value = io_cpu_ras_update_bits_prediction_bits_bht_value;
  assign _T_1831 = _T_1811 & _T_1803;
  assign _GEN_17 = _T_1831 ? BTB_io_resp_valid : s2_btb_resp_valid;
  assign _GEN_18 = _T_1831 ? BTB_io_resp_bits_taken : s2_btb_resp_bits_taken;
  assign _GEN_19 = _T_1831 ? BTB_io_resp_bits_mask : s2_btb_resp_bits_mask;
  assign _GEN_20 = _T_1831 ? BTB_io_resp_bits_bridx : s2_btb_resp_bits_bridx;
  assign _GEN_21 = _T_1831 ? BTB_io_resp_bits_target : s2_btb_resp_bits_target;
  assign _GEN_22 = _T_1831 ? BTB_io_resp_bits_entry : s2_btb_resp_bits_entry;
  assign _GEN_23 = _T_1831 ? BTB_io_resp_bits_bht_history : s2_btb_resp_bits_bht_history;
  assign _GEN_24 = _T_1831 ? BTB_io_resp_bits_bht_value : s2_btb_resp_bits_bht_value;
  assign _T_1833 = BTB_io_resp_valid & BTB_io_resp_bits_taken;
  assign _T_1834 = BTB_io_resp_bits_target[38];
  assign _T_1835 = {_T_1834,BTB_io_resp_bits_target};
  assign _GEN_25 = _T_1833 ? _T_1835 : ntpc;
  assign _T_1842 = s1_pc[39:12];
  assign _T_1849 = s0_same_block == 1'h0;
  assign _T_1850 = _T_1811 & _T_1849;
  assign _T_1851 = io_cpu_req_valid | tlb_io_resp_miss;
  assign _T_1852 = _T_1851 | tlb_io_resp_xcpt_if;
  assign _T_1853 = _T_1852 | icmiss;
  assign _T_1854 = _T_1853 | io_cpu_flush_tlb;
  assign _T_1856 = s2_cacheable == 1'h0;
  assign _T_1857 = s2_speculative & _T_1856;
  assign _T_1861 = s1_same_block == 1'h0;
  assign _T_1862 = _T_1811 & _T_1861;
  assign _T_1863 = icache_io_resp_valid | icache_io_s2_kill;
  assign _T_1864 = _T_1863 | s2_xcpt_if;
  assign _T_1865 = s2_valid & _T_1864;
  assign _T_1866 = io_cpu_req_valid ? io_cpu_req_bits_pc : npc;
  assign _T_1867 = s2_pc[2];
  assign _GEN_27 = {{5'd0}, _T_1867};
  assign _T_1868 = _GEN_27 << 5;
  assign _T_1869 = icache_io_resp_bits_datablock >> _T_1868;
  assign _T_1872 = 2'h1 << 1'h0;
  assign _T_1875 = icache_io_s2_kill & _T_1799;
  assign _T_1877 = s2_xcpt_if == 1'h0;
  assign _T_1878 = _T_1875 & _T_1877;
`ifdef RANDOMIZE
  integer initvar;
  initial begin
    `ifndef verilator
      #0.002 begin end
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_16 = {2{$random}};
  s1_pc_ = _GEN_16[39:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_26 = {1{$random}};
  s1_speculative = _GEN_26[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_28 = {1{$random}};
  s1_same_block = _GEN_28[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_29 = {1{$random}};
  s2_valid = _GEN_29[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_30 = {2{$random}};
  s2_pc = _GEN_30[39:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_31 = {1{$random}};
  s2_btb_resp_valid = _GEN_31[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_32 = {1{$random}};
  s2_btb_resp_bits_taken = _GEN_32[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_33 = {1{$random}};
  s2_btb_resp_bits_mask = _GEN_33[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_34 = {1{$random}};
  s2_btb_resp_bits_bridx = _GEN_34[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_35 = {2{$random}};
  s2_btb_resp_bits_target = _GEN_35[38:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_36 = {1{$random}};
  s2_btb_resp_bits_entry = _GEN_36[5:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_37 = {1{$random}};
  s2_btb_resp_bits_bht_history = _GEN_37[6:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_38 = {1{$random}};
  s2_btb_resp_bits_bht_value = _GEN_38[1:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_39 = {1{$random}};
  s2_xcpt_if = _GEN_39[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_40 = {1{$random}};
  s2_speculative = _GEN_40[0:0];
  `endif
  `ifdef RANDOMIZE_REG_INIT
  _GEN_41 = {1{$random}};
  s2_cacheable = _GEN_41[0:0];
  `endif
  end
`endif
  always @(posedge clock) begin
    if (io_cpu_req_valid) begin
      s1_pc_ <= io_cpu_npc;
    end else begin
      if (_T_1811) begin
        s1_pc_ <= io_cpu_npc;
      end
    end
    if (io_cpu_req_valid) begin
      s1_speculative <= io_cpu_req_bits_speculative;
    end else begin
      if (_T_1811) begin
        if (icmiss) begin
          s1_speculative <= s2_speculative;
        end else begin
          s1_speculative <= 1'h1;
        end
      end
    end
    if (io_cpu_req_valid) begin
      s1_same_block <= 1'h0;
    end else begin
      if (_T_1811) begin
        s1_same_block <= _T_1814;
      end
    end
    if (reset) begin
      s2_valid <= 1'h1;
    end else begin
      if (io_cpu_req_valid) begin
        s2_valid <= 1'h0;
      end else begin
        if (_T_1811) begin
          s2_valid <= _T_1803;
        end
      end
    end
    if (reset) begin
      s2_pc <= io_resetVector;
    end else begin
      if (_T_1811) begin
        if (_T_1803) begin
          s2_pc <= s1_pc;
        end
      end
    end
    if (reset) begin
      s2_btb_resp_valid <= 1'h0;
    end else begin
      if (_T_1831) begin
        s2_btb_resp_valid <= BTB_io_resp_valid;
      end
    end
    if (_T_1831) begin
      s2_btb_resp_bits_taken <= BTB_io_resp_bits_taken;
    end
    if (_T_1831) begin
      s2_btb_resp_bits_mask <= BTB_io_resp_bits_mask;
    end
    if (_T_1831) begin
      s2_btb_resp_bits_bridx <= BTB_io_resp_bits_bridx;
    end
    if (_T_1831) begin
      s2_btb_resp_bits_target <= BTB_io_resp_bits_target;
    end
    if (_T_1831) begin
      s2_btb_resp_bits_entry <= BTB_io_resp_bits_entry;
    end
    if (_T_1831) begin
      s2_btb_resp_bits_bht_history <= BTB_io_resp_bits_bht_history;
    end
    if (_T_1831) begin
      s2_btb_resp_bits_bht_value <= BTB_io_resp_bits_bht_value;
    end
    if (reset) begin
      s2_xcpt_if <= 1'h0;
    end else begin
      if (_T_1811) begin
        if (_T_1803) begin
          s2_xcpt_if <= _T_1823;
        end
      end
    end
    if (reset) begin
      s2_speculative <= 1'h0;
    end else begin
      if (_T_1811) begin
        if (_T_1803) begin
          s2_speculative <= s1_speculative;
        end
      end
    end
    if (reset) begin
      s2_cacheable <= 1'h0;
    end else begin
      if (_T_1811) begin
        if (_T_1803) begin
          s2_cacheable <= tlb_io_resp_cacheable;
        end
      end
    end
  end
endmodule