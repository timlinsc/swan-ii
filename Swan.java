/* Swan.java
 * This is the primary program controlling the rest of the tools that generate the Swan reconfigurable fabric.
 * 
 * Known Issues:
 * - Under the correct circumstances, searching for an FSG could enter a loop leading to stack overflow.
 */


import java.io.*;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.Iterator;
import java.lang.Math.*;
import java.lang.Runtime;

public class Swan {
	/************************************
	 ***  LIBRARY-SPECIFIC CONSTANTS  ***
	 ***       CHANGE AS NEEDED       ***
	 ************************************/
	private static final String DFF_GATE = "DFF"; // in the labels file, DFFs must all contain this string.
	private static final String BASIC_LATCH = "DFFPOSX1"; // in the labels file, this is a vanilla DFF. I'm assuming this exists in the file.
	private static final String BASIC_XNOR = "XNOR2X1"; // in the labels file, this is a 2-input XNOR gate.
	private static final String BASIC_AND = "AND2X1"; // in the labels file, this is a 2-input AND gate.
	private static final String FIXED_GATE = "TIE"; // in the labels file, all gates tied to logical hi/lo contain this string
	private static final String BUFFER = "BUF"; // in the labels file, all buffers contain this string
	public static final String CLOCK = "CLK"; //in the library, what is the name of the clock port on a DFF?

	private static final int FSG_SIZE = 10; // expecting at most 10 nodes and 10 edges
	private static final int MIN_FSG_CNT = 100; // How many instances of each FSG do we expect?

	// Global values for indexes of specific gates and nodes
	public static int CGLB_OFFSET = 20;
	public static int INPUT = -1;
	public static int OUTPUT = -1;
	public static int CANARY_NET = -1; // canary inputs from preprocessor
	public static int VSS = -1;
	public static int VDD = -1;
	public static int DFF = -1; // The number corresponding to BASIC_LATCH
	public static int XNOR = -1; // The number corresponding to BASIC_XNOR
	public static int AND = -1; // The number corresponding to BASIC_AND

	// Enum values
	private static final int READ_FILE = 0;
	private static final int READ_FSG = 1;
	private static final int FIND_NODES = 2;
	private static final int READ_NODES = 3;
	
	// Target crossbar size
	public static int EQUIV_SET_SIZE = 2; // The max size of an Equivalence set. A crossbar can be as large as x^2, but no larger. 
	public static int SECURE_EQUIV_SET = 2; // The max size of an Equivalence set for SECURED gates

	// Use when sweeping % chance to detect (canary to logic ratio). When not in use, set to value of SECURE_EQUIV_SET.
	public static int SECURE_EQUIV_WITH_CANARIES = 3; // The final expected size of an equiv with canaries.
	public static int HEADROOM_FOR_CANARY_CHECKERS = (SECURE_EQUIV_WITH_CANARIES-SECURE_EQUIV_SET)/2; // Leave space in equivalence sets for the canary checker logic to be added post-hoc

 	public static Boolean TIMING_MODEL = false; // Expose internal wires for capacitance for timing modeling. 
	public static Boolean USE_MUXES = true; // Use muxes for crossbars when generating Verilog
	
	private static ArrayList<Gate> netlist;
	private static ArrayList<ArrayList<String>> fsgLines;
	private static ArrayList<subgraphPattern> mostFrequentList;

  public static void main(String[] args) throws IOException {
    //////// Read in the command line arguments
    // arg[0] - Name of .lg netlist graph file
    // arg[1] - EQUIV_SET_SIZE
    // arg[2] - SECURE_EQUIV_SET
    // arg[3] - SECURE_EQUIV_WITH_CANARIES
    // arg[4] - "-t" if present, signifies single-fuse timing model.
  	String file_name;
  	Boolean doSnippet = false;
  	if(args.length == 0) {
  		System.out.println("Using default parameters...");
  		file_name = "netlist.lg";
  	}
  	else {
  		file_name = args[0];
  		if(file_name.endsWith("sv"))
  			file_name = file_name.substring(0,file_name.length()-2) + "lg";
  		else if(file_name.endsWith(".v"))
  			file_name = file_name.substring(0,file_name.length()-1) + "lg";

  		if(args.length >= 4) {
  			EQUIV_SET_SIZE = Integer.parseInt(args[1]);
			SECURE_EQUIV_SET = Integer.parseInt(args[2]);
			SECURE_EQUIV_WITH_CANARIES = Integer.parseInt(args[3]);
    		if(args.length == 5 && args[4].equals("-t")) {
    			TIMING_MODEL = true;
		    	System.out.println("Running Swan for timing model generation.");
    		}
  		}
    }

    ///////// Display startup message.
    System.out.println("SWAN: Security With Ambiguous Netlists");
    System.out.println("created by Timothy Linscott\n");
  	System.out.println("Parameterizing fabric with "+SECURE_EQUIV_WITH_CANARIES+"/"+SECURE_EQUIV_SET+"/"+EQUIV_SET_SIZE);
  	System.out.println("Security Target:");
  	System.out.println("1. Attacker guesses location of target");
  	System.out.println("     P(attack success) = "+(1.0f/SECURE_EQUIV_SET));
  	System.out.println("     P(attack success)/P(attack detected) = "+(1.0f/(SECURE_EQUIV_WITH_CANARIES-SECURE_EQUIV_SET)));
  	System.out.println("2. Attacker analyzes configuration");
  	System.out.println("     % configurations compromised with 90um^2 trojan (in 45nm node): "+(400.0f/(EQUIV_SET_SIZE*EQUIV_SET_SIZE*SECURE_EQUIV_WITH_CANARIES*SECURE_EQUIV_WITH_CANARIES))+"%");
  	System.out.println("\n");
  	System.out.println("Launching SWAN toolchain");
    System.out.println("Loading from: "+file_name);

    // Instantiate important data structures
    ///////// Extract the constants for input and output ports and for DFFs
		FileInputStream fstream = new FileInputStream("gates.txt");
    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
    ArrayList<Integer> dff_types = new ArrayList<Integer>(5);
    ArrayList<Integer> fixed_types = new ArrayList<Integer>(5);
    ArrayList<Integer> buffer_types = new ArrayList<Integer>(5);
		String strLine;
	    
    while ((strLine = br.readLine()) != null) {
    	if(!strLine.startsWith("\t")) { // skip the port definitions
    		String[] tokens = strLine.split(" : ");
	    	if(strLine.startsWith("input")) {
	    		INPUT = Integer.parseInt(tokens[1]);
	    	}
	    	else if(strLine.startsWith("output")) {
	    		OUTPUT = Integer.parseInt(tokens[1]);
	    	}
	    	else if(strLine.startsWith("net")) {
	    		CANARY_NET = Integer.parseInt(tokens[1]);
	    	}
	    	else if(strLine.startsWith("VSS")) {
	    		VSS = Integer.parseInt(tokens[1]);
    			fixed_types.add(VSS);
	    	}
	    	else if(strLine.startsWith("VDD")) {
	    		VDD = Integer.parseInt(tokens[1]);
    			fixed_types.add(VDD);
	    	}
	    	else {
	    		int val = Integer.parseInt(tokens[1]);
	    		//identify type
	    		if(strLine.contains(DFF_GATE)) {
		    		dff_types.add(val);
		    		if(strLine.startsWith(BASIC_LATCH))
		    			DFF = val;
	    		}
	    		else if(strLine.contains(FIXED_GATE))
	    			fixed_types.add(val);
	    		else if(strLine.startsWith(BASIC_XNOR))
	    			XNOR = val;
	    		else if(strLine.startsWith(BASIC_AND))
	    			AND = val;
	    		else if(strLine.startsWith(BUFFER))
	    			buffer_types.add(val);

	    		// increment CGLB start point
	    		if (val >= CGLB_OFFSET)
	    			CGLB_OFFSET = val+1;
	    	}
	    }
    }
    System.out.println("CGLBs will be numbered starting from "+CGLB_OFFSET);

    br.close();

    ///////// Extract the names of ports
    HashMap<Integer, String> port_names;
    try {
		fstream = new FileInputStream("ports.txt");
	    br = new BufferedReader(new InputStreamReader(fstream));
	    port_names = new HashMap<Integer, String>();
	    
	    while ((strLine = br.readLine()) != null) {
	    	String[] toks = strLine.split(" ");
	    	port_names.put(new Integer(toks[0]), toks[1]);
	    }
		}
		catch (IOException e) {
			System.out.println("ports.txt has not been provided by preprocessor. Assuming no snippets.");
			port_names = new HashMap<Integer, String>(1);
		}

    //////// Read in the list of secured vertices passed from preprocessor
    HashSet<Integer> secured_set;
    try {
	    List<String> sec_list = Files.readAllLines(Paths.get("secure.txt"));
	    secured_set = new HashSet<Integer>(sec_list.size());
	    for(String s : sec_list)
	    	secured_set.add(new Integer(s));
		}
		catch (IOException e) {
			System.out.println("secure.txt has not been provided by preprocessor. Assuming no secure signals.");
			secured_set = new HashSet<Integer>(1);
		}

		//////// DEPRECATED
    //////// Read in the list of internal indices passed from preprocessor
    HashSet<Integer> internals_set = new HashSet<Integer>(1);


		//////// This set should be of length 1. It is the alarm signal from the canaries. The file is produced by the preprocessor.
    HashSet<Integer> canary_set;
    try {
	    List<String> canaries_list = Files.readAllLines(Paths.get("canaries.txt"));
	    canary_set = new HashSet<Integer>(canaries_list.size());
	    for(String s : canaries_list)
	    	canary_set.add(new Integer(s));
	    assert(canary_set.size()==1) : "There is not support for multiple inputs received from the canaries!";
		}
		catch (IOException e) {
			System.out.println("canaries.txt has not been provided by preprocessor. Assuming no canary signals.");
			canary_set = new HashSet<Integer>(1);
		}

    ///////// Read in the description of the snippets from lamppost.py
    ///////// TODO: Deprecate
    HashMap<Integer, Snippet> snippet_descriptors = readSnippetDescriptors();

    //////// Open the Frequent Subgraphs file from GraMi
    fstream = new FileInputStream("Output.txt");
    br = new BufferedReader(new InputStreamReader(fstream));

  	fsgLines = new ArrayList<ArrayList<String>>();
  	mostFrequentList = new ArrayList<subgraphPattern>();


    //First two lines (time and number of FSGs) don't matter
    strLine = br.readLine();
    strLine = br.readLine();


    int sg_num = 0; // current FSG index
    Boolean skip_me = false; // Skip the current FSG

    //Read file line-by-line
    while ((strLine = br.readLine()) != null)   {
    	String tokens[] = strLine.split(" ");

    	// Line is <FSG_number>:
    	if(tokens[0].endsWith(":")) {
    		if(skip_me)
    			skip_me = false;
    		// Initialize structures to hold new FSG
    		sg_num = Integer.parseInt(tokens[0].substring(0,tokens[0].length()-1));
    		mostFrequentList.add(sg_num, new subgraphPattern(FSG_SIZE, FSG_SIZE, sg_num));
    		fsgLines.add(new ArrayList<String>(2*FSG_SIZE));
    	}
    	// Node line structure: v <ID> <type>
    	else if(tokens[0].equals("v") && !skip_me) {
    		int type = Integer.parseInt(tokens[2]);
    		// If GraMi has identified an FSG that includes non-gate nodes, skip it. 
    		if(type == INPUT || type == OUTPUT || type == CANARY_NET) {
    			skip_me = true;
    			mostFrequentList.get(sg_num).valid = false;
    			continue;
    		}
    		mostFrequentList.get(sg_num).AddNode(type);
    		fsgLines.get(sg_num).add(strLine);
    		// Record whether this type contains a DFF
    		if(dff_types.contains(type) && !dff_types.contains(sg_num+CGLB_OFFSET))
    			dff_types.add(sg_num+CGLB_OFFSET);
    		else if(fixed_types.contains(type) && !fixed_types.contains(sg_num+CGLB_OFFSET))
    			fixed_types.add(sg_num+CGLB_OFFSET);
    	}
    	// Edge line structure: e <FROM> <TO> <LABEL>
    	else if(!skip_me) { //if (strLine.startsWith("e"))
    		mostFrequentList.get(sg_num).AddEdge(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]), Integer.parseInt(tokens[3]));
    		fsgLines.get(sg_num).add(strLine);
    	}
    }

    int sgs_found = sg_num;

    br.close();

    int gate_index = 0;
    ////// Parse the netlist to find the index we should start at when creating CGLBs
		// Open the file
		fstream = new FileInputStream(file_name);
		br = new BufferedReader(new InputStreamReader(fstream));

		// Scan the file to count the number of nodes in the netlist to correctly size array.
		while ((strLine = br.readLine()) != null) {
			String tokens[] = strLine.split(" ");
			if(strLine.startsWith("v") && gate_index < Integer.parseInt(tokens[1]))
				gate_index = Integer.parseInt(tokens[1]);
			// Finished with list of vertices. Break. 
			else if (strLine.startsWith("e"))
				break;
		}

		gate_index++;

   	////// Parse the netlist
		load_netlist(file_name, snippet_descriptors, secured_set, internals_set);
		Levelizer.Levelize(netlist, fixed_types);

		// Number of types of FSGs inserted into the design.
	    int fsgs_used = 0;

	    if(!mostFrequentList.isEmpty()) {
		    //////// Open the full output file from GraMi to retrieve all starting nodes for each FSG
		    fstream = new FileInputStream("output.log");
		    br = new BufferedReader(new InputStreamReader(fstream));

		    int state = READ_FILE;
		    ArrayList<String> check = new ArrayList<String>(2*FSG_SIZE);
		    // for each FSG, list the indices of gates in the netlist that we will use to start locating instances of the FSGs.
		    ArrayList<ArrayList<Integer>> starting_nodes = new ArrayList<ArrayList<Integer>>(sgs_found);
		    // Map Gate index to sorted list of possible FSGs
		    TreeMap<Integer, TreeSet<subgraphPattern>> gates_to_patterns = new TreeMap<Integer, TreeSet<subgraphPattern>>();
		    for (int i=0; i<=sgs_found; i++) {
		    	if(mostFrequentList.get(i).valid)
			    	starting_nodes.add(new ArrayList<Integer>(MIN_FSG_CNT));
			    else
			    	starting_nodes.add(new ArrayList<Integer>(0)); // Add the bare minimum to ensure proper functioning
		    }

		    doRead: while ((strLine = br.readLine()) != null)   {
		    	switch (state) {
		    		case READ_FILE: // Read through the file until we find points when GraMi prints potential FSGs
		    			if(strLine.startsWith("-------------------------")) {
		    				String tokens[] = strLine.split(": ");
		    				check.add(tokens[1]);
		    				state = READ_FSG;
		    			}
		    			break;

		    		case READ_FSG:
		    			// If line is empty, we have reached the end of the list of nodes+edges for this FSG
		    			if(strLine.isEmpty()) {
		    				// Check if this subgraph is in the final set without looking ahead. 
		    				pickFSG: for(int j=0; j<fsgLines.size(); j++) {
		    					if (fsgLines.get(j).size() == check.size()) {
		    						Boolean match = true;
		    						// Compare each line of this FSG with the lines of the final FSGs
		    						for(int i=0; i<fsgLines.get(j).size() && match; i++) {
		    							if(!fsgLines.get(j).get(i).equals(check.get(i))) {
		    								match = false;
		    							}
		    						}
		    						// Found a match with one of the FSGs in the final set
		    						if(match) {
		    							check.clear();
		    							if(!mostFrequentList.get(j).valid) { // Ignore this FSG
		    								System.out.println("Skipping bad FSG: "+j);
		    								state = READ_FILE;
		    							}
		    							else { // FSG doesn't contain non-gates. Proceed to identifying nodes for this FSG
			    							sg_num = j;
			    							state = FIND_NODES;
			    						}
		    							continue doRead;
		    						}
		    					}
		    				} //end pickFSG
		    				check.clear();
		    				state = READ_FILE;
		    			}
		    			else //if(strLine.startsWith("e") || strLine.startsWith("v"))
		    				check.add(strLine);
		    			break;

		    		case FIND_NODES:
		    			// Skip over stuff until we reach the actual node list
		    			if(strLine.startsWith("\tv ")) {
		    				String tokens[] = strLine.split(" ");
		    				String targetType = Integer.toString(mostFrequentList.get(sg_num).types.get(0));
		    				if(tokens[2].equals(targetType)) {
		    					starting_nodes.get(sg_num).add(Integer.decode(tokens[1])); // add the node ID to the list

		    					state = READ_NODES; 
		    				}
		    			}
		    			break;

		    		case READ_NODES:
		    			if(strLine.startsWith("\tv ") && strLine.endsWith(" "+Integer.toString(mostFrequentList.get(sg_num).types.get(0)))) {
		    				String tokens[] = strLine.split(" ");
		    				starting_nodes.get(sg_num).add(Integer.decode(tokens[1])); // add the node ID to the list
		    			}
		    			else if (strLine.startsWith("Freq")) {
		    				// Hit end of node list. Resume scanning for FSGs.
		    				state = READ_FILE;
		    			}
		    			break;

		    		default:
		    			state = READ_FILE;
		    	} //end switch
		    } //end doRead

		    br.close();


		    // Open the output stream where we will store a shortened list of FSGs that we use
		    FileOutputStream ofstream, fsg_templ_stream;
		    fsg_templ_stream = new FileOutputStream("fsg_templates.txt");

		    // Track which of the subgraphs we have successfully found
		    Boolean[] first = new Boolean[sgs_found+1];
		    for(int i=0; i<=sgs_found; i++)
		    	first[i] = true;


			System.out.println();
			System.out.println();

			System.out.println("Adding new gates starting with U"+(gate_index));

			// sort the subgraphs by size
			TreeMap<subgraphPattern, Integer> sortedFSGs = new TreeMap<subgraphPattern, Integer>();
			for(int s=0; s<=sgs_found; s++) {
				if(mostFrequentList.get(s).valid)
					sortedFSGs.put(mostFrequentList.get(s), s);
			}

			// Analyze where we lose FSGs
			int total_start_points = 0;
			for(int s=0; s<=sgs_found; s++)
				total_start_points += starting_nodes.get(s).size();
			int total_replaced = 0;
			int successfully_added = 0;

			// Explore the frequent subgraphs, and make replacements in the original structure. 
			// pick_fsg: for(int s=0; s<=sgs_found; s++) { // For each subgraph pattern
			pick_fsg: for(subgraphPattern subg : sortedFSGs.descendingKeySet()) {
				int s = sortedFSGs.get(subg);
				// We don't yet support FSGs of size > 9
				if(!subg.valid || subg.types.size()>9) {
					System.out.println("FSG "+s+" is not valid.");
					continue;
				}

				// If potentials for node #0 were found, skip this FSG
				if(starting_nodes.get(s).size() == 0) {
					System.out.println("*******No starting points for "+s+"!!!");
					continue;
				}

				// Create a list of node #0's, sorted by index in the netlist.
				TreeSet<Gate> sorted_start_nodes = new TreeSet<Gate>();
				for(int j=0; j<starting_nodes.get(s).size(); j++) { // For each starting node
					int start = starting_nodes.get(s).get(j);
					Gate g = netlist.get(start);
					if(g.Valid() && g.Label() == subg.types.get(0)) {
						sorted_start_nodes.add(g);
					}
				}

				Iterator<Gate> it = sorted_start_nodes.iterator();
				int successes_so_far = 0;
				while(it.hasNext()) {
					Gate g = it.next();
					if(g.Valid()) {
						// Begin searching near g for an instance of the FSG
						HashMap<Gate, Integer> found = new HashMap<Gate, Integer>(FSG_SIZE);
						found.put(g, 0);
						ArrayList<Edge> resolved = new ArrayList<Edge>(FSG_SIZE);
						Boolean success = false;
						// On rare occasion, loops in the explore algorithm, may cause search to recurse endlessly.
						// This bug may have been resolved, but we continue to guard against it. 
						try {
							success = explore(subg, 0, g, found, resolved);
						}
						catch(java.lang.StackOverflowError e) {
							System.out.println("Critical failure exploring fsg #"+s);
							continue pick_fsg;
						}

						// Don't insert copies of the FSG unless we have enough to build one complete set
						// Otherwise, we would need to insert lots of canaries, defeating the cost savings of the FSG
						if(success) {
							// Test to see whether we've accumulated enough successes
							if(successes_so_far < EQUIV_SET_SIZE) {
								successes_so_far++;
								continue;
							}
							if(successes_so_far == EQUIV_SET_SIZE) {
								successes_so_far++;
								it = sorted_start_nodes.iterator();
								continue;
							}

							// Replace the matched gates with the FSG's corresponding CGLB.
							if(found.size() == subg.types.size()) {
								Boolean gate_sec = false;
								for(Gate f : found.keySet()) {
									assert(!f.isSnippet()) : "Tried to put a snippet into a CGLB!";
									if(f.Secured()) {
										gate_sec = true;
										break;
									}
								}

								//sort the gates
								ArrayList<Gate> cglb_gates = new ArrayList<Gate>(found.size());
								for(int i=0; i<found.size(); i++) {
									for(Gate k : found.keySet())
										if(found.get(k)==i) {
											cglb_gates.add(k);
											break;
										}
								}
								total_replaced += cglb_gates.size();

								// Create the CGLB gate
								Gate cglb = new Gate(cglb_gates, subg, s+CGLB_OFFSET, gate_index, gate_sec);
								netlist.add(cglb);
								successfully_added++;
								gate_index++;
								// Remove the replaced gates from the netlist
								for(Gate f : cglb_gates) {
									f.setValid(false);
								}

								// If this was the first time we inserted this FSG, include the FSG in our record of which were used
								if(first[s]) {
									fsgs_used++;
									first[s] = false;
									String header = s+":\n";
									fsg_templ_stream.write(header.getBytes());
									for(Gate piece: cglb_gates) {
										fsg_templ_stream.write(piece.VertexString().getBytes());
									}
									for(Gate piece: cglb_gates) {
										fsg_templ_stream.write(piece.EdgesString(cglb_gates).getBytes());
									}
								}
							}
						}
					}
				}
			}

			// We kept a list of FSGs that used DFFs.
			// clean up the DFF list, removing unused FSGs
			for(int i=0; i<sgs_found; i++) {
				if(first[i])
					dff_types.remove(Integer.valueOf(i+CGLB_OFFSET));
			}

			System.out.println("Out of a total "+total_start_points+" start points, "+successfully_added+" CGLBs were added to the final design.");
			System.out.println(total_replaced+" total gates were replaced by FSGs.");
		}
		else {
			System.out.println("\n>>>>>>>>>WARNING<<<<<<<<<");
			System.out.println("GraMi identified no FSGs!");
			System.out.println("Skipping CGLB flow...\n");
		}

		// Remove all buffers from the netlist. Buffers in FSGs will remain for ease of use.
		for(Gate g : netlist) {
			if(g.Valid() && buffer_types.contains(g.Label()))
				g.Short();
		}

		for(Gate g : netlist)
			g.setLayer(Gate.UNSET);
		Levelizer.Levelize(netlist, fixed_types);

		//////// Add the LFSR used to drive the canaries
		Gate[] lfsr = null;
	    if(!secured_set.isEmpty()) { // Only if we will be using Canaries
	    	int max_ports = 0;
	    	for(Gate g : netlist)
	    		if(max_ports < g.Inputs().size())
	    			max_ports = g.Inputs().size();
	    	System.out.println("Canary LFSR sized to "+2*max_ports);
	    	lfsr = CanaryManager.makeLFSR(netlist, 2*max_ports);
	    }

		System.out.println("Adding canaries starting with U"+(gate_index));
		System.out.println(fsgs_used+" types of FSGs were used in the final design.");

		////////// Call `python generate_fsg_verilog.py` to create the CGLBs in Verilog
		System.out.println("Calling python generate_fsg_verilog.py");
		String[] cmd = {
				"python3.6", 
				"generate_fsg_verilog.py",
				"fsg_templates.txt", 
				"./components/frequentSubgraphs.sv"
		    };
		Process p = Runtime.getRuntime().exec(cmd);
		BufferedReader python_output = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while((strLine = python_output.readLine()) != null) {
			System.out.println(strLine);
		}


		////////// Deprecate???
		////////// Re-levelize and retime the module
		// for(Gate g : netlist)
		// 	g.setLayer(Gate.UNSET);
		// int max_level = Levelizer.Levelize(netlist, fixed_types, dff_types);
		// Levelizer.retime(netlist, max_level/2, dff_types);

		// System.out.println("Added "+(netlist.size()-gate_index)+" registers to retime the module.");

		// Group gates into equivalence sets of identitical types
		System.out.println("Calling setFinder to construct equivalence sets");
		setFinder.findEquivalenceSets(netlist);

		// If the user defined secure gates and a hook for canaries, insert canary gates. 
		if(!secured_set.isEmpty() && !canary_set.isEmpty()) { // If we are using canaries
			System.out.println("Performing Canary generation.");
			Gate alarm_port = netlist.get(canary_set.iterator().next());
			assert(alarm_port.Label() == CANARY_NET) : "Canary alarm port is not labeled "+ CANARY_NET;
			System.out.println("Calling CanaryManager to insert canaries.");
			CanaryManager.insertCanaries(netlist, lfsr, alarm_port, fixed_types);
			assert(!alarm_port.Valid());
		}
		else if(!canary_set.isEmpty()) {
			System.out.println("Removing alarm port.");
			Gate alarm_port = netlist.get(canary_set.iterator().next());
			alarm_port.setValid(false);
			assert(!alarm_port.Valid());
		}
		else
			assert(false) : "No alarm port given!";


		// Generate a mapping from the logical design to the physical fabric
		mapGates(netlist);

		// Generate a Verilog netlist for the final fabric.
		int xb_wires = VerilogGenerator.GenerateVerilog(netlist, port_names, fixed_types);

		System.out.println("Swan.java complete!\n");
		FileOutputStream ofstream = new FileOutputStream("./cost.bin");
		ofstream.write((""+xb_wires).getBytes());
	} //end main()

	/* Explore the netlist, attempting to find a set of gates satisfying the given FSG. Recurses until all nodes/edges in fsg are found.
	 * fsg - the FSG pattern to search for
	 * focus - the node index in fsg we are currently looking for
	 * focusGate - the gate in the netlist we are beginning the search from
	 * found - mapping of gates in the netlist and the indices of nodes in the FSG they correspond to.
	 * resolvedEdges - edges in the FSG pattern we have identified in the exploration session
	 */
	private static Boolean explore(subgraphPattern fsg, int focus, Gate focusGate, HashMap<Gate, Integer> found, ArrayList<Edge> resolvedEdges)
	{
		// New values for found and resolvedEdges
		HashMap<Gate, Integer> tentative_found = new HashMap<Gate, Integer>(found);
		ArrayList<Edge> tentative_resolved = new ArrayList<Edge>(resolvedEdges);

		ArrayList<Edge> inputs_to_explore = fsg.TargetInputs(focus, -1); //unless I remove support for prior
		inputs_to_explore.removeAll(resolvedEdges);
		// for each input in the pattern
		for(Edge inputEdge : inputs_to_explore) {
			if(tentative_resolved.contains(inputEdge)) 
				continue;

			tentative_resolved.add(inputEdge);
			// get possible matches
			ArrayList<Gate> connections = focusGate.Inputs();
			ArrayList<Integer> wireLabels = focusGate.InputLabels();
			Boolean success = false;
			for(int i=0; i<connections.size(); i++) {
				if( connections.get(i).Valid() && fsg.types.get(inputEdge.start) == connections.get(i).Label() && inputEdge.label == wireLabels.get(i) 
					&& (!tentative_found.containsKey(connections.get(i)) || tentative_found.get(connections.get(i))==inputEdge.start) ) 
				{
					tentative_found.put(connections.get(i), inputEdge.start);
					
					success = explore(fsg, inputEdge.start, connections.get(i), tentative_found, tentative_resolved);
					if (success)
						break; //go to next edge
					else
						tentative_found.remove(connections.get(i)); // wrong assumption about next Gate.
				}
			}
			if(!success){ // Searched all connections, none matched. 
				return false;
			}
		}


		ArrayList<Edge> outputs_to_explore = fsg.TargetOutputs(focus, -1); //unless I remove support for prior
		outputs_to_explore.removeAll(tentative_resolved);
		for(Edge outputEdge : outputs_to_explore){
			if(tentative_resolved.contains(outputEdge)) 
				continue;

			tentative_resolved.add(outputEdge);
			// get possible matches
			ArrayList<Gate> connections = focusGate.Outputs();
			ArrayList<Integer> wireLabels = focusGate.OutputLabels();
			Boolean success = false;
			for(int i=0; i<connections.size(); i++) {
				if( connections.get(i).Valid() && fsg.types.get(outputEdge.end) == connections.get(i).Label() && outputEdge.label == wireLabels.get(i)
					&& (!tentative_found.containsKey(connections.get(i)) || tentative_found.get(connections.get(i))==outputEdge.end) )
				{
					tentative_found.put(connections.get(i), outputEdge.end);

					success = explore(fsg, outputEdge.end, connections.get(i), tentative_found, tentative_resolved);
					if (success) 
						break;
					else
						tentative_found.remove(connections.get(i));
				}
			}
			if(!success) {
				return false;
			}
		}

		found.putAll(tentative_found);
		resolvedEdges.addAll(resolvedEdges.size(), tentative_resolved);
		return true;
	}

	/* Load in the netlist from a graph file. Writes to global netlist ArrayList
	 * file_name - string name of file to open
	 * snippet_descriptors - DEPRECATED
	 * secured_set - all gates (netlist indices) marked as secure
	 * internals_set - DEPRECATED
	 */
	private static void load_netlist(String file_name, HashMap<Integer, Snippet> snippet_descriptors, HashSet<Integer> secured_set, HashSet<Integer> internals_set) throws IOException {
		System.out.println("Processing: " + file_name);
		// Open the file containing the netlist
		FileInputStream fstream = new FileInputStream(file_name);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;

		// Estimate how large the final netlist will be
		int size = 0;
		while ((strLine = br.readLine()) != null)
			if(strLine.startsWith("v"))
				size++;
		// Best guess given that we will add CGLBs and canaries
		netlist = new ArrayList<Gate>(size+size/2);

		// Restart the stream from the beginning.
		fstream = new FileInputStream(file_name);
		br = new BufferedReader(new InputStreamReader(fstream));
		strLine = br.readLine();


		// Verify this is the format we expect
		if(! strLine.startsWith("# t 1")) {
			System.out.println("Bad File: "+file_name);
			System.exit(-1);
		}

		//Read file line-by-line
		while ((strLine = br.readLine()) != null)   {
			// System.out.println (strLine);
			String tokens[] = strLine.split(" ");

			// Node: v <ID> <type>
			if(tokens[0].equals("v")) {
				int vertex_index = Integer.parseInt(tokens[1]);
				netlist.add(new Gate(Integer.parseInt(tokens[2]), vertex_index,	secured_set.contains(vertex_index) ));
				if(snippet_descriptors.containsKey(vertex_index)) {
					System.out.println("Added snippet at "+netlist.get(vertex_index).Index()+" of type "+netlist.get(vertex_index).Label());
					netlist.get(vertex_index).setupSnippet(snippet_descriptors.get(vertex_index));
					assert(netlist.get(vertex_index).isSnippet()) : "Failed to create snippet at "+netlist.get(vertex_index).Index()+" of type "+netlist.get(vertex_index).Label();
				}
			}
			// Edge: e <FROM> <TO> <label>
			else { //if (strLine.startsWith("e"))
				// Identify edges and wire gates together
				int fromID = Integer.parseInt(tokens[1]);
				int toID = Integer.parseInt(tokens[2]);
				int label = Integer.parseInt(tokens[3]);

				Gate from = netlist.get(fromID);
				Gate to = netlist.get(toID);
				from.AddOutput(to, label);
				to.AddInput(from, label);
			}
		}

		System.out.println("Read in " + netlist.size() + " gates from "+file_name);
		
		//Close the input stream
		br.close();
	}

	// Print a vertex and all outputs going to any node in graph
	private static int countConnections(ArrayList<Gate> graph) {
		int connect = 0;
		for (Gate g: graph) {
			for(int i=0; i< g.Outputs().size(); i++) {
				if(graph.contains(g.Outputs().get(i)))
					connect++;
			}	
		}
		return connect;
	}

	/* Map gates to physical resources. Since each gate in the logical design corresponds to a gate in the physical fabric
	 * the Gate object represents both physical and logical objects. The mapping allows the Gate object to both serve as position
	 * in the list of outputs to a crossbar network and the logical mapping onto that position
	 * netlist - the list of Gates in the design, with each valid gate assigned to an equivalance set
	 */
	public static void mapGates(ArrayList<Gate> netlist) {
		Boolean conn_to_out = false;
		for(Gate g : netlist) {
			if(g.Valid() && g.Mapping() == -1 && g.Equivalents() != null) {
				ArrayList<Integer> mappings = Util.getShuffledRangeList(g.Equivalents().size());

				for(int i=0; i<g.Equivalents().size(); i++) {
					//	 logical implemented by physical g, physical implementing logical g
					Gate eq = g.Equivalents().get(i);
					if(USE_MUXES)
						eq.Map(i, i);
					else
						eq.Map(mappings.get(i), mappings.indexOf(i));
				}
			}
		}
	}

	// TODO: Deprecate. 
	private static HashMap<Integer, Snippet> readSnippetDescriptors() throws IOException {
		int INITIAL_SNIPPETS = 4; // how many snippets do we expect there to be?
		int INITIAL_SNIPPET_PORTS = 100; // how many ports per snippet do we expect there to be?

		try {
			FileInputStream fstream = new FileInputStream("snippets.txt");
		    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		    String strLine;
		    
		    HashMap<Integer, Snippet> snippet_descriptors = new HashMap<Integer, Snippet>(INITIAL_SNIPPETS);
		    Integer snip_num = null;
		    Snippet snip = null;
		    ArrayList<String> port_names = new ArrayList<String>(INITIAL_SNIPPET_PORTS);
			ArrayList<String[]> connections = new ArrayList<String[]>(INITIAL_SNIPPET_PORTS);
			String[] c_dump = new String[1];
			ArrayList<String> connection_list = new ArrayList<String>(INITIAL_SNIPPET_PORTS);

			// int current_port = 0;

		    while((strLine = br.readLine()) != null)   {
		    	if(strLine.startsWith("  ")) {
		    		//read in wire/edge
		    		connection_list.add(strLine.trim());
		    	}
		    	else if (strLine.startsWith(" ")) {
		    		// read in port
		    		if(!connection_list.isEmpty()) {
		    			connections.add(connection_list.toArray(c_dump));
		    			if(connection_list.size() == 1)
		    				c_dump = new String[1];
		    			connection_list.clear();
		    		}

		    		port_names.add(strLine.trim());
		    	}
		    	else {
		    		if(snip_num != null) {
		    			connections.add(connection_list.toArray(c_dump));
		    			if(connection_list.size() == 1)
		    				c_dump = new String[1];
		    			connection_list.clear();
			    		snippet_descriptors.put(snip_num, new Snippet(port_names, connections));
			    		port_names = new ArrayList<String>(INITIAL_SNIPPET_PORTS);
						connections = new ArrayList<String[]>(INITIAL_SNIPPET_PORTS);
		    		}
		    		snip_num = new Integer(strLine);
		    	}
		    }
		    if(snip_num != null) {
	    		connections.add(connection_list.toArray(c_dump));
				connection_list.clear();
	    		snippet_descriptors.put(snip_num, new Snippet(port_names, connections));
		    }

	    	br.close();
	    	return snippet_descriptors;
    	}
		catch(FileNotFoundException e) {
			System.out.println("snippets.txt does not exist. Assuming no snippets");
			return new HashMap<Integer, Snippet>(0);
		}
	}
} //end class