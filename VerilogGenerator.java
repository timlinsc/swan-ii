import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Random;
import java.lang.Math.*;

class VerilogGenerator {
	private static final String CLOCK = Swan.CLOCK; // In a netlist, what is the clock port on a DFF called?
	private static int CGLB_OFFSET = Swan.CGLB_OFFSET; // Label for m0
	private static final int MAX_SYMM_PORTS = 4; // largest number of ports in a gate that are interchangeable.
	private static final Boolean ADD_TROJAN = false;


	public static int GenerateVerilog(ArrayList<Gate> netlist, HashMap<Integer, String> port_names, ArrayList<Integer> fixed_types) throws IOException
	{
		System.out.println("\nGenerating Verilog output...");
		String crossbar_name = Swan.USE_MUXES ? "crossbar_mux" : "crossbar_otp_lite";
		int num_inputs = 0;
		int num_internal_inputs = 0;
		int num_gate_inputs = 0;
		int num_outputs = 0;
		int outputs_assigned = 0;
		int max_index = 0;
		int min_index = Integer.MAX_VALUE;
		int max_port_wire = 1;
		// Load in gate name to ID# mapping
		HashMap<Integer, String> gate_types = getLabelMap();
		HashMap<String, GatePorts> gate_ports = createTemplates();
		HashSet<String> all_inpts = new HashSet<String>();
		HashSet<String> all_outpts = new HashSet<String>();

		Random rand = new Random(Util.seed_numbers);

		// infer the number of ports and wires
		int num_gates_est = netlist.size();
		for(Gate g : netlist) {
			if(g.Valid() && !g.Inputs().isEmpty()) {
				if(g.Index() > max_index)
					max_index = g.Index();
				if (g.Index() < min_index)
					min_index = g.Index();
				if(g.Label() != Swan.OUTPUT && g.Label() != Swan.INPUT && g.Label() != Swan.CANARY_NET /*&& g.Label() != Swan.CANARY_DRIVER*/)
					num_gate_inputs += g.Inputs().size();
			}
			if(g.Valid() && g.Label() == Swan.INPUT) {
				if(!g.Secured()) {
					num_inputs++;
					if(port_names.get(g.Index()) != null) {
						all_inpts.add(port_names.get(g.Index()));
					}

				}
				else
					num_internal_inputs++;
			}
			if(g.Valid() && g.Label() == Swan.OUTPUT  && !g.Inputs().isEmpty()) {
				num_outputs++;
				if(port_names.get(g.Index()) != null) {
					all_outpts.add(port_names.get(g.Index()));
				}
			}

		}

		// prep output files
		FileOutputStream ofstream = new FileOutputStream("./components/swan_full_custom.sv");
		FileOutputStream ofstream_cfg = new FileOutputStream("./components/swan_config.sv");

		int out_cntr=0;
		int in_cntr=0;
		int internal_in_cntr=0;
		int snippet_wire_cntr=0;
		int xb_wire_cnt = 0;
		int xb_wire_start;
		int total_cfg_bits = 0;
		int cfg_bits_next = 0;

		// bookkeeping
		int total_inputs = 0;
		int sum_sq_inputs = 0;
		int total_muxes = 0;
		int max_xbar_in = 0;
		int max_xbar_out = 0;

		int max_in_width = 0;
		int max_out_width = 0;

		int muxes_2x1 = 0; // how many 2x1 muxes would it take to implement all our crossbars?

		int line_cntr = 0;
		String out = "";

		//For timing model: wires exposed from crossbars to outside world.
		int tgate_wire_idx = 0;

		//Used to associate input Gates to the numbers of the input vector. 
		// I'm not thrilled with this design decision.
		HashMap<Integer, Integer> input_names = new HashMap<Integer, Integer>(num_inputs+num_internal_inputs);


		// The configurations of each mux, stored as an assignment in a verilog file
		ArrayList<String> cfg_bit_assignments = new ArrayList<String>(10000);
		int snipp_cntr = 0; // number of snippets instantiated in this module so far

		// In this port, a gate that feeds the trojan target also feeds another gate in the Eqv
		// should be initialized to all false at the beginning. 
		// Boolean[] portHasOverlap = new Boolean[MAX_SYMM_PORTS];

		TreeSet<String> wires_to_add = new TreeSet<String>();

		///////////////////////////////////
		/// BEGINNING VERILOG GENERATION //
		///////////////////////////////////

		for(Gate g : netlist) {
			if(!g.Valid() || g.Label()==Swan.INPUT) 
				continue;

			if(!fixed_types.contains(g.Label())) {
				// write Verilog for the crossbar
				out = "";
				xb_wire_start = xb_wire_cnt;
				int cfg_bits_start = total_cfg_bits;


				int port_cntr;

				// Generate the crossbar for each port
				for(Integer lab : g.getSinkLabelsAsSortedSet()) {
					int[] lab_arr = Label.sinkLabelfromPartial(lab);
					int count_of_sink_label = g.countSinkLabel(lab);
					//Separate inputs from ports A,B,C...
					for(int p=0; p<count_of_sink_label; p++) { // for each port, for real this time. 
						wires_to_add.clear();
						String[] chosen_wires = new String[g.Equivalents().size()]; // chosen wire to input to each gate in set

						// get all the incoming wires
						for(Gate eq : g.Equivalents()) {
							// Get index in eqv set of the gate mapped to this gate.
							int mapped_here = g.Equivalents().indexOf(eq.MappingOfLogical());
							assert (eq.Equivalents().contains(g)) : g.Index()+" is equated with "+eq.Index()+" but not the other way around! g.eq.size="+g.Equivalents().size()+"   eq.Eqs.size="+eq.Equivalents().size();
							int matches_seen = 0;
							for(int i=0; i<eq.Inputs().size() && matches_seen<=p; i++){ //(Gate in : eq.Inputs())
								if(!Label.sinkMatches(lab_arr, eq.InputLabels().get(i)))
									continue;
								if((matches_seen++) != p ) 
									continue;

								Gate in = eq.Inputs().get(i);

								assert(in.Label() != Swan.CANARY_NET) : "Canary nets are used strictly internally. Verilog generation using them is deprecated.";

								int symm_here = Label.srcLabelfromFull(eq.InputLabels().get(i))[Label.SYMM];
								int gate_here = Label.srcLabelfromFull(eq.InputLabels().get(i))[Label.SUBGATE];
								if(in.Equivalents() != null && in.Label()!=Swan.OUTPUT && !fixed_types.contains(in.Label())) { // normal gate
									assert(in.Equivalents().contains(in.MappingOfLogical())) : in.Index()+" does not contain its mapping: "+(in.MappingOfLogical()!=null ? in.MappingOfLogical().Index() : "null");
									for(Gate in_eq : in.Equivalents()) {
										// PARTIALS
										// if(skip_xcnt && in_eq != in)
										// 	continue;

										String w;
										if(gate_here==0)
											w = "w["+in_eq.Index()+"]["+symm_here+"]";
										else {
											w = "w["+in_eq.Index()+"]["+((gate_here-1)+10*(symm_here))+"]";

											// Test that that wire exists.
											assert(gate_ports.get("m"+(in_eq.Label()-CGLB_OFFSET)) != null) : "Mistook type "+in_eq.Label()+" for CGLB! "+in_eq.Index();
											Boolean found = false;
											String[] ports = gate_ports.get("m"+(in_eq.Label()-CGLB_OFFSET)).outputs;
											for(String port : ports) {
												if(port.equals("OUT"+gate_here+""+symm_here)) {
													found = true;
													break;
												}
											}

											assert(found) : w+" does not exist for type "+in_eq.Label();
										}
										wires_to_add.add(w);
										if(in_eq == in.MappingOfLogical())
											chosen_wires[mapped_here] = w;
									}
								}
								else {
									if(in.Label()==Swan.INPUT && !in.Secured() || in.Label()==Swan.OUTPUT) { // We know this is an input
										wires_to_add.add(port_names.get(in.Index()));
									}
									else if(in.Label()==Swan.VSS) {
										wires_to_add.add("1'b1");
										// portHasOverlap[port_cntr] = true; // Fixed values can theoretically be used by canaries. 
									}
									else if(in.Label()==Swan.VDD) {
										wires_to_add.add("1'b0");
										// portHasOverlap[port_cntr] = true; // Fixed values can theoretically be used by canaries. 
									}
									else if(fixed_types.contains(in.Label())) {
										wires_to_add.add("w["+in.Index()+"]["+symm_here+"]");
										// portHasOverlap[port_cntr] = true; // Fixed values can theoretically be used by canaries. 
									}
									else if(in.Label()==Swan.INPUT) { // We know this is an internal input
										if(input_names.containsKey(in.Index()))
											wires_to_add.add("internal["+input_names.get(in.Index())+"]");
										else {
											input_names.put(eq.Inputs().get(i).Index(), internal_in_cntr);
											wires_to_add.add("internal["+internal_in_cntr+"]");
											internal_in_cntr++;
										}
									}
									else
										assert(!in.Valid()) : "Invalid gate detected! idx: "+in.Index()+" Label: "+in.Label();
									// PARTIALS
									chosen_wires[mapped_here] = wires_to_add.first(); // The chosen wire is the only element in the array.
									// chosen_wires[mapped_here] = wires_to_add.get(0); // The chosen wire is the only element in the array.
								}

								assert(!(in.Label()==Swan.OUTPUT || in.Label()==Swan.INPUT) || !wires_to_add.first().startsWith("w["));
							} // for(in : eq.Inputs)
							assert(chosen_wires[mapped_here]!=null) : "Failed to assign inputs for gate #"+mapped_here+" ("+g.Equivalents().get(mapped_here).Index()+","+g.Label()+"), p="+p+", lab="+lab+"\n"+java.util.Arrays.toString(chosen_wires);
						} // for(eq : g.Equivalents())

						// write out current crossbar
						// Generate the verilog for this gate/port and its crossbar(s)
						if(g.Label() != Swan.OUTPUT) {
							assert(wires_to_add.size()>0) : "Found no connections for "+g.Index()+"  type: "+g.Label()+"  port: "+lab+"  Equivalents: "+g.Equivalents().size()+"  p: "+p+"\nWire Names: "+wires_to_add.toString();
							int num_outs = g.Equivalents().size();
							if(wires_to_add.size() == 1) {
								if(Swan.TIMING_MODEL) {
									out += "\tSET_TGATE xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+"_A ( .Y(internal["+tgate_wire_idx+"]), .A("+wires_to_add.iterator().next()+") );\n";
									out += "\tSET_FUSE  xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+"_B ( .D(internal["+(tgate_wire_idx+1)+"]), .S(internal["+tgate_wire_idx+"]) );\n";
									out += "\tSET_TGATE xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+"_C ( .Y(xb_out["+xb_wire_cnt+"]), .A(internal["+(tgate_wire_idx)+"]) );\n";
									tgate_wire_idx += 2;
								}
								else
									out += "\tassign xb_out["+(xb_wire_cnt+num_outs-1)+":"+xb_wire_cnt+"] = {"+num_outs+"{"+wires_to_add.iterator().next()+"}};\n";
							}
							else {
								total_inputs += wires_to_add.size();
								sum_sq_inputs += wires_to_add.size() * wires_to_add.size(); // used to compute variance of mux size
								// out += "\tcrossbar #("+wires_to_add.size()+","+num_outs+") xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+
								// if(skip_xcnt)
								// 	out += "\twires #(.WIDTH("+num_outs+")) xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+
								// 		" ( .out(xb_out["+(xb_wire_cnt+num_outs-1)+":"+xb_wire_cnt+"]), .in({ ";
								// else
								out += "\t"+crossbar_name+" #(.IN_WIDTH("+wires_to_add.size()+"), .OUT_WIDTH("+num_outs+")) xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+
										" ( .out(xb_out["+(xb_wire_cnt+num_outs-1)+":"+xb_wire_cnt+"]), .in({ ";



								Iterator<String> it;	
								if(Swan.USE_MUXES)
									it = wires_to_add.descendingIterator();
								else
									it = wires_to_add.iterator();
								while(it.hasNext()) {
									out += it.next();
									out += (it.hasNext() ? ", " : " ");
								}
								if (Swan.USE_MUXES)
									cfg_bits_next = total_cfg_bits + Util.ceil_log2(wires_to_add.size())*num_outs; // clog2(M)*N
								else
									cfg_bits_next = total_cfg_bits + wires_to_add.size()*num_outs; // M*N cfg bits for MxN crossbar
								// out += "}), .cfg(CFG["+(cfg_bits_next-1)+":"+total_cfg_bits+"])  );";
								// if(skip_xcnt)
								// 	out += "}) );";
								// else
								if(Swan.USE_MUXES)
									out += "}), .cfg(CFG["+(cfg_bits_next-1)+(cfg_bits_next-1 == total_cfg_bits ? "" : ":"+total_cfg_bits)+"]) );\n";
								else
									out += "}), .we(we["+total_muxes+"]), .not_we(~we["+total_muxes+"]), .rDataG(rSel["+(num_outs-1)+
												 ":0]), .rDataD(rSel["+(num_outs-1)+":0]), .cData(cSel["+(wires_to_add.size()-1)+":0]) );\n";

								assert(g.Label() == g.MappingOfLogical().Label());

								if(wires_to_add.size() > max_in_width)
									max_in_width = wires_to_add.size();
								if(num_outs > max_out_width)
									max_out_width = num_outs;
								total_muxes++;
								muxes_2x1 += (wires_to_add.size() - 1) * num_outs;

							}
							xb_wire_cnt += num_outs;
						}
						else { // OUPUT
							assert(wires_to_add.size()>0) : "Found no connections for "+g.Index()+"  type: "+g.Label()+"  port: "+lab+" San test: "+(g.Inputs().get(0).OutputLabels());

							if(wires_to_add.size() == 1) {
								if(Swan.TIMING_MODEL) {
									out += "\tSET_TGATE xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+"_A ( .Y(internal["+tgate_wire_idx+"]), .A("+wires_to_add.iterator().next()+") );\n";
									out += "\tSET_FUSE  xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+"_B ( .D(internal["+(tgate_wire_idx+1)+"]), .S(internal["+tgate_wire_idx+"]) );\n";
									out += "\tSET_TGATE xbar"+g.Index()+"_"+lab+(p==0 ? "" : "_"+p)+"_C ( .Y(xb_out["+xb_wire_cnt+"]), .A(internal["+(tgate_wire_idx)+"]) );\n";
									tgate_wire_idx += 2;
								}
								else
									out += "\tassign "+port_names.get(g.Index())+" = "+wires_to_add.iterator().next()+";\n";
							}
							else { // wires_to_add.size() > 1
								// assert(!skip_xcnt) : "Should have skipped interconnect on "+port_names.get(g.Index())+", but have "+wires_to_add.size()+" possible inputs.";
								total_inputs += wires_to_add.size();
								sum_sq_inputs += wires_to_add.size() * wires_to_add.size(); // used to compute variance of mux size
								// out += "\tcrossbar #("+wires_to_add.size()+",1) xbar"+g.Index()+" ( .out("+port_names.get(g.Index())+"), .in({ ";
								out += "\t"+crossbar_name+" #(.IN_WIDTH("+wires_to_add.size()+"), .OUT_WIDTH(1)) xbar"+g.Index()+" ( .out("+port_names.get(g.Index())+"), .in({ ";

								muxes_2x1 += wires_to_add.size() - 1; 

								Iterator<String> it = wires_to_add.iterator();
								while(it.hasNext()) {
									out += it.next();
									out += (it.hasNext() ? ", " : " ");
								}
								if (Swan.USE_MUXES)
									cfg_bits_next = total_cfg_bits + Util.ceil_log2(wires_to_add.size()); // clog2(M)*N
								else
									cfg_bits_next = total_cfg_bits + wires_to_add.size(); // M*N cfg bits for MxN crossbar
								// out += "}), .cfg(CFG["+(cfg_bits_next-1)+":"+total_cfg_bits+"])  );";
								// if(skip_xcnt)
								// 	out += "}) );";
								// else
								if(Swan.USE_MUXES)
									out += "}), .cfg(CFG["+(cfg_bits_next-1)+(cfg_bits_next-1 == total_cfg_bits ? "" : ":"+total_cfg_bits)+"]) );\n";
								else
									out += "}), .we(we["+total_muxes+"]), .not_we(~we["+total_muxes+"]), .rDataG(rSel[0]), .rDataD(rSel[0]), .cData(cSel["+(wires_to_add.size()-1)+":0]) );\n";

								int bitwidth = cfg_bits_next - total_cfg_bits;
								if(wires_to_add.size() > max_in_width)
									max_in_width = wires_to_add.size();
								total_muxes++;
							}
							out_cntr++;
						}

						// generate configuration bits
						if(wires_to_add.size() > 1) {
							for(int i=0; i<chosen_wires.length; i++) {
								String wire_name = chosen_wires[i];
								assert(wire_name!=null) : "Failed to choose wire for input "+i+" while processing gate of type "+g.Label();
								int cfg = 0;
								for(String w : wires_to_add) {
									if(wire_name.equals(w))
										break;
									else
										cfg++;
								}
								if(Swan.USE_MUXES) {
									int low = total_cfg_bits + i * Util.ceil_log2(wires_to_add.size());
									int high = low + Util.ceil_log2(wires_to_add.size())-1;
									cfg_bit_assignments.add("\tassign CFG["+high+(low == high ? "" : ":"+low)+"] = "+cfg+";\n");
								}
								else {
									String cfg_bits = "";
									for(int j=0; j<wires_to_add.size(); j++)
										cfg_bits += (j==cfg) ? "1" : "0";
									cfg_bit_assignments.add("\tassign CFG["+((i+1)*cfg_bits.length()+total_cfg_bits-1)+":"+(i*cfg_bits.length()+total_cfg_bits)+"] = "+cfg_bits.length()+"'b"+cfg_bits+";\n");
								}
							}
						}

						// All done for outputs. No need to write out gates.
						if(g.Label() == Swan.OUTPUT) {
							total_cfg_bits = cfg_bits_next;
							continue;
						}
						total_cfg_bits = cfg_bits_next;
					} // for (p<count_of_sink_label)
				} // for (lab : getSinkLabelsAsSortedSet)

				////////////////////////////////
				// write verilog for each gate// 
				////////////////////////////////
				if(g.Label()!=Swan.OUTPUT) {
					assert(g.Equivalents() != null) : "Null eqv! type "+g.Label();
					for(Gate eq : g.Equivalents()) {
						int xb_current = xb_wire_start;
						String type = gate_types.get(eq.Label()) != null ? gate_types.get(eq.Label()) : "m"+(eq.Label()-CGLB_OFFSET);
						out += "\t"+type+" U"+eq.Index()+" ( ";

						assert(gate_ports.get(type) != null) : type+" was not found in gate_ports.";

						// Add ports which are always connected to the clock.
						if(gate_ports.get(type).clocked) {
							out += "."+CLOCK+"(clock), ";
						}

						assert gate_ports.containsKey(type) : "Gate templates in gate_ports does not contain "+type+ "(CGLB offset is "+CGLB_OFFSET+", gLab is "+eq.Label()+") ";
						int port_len;
						// Add ports that are tied to fixed values. Not sure when this comes up.
						if(gate_ports.get(type).fixed_ports != null) {
							port_len = gate_ports.get(type).fixed_ports.length;
							for(int j=0; j<port_len; j+=2) {
								if(gate_ports.get(type).fixed_ports[j] != null)
									out += "."+gate_ports.get(type).fixed_ports[j]+"("+gate_ports.get(type).fixed_ports[j+1]+"), ";
							}
						}

						port_len = gate_ports.get(type).inputs.length;
						int num_ports_added = 0;
						for(Integer lab_sink : eq.getSinkLabelsAsSortedSet()) {
							for(int j=0; j <port_len; j++) {
								if(gate_ports.get(type).in_symm[j].equals(lab_sink) && gate_ports.get(type).inputs[j] != null) {
									String s = gate_ports.get(type).inputs[j];
									out += "."+s+"(xb_out["+xb_current+"]), ";
									xb_current += g.Equivalents().size();
									num_ports_added++;
								}
							}
						}
						assert(num_ports_added == port_len) : "Failed to instantiate all ports! "+eq.Index()+", "+eq.Label()+" ("+num_ports_added+"/"+port_len+")\nPorts are: "+eq.getSinkLabelsAsSortedSet().toString()+"\nTemplate ports are: "+gate_ports.get(type).in_symm[0]+", "+gate_ports.get(type).in_symm[1]+"...\nTemplate inputs: "+gate_ports.get(type).inputs[0]+", "+gate_ports.get(type).inputs[1]+"...";

						// Add common TGATEs at each output to avoid needing to share them at inputs to crossbars. 
						String tgates = "";
						int tgate_cnt = 0;

						port_len = gate_ports.get(type).outputs.length;
						assert(eq.Equivalents() != null) : "No equivalents to Indx: "+eq.Index()+" lab: "+eq.Label();
						String output_wire = Swan.USE_MUXES ? "w" : "v";
						if(gate_ports.get(type).outputs[0].startsWith("OUT")) { // This is a CGLB
							for(int j=0; j <port_len; j++) { 
								Boolean port_has_connections = true;
								String s = gate_ports.get(type).outputs[j];

								int gate_here = Integer.parseInt(s.substring(3,4));
								int symm_here = Integer.parseInt(s.substring(4,5));
								int wire_num = gate_here-1 + 10*symm_here;

								loopeq: for(Gate eq_g : eq.Equivalents()) {
									for(int lab_out : eq_g.OutputLabels()) {
										if(Label.srcLabelfromFull(lab_out)[Label.SYMM]==symm_here && Label.srcLabelfromFull(lab_out)[Label.SUBGATE]==gate_here+1) {
											port_has_connections = true;
											break loopeq;
										}
									}
								}
								if(s != null) {
									if(port_has_connections && max_port_wire < wire_num)
										max_port_wire = wire_num;
									out += "."+s+"("+
										(port_has_connections ? output_wire+"["+eq.Index()+"]["+wire_num+"]" : "")+
										")"+(j==port_len-1 ? "" : ", ");
									if(!Swan.USE_MUXES && port_has_connections) {
										tgates += "\tTGATE T"+eq.Index()+(tgate_cnt>0 ? "_"+tgate_cnt : "")+" ( .B(v["+eq.Index()+"]["+wire_num+"]), .S(all_we), .SN(~all_we), .Y(w["+eq.Index()+"]["+wire_num+"]) );\n";
										tgate_cnt++;
									}
								}
							}
						}
						else { // Not a CGLB
							// The way my templates are created guarantees j==symmetry of the port for normal gates
							for(int j=0; j <port_len; j++) { 
								Boolean port_has_connections = false;
								loopeq: for(Gate test_outs : eq.Equivalents()) {
									for(int lab_out : test_outs.OutputLabels()) {
										if(Label.srcLabelfromFull(lab_out)[Label.SYMM]==j) {
											port_has_connections = true;
											break loopeq;
										}
									}
								}

								String s = gate_ports.get(type).outputs[j];
								if(s != null) {
									out += "."+s+"("+
										(port_has_connections ? output_wire+"["+eq.Index()+"]["+j+"]" : "")+
										")"+(j==port_len-1 ? "" : ", ");
									if(!Swan.USE_MUXES && port_has_connections) {
										tgates += "\tTGATE T"+eq.Index()+(tgate_cnt>0 ? "_"+tgate_cnt : "")+" ( .B(v["+eq.Index()+"]["+j+"]), .S(all_we), .SN(~all_we), .Y(w["+eq.Index()+"]["+j+"]) );\n";
										tgate_cnt++;
									}

								}
							}
						}

						out += " );\n";
						out += tgates;
						xb_wire_start++;
					} // for(Gate eq : equivalents)
				} //if (!output)
						

				for(Gate eq : g.Equivalents())
					eq.setValid(false);
				g.setString(out);
			} // if (normal gate)
			else if(fixed_types.contains(g.Label()) && g.Label() != Swan.VSS && g.Label() != Swan.VDD) {
				assert (gate_types.get(g.Label()) != null) : "Failed to retrieve fixed type: "+g.Label();
				String type = gate_types.get(g.Label());
				out = "\t"+type+" U"+g.Index()+" ( ";

				// I have no idea why a fixed gate would be clocked. But hey, better safe than sorry, right?
				if(gate_ports.get(type).clocked) {
					out += "."+CLOCK+"(clock), ";
				}

				assert(gate_ports.get(type) != null) : "Failed to retrieve ports for type: "+type;
				int port_len;
				if(gate_ports.get(type).fixed_ports != null) {
					port_len = gate_ports.get(type).fixed_ports.length;
					for(int j=0; j<port_len; j+=2) {
						if(gate_ports.get(type).fixed_ports[j] != null)
							out += "."+gate_ports.get(type).fixed_ports[j]+"("+gate_ports.get(type).fixed_ports[j+1]+"), ";
					}
				}

				assert(gate_ports.get(type).inputs.length == 0) : "Fixed type with inputs. Please relabel appropriately. "+g.Label();

				port_len = gate_ports.get(type).outputs.length;
				for(int j=0; j <port_len; j++) { // The way my templates are created guarantees j==symmetry of the port
					Boolean port_has_connections = false;
					for(int lab : g.OutputLabels()) {
						if(Label.srcLabelfromFull(lab)[Label.SYMM]==j) {
							port_has_connections = true;
							break;
						}
					}
					String s = gate_ports.get(type).outputs[j];
					if(s != null)
						out += "."+s+"("+
								(port_has_connections ? "w["+g.Index()+"]["+j+"]" : "")+
								")"+(j==port_len-1 ? "" : ", ");
				}

				out += " );\n";
				g.setString(out);
			} // if (in fixed_types)
			else
				assert(g.Label() == Swan.VSS || g.Label() == Swan.VDD) : "Unhandled gate type in verilog generation! Type: "+g.Label();
			// PARTIALS : currently disabled.
			// skip_xcnt = !skip_xcnt;
		}

		String top;
		if(Swan.TIMING_MODEL)
			top = "module swan (\n"+
					"\tinput clock,\n"+
					"\tinput program_data,\n"+
					"\tinput program_activate,\n"+
					"\toutput gnd, pulse, // power ports for otp crossbars. Arguably inputs, but not with verilog-synthesizeable logic.\n"+
					"\toutput logic ["+(tgate_wire_idx-1)+":0] internal,\n";
		else
			top = "module swan (\n"+
					"\tinput clock,\n"+
					(Swan.USE_MUXES ? "" :"\tinput program_data,\n"+
					"\tinput program_activate,\n"+
					"\toutput gnd, pulse, // power ports for otp crossbars. Arguably inputs, but not with verilog-synthesizeable logic.\n");

		HashMap<String, portAggregator> compact_ports = new HashMap<String, portAggregator>(all_inpts.size());
		for(String p : all_inpts) {
			String[] toks = p.split("\\[");
			if(compact_ports.containsKey(toks[0]))
				compact_ports.get(toks[0]).addWire(p);
			else
				compact_ports.put(toks[0], new portAggregator(p));
		}
		top += "\n\t";
		for(String k : compact_ports.keySet()) {
			top += "input logic "+compact_ports.get(k).toString() + ",\n\t";
		}

		compact_ports.clear();
		for(String p : all_outpts) {
			String[] toks = p.split("\\[");
			if(compact_ports.containsKey(toks[0]))
				compact_ports.get(toks[0]).addWire(p);
			else
				compact_ports.put(toks[0], new portAggregator(p));
		}
		top += "\n\t";
		for(String k : compact_ports.keySet()) {
			top += "output logic "+compact_ports.get(k).toString() +",\n\t";
		}
		Boolean expose_internal_wires = !Swan.TIMING_MODEL && !Swan.USE_MUXES;
		top =	top.substring(0,top.length()-3) + 
				"\n);\n\n"+
				// "\n\tlogic ["+(total_cfg_bits-1)+":0] CFG;\n"+
				(expose_internal_wires ? "\n\tlogic ["+(total_muxes-1)+":0] we;\n" : "") +
				(expose_internal_wires ? "\n\tlogic all_we;\n" : "") +
				(expose_internal_wires ? "\n\tlogic ["+(max_out_width-1)+":0] rSel, not_rSel;\n" : "") +
				(expose_internal_wires ? "\n\tlogic ["+(max_in_width-1)+":0] cSel, not_cSel;\n" : "") +
				"\tlogic ["+max_index+":"+min_index+"]["+max_port_wire+":0] v, w;\n"+
				"\tlogic ["+(num_gate_inputs-1)+":0] xb_out;\n"+
				(snippet_wire_cntr > 0 ? "\tlogic sw["+(snippet_wire_cntr-1)+":0];\n" : "") +
				(!Swan.TIMING_MODEL && num_internal_inputs > 0 ? "\tlogic internal["+(num_internal_inputs-1)+":0];\n" : "");


		// top += "\n\tswan_cfg cfgFile ( .CFG(CFG) );\n\n";
		if(Swan.USE_MUXES) {
			top += "\tlogic ["+(cfg_bits_next-1)+":0] CFG;";
			top += "\n\tswan_cfg cfg_file ( .CFG(CFG) );\n\n";
		}
		else if(!Swan.TIMING_MODEL) {
			top += "\n\tswan_programmer #(.M("+total_muxes+"), .L("+max_out_width+"), .W("+max_in_width+")) programmer ( .clk(clock), .activate(program_activate), .in(program_data), "+
					".out({we, cSel, rSel}) );\n\n";

			top += "\n\tassign not_cSel = ~cSel;";
			top += "\n\tassign not_rSel = ~rSel;\n";
			top += "\n\tassign all_we = |we;\n\n";
		}

		ofstream.write(top.getBytes());
		
		for(Gate g : netlist) {
			if(g.hasString()) {
				ofstream.write(g.verilogString().getBytes());
			}
		}

		out = "endmodule: swan";
		ofstream.write(out.getBytes());
		ofstream.close();


		System.out.println("Number of fuses needed: "+total_cfg_bits);
		System.out.println("Total number of crossbars: "+total_muxes);
		float mean_mux = ((float)total_inputs/total_muxes);
		System.out.println("Average crossbar size: "+mean_mux);
		System.out.println("Largest possible corssbar: "+max_in_width+"x"+max_out_width);
		System.out.println("Number of 2x1 muxes to implement all crossbars: "+muxes_2x1);
		System.out.println("Variance of crossbar size: "+((float)sum_sq_inputs/total_muxes - mean_mux*mean_mux));

		top = "module swan_cfg (\n"+
				"\toutput logic ["+(total_cfg_bits-1)+":0] CFG\n"+
				");\n\n";
		ofstream_cfg.write(top.getBytes());
		
		for (String assign : cfg_bit_assignments)
			ofstream_cfg.write(assign.getBytes());

		out = "endmodule: swan_cfg";
		ofstream_cfg.write(out.getBytes());
		ofstream_cfg.close();

		// return the number of choices made by crossbars as a metric of total design cost. 
		return num_gate_inputs-1;
	} // end GenerateVerilog


	
	/* Helper functions
	 */
	private static HashMap<Integer, String> getLabelMap() throws IOException {
		FileInputStream fstream = new FileInputStream("gates.txt");
	    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		HashMap<Integer, String> map = new HashMap<Integer, String>(50);
	    
	    while ((strLine = br.readLine()) != null) {
	    	if(!strLine.startsWith("\t")) {
	    		String[] tokens = strLine.split(" : ");
		    	map.put(Integer.valueOf(tokens[1]), tokens[0]);
		    }
	    }//end while
	    return map;
	}//end getLabelMap

	private static class GatePorts {
		public String[] inputs, outputs;
		public Integer[] in_symm;
		public Boolean clocked;
		String[] fixed_ports;
		GatePorts()
		{	clocked = false;	}
		GatePorts (String[] i, String[] o, Integer[] s, String[] f) {
			inputs = i;
			outputs = o;
			in_symm = s;
			fixed_ports = f;
			clocked = false;
		}
	}

	// Map gate name to the names of its input and output ports
	private static HashMap<String, GatePorts> createTemplates() throws IOException {
		HashMap<String, GatePorts> map = new HashMap<String, GatePorts>();

		FileInputStream fstream = new FileInputStream("gates.txt");
	    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		String module_name = "";
		GatePorts tmp = null;
		ArrayList<String> input_ports = new ArrayList<String>(5);
		ArrayList<Integer> in_symm = new ArrayList<Integer>(5);
		ArrayList<String> output_ports = new ArrayList<String>(5);
		ArrayList<String> fixed_ports = new ArrayList<String>(5);
		Boolean clked = false;
		while ((strLine = br.readLine()) != null) {
			if(!strLine.startsWith("\t")) { // Line is gate name
				if(!module_name.isEmpty()) { // Don't do this the first time through
					tmp = new GatePorts(input_ports.toArray(new String[input_ports.size()]),
										output_ports.toArray(new String[output_ports.size()]),
										in_symm.toArray(new Integer[in_symm.size()]),
										fixed_ports.toArray(new String[fixed_ports.size()]));
					tmp.clocked = clked;
		    		map.put(module_name, tmp);
				} 
				module_name	= strLine.substring(0, strLine.indexOf(" ")); // extract the first token: the name
				input_ports.clear();
				in_symm.clear();
				output_ports.clear();
				fixed_ports.clear();
				clked = false;
			}
			else {
	    		String[] tokens = strLine.trim().split(" ");
    			if(tokens.length == 2) { // fixed port
    				if(tokens[0].equals(CLOCK))
    					clked = true;
    				else {
	    				fixed_ports.add(tokens[0]);
	    				fixed_ports.add(tokens[1]);
    				}
    			}
	    		else if(tokens[1].equals("1")) { // output
	    			// Assume that output ports are ordered by symmetry number in the file.
	    			output_ports.add(tokens[0]);
	    		}
    			else {
	    			input_ports.add(tokens[0]);
	    			in_symm.add(Integer.parseInt(tokens[2]));
	    		}
			}
		}
		if(!module_name.isEmpty()) { // Don't do this the first time through
			tmp = new GatePorts(input_ports.toArray(new String[input_ports.size()]),
								output_ports.toArray(new String[output_ports.size()]),
								in_symm.toArray(new Integer[in_symm.size()]),
								fixed_ports.toArray(new String[fixed_ports.size()]));
			tmp.clocked = clked;
			map.put(module_name, tmp);
		} 


		// Get CGLB defintions from output file.
		// Note: we do not store symmetry numberings for CGLBs, because all ports are considered unique
		fstream = new FileInputStream("./components/frequentSubgraphs.sv");
	    br = new BufferedReader(new InputStreamReader(fstream));
		module_name = "";
		String[] port_names;
		Integer[] in_symmetry;
		tmp = null;
	    while ((strLine = br.readLine()) != null) {
    		String[] tokens = strLine.trim().split(" ");
	    	if(tokens[0].contains("endmodule")) {
	    		map.put(module_name, tmp);
	    		// System.out.println("Adding \""+module_name+"\"");
	    	}
	    	else if(tokens[0].contains("module")) {
	    		tmp = new GatePorts();
	    		module_name = tokens[1];
	    		// System.out.println("Found "+module_name);
	    	}
	    	else if (tokens[0].contains("input")) {
	    		int arrayLength = strLine.contains(CLOCK) ? tokens.length-2 : tokens.length-1;
	    		port_names = new String[arrayLength];
	    		in_symmetry = new Integer[arrayLength];
	    		int i=0;
	    		for(String port : tokens) {
	    			// System.out.println(port);
	    			if(port.contains(CLOCK) || port.contains("clock"))
						tmp.clocked=true;
	    			else if(! port.contains("input")/* && ! port.contains("clock")*/) {
		    			port_names[i] = port.endsWith(",") ? port.substring(0,port.length()-1) : port;
		    			String[] extracted_symm = port_names[i].split("[a-zA-Z]+|_\\d+"); //Port of the form "INX..XY(_Z)?"
		    			in_symmetry[i++]  = new Integer(extracted_symm[1]); // expecting that the first entry is empty
	    			}
				}
	    		tmp.inputs = port_names;
	    		tmp.in_symm = in_symmetry;
	    		// for(String str : port_names)
	    		// 	System.out.print(str+" ");
	    		// System.out.println();
	    	}
	    	else if (tokens[0].contains("output")) {
	    		port_names = new String[tokens.length-1];
	    		for(int i=1; i<tokens.length; i++)
	    			port_names[i-1] = tokens[i].endsWith(",") ? tokens[i].substring(0,tokens[i].length()-1) : tokens[i];
	    		tmp.outputs = port_names;
	    	}
	    }//end while

		return map;
	}
}