import java.util.ArrayList;

/* Stores the structure of a frequent subgraph: its edges and nodes
 */
public class subgraphPattern implements Comparable<subgraphPattern> {
	public ArrayList<Integer> types;
	public ArrayList<Edge> edges;
	public Boolean valid;
	public int id;

	//Creates an empty pattern, sizing data structures accordingly
	public subgraphPattern(int nodes, int wires, int id) {
		types = new ArrayList<Integer>(nodes);
		edges = new ArrayList<Edge>(wires);
		valid = true;
		this.id = id;
	}

	public void AddNode(int type) {	
		types.add(new Integer(type));
	}

	public void AddEdge(int from, int to, int lab) {
		edges.add(new Edge(from, to, lab));
	}

	// Returns all edges coming from the target node
	public ArrayList<Edge> TargetOutputs(int focus, int prior) {
		ArrayList<Edge> ret = new ArrayList<Edge>(edges.size());

		for(int i=0; i<edges.size(); i++) {
			if (edges.get(i).start == focus && edges.get(i).end != prior) {
				ret.add(edges.get(i));
			}
		}
		return ret;
	}

	// Returns all edges going to the target node
	public ArrayList<Edge> TargetInputs(int focus, int prior) {
		ArrayList<Edge> ret = new ArrayList<Edge>(edges.size());

		for(int i=0; i<edges.size(); i++) {
			if (edges.get(i).end == focus && edges.get(i).start != prior) {
				ret.add(edges.get(i));
			}
		}
		return ret;
	}

	// Removes all nodes and edges from the FSG pattern
	public void clear() {
		types.clear();
		edges.clear();
	}

	// Returns the labels of all edges coming from the target node
	private ArrayList<Integer> AdjacentNodes(int focus) {
		ArrayList<Integer> ret = new ArrayList<Integer>(edges.size());

		for(int i=0; i<edges.size(); i++) {
			if (edges.get(i).start == focus) {
				ret.add(new Integer(edges.get(i).end));
			} else if(edges.get(i).end == focus) {
				ret.add(new Integer(edges.get(i).start));
			}
		}
		return ret;
	}

	// Returns the FSG with the most edges, then the FSG with the most nodes
	// breaks ties based on labels, which are inconsequential
	public int compareTo(subgraphPattern o) {
		if(this.edges.size() != o.edges.size()) {
			assert(this.edges.size() - o.edges.size() != 0);
			return this.edges.size() - o.edges.size();
		}
		if(this.types.size() != o.types.size()) {
			assert(this.types.size() - o.types.size() != 0);
			return this.types.size() - o.types.size();
		}
		
		// Desperate attempts at preventing two different FSGs from being equal
		for(int i=0;i<types.size();i++)
			if(!this.types.get(i).equals(o.types.get(i))) {
				assert(this.types.get(i)-o.types.get(i) != 0);
				return this.types.get(i)-o.types.get(i);
			}
		for(int i=0;i<edges.size();i++) {
			if(this.edges.get(i).start != o.edges.get(i).start) {
				assert(this.edges.get(i).start-o.edges.get(i).start != 0);
				return this.edges.get(i).start-o.edges.get(i).start;
			}
			if(this.edges.get(i).end != o.edges.get(i).end) {
				assert(this.edges.get(i).end-o.edges.get(i).end != 0);
				return this.edges.get(i).end-o.edges.get(i).end;
			}
			if(this.edges.get(i).label != o.edges.get(i).label) {
				assert(this.edges.get(i).label-o.edges.get(i).label != 0);
				return this.edges.get(i).label-o.edges.get(i).label;
			}
		}

		return 0;
	}
}