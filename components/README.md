##What are these files?

#crossbars.sv
A SystemVerilog description of the OTP and multiplexor-based crossbars used in the SWAN fabric.

#otp.db
A FUSE and TGATE module used by crossbars.sv for the OTP crossbars. Based on the NanGate 45nm technology library.

#programming_interfaces.sv
Some programming logic for programming the OTP crossbars. Iterates through the crossbars burning one row of fuses at a time, given data from a pin off-chip.

#synth.tcl
An example .tcl script for Synopsys Design Compiler to compile the SWAN fabric. What is notable in this file is that we ensure that the configuration is present to guide energy calculations, but is not completely used to optimize out the crossbars. 
