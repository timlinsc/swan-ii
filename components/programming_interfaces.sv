/* A combination of a shift register and decoder to program an OTP fabric.
 * The shift register is log(Number of crossbars) + (max xbar width + max xbar length)
 * Where the bits are laid out in that order (xbar, width, length)
 */

module swan_programmer(clk, activate, in, out);
	parameter M = 1000; // number of crossbars in the system
	parameter L = 10; // max crossbar length (number of outputs)
	parameter W = L*L; // max crossbar width (number of inputs)
	parameter SIZE = $clog2(M)+W+L;

	input clk;
	input activate;
	input in;
	output logic [M+W+L-1:0] out;

	logic [SIZE-1:0] regs; // shift registers to hold the programming word.
	logic [$clog2(SIZE)-1:0] cntr; // count until we have loaded a full word.
	logic word_loaded;


	assign word_loaded = cntr == SIZE;

	always_comb begin
		if(activate && word_loaded) begin
			out[W+L-1:0] = regs[W+L-1:0];
			for(int i=W+L; i<M+W+L; i=i+1)
				out[i] = regs[SIZE-1:W+L] == (i-W-L);
		end
		else
			out = 0;
	end

	always_ff @(posedge clk) begin
		if(activate) begin
			regs <= {regs[$clog2(M)+W+L-2:0], in};
			if(word_loaded)
				cntr <= 0;
			else
				cntr <= cntr+1;
		end
		else begin
			regs <= 0;
			cntr <= 0;
		end
	end
endmodule // swan_programmer

