# ================================ SETUP =================================

set src [list ./crossbar.sv ./frequentSubgraphs.sv ./swan_full_custom.sv ./swan_config.sv]
set top_module CSRFile

# Our OTP library, if needed. You may want to use your own library here.
set target_library [list ./otp.db]
set link_library [list ./otp.db]

set_host_options -max_cores 16

set hdlin_while_loop_iterations 1000000
set hdlin_keep_signal_name user_driving

set_flatten false

define_design_lib WORK -path "./work"

analyze -format sverilog -lib WORK $src
elaborate $top_module -lib WORK
current_design $top_module

link
uniquify

# =============================== CLOCKING ===============================

create_clock -period 20.0 clock

set real_inputs [remove_from_collection [all_inputs] [list clock] ]

set_input_delay -clock clock -max 0 $real_inputs
set_output_delay -clock clock -max 0 [all_outputs]

set_max_delay 20.0 [all_outputs]

# ============================= COMPILATION ==============================

# Ungrouping of CGLBs and crossbars okay, but don't flatten config file
ungroup -flatten [get_cells {U* xbar*} ]

check_design
compile_ultra -no_boundary_optimization -no_autoungroup -exact_map

# =============================== REPORTS ================================

report_area > area.rpt
report_timing > timing.rpt
report_power > power.rpt
write -format verilog -output nl.sv

exit
