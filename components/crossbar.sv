/* crossbar.sv
 * This file contains three crossbars used in a SWAN fabric. The first is an OTP
 * crossbar with all programming logic (two levels of TGATEs between programming
 * line and the fuse) attached to each crossbar. crossbar_otp_lite is a crossbar
 * with one level of TGATEs with the assumptions that the additional programming
 * logic is shared by all crossbars. crossbar_mux is a multiplexor-based
 * crossbar and assumes that the programming bits are stored in an external data
 * structure.
 */

`timescale 1ns/1ps

// Inputs are associated with columns (bitlines). Outputs are associated with rows (wordlines).
// The crossbar is programmed row by row. So rSel is one-hot, but cSel (effectively the cfg bits) is not.
module crossbar_otp(in,out, we, rSel, not_rSel, cSel, not_cSel, /*fake outs*/ ground, pulse);
	parameter OUT_WIDTH=2;
	parameter IN_WIDTH=OUT_WIDTH*OUT_WIDTH; 
	
	input logic [IN_WIDTH-1:0]  in; // input drivers
	input logic				    we; // write enable
	input logic [IN_WIDTH-1:0]  cSel, not_cSel; // column select and inverse (bitline, inputs)
	input logic [OUT_WIDTH-1:0] rSel, not_rSel; // row select and inverse (wordline, outputs)
	output logic [OUT_WIDTH-1:0]  out; // output vector
	output ground, pulse;
	
	// Word line (transistor gate and drain)
	// _src wires change based on data values, while wlg and wld are final values based on we.
	logic [OUT_WIDTH-1:0] wlg, wlg_src, wld, wld_src;
	// Bit line (transistor source)
	logic [IN_WIDTH-1:0] bl, bl_src;

	// include this signal to avoid generating lots of inverters.
	logic not_we;
	assign not_we = ~we;

	supply0 gnd; // ground
	supply1 half_Vpp; // Programming voltage and Vpp/2
	supply1 Vdd; // Activates a transistor during read

	genvar i,j;
	generate
		// Bitline (column) logic
		for(i=0; i<IN_WIDTH; i+=1)
			TGATE_MUX T_bl (.A(in[i]), .B(bl_src[i]), .Y(bl[i]), .S(not_we), .SN(we));
		for(i=0; i<IN_WIDTH; i+=1)
			TGATE_MUX T_bl_src (.A(gnd), .B(half_Vpp), .Y(bl_src[i]), .S(not_cSel[i]), .SN(cSel[i]));

		// Wordline (row) logic at gate
		for(i=0; i<OUT_WIDTH; i+=1)
			TGATE_MUX T_wlg (.A(Vdd), .B(wlg_src[i]), .Y(wlg[i]), .S(we), .SN(not_we));
		for(i=0; i<OUT_WIDTH; i+=1)
			TGATE_MUX T_wlg_src (.A(gnd), .B(half_Vpp), .Y(wlg_src[i]), .S(not_rSel[i]), .SN(rSel[i]));
		
		// Wordline (row) logic at drain
		for(i=0; i<OUT_WIDTH; i+=1)
			TGATE_SWITCH T_wld (.A(out[i]), .B(wld_src[i]), .Y(wld[i]), .S(not_we), .SN(we));
		for(i=0; i<OUT_WIDTH; i+=1)
			TGATE_SWITCH T_wld_src (.A(pulse), .B(ground), .Y(wld_src[i]), .S(rSel[i]), .SN(not_rSel[i]));

		// Fuse array
		for(i=0; i<IN_WIDTH; i+=1) begin
			for(j=0; j<OUT_WIDTH; j+=1) begin
				FUSE F (.S(bl[i]), .G(wlg[j]), .D(wld[j]) );
			end
		end
	endgenerate // generate
endmodule: crossbar_otp


// Inputs are associated with columns (bitlines). Outputs are associated with rows (wordlines).
// The crossbar is programmed row by row. So rSel is one-hot, but cSel (effectively the cfg bits) is not.
module crossbar_otp_lite(in,out, we, not_we, rDataG, rDataD, cData);
	parameter OUT_WIDTH=2;
	parameter IN_WIDTH=OUT_WIDTH*OUT_WIDTH;

	input logic [IN_WIDTH-1:0]  in; // input drivers
	input logic 				we, not_we; // write enable
	input logic [IN_WIDTH-1:0]  cData;
	input logic [OUT_WIDTH-1:0] rDataG; 
	output logic [OUT_WIDTH-1:0] rDataD;
	output logic [OUT_WIDTH-1:0] out; // output vector

	// Word line (transistor gate and drain)
	// _src wires change based on data values, while wlg and wld are final values based on we.
	logic [OUT_WIDTH-1:0] wlg, wlg_src, wld, wld_src;
	// Bit line (transistor source)
	logic [IN_WIDTH-1:0] bl, bl_src;

	genvar i,j;
	generate
		// Bitline (column) logic
		for(i=0; i<IN_WIDTH; i+=1)
			TGATE_MUX T_bl (.A(in[i]), .B(cData[i]), .Y(bl[i]), .S(not_we), .SN(we));

		// Wordline (row) logic at gate
		for(i=0; i<OUT_WIDTH; i+=1)
			TGATE_MUX T_wlg (.A(Vdd), .B(rDataG[i]), .Y(wlg[i]), .S(we), .SN(not_we));

		// Wordline (row) logic at drain
		for(i=0; i<OUT_WIDTH; i+=1)
			TGATE_SWITCH T_wld (.A(out[i]), .B(rDataD[i]), .Y(wld[i]), .S(not_we), .SN(we));

		// Fuse array
		for(i=0; i<IN_WIDTH; i+=1) begin
			for(j=0; j<OUT_WIDTH; j+=1) begin
				FUSE F (.S(bl[i]), .G(wlg[j]), .D(wld[j]) );
			end
		end
	endgenerate // generate
endmodule: crossbar_otp_lite

module crossbar_mux (cfg, in, out);
	parameter IN_WIDTH=2; 
	parameter OUT_WIDTH=2;
	
	input logic [IN_WIDTH-1:0] in;
	input logic [OUT_WIDTH-1:0][$clog2(IN_WIDTH)-1:0] cfg;
	output logic [OUT_WIDTH-1:0] out;

	int i;
	always_comb begin
		for(i=0; i<OUT_WIDTH; i=i+1) begin 
			out[i] = in[cfg[i]];
		end
	end
endmodule: crossbar_mux 
