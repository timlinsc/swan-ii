import java.util.LinkedList;
import java.util.ArrayList;

class Levelizer {
	public static void Levelize(ArrayList<Gate> netlist, ArrayList<Integer> fixed_types) {
		LinkedList<Gate> this_layer = new LinkedList<Gate>();
		LinkedList<Gate> next_layer;

		//Begin with only inputs
		for(Gate g : netlist) {
			if(g.Label()==Swan.INPUT)
				this_layer.add(g);
		}

		int current = 0;
		while(!this_layer.isEmpty()) {
			next_layer = new LinkedList<Gate>();
			for(Gate g : this_layer) {
				g.setLayer(current);
				for(Gate o : g.Outputs())
					if(o.Layer()==Gate.UNSET)
						next_layer.add(o);
			}
			this_layer = next_layer;
			current++;
		}

		// for(Gate g : netlist)
		// 	assert(fixed_types.contains(g.Label()) || g.Label()==Swan.OUTPUT || g.Layer()!=Gate.UNSET) : "A gate was unreached during levelization: v "+g.Index()+" "+g.Label();
		System.out.println("Total levels: "+(current));
	}


	// Cut inputs to DFFs so layer is the distance from the nearest latch
	public static int Levelize(ArrayList<Gate> netlist, ArrayList<Integer> fixed_types, ArrayList<Integer> dff_types) {
		LinkedList<Gate> this_layer = new LinkedList<Gate>();
		LinkedList<Gate> next_layer;

		//Begin with only inputs
		for(Gate g : netlist) {
			if(g.Label()==Swan.INPUT || dff_types.contains(g.Label()))
				this_layer.add(g);
		}

		int current = 0;
		while(!this_layer.isEmpty()) {
			next_layer = new LinkedList<Gate>();
			for(Gate g : this_layer) {
				g.setLayer(current);
				for(Gate o : g.Outputs())
					if(o.Layer()==Gate.UNSET)
						next_layer.add(o);
			}
			this_layer = next_layer;
			current++;
		}

		// for(Gate g : netlist)
		// 	assert(!g.Valid() || fixed_types.contains(g.Label()) || g.Label()==Swan.OUTPUT || g.Layer()!=Gate.UNSET) : "A gate was unreached during levelization: v "+g.Index()+" "+g.Label();
		System.out.println("Total levels (DFF as starting points): "+(current));
		return current;
	}

	public static void retime(ArrayList<Gate> netlist, int layer_size, ArrayList<Integer> dff_types) {
		int end = netlist.size();
		for(int i=0; i<end; i++) {
			Gate g = netlist.get(i);
			if(g.Valid() && g.Layer()!=0 && g.Layer()%layer_size==0) {
				for(int[] lab : g.getUniqueifiedSrcLabels()) { //for each output port
					Boolean doRetime = false;
					for(int j=0; j<g.Outputs().size(); j++)
						if(Label.srcMatches(lab, g.OutputLabels().get(j)) && !dff_types.contains(g.Outputs().get(j).Label())) {
							doRetime = true;
							break;
						}
						
					if(doRetime)	
						netlist.add(new Gate(g, lab, Swan.DFF, netlist.size(), g.Secured()));
				}
			}
		}
	}
}