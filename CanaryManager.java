/* CanaryManager.java
 * Handles the insertion of canaries and canary driver logic for SWAN.
 * NOTE: As written, this may cause assumptions about equivalence set sizes to be violated!!
 * In order to handle unmatched, odd-numbered canaries, this will introduce one additional canary to even the score.
 */

import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Iterator;

class CanaryManager {
	// minimal taps on LFSRs. Note that all these also tap from register N.
	private static final int[] one = {0};
	private static final int[] two = {1};
	private static final int[] three = {2};
	private static final int[] four = {3};
	private static final int[] six = {5,4,0};
	private static final int[] seven = {6,3,2};
	private static final int[] twelve = {11,10,0};
	private static final int[] fourteen = {14,13,11};
	private static final int[] seven_nil = {6};

	// Constructs an maximal-length LFSR of length size and inserts it into the given netlist.
	// Returns the registers of the LFSR with their indexes corresponding to the array index.
	public static Gate[] makeLFSR(ArrayList<Gate> netlist, int size) {
		assert(Swan.DFF >= 0) : "DFF not included in gates.txt!";
		final int[] taps = getTaps(size);
		Gate[] lfsr = new Gate[size];
		for(int i=0; i<size; i++) {
			lfsr[i] = new Gate(Swan.DFF, netlist.size(), true);
			netlist.add(lfsr[i]);
			if(i>0) {
				lfsr[i].AddInput(lfsr[i-1], 0);
				lfsr[i-1].AddOutput(lfsr[i], 0);
			}
		}

		Gate tap = new Gate(Swan.XNOR, netlist.size(), true);
		netlist.add(tap);
		tap.AddInput(lfsr[size-1], 0);
		lfsr[size-1].AddOutput(tap, 0);

		for(int i=0; i<taps.length-1; i++) {
			int t = taps[i];
			tap.AddInput(lfsr[t], 0);
			lfsr[t].AddOutput(tap, 0);

			Gate lastTap = tap;
			tap = new Gate(Swan.XNOR, netlist.size(), true);
			tap.AddInput(lastTap, 0);
			lastTap.AddOutput(tap, 0);
			netlist.add(tap);
		}

		tap.AddInput(lfsr[taps[taps.length-1]], 0);
		lfsr[taps[taps.length-1]].AddOutput(tap, 0);

		lfsr[0].AddInput(tap, 0);
		tap.AddOutput(lfsr[0], 0);

		for(int i=0; i<lfsr.length; i++)
			assert(lfsr[i].Inputs().size()==1) : "Register "+i+" has "+lfsr[i].Inputs().size()+" inputs!";

		return lfsr;
	}

	public static void insertCanaries(ArrayList<Gate> netlist, Gate[] lfsr, Gate alarm, ArrayList<Integer> fixed_types) {
		assert(Swan.XNOR >= 0 && Swan.AND >= 0) : "Missing gate definitions for canary checker! AND: "+Swan.AND+" XNOR: "+Swan.XNOR;
		HashMap<Integer, ArrayList<Gate>> canaryList = new HashMap<Integer, ArrayList<Gate>>();
		HashMap<Integer, Gate> feeder_canaries = new HashMap<Integer, Gate>();
		Gate last = null;
		// Create all the canaries and connect them to the LFSR
		// DEBUG
		int num_sec =0;
		int nl_size = netlist.size();
		System.out.println("nl size at start of insertCanaries: "+nl_size);
		for(int j=0; j<nl_size; j++) {
			Gate g = netlist.get(j);
			if(g.Label()==Swan.VDD) 
				last = g; // Record in case we need to tie the alarm signal to something.
			if(!g.Valid() || g.Label()==Swan.INPUT || g.Label()==Swan.OUTPUT || g.Label()==Swan.VSS || g.Label()==Swan.VDD || g.Label()==Swan.CANARY_NET || fixed_types.contains(g.Label()))
				continue;

			int target = g.Secured() ? Swan.SECURE_EQUIV_WITH_CANARIES : Swan.EQUIV_SET_SIZE;
			if(g.Label()==Swan.AND || g.Label()==Swan.XNOR)
				target -= Swan.HEADROOM_FOR_CANARY_CHECKERS;

			if(g.Secured())
				num_sec++;

			while(g.Equivalents().size()<target)
			{
				supplyCanary(g, netlist, lfsr, feeder_canaries, canaryList);
			}
		}
		System.out.println("Found "+num_sec+" secured gates.");
		System.out.println("nl size after adding canaries, before checking logic: "+netlist.size());

		// Add the checking logic for the canaries
		ArrayDeque<Gate> checks = new ArrayDeque<Gate>();
		// TODO: optimize by using something other than 2-input gates
		for(Integer type : canaryList.keySet()) {
			ArrayList<Gate> list = canaryList.get(type);
			if(list.size()==1)
				supplyCanary(list.get(0), netlist, lfsr, canaryList);
			for(int i=0; i<list.size(); i+=2) {
				Gate first = list.get(i);
				Gate second = list.get(i+1>=list.size() ? i-1 : i+1); // reuse a gate if we've run out.
				Gate check = new Gate(Swan.XNOR, netlist.size(), true);
				check.AddInput(first, 0); // TODO: check all outputs for a CGLB
				check.AddInput(second, 0);
				first.AddOutput(check, 0);
				second.AddOutput(check, 0);

				checks.add(check);
				netlist.add(check);
			}
		}

		if(checks.isEmpty()) {
			System.out.println("Not enough canaries added to justify checking logic!!");
			for(Gate uses_alarm : alarm.Outputs()) {
				for(int i=0; i<uses_alarm.Inputs().size(); i++) {
					if(uses_alarm.Inputs().get(i)==alarm) {
						uses_alarm.Inputs().set(i, last);
						last.AddOutput(uses_alarm, uses_alarm.InputLabels().get(i));
					}
				}
			}
			alarm.setValid(false);
			return;
		}


		// if there are any preexisting XNOR gates, add the check gates to their equivalence sets. 
		ArrayList<Gate> xnors = new ArrayList<Gate>(checks);
		Iterator<Gate> it = xnors.iterator();
		embedGates(netlist, it, nl_size, Swan.XNOR);

		// Reduce comparisons (XNOR) with ANDs to produce final signal
		// Note that we use 2-input AND gates
		ArrayList<Gate> reductions = new ArrayList<Gate>(checks.size());
		while(checks.size()>1) {
			Gate first = checks.poll();
			Gate second = checks.poll();
			// Gate third = checks.isEmpty() ? first : checks.poll();
			// Gate fourth = checks.isEmpty() ? second : checks.poll();
			Gate reduction = new Gate(Swan.AND, netlist.size(), true);
			reduction.AddInput(first, 0);
			reduction.AddInput(second, 0);
			// reduction.AddInput(third, 0);
			// reduction.AddInput(fourth, 0);
			first.AddOutput(reduction, 0);
			second.AddOutput(reduction, 0);
			// third.AddOutput(reduction, 0);
			// fourth.AddOutput(reduction, 0);

			netlist.add(reduction);
			checks.offer(reduction);
			reductions.add(reduction);
		}

		last = checks.poll();
		for(Gate uses_alarm : alarm.Outputs()) {
			for(int i=0; i<uses_alarm.Inputs().size(); i++) {
				if(uses_alarm.Inputs().get(i)==alarm) {
					uses_alarm.Inputs().set(i, last);
					last.AddOutput(uses_alarm, uses_alarm.InputLabels().get(i));
				}
			}
		}
		alarm.setValid(false);

		Iterator<Gate> and_it = reductions.iterator();
		embedGates(netlist, and_it, nl_size, Swan.AND);

		System.out.println("nl size before injecting gates: "+netlist.size());
		// Inject additional XNORs and ANDs into the netlist and use them for equivalence sets
		injectGates(netlist, it, and_it, nl_size);

		// group the remaining gates into equivalence sets. We couldn't inject enough XNORs and ANDs. :(
		// Given that we insert new gates everywhere, it is unlikely that this will be neccessary.
		emptyIterator(it);
		emptyIterator(and_it);

		System.out.println("nl size at end of insertCanaries: "+netlist.size());
	}

	// if there are any preexisting gates, add the check gates to their equivalence sets. 
	private static void embedGates(ArrayList<Gate> nl, Iterator<Gate> toEmbed, int nl_size, int type) {
		for(int j=0; j<nl_size && toEmbed.hasNext(); j++) {
			Gate g = nl.get(j);
			if(g.Valid() && g.Label()==type) {
				int numToAdd = Swan.SECURE_EQUIV_WITH_CANARIES - g.Equivalents().size();
				for(int k=0; k<numToAdd && toEmbed.hasNext(); k++) {
					Gate next_gate = toEmbed.next();
					assert(g.Label() == next_gate.Label());
					g.Equivalents().add(next_gate);
					next_gate.setEquivalents(g.Equivalents());
				}
			}
		}
	}

	// Empties an iterator of gates, separate each N gates into an equivalence set
	private static void emptyIterator(Iterator<Gate> it) {
		ArrayList<Gate> Equiv = new ArrayList<Gate>(Swan.SECURE_EQUIV_WITH_CANARIES);
		while(it.hasNext()) {
			Gate g = it.next();
			Equiv.add(g);
			g.setEquivalents(Equiv);
			if(Equiv.size()==Swan.SECURE_EQUIV_WITH_CANARIES) {
				Equiv = new ArrayList<Gate>(Swan.SECURE_EQUIV_WITH_CANARIES);
			}
		}
	}

	// For a given LFSR width, return the register indices that must be XOR'ed
	// for the LFSR to be maximal-length. 
	// Note: not all LFSR sizes are currently supported. This may need expansion
	// if we come to support very large gates. 
	private static int[] getTaps(int lfsr_size) {
		switch(lfsr_size) {
			case 3:
			case 4:
			case 6:
			case 7:
				return one;
			case 5:
			case 11:
				return two;
			case 8:
				return six;
			case 9:
				return four;
			case 10:
			case 20:
				return three;
			case 12:
				return seven;
			case 14:
				return twelve;
			case 16:
				return fourteen;
			case 18:
				return seven_nil;
			default:
				assert(false) : "Bad size for LFSR: "+lfsr_size+"!"	;
				return null;
		}
	}

	// Inject gates to ensure the security of the checker logic: (a XNOR a) AND a == a
	// Group these new ANDs into equivalence sets with the xnors
	private static void injectGates(ArrayList<Gate> nl, Iterator<Gate> xnors, Iterator<Gate> ands, int nl_size) {
		ArrayList<Gate> Equiv_xnor = new ArrayList<Gate>(Swan.SECURE_EQUIV_WITH_CANARIES);
		ArrayList<Gate> Equiv_and = new ArrayList<Gate>(Swan.SECURE_EQUIV_WITH_CANARIES);
		for(int i=0; i<nl_size && (xnors.hasNext() || ands.hasNext()); i++) {
			Gate g = nl.get(i);
			if(g.Valid() && !g.Secured() && !g.Outputs().isEmpty() && g.Label()!=Swan.INPUT && g.Label()!=Swan.OUTPUT) { // Avoid taxing critical path if possible. Minimize xbar size by avoiding secure inputs.
				int label = Label.fullFromSrc(Label.srcLabelfromFull(g.OutputLabels().get(0)));
				Gate xnor = new Gate(Swan.XNOR, nl.size(), true);
				xnor.AddInput(g, label);
				xnor.AddInput(g, label);
				g.AddOutput(xnor, label);
				g.AddOutput(xnor, label);
				Equiv_xnor.add(xnor);
				xnor.setEquivalents(Equiv_xnor);
				nl.add(xnor);

				Gate and = new Gate(g, Label.srcLabelfromFull(g.OutputLabels().get(0)), Swan.AND, nl.size(), true);
				and.AddInput(g, label);
				and.AddInput(g, label);
				g.AddOutput(and, label);
				g.AddOutput(and, label);
				and.AddInput(xnor, 0);
				xnor.AddOutput(and, 0);
				Equiv_and.add(and);
				and.setEquivalents(Equiv_and);
				nl.add(and);
				if(Equiv_and.size()==(Swan.SECURE_EQUIV_WITH_CANARIES-Swan.SECURE_EQUIV_SET)) {
					for(int j=0; j<Swan.SECURE_EQUIV_SET; j++) {
						if(xnors.hasNext()) {
							Gate xnor_next = xnors.next();
							xnor_next.setEquivalents(Equiv_xnor);
							Equiv_xnor.add(xnor_next);
						}
						if(ands.hasNext()) {
							Gate and_next = ands.next();
							and_next.setEquivalents(Equiv_and);
							Equiv_and.add(and_next);
						}
					}
					Equiv_xnor = new ArrayList<Gate>(Swan.SECURE_EQUIV_WITH_CANARIES);
					Equiv_and = new ArrayList<Gate>(Swan.SECURE_EQUIV_WITH_CANARIES);
				}
			}
		}
	}

	/* Adds a canary to the equivalence set of the target gate
	 * @param g the gate to add a canary to
	 * @param netlist The netlist of which the target gate is a member
	 * @param lfsr The LFSR which will drive the canaries, properly indexed in the array
	 * @param feeder_canaries A map from the type of Gate to a waiting canary of that type ready to drive another canary in the two-gate chain
	 * @param terminal_canaries A map from the type of Gate to a list of second gates in the two-gate canary chains, in need of being compared with the other terminal canaries
	 */
	private static void supplyCanary(Gate g, ArrayList<Gate> netlist, Gate[] lfsr, HashMap<Integer, Gate> feeder_canaries, HashMap<Integer, ArrayList<Gate>> terminal_canaries) {
		Gate canary = new Gate(g.Label(), netlist.size(), true);
		if(feeder_canaries.containsKey(g.Label()) && g.Label() < Swan.CGLB_OFFSET) { //TODO: more conditions?
			Gate old = feeder_canaries.remove(g.Label());
			// CRITICAL ASSUMPTION: All gates that we permit to chain must have one output and an input labeled 0.
			canary.AddInput(old, 0);
			old.AddOutput(canary, 0);

			// Add other inputs
			int lfsr_reg = old.Inputs().size();
			for(Integer lab : g.getSinkLabelsAsSortedSet()) {
				int count_of_sink_label = g.countSinkLabel(lab);
				int label = Label.fullFromSink(Label.sinkLabelfromPartial(lab));
				if(label==0) count_of_sink_label--; // already added one for port 0.
				for(int i=0; i<count_of_sink_label; i++) {
					assert(lfsr_reg<lfsr.length) : "Exceeded bounds on type "+g.Label()+" at port "+lab+" ("+i+"/"+count_of_sink_label+") called from g: "+g.Index()+", which has "+g.Inputs().size()+" inputs.";
					canary.AddInput(lfsr[lfsr_reg], label);
					lfsr[lfsr_reg++].AddOutput(canary, label);
				}
			}
			if(!terminal_canaries.containsKey(g.Label()))
				terminal_canaries.put(g.Label(), new ArrayList<Gate>());
			terminal_canaries.get(g.Label()).add(canary);
		}
		else {
			int lfsr_reg = 0;
			for(Integer lab : g.getSinkLabelsAsSortedSet()) {
				int count_of_sink_label = g.countSinkLabel(lab);
				int label = Label.fullFromSink(Label.sinkLabelfromPartial(lab));
				for(int i=0; i<count_of_sink_label; i++) {
					assert(lfsr_reg<lfsr.length) : "Exceeded bounds on type "+g.Label()+" at port "+lab+" ("+i+"/"+count_of_sink_label+") called from g: "+g.Index()+", which has "+g.Inputs().size()+" inputs.";
					canary.AddInput(lfsr[lfsr_reg], label);
					lfsr[lfsr_reg++].AddOutput(canary, label);
				}
			}
			feeder_canaries.put(g.Label(), canary);
		}
		netlist.add(canary);
		g.Equivalents().add(canary);
		canary.setEquivalents(g.Equivalents());
		assert(g.Equivalents().size()<=Swan.SECURE_EQUIV_WITH_CANARIES) : "Added too many canaries!";
	}

	// Supply canaries when we aren't interested in chaining them together
	private static void supplyCanary(Gate g, ArrayList<Gate> netlist, Gate[] lfsr, HashMap<Integer, ArrayList<Gate>> canaryList) {
		Gate canary = new Gate(g.Label(), netlist.size(), true);
		int lfsr_reg = 0;
		for(Integer lab : g.getSinkLabelsAsSortedSet()) {
			int count_of_sink_label = g.countSinkLabel(lab);
			int label = Label.fullFromSink(Label.sinkLabelfromPartial(lab));
			for(int i=0; i<count_of_sink_label; i++) {
				assert(lfsr_reg<lfsr.length) : "Exceeded bounds on type "+g.Label()+" at port "+lab+" ("+i+"/"+count_of_sink_label+") called from g: "+g.Index();
				canary.AddInput(lfsr[lfsr_reg], label);
				lfsr[lfsr_reg++].AddOutput(canary, label);
			}
		}
		if(!canaryList.containsKey(g.Label()))
			canaryList.put(g.Label(), new ArrayList<Gate>());
		canaryList.get(g.Label()).add(canary);
		netlist.add(canary);
		g.Equivalents().add(canary);
		canary.setEquivalents(g.Equivalents());
	}
}
