/* Helpful functions for handling labels of edges. 
 */

public class Label {
	public static final int SRC_SUBGATE = 3;
	public static final int SINK_SUBGATE = 2;
	public static final int SRC_SYMM = 1;
	public static final int SINK_SYMM = 0;

	// whenever I only have a two element array
	public static final int SUBGATE = 1;
	public static final int SYMM = 0;

	public static int[] translate(int label) {
		int[] res = new int[4];
		int ind = 0;
		while (label > 0) {
			res[ind++] = label % 10;
			label /= 10;
		}
		return res;
	}

	public static int[] sinkLabelfromFull(int label) {
		int[] res = new int[2];
		int ind = 0;
		while (label > 0) {
			res[ind++] = label % 10;
			label /= 100;
		}
		return res;
	}

	public static int[] sinkLabelfromPartial(int label) {
		int[] res = new int[2];
		res[SYMM] = label % 10;
		res[SUBGATE] = label / 10;
		return res;
	}

	public static int[] srcLabelfromFull(int label) {
		int[] res = new int[2];
		
		label /= 10;
		res[SYMM] = label % 10;
		label /= 100;
		res[SUBGATE] = label % 10;

		return res;
	}

	public static int fullFromSink(int[] label) {
		return label[SUBGATE]*100 + label[SYMM];
	}

	public static int fullFromSrc(int[] label) {
		return label[SUBGATE]*1000 + label[SYMM]*10;
	}

	// Return a full label where the src numbers have been zeroed out
	public static int stripSrc(int full) {
		return fullFromSink(sinkLabelfromFull(full));
	}

	// Return a full label where the sink numbers have been zeroed out
	public static int stripSink(int full) {
		return fullFromSrc(srcLabelfromFull(full));
	}

	public static int mergeIntoFull(int[] src_port, int[] sink_port) {
		return fullFromSrc(src_port)+fullFromSink(sink_port);
	}

	public static int mergeIntoFull(int src_port, int[] sink_port) {
		return mergeIntoFull(srcLabelfromFull(src_port), sink_port);
	}

	public static int mergeIntoFull(int[] src_port, int sink_port) {
		return mergeIntoFull(src_port, sinkLabelfromFull(sink_port));
	}

	public static int mergeIntoFull(int src_port, int sink_port) {
		return mergeIntoFull(srcLabelfromFull(src_port), sinkLabelfromFull(sink_port));
	}


	// Comparison tools
	public static Boolean arrayCompare(int[] l1, int[] l2) {
		for(int i=0; i<l1.length; i++) {
			if(l1[i] != l2[i])
				return false;
		}
		return true;
	}

	public static Boolean sinkMatches(int[] sink, int full) {
		int[] full_arr = sinkLabelfromFull(full);
		return (sink[SUBGATE]==full_arr[SUBGATE] && sink[SYMM]==full_arr[SYMM]);
	}

	public static Boolean srcMatches(int[] sink, int full) {
		int[] full_arr = srcLabelfromFull(full);
		return (sink[SUBGATE]==full_arr[SUBGATE] && sink[SYMM]==full_arr[SYMM]);
	}
}