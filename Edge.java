public class Edge {
	public int start, end, label;

	public Edge(int s, int e, int l) {
		start = s;
		end = e;
		label = l;
	}
}