# generate_fsg_verilog.py
# v3.0
# Takes the component to label mapping from Lamppost.py and the frequent subgraphs from Swan.java
#  and builds the frequent subgraphs into valid verilog modules

import sys
import re
import copy

### CONSTANTS
non_gate_identifiers = ['output', 'input', 'net', 'VSS', 'VDD']
hardwiring = ["1'b1", "1'b0", "SYNOPSYS_UNCONNECTED__0", "SYNOPSYS_UNCONNECTED__1"]
clock_name = "CK"
# clock_flag = "clock"
IN = 0
OUT = 1

class GateTemplate(object):
	def __init__(self, n):
		self.name = n
		self.ports = [ [None]*5, [None]*5 ] # Inputs and outputs, ports[dir][symm] is a list of names with those properties.
		self.fixed_ports = {}

	def addPort(self, toks):
		if len(toks) < 3: # This port has a known, fixed value, like "1'b0"
			self.fixed_ports[toks[0]] = toks[1]
		else:
			direction = int(toks[1])
			symm = int(toks[2])
			if not self.ports[direction][symm]:
				self.ports[direction][symm] = []
			self.ports[direction][symm].append(toks[0])

class GateInstance(object):
	def __init__(self, templ):
		self.template = templ
		self.ports = {}

	def addPort(self, label='', direction=0, srcName=0):
		if(direction == OUT and len(label) < 2):
			symm = 0
		else:
			symm = int(label[0])
		for port in self.template.ports[direction][symm]:
			if port not in self.ports:
				# self.ports[port] = 'w'+str(srcName)+label[-1]
				if len(label)==1:
					self.ports[port] = 'OUT'+str(srcName)+'0'
				else:
					self.ports[port] = 'OUT'+str(srcName)+label[-2]
				break

	def addExternalConnections(self, myName=0):
		strName = str(myName)
		symm_recover = 0
		for symm in [s for s in self.template.ports[OUT] if s]:
			for port in symm:
				if port not in self.ports:
					self.ports[port] = 'OUT'+strName+str(symm_recover) #replaced str(cntr) with str(symm). ASSUMPTION: this works.
				symm_recover += 1
		symm_recover = 0
		for symm in [s for s in self.template.ports[IN] if s]:
			cntr = 0
			for port in symm:
				if port not in self.ports:
					self.ports[port] = 'IN'+strName+str(symm_recover)+("_"+str(cntr) if cntr>0 else "")
					cntr += 1
			symm_recover += 1	

		for port in self.template.fixed_ports:
			self.ports[port] = self.template.fixed_ports[port]

def parseGates(filename):
	list_of_gates = []
	with open(filename, 'r') as fin:
		lines = fin.readlines()
		for l in lines:
			tokens = l.split()
			if not l.startswith('\t') and not tokens[0] in non_gate_identifiers:
				# ASSUMPTION: gates are in order in the gate description file. Label # matches ordering.
				list_of_gates.append(GateTemplate(tokens[0]))
			elif l.startswith('\t'):
				list_of_gates[-1].addPort(tokens)
		return list_of_gates

def composeModule(gate_templates=[], gates_in_module={}, rename_gates = {}, modNum = 0):
	in_list = set()
	out_list = set()
	wire_list = set()
	gate_strs = []

	for key in gates_in_module:
		gate = gates_in_module[key]
		gate_str = "\t"+gate.template.name+" U"
		gate_str += str(rename_gates[key])+" ("
		for port in gate.ports:
			gate_str += " ."+port+"("+gate.ports[port]+"),"
			if gate.ports[port] not in hardwiring:
				if gate.ports[port].startswith('OUT'):
					out_list.add(gate.ports[port])
				elif (gate.ports[port].startswith('IN') or port in gate.template.fixed_ports) and gate.ports[port] not in in_list:
					in_list.add(gate.ports[port])
				else:
					wire_list.add(gate.ports[port])
		gate_str = gate_str[:-1] + " );\n"
		gate_strs.append(gate_str)

	outStr = "module m"+str(modNum)+" (\n\tinput "
	for s in in_list:
		outStr += s+", "
	outStr += "\n\toutput "
	for s in out_list:
		outStr += s+", "
	outStr = outStr[:-2]+"\n);\n"
	if wire_list:
		outStr += "\tlogic "
		for s in wire_list:
			outStr += s+", "
		outStr = outStr[:-2]+";\n"
	for s in gate_strs:
		outStr += s
	outStr += "endmodule\n"

	return outStr



############################################################
################### Stage 1: parse args ####################
############################################################
if len(sys.argv) < 3:
	print( "Error, invalid args! USAGE: python3.6 lamppost.py <FILEIN> <FILEOUT>")
	sys.exit(0)
else:
	filein = sys.argv[1]
	fileout = sys.argv[2]



############################################################
################### Stage 2: parse module ##################
############################################################
gate_templates = parseGates('gates.txt')
gates_in_module = {}
rename_gates = {}
composed_modules = []

with open(filein, 'r') as fin:
	gate_cnt = 1
	module_cnt = 0
	lines = fin.readlines()
	for l in lines:
		if l.startswith('v'):
			toks = l.split()
			gates_in_module[toks[1]] = GateInstance(gate_templates[int(toks[2])])
			rename_gates[toks[1]] = gate_cnt
			gate_cnt += 1
		elif l.startswith('e'):
			toks = l.split()
			src = toks[1]
			sink = toks[2]
			gates_in_module[src].addPort(toks[3], OUT, rename_gates[toks[1]])
			gates_in_module[sink].addPort(toks[3], IN, rename_gates[toks[1]])
		elif gates_in_module:
			for key in gates_in_module:
				gates_in_module[key].addExternalConnections(rename_gates[key])
			composed_modules.append(composeModule(gate_templates, gates_in_module, rename_gates, module_cnt))
			module_cnt = int(l[:l.index(':')])
			gate_cnt = 1
			gates_in_module = {}
			rename_gates = {}
		else:
			module_cnt = int(l[:l.index(':')]) 

	for key in gates_in_module:
		gates_in_module[key].addExternalConnections(rename_gates[key])
	if(gates_in_module):
		composed_modules.append(composeModule(gate_templates, gates_in_module, rename_gates, module_cnt))

if not composed_modules:
	print("Nothing was read in!")

with open(fileout, 'w') as fout:
	for module in composed_modules:
		fout.write(module)