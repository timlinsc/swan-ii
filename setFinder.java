import java.util.LinkedList;
import java.util.ArrayList;
import java.util.HashSet;
import com.google.common.collect.Iterables;

// We can remove Guava-dependant library Iterables if needed.

class setFinder {	
	/* Walks through the netlist, grouping gates into equivalence sets and setting their internal set variables to match.
	 * Equivalence sets are chosen by a BFS search for identical gates in the netlist. 
	 * This function continues adding gates to a set until it of the size given by Swan.EQUIV_SET_SIZE or Swan.SECURE_EQUIV_SET
	 * if at least one of the gates in the set has been marked secure per the preprocessor directive. 
	 */
	public static void findEquivalenceSets(ArrayList<Gate> netlist) {
		LinkedList<Gate> explore_graph;
		HashSet<Integer> visited;
		ArrayList<Gate> Eqvs;
		int label;
		//for analysis
		int total_elems=0;

		// For performance, check to see if any gates are so uncommon that we should group them immediately.
		int num_types = 1;
		for(Gate g : netlist) // Count the number of types
			if(g.Label()>num_types && g.Label()!=Swan.INPUT && g.Label()!=Swan.OUTPUT && g.Label()!=Swan.CANARY_NET)
				num_types = g.Label();
		int[] type_counts = new int[num_types+1];
		for(int i=0; i<type_counts.length; i++) 
			type_counts[i] = 0;

		// Count the number of each type of gate
		for(Gate g : netlist) 
			if(g.Label()!=Swan.INPUT && g.Label()!=Swan.OUTPUT && g.Label()!=Swan.CANARY_NET) {
				assert(g.Label() >= 0 && g.Label()<type_counts.length) : "Bad label: "+g.Label()+" for "+g.Index();
				type_counts[g.Label()]++;
			}

		// Build sets for rare gates. 
		for(int i=0; i<type_counts.length; i++) {
			if(type_counts[i] < Swan.EQUIV_SET_SIZE) {
				ArrayList<Gate> Eqv = new ArrayList<Gate>(type_counts[i]);
				for(Gate g : netlist) {
					if(g.Label()==i) {
						Eqv.add(g);
						g.setEquivalents(Eqv);
					}
				}
			}
		}

		// for each gate, explore the graph to find gates of the same type and group.
		for(Gate g : netlist) {
			label = g.Label();

			// skip over inputs and outputs
			if(label==Swan.INPUT || g.Label()==Swan.CANARY_NET)
				continue;
			if(label==Swan.OUTPUT || g.Label()==Swan.VSS || g.Label()==Swan.VDD) {
				Eqvs = new ArrayList<Gate>(1);
				Eqvs.add(g);
				g.setEquivalents(Eqvs);
				continue;
			}

			// for performance, skip the search if the eqv set is initialized (since we search the whole graph anyways)
			if(!g.Valid() || g.Equivalents()!=null)
				continue;
			
			int target = g.Secured() ? Swan.SECURE_EQUIV_SET : Swan.EQUIV_SET_SIZE;

			//initialize search structures
			explore_graph = new LinkedList<Gate>();
			// visited = BloomFilter.create(Funnels.integerFunnel(), BF_SIZE);
			visited = new HashSet<Integer>(netlist.size());
			Eqvs = g.Equivalents()!=null ? g.Equivalents() : new ArrayList<Gate>(target);
			if(g.Equivalents()==null) Eqvs.add(g);

			//begin search
			visited.add(g.Index());
			// Comment out to remove Guava
			for(Gate start : Iterables.concat(g.Inputs(), g.Outputs())) {
				if(start.Valid() && !visited.contains(start.Index())) {
					explore_graph.add(start);
					visited.add(start.Index());
				}
			}
			// Uncomment to replace above code without Guava
			/*for(Gate start : g.Inputs()) {
				if(start.Valid() && !visited.contains(start.Index())) {
					explore_graph.add(start);
					visited.add(start.Index());
				}
			}
			for(Gate start : g.Outputs()) {
				if(start.Valid() && !visited.contains(start.Index())) {
					explore_graph.add(start);
					visited.add(start.Index());
				}
			}*/

			while(Eqvs.size()<target && !explore_graph.isEmpty()) {
				Gate candidate = explore_graph.poll();
				if(candidate.Label()==label && candidate.Equivalents()==null /*!candidate.hasAllEquivalents(max_cluster)*/) {
					// When enforcing canary ratios with Secure sets, eqv_set > sec_eqv_set
					if(candidate.Secured() && Eqvs.size() < Swan.SECURE_EQUIV_SET) {  
						target = Swan.SECURE_EQUIV_SET;
						Eqvs.ensureCapacity(Swan.SECURE_EQUIV_SET);
					}
					Eqvs.add(candidate);
				}
				// Comment out to remove Guava
				for(Gate next : Iterables.concat(candidate.Inputs(), candidate.Outputs())) {
					if(next.Valid() && !visited.contains(next.Index())) {
						visited.add(next.Index());
						explore_graph.add(next);
					}
				}
				// Uncomment to replace above code without Guava
				/*for(Gate next : candidate.Inputs()) {
					if(next.Valid() && !visited.contains(next.Index())) {
						visited.add(next.Index());
						explore_graph.add(next);
					}
				}
				for(Gate next : candidate.Outputs()) {
					if(next.Valid() && !visited.contains(next.Index())) {
						visited.add(next.Index());
						explore_graph.add(next);
					}
				}*/
			}
				
			// Filled an equivalence set. Now make that set the Eqv of each gate.
			for(Gate eqv : Eqvs) {
				eqv.setEquivalents(Eqvs);
				total_elems += Eqvs.size();
			}
			// I don't know why, but this makes too many sets secure.
			// if(target == Swan.SECURE_EQUIV_SET)
			// 	for(Gate eqv : Eqvs)
			// 		eqv.makeSecured();

		}
		System.out.println("Average set size: "+(total_elems/netlist.size()));
		HashSet<ArrayList<Gate>> all_sets = new HashSet<ArrayList<Gate>>();
		for(Gate g : netlist)
			if(g.Equivalents()!=null && g.Label()!=Swan.OUTPUT)
				all_sets.add(g.Equivalents());
		int total_missing_gates = 0;
		for(ArrayList<Gate> eSet : all_sets)
			if(eSet.get(0).Secured())
				total_missing_gates += (Swan.SECURE_EQUIV_WITH_CANARIES-eSet.size());
			else
				total_missing_gates += (Swan.EQUIV_SET_SIZE-eSet.size());

		System.out.println("Excepted number of gates to add in canary generation: "+(total_missing_gates));

	}
}